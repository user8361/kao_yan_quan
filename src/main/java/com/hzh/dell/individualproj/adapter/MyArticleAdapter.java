package com.hzh.dell.individualproj.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hzh.dell.Bmob.entity.Article;
import com.hzh.dell.tree.R;
import com.hzh.dell.tree.custom.MyGridView;
import com.jude.easyrecyclerview.adapter.BaseViewHolder;
import com.jude.easyrecyclerview.adapter.RecyclerArrayAdapter;

import java.util.ArrayList;
import java.util.List;

import cn.bmob.v3.datatype.BmobFile;

/**
 * create by hzh
 * on 2019/4/19
 */
public class MyArticleAdapter extends RecyclerArrayAdapter<Article> {

    private Context mContext;
    private List<Article> articleList;
    private LayoutInflater mInflater;

    public MyArticleAdapter(Context context, List<Article> articleList, LayoutInflater inflater) {
        super(context, articleList);
        this.mContext = context;
        this.articleList = articleList;
        this.mInflater = inflater;
    }

    @Override
    public BaseViewHolder OnCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(parent, mContext, mInflater);
    }


    public final static class ViewHolder extends BaseViewHolder<Article> {
        Context context;
        LayoutInflater inflater;
        TextView tvArticleTitle,
                tvArticleContent,
                tvSubmitDay,
                tvSubmitTime;
        MyGridView gridView;

        public ViewHolder(ViewGroup parent, Context context, LayoutInflater inflater) {
            super(parent, R.layout.item_mine_article);
            this.context = context;
            this.inflater = inflater;
            tvArticleTitle = $(R.id.article_title);
            tvArticleContent = $(R.id.article_content);
            tvSubmitDay = $(R.id.user_submitDay);
            tvSubmitTime = $(R.id.user_submitTime);
            gridView = $(R.id.gridView);
        }

        @Override
        public void setData(Article data) {
            super.setData(data);
            tvArticleTitle.setText(data.getArticleTitle());
            tvArticleContent.setText(data.getArticleContent());
            tvSubmitDay.setText(data.getCreatedAt().substring(0, 10));
            tvSubmitTime.setText(data.getCreatedAt().substring(10, 19));

            List<BmobFile> imgFile = data.getArticleImgFile();
            List<String> imgPathList = new ArrayList<>();
            for (BmobFile file : imgFile) {
                imgPathList.add(file.getFileUrl());
                Log.e("imgUrl", " --> " + file.getFileUrl());
            }


            if (imgPathList != null) {
                if (imgPathList.size() == 0) {
                    gridView.setVisibility(View.GONE);
                } else {
                    GridViewShowImgAdapter adapter = new GridViewShowImgAdapter(imgPathList, context, inflater);
                    gridView.setAdapter(adapter);
                }

            }
        }
    }


}
