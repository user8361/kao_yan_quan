package com.hzh.dell.individualproj.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.hzh.dell.tree.R;

import java.util.List;

/**
 * create by hzh
 * on 2019/4/17
 */
public class GridViewShowImgAdapter extends BaseAdapter {
    private List<String> urls;
    private Context mContext;
    private LayoutInflater mInflater;

    private int maxImages = 9;

    public int getMaxImages() {
        if (urls.size() > maxImages) {
            return maxImages;
        } else {
            return urls.size();
        }

    }

    public void setMaxImages(int maxImages) {
        this.maxImages = maxImages;
    }

    public GridViewShowImgAdapter(List<String> urls, Context mContext, LayoutInflater mInflater) {
        this.urls = urls;
        this.mContext = mContext;
        this.mInflater = mInflater;
    }


    /**
     * 让gridview中的数据项目加1后，显示+号
     * 到达最大后不在显示加号
     *
     * @return 返回GridView中的数量
     */
    @Override
    public int getCount() {
        int count = (urls == null) ? 0 : urls.size();
        if (count >= maxImages) {
            return maxImages;
        }
        return count;
    }

    @Override
    public Object getItem(int position) {
        return urls.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.article_show_word_pic_item, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        if (urls != null && position < urls.size()) {
            Glide.with(convertView).load("file://" + urls.get(position)).into(viewHolder.ivAddImg);
            viewHolder.ivAddImg.setVisibility(View.VISIBLE);
            Log.e("DETAIL", "urls.get(" + position + ") ---> " + urls.get(position));
        }
        return convertView;
    }

    public class ViewHolder {
        public final ImageView ivAddImg;
        public final View root;

        public ViewHolder(View root) {
            this.ivAddImg = root.findViewById(R.id.iv_add_pic);
            this.root = root;
        }
    }
}
