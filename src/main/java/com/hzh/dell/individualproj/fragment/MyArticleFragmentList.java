package com.hzh.dell.individualproj.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.hzh.dell.tree.R;
import com.hzh.dell.tree.adapter.listview_adapter.CommonAdapter;
import com.hzh.dell.tree.adapter.listview_adapter.base.ViewHolder;
import com.hzh.dell.tree.entity.Article;
import com.hzh.dell.tree.utils.DateUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * create by hzh
 * on 2019/4/19
 */
public class MyArticleFragmentList extends Fragment {
    private View view;
    private ListView listView;
    private List<Article> articleList;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_article, container, false);
        initView();
        initData();
        return view;
    }


    private void initView() {
        listView = view.findViewById(R.id.listView);
    }

    private void initData() {
        articleList = new ArrayList<>();
        CommonAdapter<Article> adapter = new CommonAdapter<Article>(getContext(), articleList, R.layout.include_item_publish_article) {
            @Override
            public void convert(ViewHolder helper, Article item) {
                helper.setText(R.id.article_title, item.getArticleTitle());
                helper.setText(R.id.article_content, item.getArticleContent());
                helper.setText(R.id.user_submitDay, DateUtil.getFormattedDate(item.getCreateTime()));
                helper.setText(R.id.user_submitTime, DateUtil.getFormattedTime(item.getCreateTime()));
                String imgpath = item.getArticleImgId();
                List<String> pathList = Arrays.asList(imgpath.split(","));


            }
        };
    }
}
