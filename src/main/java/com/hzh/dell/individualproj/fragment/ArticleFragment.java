package com.hzh.dell.individualproj.fragment;

import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.hzh.dell.Bmob.entity.Article;
import com.hzh.dell.Bmob.entity.User;
import com.hzh.dell.individualproj.adapter.MyArticleAdapter;
import com.hzh.dell.individualproj.fragment.base.LazyFragment;
import com.hzh.dell.individualproj.view.NormalDecoration;
import com.hzh.dell.tree.R;
import com.jude.easyrecyclerview.adapter.RecyclerArrayAdapter;

import java.util.List;

import butterknife.BindView;
import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.FindListener;



public class ArticleFragment extends LazyFragment {
    private User user;
    private final String USER_NAME = "userName";
    private String userName = "";


    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    private MyArticleAdapter adapter;

    public static ArticleFragment getInstance() {
        return new ArticleFragment();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_article;
    }

    @Override
    protected void initData() {

    }

    @Override
    public void lazyInitView(View view, Bundle savedInstanceState) {
        Bundle bundle = getArguments();
        userName = bundle.getString(USER_NAME);
        BmobQuery<User> userBmobQuery = new BmobQuery<>();
        userBmobQuery.addWhereEqualTo(USER_NAME, userName);
        userBmobQuery.findObjects(new FindListener<User>() {
            @Override
            public void done(List<User> list, BmobException e) {
                if (list != null) {
                    user = list.get(0);
                    if (user != null) {
                        initList();
                    }
                }
            }
        });

    }


    public void initList() {
        Log.e("LIST", "user.getObjectId:" + user.getObjectId());
        BmobQuery<Article> articleBmobQuery = new BmobQuery<>();
        articleBmobQuery.addWhereEqualTo("author", user.getObjectId());
        articleBmobQuery.findObjects(new FindListener<Article>() {
            @Override
            public void done(List<Article> list, BmobException e) {
                if (list != null) {
                    Log.e("LIST", list.get(0).getArticleTitle() + "--" + list.get(0).getAuthor().getObjectId());
                    recyclerView.setLayoutManager(new LinearLayoutManager(mActivity));
                    recyclerView.addItemDecoration(new NormalDecoration(ContextCompat.getColor(mActivity, R.color.mainGrayF8), (int) mActivity.getResources().getDimension(R.dimen.one)));
                    adapter = new MyArticleAdapter(mActivity, list, getLayoutInflater());
                    adapter.addAll(list);
                    recyclerView.setAdapter(adapter);
                    adapter.setOnItemClickListener(new RecyclerArrayAdapter.OnItemClickListener() {
                        @Override
                        public void onItemClick(int position) {
                            Toast.makeText(getContext(), position + "", Toast.LENGTH_SHORT).show();
                        }
                    });
                    adapter.setNoMore(R.layout.view_no_more);
                    final List<Article> finalArticleList = list;
                    adapter.setMore(R.layout.view_more, new RecyclerArrayAdapter.OnMoreListener() {
                        @Override
                        public void onMoreShow() {
//                            adapter.addAll(finalArticleList);
//                            adapter.notifyDataSetChanged();
                        }

                        @Override
                        public void onMoreClick() {

                        }
                    });
                } else {
                    Log.e("LIST", "数据为空");
                }
            }
        });
    }

}
