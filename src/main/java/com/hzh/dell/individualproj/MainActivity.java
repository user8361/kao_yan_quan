package com.hzh.dell.individualproj;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.ButtonBarLayout;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.hzh.dell.Bmob.entity.Article;
import com.hzh.dell.Bmob.entity.User;
import com.hzh.dell.individualproj.adapter.ComFragmentAdapter;
import com.hzh.dell.individualproj.fragment.ArticleFragment;
import com.hzh.dell.individualproj.fragment.QuestionFragment;
import com.hzh.dell.individualproj.util.ScreenUtil;
import com.hzh.dell.individualproj.util.StatusBarUtil;
import com.hzh.dell.individualproj.view.ColorFlipPagerTitleView;
import com.hzh.dell.individualproj.view.JudgeNestedScrollView;
import com.hzh.dell.tree.R;
import com.hzh.dell.tree.utils.ImgUtils;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshHeader;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.scwang.smartrefresh.layout.listener.SimpleMultiPurposeListener;
import com.scwang.smartrefresh.layout.util.DensityUtil;

import net.lucode.hackware.magicindicator.MagicIndicator;
import net.lucode.hackware.magicindicator.ViewPagerHelper;
import net.lucode.hackware.magicindicator.buildins.UIUtil;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.CommonNavigator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.CommonNavigatorAdapter;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerTitleView;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.indicators.LinePagerIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.titles.SimplePagerTitleView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.FindListener;
import de.hdodenhof.circleimageview.CircleImageView;


public class MainActivity extends BaseActivity {
    public static final String TAG = MainActivity.class.getSimpleName();
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.iv_menu)
    ImageView ivMenu;

    @BindView(R.id.iv_header)
    ImageView ivHeader;

    @BindView(R.id.iv_userHeadimg)
    ImageView ivHeadImg;

    @BindView(R.id.tv_username)
    TextView tvUserName;

    @BindView(R.id.tv_currentSchool)
    TextView tvCurrentSchool;

    @BindView(R.id.tv_concern_num)
    TextView tvConcernedNum;

    @BindView(R.id.tv_fans_num)
    TextView tvFansNum;

    @BindView(R.id.refreshLayout)
    SmartRefreshLayout refreshLayout;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.view_pager)
    ViewPager viewPager;

    @BindView(R.id.toolbar_userHeadimg)
    CircleImageView toolbarHeadImg;

    @BindView(R.id.toolbar_username)
    TextView toolbarUserName;


    @BindView(R.id.scrollView)
    JudgeNestedScrollView scrollView;

    @BindView(R.id.buttonBarLayout)
    ButtonBarLayout buttonBarLayout;
    @BindView(R.id.magic_indicator)
    MagicIndicator magicIndicator;
    @BindView(R.id.magic_indicator_title)
    MagicIndicator magicIndicatorTitle;
    int toolBarPositionY = 0;
    private int mOffset = 0;
    private int mScrollY = 0;
    private String[] mTitles = new String[]{"动态", "文章", "问答"};
    private List<String> mDataList = Arrays.asList(mTitles);
    //    private UserDao userDao;
//    private User user;
//    private ArticleDao articleDao;
//    private List<Article> articleList;
//    private UserConcernDao userConcernDao;
//    private List<UserConcern> userConcernList;
    private final String USER_NAME = "userName";
    private String userName = "";
    private Intent getInfoIntent;
    private Bundle bundle = new Bundle();
    private User user;
    private Article article;

//    private ContentObserver mNavigationStatusObserver = new ContentObserver(new Handler()) {
//        @Override
//        public void onChange(boolean selfChange) {
//            dealWithHuaWei();
//            int navigationBarIsMin = Settings.System.getInt(getContentResolver(), "navigationbar_is_min", 0);
//            if (navigationBarIsMin == 1) {
//                Log.d(TAG, "onChange: ------------------导航键隐藏了");
//            } else {
//                Log.d(TAG, "onChange: ------------------导航键显示了");
//            }
//        }
//    };

    @Override
    public int setLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    public void initView(Bundle savedInstanceState) {
        initView();
    }

    @Override
    public void initData() {

    }

    private void initView() {
//        获取个人信息
        getInfoIntent = getIntent();
        userName = getInfoIntent.getStringExtra(USER_NAME);

        StatusBarUtil.immersive(this);
        StatusBarUtil.setPaddingSmart(this, toolbar);
//        refreshLayout.setOnMultiPurposeListener(new SimpleMultiPurposeListener());
        refreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                refreshLayout.finishRefresh(2000);
            }
        });

        refreshLayout.setOnMultiPurposeListener(new SimpleMultiPurposeListener() {

            @Override
            public void onHeaderMoving(RefreshHeader header, boolean isDragging, float percent, int offset, int headerHeight, int maxDragHeight) {
                mOffset = offset / 2;
                ivHeader.setTranslationY(mOffset - mScrollY);
                toolbar.setAlpha(1 - Math.min(percent, 1));
            }

            @Override
            public void onHeaderFinish(RefreshHeader header, boolean success) {
                super.onHeaderFinish(header, success);

            }

            @Override
            public void onHeaderReleased(RefreshHeader header, int headerHeight, int maxDragHeight) {
                super.onHeaderReleased(header, headerHeight, maxDragHeight);
            }
        });


        BmobQuery<User> userBmobQuery = new BmobQuery<>();
        userBmobQuery.addWhereEqualTo(USER_NAME, userName);
        userBmobQuery.findObjects(new FindListener<User>() {
            @Override
            public void done(List<User> list, BmobException e) {
                if (list != null) {
                    user = list.get(0);
                    bundle.putString(USER_NAME, user.getUserName());
                    ivHeadImg.setImageBitmap(ImgUtils.stringToBitmap(user.getUserHeadImgPath()));
                    ivHeader.setImageBitmap(ImgUtils.stringToBitmap(user.getUserHeadImgPath()));
                    toolbarHeadImg.setImageBitmap(ImgUtils.stringToBitmap(user.getUserHeadImgPath()));
                    toolbarUserName.setText(user.getUserName());
                    tvUserName.setText(user.getUserName());
                    tvCurrentSchool.setText(user.getUserCurrentSchool() != null ? user.getUserCurrentSchool() : "2020届考研");

//        关注数目
                    int concernNum = 0;
                    int fansNum = 0;
                    tvConcernedNum.setText(concernNum + "");
                    tvFansNum.setText(fansNum + "");


                    toolbar.post(new Runnable() {
                        @Override
                        public void run() {
                            dealWithViewPager();
                        }
                    });
                    scrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
                        int lastScrollY = 0;
                        int h = DensityUtil.dp2px(170);
                        int color = ContextCompat.getColor(getApplicationContext(), R.color.mainWhite) & 0x00ffffff;

                        @Override
                        public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                            int[] location = new int[2];
                            magicIndicator.getLocationOnScreen(location);
                            int yPosition = location[1];
                            if (yPosition < toolBarPositionY) {
                                magicIndicatorTitle.setVisibility(View.VISIBLE);
                                scrollView.setNeedScroll(false);
                            } else {
                                magicIndicatorTitle.setVisibility(View.GONE);
                                scrollView.setNeedScroll(true);

                            }

                            if (lastScrollY < h) {
                                scrollY = Math.min(h, scrollY);
                                mScrollY = scrollY > h ? h : scrollY;
                                buttonBarLayout.setAlpha(1f * mScrollY / h);
                                toolbar.setBackgroundColor(((255 * mScrollY / h) << 24) | color);
                                ivHeader.setTranslationY(mOffset - mScrollY);
                            }
                            if (scrollY == 0) {
                                ivBack.setImageResource(R.drawable.back_white);
                                ivMenu.setImageResource(R.drawable.icon_menu_white);
                            } else {
                                ivBack.setImageResource(R.drawable.back_black);
                                ivMenu.setImageResource(R.drawable.icon_menu_black);
                            }

                            lastScrollY = scrollY;
                        }
                    });
                    buttonBarLayout.setAlpha(0);
                    toolbar.setBackgroundColor(0);


                    viewPager.setAdapter(new ComFragmentAdapter(getSupportFragmentManager(), getFragments()));
                    viewPager.setOffscreenPageLimit(10);
                    initMagicIndicator();
                    initMagicIndicatorTitle();
                }
            }
        });



    }

    private void dealWithViewPager() {
        toolBarPositionY = toolbar.getHeight();
        ViewGroup.LayoutParams params = viewPager.getLayoutParams();
        params.height = ScreenUtil.getScreenHeightPx(getApplicationContext()) - toolBarPositionY - magicIndicator.getHeight() + 1;
        viewPager.setLayoutParams(params);
    }

    private List<Fragment> getFragments() {
        List<Fragment> fragments = new ArrayList<>();
        bundle.putString(USER_NAME, user.getUserName());
        ArticleFragment articleFragment = (ArticleFragment) Fragment.instantiate(this, ArticleFragment.class.getName(), bundle);
        fragments.add(articleFragment);
//        fragments.add(DynamicFragment.getInstance());
        fragments.add(QuestionFragment.getInstance());
        fragments.add(QuestionFragment.getInstance());
//        fragments.add(QuestionFragment.getInstance());
        return fragments;
    }

    private void initMagicIndicator() {
        CommonNavigator commonNavigator = new CommonNavigator(this);
        commonNavigator.setScrollPivotX(0.65f);
        commonNavigator.setAdjustMode(true);
        commonNavigator.setAdapter(new CommonNavigatorAdapter() {
            @Override
            public int getCount() {
                return mDataList == null ? 0 : mDataList.size();
            }

            @Override
            public IPagerTitleView getTitleView(Context context, final int index) {
                SimplePagerTitleView simplePagerTitleView = new ColorFlipPagerTitleView(context);
                simplePagerTitleView.setText(mDataList.get(index));
                simplePagerTitleView.setNormalColor(ContextCompat.getColor(MainActivity.this, R.color.mainBlack));
                simplePagerTitleView.setSelectedColor(ContextCompat.getColor(MainActivity.this, R.color.mainBlack));
                simplePagerTitleView.setTextSize(16);
                simplePagerTitleView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        viewPager.setCurrentItem(index, false);
                    }
                });
                return simplePagerTitleView;
            }

            @Override
            public IPagerIndicator getIndicator(Context context) {
                LinePagerIndicator indicator = new LinePagerIndicator(context);
                indicator.setMode(LinePagerIndicator.MODE_EXACTLY);
                indicator.setLineHeight(UIUtil.dip2px(context, 2));
                indicator.setLineWidth(UIUtil.dip2px(context, 20));
                indicator.setRoundRadius(UIUtil.dip2px(context, 3));
                indicator.setStartInterpolator(new AccelerateInterpolator());
                indicator.setEndInterpolator(new DecelerateInterpolator(2.0f));
                indicator.setColors(ContextCompat.getColor(MainActivity.this, R.color.colorTheme));
                return indicator;
            }
        });
        magicIndicator.setNavigator(commonNavigator);
        ViewPagerHelper.bind(magicIndicator, viewPager);
    }

    private void initMagicIndicatorTitle() {
        CommonNavigator commonNavigator = new CommonNavigator(this);
        commonNavigator.setScrollPivotX(0.65f);
        commonNavigator.setAdjustMode(true);
        commonNavigator.setAdapter(new CommonNavigatorAdapter() {
            @Override
            public int getCount() {
                return mDataList == null ? 0 : mDataList.size();
            }

            @Override
            public IPagerTitleView getTitleView(Context context, final int index) {
                SimplePagerTitleView simplePagerTitleView = new ColorFlipPagerTitleView(context);
                simplePagerTitleView.setText(mDataList.get(index));
                simplePagerTitleView.setNormalColor(ContextCompat.getColor(MainActivity.this, R.color.mainBlack));
                simplePagerTitleView.setSelectedColor(ContextCompat.getColor(MainActivity.this, R.color.mainBlack));
                simplePagerTitleView.setTextSize(16);
                simplePagerTitleView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        viewPager.setCurrentItem(index, false);
                    }
                });
                return simplePagerTitleView;
            }

            @Override
            public IPagerIndicator getIndicator(Context context) {
                LinePagerIndicator indicator = new LinePagerIndicator(context);
                indicator.setMode(LinePagerIndicator.MODE_EXACTLY);
                indicator.setLineHeight(UIUtil.dip2px(context, 2));
                indicator.setLineWidth(UIUtil.dip2px(context, 20));
                indicator.setRoundRadius(UIUtil.dip2px(context, 3));
                indicator.setStartInterpolator(new AccelerateInterpolator());
                indicator.setEndInterpolator(new DecelerateInterpolator(2.0f));
                indicator.setColors(ContextCompat.getColor(MainActivity.this, R.color.colorTheme));
                return indicator;
            }
        });
        magicIndicatorTitle.setNavigator(commonNavigator);
        ViewPagerHelper.bind(magicIndicatorTitle, viewPager);

    }

    /**
     * 处理华为虚拟键显示隐藏问题导致屏幕高度变化，ViewPager的高度也需要重新测量
     */
//    private void dealWithHuaWei() {
//        flActivity.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
//            @Override
//            public void onGlobalLayout() {
//                dealWithViewPager();
//                flActivity.getViewTreeObserver().removeOnGlobalLayoutListener(this);
//            }
//        });
//    }
    @OnClick({R.id.tv_username, R.id.iv_userHeadimg, R.id.tv_currentSchool, R.id.iv_back})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_username:

                break;
            case R.id.iv_userHeadimg:

                break;

            case R.id.tv_currentSchool:

                break;
            case R.id.iv_back:
                onBackPressed();
                break;
        }
    }

    private void toast(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }
}