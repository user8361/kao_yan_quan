package com.hzh.dell.tree.community_fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.hzh.dell.tree.R;
import com.hzh.dell.tree.adapter.listview_adapter.CommonAdapter;
import com.hzh.dell.tree.adapter.listview_adapter.base.ViewHolder;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.footer.ClassicsFooter;
import com.scwang.smartrefresh.layout.header.ClassicsHeader;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.util.ArrayList;
import java.util.List;

public class NewFragmentList extends Fragment {
    private View view;
    private SmartRefreshLayout smartRefreshLayout;
    //    private RecyclerView mRecyclerView;
    private ListView mListView;
    private List<NewBean> mDatas;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.communication_fragment_new, container, false);
        initView();//初始化组件对象
        initData();//初始化数据
        refresh();//刷新
        return view;
    }

    private void initData() {
        mDatas = new ArrayList<NewBean>();
        for (int i = 0; i < 10; i++) {
            NewBean bean = new NewBean();
            bean.setHeadImg(R.drawable.logo);
            bean.setUserName("用户名" + i);
            bean.setUserSubmitTime(String.valueOf(10 + i));
            bean.setHourMinute("分钟");
            bean.setUserContent("文章详情第" + i + "条");
            bean.setImgOne(R.drawable.example);
//            bean.setImgTwo(R.drawable.example);
//            bean.setImgThree(R.drawable.example);
            bean.setCollege("山东大学" + i);
            bean.setLikeImgId(R.mipmap.heart_pink);
            bean.setLikeText(String.valueOf(i));
            bean.setMsgImg(R.mipmap.msg);
            bean.setMsgText(String.valueOf(i));
            mDatas.add(bean);
        }
        CommonAdapter<NewBean> mAdapter = new CommonAdapter<NewBean>(view.getContext(), mDatas, R.layout.communication_fragment_new_items) {

            @Override
            public void convert(ViewHolder helper, NewBean item) {
                helper.setImageResource(R.id.user_headImg, item.getHeadImg());
                helper.setText(R.id.user_name, item.getUserName());
                helper.setText(R.id.user_submitTime, item.getUserSubmitTime());
                helper.setText(R.id.hour_minute, item.getHourMinute());
                helper.setText(R.id.article_content, item.getUserContent());
                helper.setImageResource(R.id.img_one, item.getImgOne());
                helper.setText(R.id.article_type, item.getCollege());
                helper.setImageResource(R.id.like_img, item.getLikeImgId());
                helper.setText(R.id.like_text, item.getLikeText());
                helper.setImageResource(R.id.msg_img, item.getMsgImg());
                helper.setText(R.id.msg_text, item.getMsgText());
            }
        };
//        mRecyclerView.setAdapter(mAdapter);
        mListView.setAdapter(mAdapter);
    }

    private void initView() {
        mListView = view.findViewById(R.id.tab_new_listView);
    }

    public void refresh() {
        /**
         * 刷新
         */
        smartRefreshLayout = view.findViewById(R.id.tab_new_smartRefresh);
        smartRefreshLayout.setRefreshHeader(new ClassicsHeader(getContext()));
        smartRefreshLayout.setRefreshFooter(new ClassicsFooter(getContext()));
        smartRefreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshlayout) {
                refreshlayout.finishRefresh(2000);
            }
        });
        smartRefreshLayout.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                refreshLayout.finishLoadMore(1500);
            }
        });
    }

}
