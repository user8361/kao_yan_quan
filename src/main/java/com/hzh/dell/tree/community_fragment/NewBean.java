package com.hzh.dell.tree.community_fragment;

public class NewBean {
    private int headImg;
    private String userName;
    private String userSubmitTime;
    private String hourMinute;
    private String userContent;
    private int imgOne, imgTwo, imgThree;
    private String college;
    private int likeImgId;
    private String likeText;
    private int msgImg;
    private String msgText;


    public int getHeadImg() {
        return headImg;
    }

    public void setHeadImg(int headImg) {
        this.headImg = headImg;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserSubmitTime() {
        return userSubmitTime;
    }

    public void setUserSubmitTime(String userSubmitTime) {
        this.userSubmitTime = userSubmitTime;
    }

    public String getHourMinute() {
        return hourMinute;
    }

    public void setHourMinute(String hourMinute) {
        this.hourMinute = hourMinute;
    }

    public String getUserContent() {
        return userContent;
    }

    public void setUserContent(String userContent) {
        this.userContent = userContent;
    }

    public int getImgOne() {
        return imgOne;
    }

    public void setImgOne(int imgOne) {
        this.imgOne = imgOne;
    }

    public int getImgTwo() {
        return imgTwo;
    }

    public void setImgTwo(int imgTwo) {
        this.imgTwo = imgTwo;
    }

    public int getImgThree() {
        return imgThree;
    }

    public void setImgThree(int imgThree) {
        this.imgThree = imgThree;
    }

    public String getCollege() {
        return college;
    }

    public void setCollege(String college) {
        this.college = college;
    }

    public int getLikeImgId() {
        return likeImgId;
    }

    public void setLikeImgId(int likeImgId) {
        this.likeImgId = likeImgId;
    }

    public String getLikeText() {
        return likeText;
    }

    public void setLikeText(String likeText) {
        this.likeText = likeText;
    }

    public int getMsgImg() {
        return msgImg;
    }

    public void setMsgImg(int msgImg) {
        this.msgImg = msgImg;
    }

    public String getMsgText() {
        return msgText;
    }

    public void setMsgText(String msgText) {
        this.msgText = msgText;
    }
}
