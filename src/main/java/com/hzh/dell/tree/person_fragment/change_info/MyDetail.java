package com.hzh.dell.tree.person_fragment.change_info;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bigkoo.pickerview.builder.OptionsPickerBuilder;
import com.bigkoo.pickerview.listener.OnOptionsSelectListener;
import com.bigkoo.pickerview.view.OptionsPickerView;
import com.google.gson.Gson;
import com.hzh.dell.tree.R;
import com.hzh.dell.tree.bean.AddressJsonBean;
import com.hzh.dell.tree.common_university.CommonUniversity;
import com.hzh.dell.tree.utils.GetJsonDataUtil;
import com.hzh.dell.tree.utils.ImgUtils;

import org.json.JSONArray;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.FindListener;
import cn.bmob.v3.listener.UpdateListener;


public class MyDetail extends AppCompatActivity implements View.OnClickListener {
    //    private UserDao userDao;
//    private User user;
    private static final String TAG = "MY_DETAIL";
    private static final String USER_NAME = "userName";
    public static final String USER_ID = "objectId";
    private static final String SCHOOL = "SCHOOL";
    private static final String SUBJECT = "SUBJECT";
    private LinearLayout headImgLayout, userNameLayout, userPhoneLayout,
            userPasswordLayout, userAddressLayout, userCurrentSchoolLayout,
            userTargetSchoolLayout, userExamYearLayout, userExamSubjectLayout;//头像,用户名，手机号，密码
    private Dialog bottomDialog;//底部弹窗
    private int SELECT_PICTURE = 0x00;//选择相册
    private int SELECT_CAMER = 0x01;//选择相机
    private int CURRENT_SCHOOL = 0x02;//选择当前学校
    private int TARGET_SCHOOL = 0x03;//选择目标院校
    private int SELECT_SUBJECT = 0x04;//选择专业
    private int CHANGE_NAME = 0x05;//修改名字
    private int CHANGE_PHONE = 0x06;//修改手机
    private int CHANGE_INFO = 0x99;//返回父界面的代码
    private Bitmap bitmap;
    private ImageView headImg, ivReturn;
    private TextView tvUserName, tvUserPhone, tvUserAddress, tvUserCurrentSchool,
            tvUserTargetSchool, tvUserExamYear, tvUserExamSubject;
    private Intent intent;
    private String userName = "";//用户名


    private com.hzh.dell.Bmob.entity.User user;

    private ArrayList<AddressJsonBean> options1Items = new ArrayList<>();//省
    private ArrayList<ArrayList<String>> options2Items = new ArrayList<>();//市
    private ArrayList<ArrayList<ArrayList<String>>> options3Items = new ArrayList<>();//区

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.person_detail_info);
        intent = getIntent();
        userName = intent.getStringExtra(USER_NAME);
        initView();
//        initData();
    }

    private void initData() {
        if (user != null) {
            tvUserName.setText(user.getUserName());
            tvUserPhone.setText(user.getUserPhone());
            if (user.getUserHeadImgPath() != null) {
                headImg.setImageBitmap(ImgUtils.stringToBitmap(user.getUserHeadImgPath()));
            }
            if (!"".equals(user.getUserAddress()) && user.getUserAddress() != null) {
                tvUserAddress.setText(user.getUserAddress());
            }
            if (!"".equals(user.getUserCurrentSchool()) && user.getUserCurrentSchool() != null) {
                tvUserCurrentSchool.setText(user.getUserCurrentSchool());
            }
            if (!"".equals(user.getUserTargetSchool()) && user.getUserTargetSchool() != null) {
                tvUserTargetSchool.setText(user.getUserTargetSchool());
            }
            if (!"".equals(user.getUserExamYear()) && user.getUserExamYear() != null) {
                tvUserExamYear.setText(user.getUserExamYear());
            }
            if (!"".equals(user.getUserExamSubject()) && user.getUserExamSubject() != null) {
                tvUserExamSubject.setText(user.getUserExamSubject());
            }
            if (!"".equals(user.getUserExamSubject()) && user.getUserExamSubject() != null) {
                tvUserExamSubject.setText(user.getUserExamSubject());
            }
        }

    }

    private void initView() {
        BmobQuery<com.hzh.dell.Bmob.entity.User> userBmobQuery = new BmobQuery<>();
        userBmobQuery.addWhereEqualTo(USER_NAME, userName);
        userBmobQuery.findObjects(new FindListener<com.hzh.dell.Bmob.entity.User>() {
            @Override
            public void done(List<com.hzh.dell.Bmob.entity.User> list, BmobException e) {
                if (list != null) {
                    user = list.get(0);
                    initData();
                    initClickListener();
                }
            }
        });


        ivReturn = findViewById(R.id.iv_return);

        //Layout
        headImgLayout = findViewById(R.id.head_img_layout);
        userNameLayout = findViewById(R.id.user_name_layout);
        userPhoneLayout = findViewById(R.id.user_phone_layout);
        userPasswordLayout = findViewById(R.id.user_password_layout);
        userAddressLayout = findViewById(R.id.user_address_layout);
        userCurrentSchoolLayout = findViewById(R.id.user_current_school_layout);
        userTargetSchoolLayout = findViewById(R.id.user_target_school_layout);
        userExamYearLayout = findViewById(R.id.user_exam_year_layout);
        userExamSubjectLayout = findViewById(R.id.user_exam_subject_layout);


        //组件
        headImg = findViewById(R.id.iv_head_img);

        tvUserName = findViewById(R.id.tv_user_name);
        tvUserPhone = findViewById(R.id.tv_user_phone);
        tvUserAddress = findViewById(R.id.tv_user_address);
        tvUserCurrentSchool = findViewById(R.id.tv_user_current_school);
        tvUserTargetSchool = findViewById(R.id.tv_user_target_school);
        tvUserExamYear = findViewById(R.id.tv_user_exam_year);
        tvUserExamSubject = findViewById(R.id.tv_user_exam_subject);

    }

    public void initClickListener() {
        //点击事件
        ivReturn.setOnClickListener(this);
        headImgLayout.setOnClickListener(this);
        userNameLayout.setOnClickListener(this);
        userPhoneLayout.setOnClickListener(this);
        userPasswordLayout.setOnClickListener(this);
        userAddressLayout.setOnClickListener(this);
        userCurrentSchoolLayout.setOnClickListener(this);
        userTargetSchoolLayout.setOnClickListener(this);
        userExamYearLayout.setOnClickListener(this);
        userExamSubjectLayout.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_return://返回按钮
                onBackPressed();
                break;
            case R.id.head_img_layout://修改头像
                bottomDialog = new Dialog(this, R.style.BottomDialog);
                View contentView = LayoutInflater.from(this).inflate(R.layout.dialog_bottom_content_normal, null);
                TextView tvCamera = contentView.findViewById(R.id.tv_camera);
                TextView tvChose = contentView.findViewById(R.id.tv_chose);
                TextView tvCancel = contentView.findViewById(R.id.tv_cancle);
                tvCamera.setOnClickListener(this);
                tvCancel.setOnClickListener(this);
                tvChose.setOnClickListener(this);
                bottomDialog.setContentView(contentView);
                ViewGroup.LayoutParams layoutParams = contentView.getLayoutParams();
                layoutParams.width = getResources().getDisplayMetrics().widthPixels;
                contentView.setLayoutParams(layoutParams);
                bottomDialog.getWindow().setGravity(Gravity.BOTTOM);//弹窗位置
                bottomDialog.getWindow().setWindowAnimations(R.style.BottomDialog_Animation);
                bottomDialog.show();//显示弹窗
                break;
            case R.id.tv_camera://自定义Dialog点击事件
                Intent intentCamera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intentCamera, SELECT_CAMER);
                bottomDialog.dismiss();//取消弹窗
                break;
            case R.id.tv_chose://相册
                Intent intentGallary = new Intent(Intent.ACTION_GET_CONTENT);
                intentGallary.addCategory(intentGallary.CATEGORY_OPENABLE);
                intentGallary.setType("image/*");
                startActivityForResult(intentGallary.createChooser(intentGallary, "选择图片"), SELECT_PICTURE);
                bottomDialog.dismiss();
                break;
            case R.id.tv_cancle://取消
                bottomDialog.dismiss();
                break;
            case R.id.user_name_layout:
                Intent changeUserNameIntent = new Intent(this, ChangeUserName.class);
                changeUserNameIntent.putExtra(USER_NAME, user.getUserName());
                startActivityForResult(changeUserNameIntent, CHANGE_NAME);
                break;
            case R.id.user_phone_layout://修改手机
                showDialogWarn();
                break;
            case R.id.user_password_layout://修改密码
                Intent changeUserPasswordIntent = new Intent(this, ChangeUserPassword.class);
                changeUserPasswordIntent.putExtra(USER_NAME, userName);
                startActivity(changeUserPasswordIntent);
                break;
            case R.id.user_address_layout://修改地址
                changeUserAddress();
                break;
            case R.id.user_current_school_layout://修改当前学校
                Intent currentSchoolIntent = new Intent(this, CommonUniversity.class);
                currentSchoolIntent.putExtra(SCHOOL, CURRENT_SCHOOL);
                startActivityForResult(currentSchoolIntent, CURRENT_SCHOOL);//
                break;
            case R.id.user_target_school_layout://修改目标学校
                Intent targetSchoolIntent = new Intent(this, CommonUniversity.class);
                targetSchoolIntent.putExtra(SCHOOL, TARGET_SCHOOL);
                startActivityForResult(targetSchoolIntent, TARGET_SCHOOL);//
                break;
            case R.id.user_exam_year_layout://修改考试年份
                changeExamYear();
                break;
            case R.id.user_exam_subject_layout://修改考试科目
                Intent subjectIntent = new Intent(this, ChangeSubject.class);
                subjectIntent.putExtra(SUBJECT, user.getUserExamSubject());
                startActivityForResult(subjectIntent, SELECT_SUBJECT);
                break;
            default:
                break;

        }
    }

    private void changeExamYear() {
        final String[] data = new String[]{
                "2014届", "2015届", "2016届", "2017届", "2018届", "2019届", "2020届", "2021届", "2022届", "2023届"
        };
        android.support.v7.app.AlertDialog.Builder select_year_list = new android.support.v7.app.AlertDialog.Builder(this);
        select_year_list.setItems(data, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                tvUserExamYear.setText(data[which]);
//                user.setUserExamYear(data[which]);
//                userDao.update(user);
                user.setUserExamYear(data[which]);
                updateUserInfo();//更新信息

            }
        });
        select_year_list.show();
    }


    //修改地址
    private void changeUserAddress() {
        ArrayList<AddressJsonBean> options1Items = new ArrayList<>();//省
        ArrayList<ArrayList<String>> option2Items = new ArrayList<>();//市
        ArrayList<ArrayList<ArrayList<String>>> option3Items = new ArrayList<>();//区
        initJasonData();
        showPickerView();

    }

    private void showPickerView() {
        OptionsPickerView pvOptions = new OptionsPickerBuilder(this, new OnOptionsSelectListener() {
            @Override
            public void onOptionsSelect(int options1, int options2, int options3, View v) {
                tvUserAddress.setText(options1Items.get(options1).getPickerViewText() + "  "
                        + options2Items.get(options1).get(options2) + "  "
                        + options3Items.get(options1).get(options2).get(options3));
                user.setUserAddress(tvUserAddress.getText().toString());
//                userDao.update(user);


            }
        }).setTitleText("城市选择")//标题
                .setTitleSize(16)
                .setDividerColor(Color.BLACK)
                .setSubmitText("确认")
                .setSubmitColor(0x9506a6e6)//确认按钮颜色
                .setCancelText("取消")
                .setCancelColor(0x9506a6e6)//取消按钮颜色
                .setContentTextSize(16)//列表文字大小
                .setTextColorCenter(Color.BLACK)//设置选中项
                .build();
        /*pvOptions.setPicker(options1Items);//一级选择器
        pvOptions.setPicker(options1Items, options2Items);//二级选择器*/
        pvOptions.setPicker(options1Items, options2Items, options3Items);//三级选择器
        pvOptions.show();
    }

    //初始化Json数据
    private void initJasonData() {
        //获得assets目录下的json文件数据
        String JsonData = new GetJsonDataUtil().getJson(this, "province.json");
        ArrayList<AddressJsonBean> addressJsonBean = parseData(JsonData);//用Gson 转成实体
        /**
         * 添加省份数据
         *
         * 注意：如果是添加的JavaBean实体，则实体类需要实现 IPickerViewData 接口，
         * PickerView会通过getPickerViewText方法获取字符串显示出来。
         */
        options1Items = addressJsonBean;

        for (int i = 0; i < addressJsonBean.size(); i++) {//遍历省份
            ArrayList<String> CityList = new ArrayList<>();//该省的城市列表（第二级）
            ArrayList<ArrayList<String>> Province_AreaList = new ArrayList<>();//该省的所有地区列表（第三级）

            for (int c = 0; c < addressJsonBean.get(i).getCityList().size(); c++) {//遍历该省份的所有城市
                String CityName = addressJsonBean.get(i).getCityList().get(c).getName();
                CityList.add(CityName);//添加城市
                ArrayList<String> City_AreaList = new ArrayList<>();//该城市的所有地区列表

                //如果无地区数据，建议添加空字符串，防止数据为null 导致三个选项长度不匹配造成崩溃
                if (addressJsonBean.get(i).getCityList().get(c).getArea() == null
                        || addressJsonBean.get(i).getCityList().get(c).getArea().size() == 0) {
                    City_AreaList.add("");
                } else {
                    City_AreaList.addAll(addressJsonBean.get(i).getCityList().get(c).getArea());
                }
                Province_AreaList.add(City_AreaList);//添加该省所有地区数据
            }

            /**
             * 添加城市数据
             */
            options2Items.add(CityList);

            /**
             * 添加地区数据
             */
            options3Items.add(Province_AreaList);
        }

    }

    public ArrayList<AddressJsonBean> parseData(String result) {//Gson 解析
        ArrayList<AddressJsonBean> detail = new ArrayList<>();
        try {
            JSONArray data = new JSONArray(result);
            Gson gson = new Gson();
            for (int i = 0; i < data.length(); i++) {
                AddressJsonBean entity = gson.fromJson(data.optJSONObject(i).toString(), AddressJsonBean.class);
                detail.add(entity);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return detail;
    }

    //修改手机号
    private void showDialogWarn() {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle("温馨提示");
        dialog.setMessage("您当前绑定的手机号为:" + user.getUserPhone() + "\n确定要修改吗？");
        dialog.setPositiveButton("确定", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent changePhoneIntent = new Intent(MyDetail.this, ChangeUserPhone.class);
                changePhoneIntent.putExtra(USER_ID, user.getObjectId());
                startActivityForResult(changePhoneIntent, CHANGE_PHONE);
            }
        });
        dialog.setNegativeButton("取消", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(MyDetail.this, "您已取消修改", Toast.LENGTH_SHORT).show();

                dialog.dismiss();
            }
        });
        dialog.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Bitmap roundImg = null;

        if (requestCode == SELECT_PICTURE) {
            //相册
            handleResult(resultCode, data);
            roundImg = ImgUtils.toRoundBitmap(bitmap);
            headImg.setImageBitmap(roundImg);
            user.setUserHeadImgPath(ImgUtils.bitmapToString(roundImg));
            updateUserInfo();

        } else if (requestCode == SELECT_CAMER) {
            //相机
            if (data.getData() == null) {
                bitmap = (Bitmap) data.getExtras().get("data");
                roundImg = ImgUtils.toRoundBitmap(bitmap);
                headImg.setImageBitmap(roundImg);
                user.setUserHeadImgPath(ImgUtils.bitmapToString(roundImg));
            } else {
                try {
                    bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), data.getData());
                    if (bitmap != bitmap) {
                        headImg.setImageBitmap(bitmap);
                        user.setUserHeadImgPath(ImgUtils.bitmapToString(bitmap));
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            updateUserInfo();
        } else if (requestCode == CHANGE_NAME && resultCode == CHANGE_NAME) {
            String name = data.getStringExtra("userName");
            userName = name;
            tvUserName.setText(name);//更新名字
        } else if (requestCode == CHANGE_PHONE && resultCode == CHANGE_PHONE) {
            tvUserPhone.setText(data.getStringExtra("userPhone"));//更新手机
        } else if (requestCode == CURRENT_SCHOOL && resultCode == CURRENT_SCHOOL) {
            String result_value = data.getStringExtra("userCurrentSchool");
            tvUserCurrentSchool.setText(result_value);
            user.setUserCurrentSchool(result_value);
            updateUserInfo();
        } else if (requestCode == TARGET_SCHOOL && resultCode == TARGET_SCHOOL) {
            String result_value = data.getStringExtra("userTargetSchool");
            tvUserTargetSchool.setText(result_value);
            user.setUserTargetSchool(result_value);
            updateUserInfo();
        } else if (requestCode == SELECT_SUBJECT && resultCode == SELECT_SUBJECT) {
            String result_value = data.getStringExtra("userExamSubject");
            tvUserExamSubject.setText(result_value);
            user.setUserExamSubject(result_value);
            updateUserInfo();
        }

        if (roundImg != null) {
            user.setUserHeadImgPath(ImgUtils.bitmapToString(roundImg));
            updateUserInfo();
        }
    }

    private void handleResult(int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            Uri uri = data.getData();
            if (uri != null && data.getData() != null) {
                ContentResolver contentResolver = this.getContentResolver();
                if (bitmap != null) {
                    bitmap.recycle();
                }
                try {
                    bitmap = BitmapFactory.decodeStream(contentResolver.openInputStream(uri));
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            } else {
                Log.i(TAG, "uri为空或者data为空   " + "数据：" + data.getData() + " uri: " + uri);
            }
        }
    }


    private void toast(String s) {
        Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
    }

    private void updateUserInfo() {
        user.update(new UpdateListener() {
            @Override
            public void done(BmobException e) {
                if (e != null) {
                    toast("修改失败，错误信息：" + e.getMessage());
                }
            }
        });
    }
}
