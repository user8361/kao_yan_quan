package com.hzh.dell.tree.person_fragment.err_ti;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.hzh.dell.Bmob.entity.ErrTi;
import com.hzh.dell.tree.R;
import com.hzh.dell.tree.adapter.GridViewShowImgWidthAdapter;

import java.util.ArrayList;
import java.util.List;

import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.datatype.BmobFile;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.FindListener;

/**
 * create by hzh
 * on 2019/5/13
 */
public class MyErrTiDetail extends AppCompatActivity implements View.OnClickListener {
    private ImageView ivReturn;
    private TextView tvTitle, tvContent;
    private GridView gridView;
    private String id = "";
    private GridViewShowImgWidthAdapter adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.show_err_ti_detail);

        initView();
    }


    private void initView() {
        ivReturn = findViewById(R.id.iv_return);
        tvTitle = findViewById(R.id.tv_title);
        tvContent = findViewById(R.id.tv_content);
        gridView = findViewById(R.id.gridView);
        ivReturn.setOnClickListener(this);

        id = getIntent().getStringExtra("objectId");
        BmobQuery<ErrTi> errTiBmobQuery = new BmobQuery<>();
        errTiBmobQuery.addWhereEqualTo("objectId", id);
        errTiBmobQuery.findObjects(new FindListener<ErrTi>() {
            @Override
            public void done(List<ErrTi> list, BmobException e) {
                if (list != null) {
                    if (list.size() > 0) {
                        List<String> urlList = new ArrayList<>();
                        ErrTi errTi = list.get(0);
                        tvTitle.setText(errTi.getErrTiTitle());
                        tvContent.setText(errTi.getErrTiKey());
                        Log.e("title", errTi.getErrTiTitle() + "----------------------------");

                        if (errTi.getImgList().size() > 0) {
                            for (BmobFile img : errTi.getImgList()) {
                                urlList.add(img.getFileUrl());

                                Log.e("img", img.getFileUrl());
                            }
                            adapter = new GridViewShowImgWidthAdapter(urlList, getApplicationContext(), getLayoutInflater());
                            gridView.setAdapter(adapter);
                        }

                    } else {
                        Log.e("title", "查询失败");
                    }
                }
            }
        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_return:
                onBackPressed();
                break;
        }
    }
}
