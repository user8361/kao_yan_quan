package com.hzh.dell.tree.person_fragment.concern_fan;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.hzh.dell.individualproj.MainActivity;
import com.hzh.dell.tree.R;
import com.hzh.dell.tree.adapter.listview_adapter.CommonAdapter;
import com.hzh.dell.tree.adapter.listview_adapter.base.ViewHolder;
import com.hzh.dell.tree.database.MyGreenDaoApplication;
import com.hzh.dell.tree.entity.User;
import com.hzh.dell.tree.entity.UserConcern;
import com.hzh.dell.tree.utils.ImgUtils;
import com.hzh.greendao.gen.UserConcernDao;
import com.hzh.greendao.gen.UserDao;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.footer.ClassicsFooter;
import com.scwang.smartrefresh.layout.header.ClassicsHeader;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.util.List;

/**
 * create by hzh
 * on 2019/4/15
 */
public class MyConcernUserFragment extends Fragment {
    private View view;
    private SmartRefreshLayout smartRefreshLayout;
    private ListView listView;
    private UserDao userDao;
    private User user;
    private UserConcernDao userConcernDao;
    private List<UserConcern> userConcernList;
    private Bundle bundle;
    private final String USER_NAME = "userName";
    private String userName = "";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.person_fragment_my_concern_list, container, false);
        initView();
        initData();
        refresh();
        return view;
    }

    private void initData() {
        CommonAdapter<UserConcern> adapter = new CommonAdapter<UserConcern>(getContext(), userConcernList, R.layout.person_fragment_my_concern_list_items) {
            @Override
            public void convert(ViewHolder helper, UserConcern item) {
                User u = userDao.queryBuilder().where(UserDao.Properties.UserId.eq(item.getConcernId())).build().unique();
                helper.setImageBitmap(R.id.user_headImg, ImgUtils.stringToBitmap(u.getUserHeadImgId()));
                helper.setText(R.id.user_name, u.getUserName());
                helper.getView(R.id.tv_concerned).setVisibility(View.VISIBLE);
            }
        };

        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TextView tv = view.findViewById(R.id.user_name);
                Intent intent = new Intent(getContext(), MainActivity.class);
                intent.putExtra(USER_NAME, tv.getText().toString());
                startActivity(intent);
            }
        });
    }

    private void initView() {
        bundle = getArguments();
        userName = bundle.getString(USER_NAME);

        userDao = MyGreenDaoApplication.getInstance().getDaoSession().getUserDao();
        userConcernDao = MyGreenDaoApplication.getInstance().getDaoSession().getUserConcernDao();
        user = userDao.queryBuilder().where(UserDao.Properties.UserName.eq(userName)).build().unique();
        userConcernList = userConcernDao.queryBuilder().where(UserConcernDao.Properties.UserId.eq(user.getUserId()),
                UserConcernDao.Properties.FunId.isNull()).list();

        smartRefreshLayout = view.findViewById(R.id.smartRefresh);
        listView = view.findViewById(R.id.listView);
        View emptyView = view.findViewById(R.id.empty_view_content);
        listView.setEmptyView(emptyView);
    }

    private void refresh() {
        /**
         * 刷新
         */
        smartRefreshLayout.setRefreshHeader(new ClassicsHeader(getContext()));
        smartRefreshLayout.setRefreshFooter(new ClassicsFooter(getContext()));
        smartRefreshLayout.setEnableAutoLoadMore(false);//使上拉加载具有弹性效果
        smartRefreshLayout.setEnableOverScrollDrag(true);//禁止越界拖动（1.0.4以上版本）
        smartRefreshLayout.setEnableOverScrollBounce(true);//关闭越界回弹功能
        smartRefreshLayout.setEnableLoadMore(true); //不隐藏加载,使用底部加载
        smartRefreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshlayout) {
                refreshlayout.finishRefresh(1000);
            }
        });
        smartRefreshLayout.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                refreshLayout.finishLoadMore(1000);
            }
        });

    }
}
