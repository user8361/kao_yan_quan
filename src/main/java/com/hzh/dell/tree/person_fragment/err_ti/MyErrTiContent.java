package com.hzh.dell.tree.person_fragment.err_ti;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.hzh.dell.Bmob.entity.ErrTi;
import com.hzh.dell.Bmob.entity.User;
import com.hzh.dell.tree.R;
import com.hzh.dell.tree.adapter.ViewPagerAdapter;
import com.hzh.dell.tree.common_search.CommonSearch;
import com.hzh.dell.tree.null_fragment.EmptyFragment;
import com.hzh.dell.tree.utils.MagicIndicatorUtil;

import net.lucode.hackware.magicindicator.MagicIndicator;

import java.util.ArrayList;
import java.util.List;

import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.FindListener;


/**
 * create by hzh
 * on 2019/5/9
 */
public class MyErrTiContent extends AppCompatActivity implements View.OnClickListener {

    private ImageView ivReturn, ivSearch;
    private final static String USER_NAME = "userName";
    private String userName = "";
    private final static String SEARCH_TYPE = "searchType";
    private User user;

    private Toolbar toolbar;
    private MagicIndicator magicIndicator;
    private MagicIndicator magicIndicatorTitle;
    private ViewPager viewPager;
    private List<String> titles;
    private List<Fragment> fragmentList;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.show_err_ti);
        initView();
        initTooBar();
    }


    private void initView() {
        ivReturn = findViewById(R.id.iv_return);
        ivSearch = findViewById(R.id.iv_search);
        toolbar = findViewById(R.id.toolBar);
        viewPager = findViewById(R.id.viewPager);
        magicIndicator = findViewById(R.id.magic_indicator);
        magicIndicatorTitle = findViewById(R.id.magic_indicator_title);
        userName = getIntent().getStringExtra(USER_NAME);
    }

    private void initTooBar() {
        toolbar.setTitle("");
        titles = new ArrayList<>();

        BmobQuery<User> userBmobQuery = new BmobQuery<>();
        userBmobQuery.addWhereEqualTo(USER_NAME, userName);
        userBmobQuery.findObjects(new FindListener<User>() {
            @Override
            public void done(List<User> list, BmobException e) {
                if (list != null) {
                    user = list.get(0);
                    if (user != null) {
                        initClickListener();
                        //查找错题
                        BmobQuery<ErrTi> errTiBmobQuery = new BmobQuery<>();
                        errTiBmobQuery.addWhereEqualTo("userId", user.getObjectId());
                        errTiBmobQuery.findObjects(new FindListener<ErrTi>() {
                            @Override
                            public void done(List<ErrTi> list, BmobException e) {
                                if (list != null) {
                                    fragmentList = new ArrayList<>();
                                    for (ErrTi errTi : list) {
                                        if (!titles.contains(errTi.getErrTiTitle())) {
                                            titles.add(errTi.getErrTiTitle());
                                        }
                                    }

                                    if (titles.size() > 0) {
                                        for (int i = 0; i < titles.size(); i++) {
                                            Bundle bundle = new Bundle();
                                            Log.e("title", titles.get(i) + "");
                                            bundle.putString("errTiTitle", titles.get(i));
                                            bundle.putString("userId", user.getObjectId());
                                            MyErrTiFragment myErrTiFragment = (MyErrTiFragment) Fragment.instantiate(getApplicationContext(), MyErrTiFragment.class.getName(), bundle);
                                            fragmentList.add(myErrTiFragment);
                                        }

                                    } else {
                                        titles.add("还没有错题哦");
                                        fragmentList.add(new EmptyFragment());
                                    }

                                    MagicIndicatorUtil magicIndicatorUtil =
                                            new MagicIndicatorUtil(
                                                    magicIndicator, magicIndicatorTitle,
                                                    getApplicationContext(), titles, viewPager,
                                                    2, 20, 3);
                                    magicIndicatorUtil.initMagicIndicator();
                                    magicIndicatorUtil.initMagicIndicatorTitle();
                                    viewPager.setAdapter(new ViewPagerAdapter(getSupportFragmentManager(), fragmentList, titles));
                                }
                            }
                        });
                    }
                } else {
                    Log.e("USER", "null");
                }
            }
        });


    }

    public void initClickListener() {

        ivReturn.setOnClickListener(this);
        ivSearch.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_return:
                onBackPressed();
                break;
            case R.id.iv_search:
                Intent searchIntent = new Intent(this, CommonSearch.class);
                searchIntent.putExtra(USER_NAME, userName);
                searchIntent.putExtra(SEARCH_TYPE, "ErrTi");
                startActivity(searchIntent);
                break;
        }
    }
}
