package com.hzh.dell.tree.person_fragment.concern_fan;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import com.hzh.dell.tree.R;
import com.hzh.dell.tree.adapter.ViewPagerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * create by hzh
 * on 2019/4/15
 */
public class MyConcern extends AppCompatActivity implements View.OnClickListener {
    private ImageView ivReturn;
    private ViewPager viewPager;
    private TabLayout tabLayout;
    private Bundle bundle;
    private Intent getInfoIntent;
    private final String USER_NAME = "userName";
    private String userName = "";


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.person_fragment_my_concern);

        initView();

        initToolBar();
    }

    private void initToolBar() {

        List<String> titles = new ArrayList<>();
        titles.add("用户");
        titles.add("院校");
        titles.add("专业");
        titles.add("专题");

        List<Fragment> fragmentList = new ArrayList<>();
        MyConcernUserFragment myConcernUserFragment = (MyConcernUserFragment) Fragment.instantiate(this, MyConcernUserFragment.class.getName(), bundle);
        Fragment f2 = new Fragment();
        Fragment f3 = new Fragment();
        Fragment f4 = new Fragment();
        fragmentList.add(myConcernUserFragment);
        fragmentList.add(f2);
        fragmentList.add(f3);
        fragmentList.add(f4);
        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager(), fragmentList, titles);
        viewPager.setAdapter(viewPagerAdapter);
        viewPager.setOffscreenPageLimit(tabLayout.getTabCount());
        tabLayout.setupWithViewPager(viewPager);

    }

    private void initView() {
        getInfoIntent = getIntent();
        userName = getInfoIntent.getStringExtra(USER_NAME);
        bundle = new Bundle();
        bundle.putString(USER_NAME, userName);

        ivReturn = findViewById(R.id.iv_return);
        viewPager = findViewById(R.id.viewPager);
        tabLayout = findViewById(R.id.tabLayout);
        ivReturn.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_return:
                onBackPressed();
                break;
            default:
                break;
        }
    }
}
