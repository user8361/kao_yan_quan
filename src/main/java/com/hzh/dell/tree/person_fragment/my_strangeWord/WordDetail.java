package com.hzh.dell.tree.person_fragment.my_strangeWord;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.hzh.dell.Bmob.entity.User;
import com.hzh.dell.Bmob.entity.UserWord;
import com.hzh.dell.Bmob.entity.Word;
import com.hzh.dell.tree.R;
import com.hzh.dell.tree.adapter.recyclerview_adapter.adapter.RecyclerListAdapterForWordItems;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.FindListener;
import cn.bmob.v3.listener.QueryListener;
import cn.bmob.v3.listener.UpdateListener;

/**
 * create by hzh
 * on 2019/5/13
 */
public class WordDetail extends AppCompatActivity implements View.OnClickListener {
    private ImageView ivReturn;
    private TextView tvTitle;
    private RecyclerView recyclerView;
    private RecyclerListAdapterForWordItems adapterForWordItems;
    private MediaPlayer mediaPlayer;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.show_word_detail);

        initView();
    }

    private void initView() {
        ivReturn = findViewById(R.id.iv_return);
        tvTitle = findViewById(R.id.tv_title);
        recyclerView = findViewById(R.id.recycler_view);
        ivReturn.setOnClickListener(this);

        BmobQuery<Word> wordBmobQuery = new BmobQuery<>();
        wordBmobQuery.getObject(getIntent().getStringExtra("wordId"), new QueryListener<Word>() {
            @Override
            public void done(final Word word, BmobException e) {
                if (word != null) {
                    final List<Word> wordList = new ArrayList<>();
                    wordList.add(word);
                    tvTitle.setText(word.getKey());
                    BmobQuery<User> userBmobQuery = new BmobQuery<>();
                    userBmobQuery.getObject(getIntent().getStringExtra("userId"), new QueryListener<User>() {
                        @Override
                        public void done(User user, BmobException e) {
                            if (user != null) {
                                adapterForWordItems = new RecyclerListAdapterForWordItems(wordList, getApplicationContext(), getLayoutInflater(), user);
                                recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                                recyclerView.setAdapter(adapterForWordItems);
                                adapterForWordItems.setInnerItemOnClickListener(new RecyclerListAdapterForWordItems.InnerItemOnClickListener() {
                                    @Override
                                    public void itemClick(final View v, int i) {
                                        switch (v.getId()) {
                                            case R.id.iv_add_strange_word:
                                                BmobQuery<UserWord> userWordBmobQuery = new BmobQuery<>();
                                                userWordBmobQuery.addWhereEqualTo("wordId", word.getObjectId());
                                                userWordBmobQuery.findObjects(new FindListener<UserWord>() {
                                                    @Override
                                                    public void done(List<UserWord> list, BmobException e) {
                                                        if (list != null) {
                                                            UserWord userWord = list.get(0);
                                                            if (userWord != null) {
                                                                if (userWord.getStrangeWordFlag() == 1) {
                                                                    userWord.setStrangeWordFlag(0);
                                                                    userWord.update(new UpdateListener() {
                                                                        @Override
                                                                        public void done(BmobException e) {
                                                                            if (e == null) {
                                                                                ((ImageView) v).setImageResource(R.mipmap.icon_add_to_strange_word);
                                                                                Toast.makeText(getApplicationContext(), "已移除生词本", Toast.LENGTH_SHORT).show();
                                                                            }
                                                                        }
                                                                    });
                                                                } else {
                                                                    userWord.setStrangeWordFlag(1);
                                                                    userWord.update(new UpdateListener() {
                                                                        @Override
                                                                        public void done(BmobException e) {
                                                                            if (e == null) {
                                                                                ((ImageView) v).setImageResource(R.mipmap.icon_remove_from_strange_word);
                                                                                Toast.makeText(getApplicationContext(), "已添加到生词本", Toast.LENGTH_SHORT).show();
                                                                            }
                                                                        }
                                                                    });
                                                                }


                                                            }
                                                        }
                                                    }
                                                });
                                                break;
                                            case R.id.tv_psE:
                                                if (word.getPronE() != null) {
                                                    mediaPlayer = new MediaPlayer();
                                                    try {
                                                        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                                                            @Override
                                                            public void onCompletion(MediaPlayer mp) {
                                                                if (mediaPlayer.isPlaying()) {
                                                                    mediaPlayer.stop();
                                                                    mediaPlayer.release();
                                                                }
                                                                mediaPlayer = null;
                                                            }
                                                        });
                                                        mediaPlayer.setDataSource(word.getPronE());
                                                        mediaPlayer.prepare();
                                                        mediaPlayer.start();

                                                    } catch (IOException e) {
                                                        e.printStackTrace();
                                                    }
                                                }
                                                break;
                                            case R.id.tv_psA:
                                                if (word.getPronE() != null) {
                                                    mediaPlayer = new MediaPlayer();
                                                    try {
                                                        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                                                            @Override
                                                            public void onCompletion(MediaPlayer mp) {
                                                                mediaPlayer.release();
                                                                mediaPlayer = null;
                                                            }
                                                        });
                                                        mediaPlayer.setDataSource(word.getPronA());
                                                        mediaPlayer.prepare();
                                                        mediaPlayer.start();
                                                    } catch (IOException e) {
                                                        e.printStackTrace();
                                                    }
                                                }
                                                break;
                                        }

                                    }
                                });
                            }
                        }
                    });

                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_return:
                onBackPressed();
                break;
        }

    }
}
