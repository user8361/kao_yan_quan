package com.hzh.dell.tree.person_fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.hzh.dell.tree.R;
import com.hzh.dell.tree.adapter.listview_adapter.CommonAdapter;
import com.hzh.dell.tree.adapter.listview_adapter.base.ViewHolder;
import com.hzh.dell.tree.database.MyGreenDaoApplication;
import com.hzh.dell.tree.entity.Article;
import com.hzh.dell.tree.entity.User;
import com.hzh.dell.tree.utils.DateUtil;
import com.hzh.dell.tree.utils.ImgUtils;
import com.hzh.greendao.gen.ArticleDao;
import com.hzh.greendao.gen.UserDao;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.footer.ClassicsFooter;
import com.scwang.smartrefresh.layout.header.ClassicsHeader;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.util.List;

public class MyArticleFragmentList extends Fragment {
    private static final String USER_NAME = "userName";
    private View view;
    private ListView mListView;
    private List<Article> articleList;
    private SmartRefreshLayout smartRefreshLayout;
    private UserDao userDao;
    private User user;
    private ArticleDao articleDao;
    private Article article;
    private Bundle bundle;
    private String userName = "";
    private long timstamp;
    private MyArticleFragmentListItem myArticleFragmentListItem;
    private ImageView ivHeadImg, ivLikeImg, ivCollectImg, ivMsgImg;
    private LinearLayout contentLayout;
    private TextView tvArticleType;
    private Boolean likeImgFlag = false;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.person_fragment_mine_article_list, container, false);

        initView();
        initData();
//        refresh();
        return view;
    }

    private void refresh() {
        /**
         * 刷新
         */
        smartRefreshLayout = view.findViewById(R.id.smartRefresh);
        smartRefreshLayout.setRefreshHeader(new ClassicsHeader(getContext()));
        smartRefreshLayout.setRefreshFooter(new ClassicsFooter(getContext()));
        smartRefreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshlayout) {
                refreshlayout.finishRefresh(2000);
            }
        });


    }

    private void initData() {
        articleList = articleDao.queryBuilder().orderDesc(ArticleDao.Properties.ArticleId).limit(10).where(ArticleDao.Properties.UserId.eq(user.getUserId())).list();
        if (articleList.size() > 0) {
            CommonAdapter<Article> mAdapter = new CommonAdapter<Article>(view.getContext(), articleList, R.layout.person_fragment_mine_article_list_items) {
                @Override
                public void convert(final ViewHolder helper, final Article item) {

                    long time = timstamp - item.getCreateTime();
                    String strTime = DateUtil.getDayHourMinute(item.getCreateTime());//发布的时间
                    helper.setImageBitmap(R.id.user_headImg, ImgUtils.stringToBitmap(user.getUserHeadImgId()));//头像
                    helper.setText(R.id.user_name, user.getUserName());
                    helper.setText(R.id.user_submitTime, strTime);
                    helper.setText(R.id.article_title, item.getArticleTitle());
                    helper.setText(R.id.article_content, item.getArticleContent());
                    if (item.getArticleType() != null) {
                        helper.setText(R.id.article_type, item.getArticleType());
                    } else if (user.getUserTargetSchool() != null) {
                        helper.setText(R.id.article_type, user.getUserTargetSchool());
                    } else if (user.getUserCurrentSchool() != null) {
                        helper.setText(R.id.article_type, user.getUserCurrentSchool());
                    } else if (user.getUserExamYear() != null) {
                        helper.setText(R.id.article_type, user.getUserExamYear() + "考研");
                    } else {
                        helper.setText(R.id.article_type, "2020届考研");
                    }

                    helper.setImageBitmap(R.id.like_img, ImgUtils.stringToBitmap(item.getArticleCollectImgId()));
                    helper.setImageBitmap(R.id.collect_img, ImgUtils.stringToBitmap(item.getArticleCollectImgId()));
                    helper.setImageResource(R.id.msg_img, R.mipmap.msg);
                    helper.setText(R.id.collect_text, String.valueOf(item.getCollectNum()));
                    helper.setText(R.id.like_text, String.valueOf(item.getLikeNum()));
                    helper.setText(R.id.msg_text, String.valueOf(item.getReplyNum()));


                }
            };
            mListView.setAdapter(mAdapter);

        }

    }

    private void initView() {
        bundle = getArguments();
        userName = bundle.getString(USER_NAME);
        userDao = MyGreenDaoApplication.getInstance().getDaoSession().getUserDao();
        user = userDao.queryBuilder().where(UserDao.Properties.UserName.eq(userName)).build().unique();
        articleDao = MyGreenDaoApplication.getInstance().getDaoSession().getArticleDao();
        mListView = view.findViewById(R.id.listView);
        timstamp = System.currentTimeMillis();

    }
}
