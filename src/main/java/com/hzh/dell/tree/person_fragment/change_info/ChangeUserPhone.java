package com.hzh.dell.tree.person_fragment.change_info;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.hzh.dell.Bmob.entity.User;
import com.hzh.dell.tree.R;
import com.hzh.dell.tree.utils.RandomCode;

import java.util.List;

import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.FindListener;
import cn.bmob.v3.listener.QueryListener;
import cn.bmob.v3.listener.UpdateListener;

/**
 * create by hzh
 * on 2019/4/9
 */
public class ChangeUserPhone extends AppCompatActivity implements View.OnClickListener {
    private int CHANGE_PHONE = 0x06;//修改手机
    private final String USER_ID = "objectId";
    //    private UserDao userDao;
//    private User user;
    private ImageView ivReturn;
    private String userId = "", getCode = "";
    private EditText etUserPhone, etSmsCode;
    private Button btnSmsCode, btnSaveChange;
    private Intent getInfoIntent;
    private User mUser;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.person_detail_info_change_user_phone);


        initView();

    }

    private void initView() {

        //组件
        ivReturn = findViewById(R.id.iv_return);
        etUserPhone = findViewById(R.id.et_phone_num);
        etSmsCode = findViewById(R.id.et_sms_code);
        btnSmsCode = findViewById(R.id.btn_sms_code);
        btnSaveChange = findViewById(R.id.btn_save_change);
        //用户信息
        getInfoIntent = getIntent();
        userId = getInfoIntent.getStringExtra(USER_ID);
        BmobQuery<User> userBmobQuery = new BmobQuery<>();
        userBmobQuery.getObject(userId, new QueryListener<User>() {
            @Override
            public void done(User user, BmobException e) {
                if (user != null) {
                    mUser = user;
                    initData();
                }
            }
        });


    }

    private void initData() {
        ivReturn.setOnClickListener(this);
        btnSmsCode.setOnClickListener(this);
        btnSaveChange.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_return:
                onBackPressed();
                break;
            case R.id.btn_sms_code:
                getSmsCode();
                break;
            case R.id.btn_save_change:
                updateUserPhone();
                break;
        }
    }

    public void initEditTextChangeListener() {
        etUserPhone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                BmobQuery<User> userBmobQuery = new BmobQuery<>();
                userBmobQuery.addWhereEqualTo("userPhone", s.toString().trim());
                userBmobQuery.findObjects(new FindListener<User>() {
                    @Override
                    public void done(List<User> list, BmobException e) {
                        if (list != null) {
                            Toast.makeText(ChangeUserPhone.this, "手机号已注册", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }


    private void getSmsCode() {
        String etUserPhoneText = etUserPhone.getText().toString().trim();
        if (etUserPhoneText.length() == 11) {
            BmobQuery<User> userBmobQuery = new BmobQuery<>();
            userBmobQuery.addWhereEqualTo("userPhone", etUserPhoneText);
            userBmobQuery.findObjects(new FindListener<User>() {
                @Override
                public void done(List<User> list, BmobException e) {
                    if (list != null && list.size() == 1) {
                        Toast.makeText(ChangeUserPhone.this, "手机号已被注册", Toast.LENGTH_SHORT).show();
                    } else {
                        getCode = RandomCode.getSixCode();
                        Toast.makeText(getApplicationContext(), "获得验证码:" + getCode, Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }


    }

    //更新用户信息
    private void updateUserPhone() {
        if (mUser != null) {
            String etCode = etSmsCode.getText().toString().trim();
            if (!etCode.equals(getCode)) {
                Toast.makeText(this, "验证码不正确-->" + etCode + "!=" + getCode, Toast.LENGTH_SHORT).show();
            } else {
                mUser.setUserPhone(etUserPhone.getText().toString().trim());
                mUser.update(new UpdateListener() {
                    @Override
                    public void done(BmobException e) {
                        if (e == null) {
                            Toast.makeText(getApplicationContext(), "修改成功", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(getApplicationContext(), MyDetail.class);
                            intent.putExtra("userPhone", mUser.getUserPhone());
                            setResult(CHANGE_PHONE, intent);
                            onBackPressed();
                        } else {
                            Toast.makeText(getApplicationContext(), "修改失败，错入信息：" + e.toString(), Toast.LENGTH_SHORT).show();

                        }
                    }
                });

            }
        }
    }
}
