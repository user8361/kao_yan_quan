package com.hzh.dell.tree.person_fragment.change_info;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.hzh.dell.tree.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * create by hzh
 * on 2019/4/10
 */
public class ChangeSubject extends AppCompatActivity implements View.OnClickListener {
    private Intent getInfoIntent;
    private final static String SUBJECT = "SUBJECT";
    private int SELECT_SUBJECT = 0x04;//选择专业
    private int COLOR = 0x9506a6e6;
    private int DEFAULT_COLOR = 0x8A000000;
    private String userSelectedSubject = "";
    private ImageView ivReturn;
    private TextView tvSave;
    private TextView subPolitics, subMathOne, subMathTwo, subMathThree,
            subEnglishOne, subEnglishTwo, subEducation, subEducationPro, subHistory, subPsychology,
            subWestMedicine, subAccountantPro, subManagement, subManagementStu, subEconomy, subEconomyStu,
            subChinese, subFinance, subNewsDissemination;
    private boolean tvPolitics, tvMathOne, tvMathTwo, tvMathThree, tvEnglishOne, tvEnglishTwo,
            tvEducation, tvEducationPro, tvHistory, tvPsychology, tvWestMedicine, tvAccountantPro,
            tvManagement, tvManagementStu, tvEconomy, tvEconomyStu, tvChinese, tvFinance, tvNewsDissemination;
    private List<String> selectedSubject;
    private List<TextView> tvList;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.person_detail_info_change_user_exam_subject);
        initView();
        getInfoIntent = getIntent();
        String subjects = getInfoIntent.getStringExtra(SUBJECT);
        if (subjects != null) {
            List subjectList = Arrays.asList(subjects.split(","));//根据逗号分隔转化为list
            for (int i = 0; i < subjectList.size(); i++) {
                String subject = subjectList.get(i).toString();
                for (TextView t : tvList) {
                    if (subject.equals(t.getText())) {
                        t.callOnClick();
                    }
                }
            }

        }
    }

    private void initView() {
        ivReturn = findViewById(R.id.iv_return);
        tvSave = findViewById(R.id.tv_save);

        subPolitics = findViewById(R.id.subject_politics);
        subMathOne = findViewById(R.id.subject_math_one);
        subMathTwo = findViewById(R.id.subject_math_two);
        subMathThree = findViewById(R.id.subject_math_three);
        subEnglishOne = findViewById(R.id.subject_english_one);
        subEnglishTwo = findViewById(R.id.subject_english_two);
        subEducation = findViewById(R.id.subject_education);
        subEducationPro = findViewById(R.id.subject_education_pro);
        subHistory = findViewById(R.id.subject_history);
        subPsychology = findViewById(R.id.subject_psychology);
        subWestMedicine = findViewById(R.id.subject_west_medicine);
        subAccountantPro = findViewById(R.id.subject_accountant_pro);
        subManagement = findViewById(R.id.subject_management);
        subManagementStu = findViewById(R.id.subject_management_study);
        subNewsDissemination = findViewById(R.id.subject_news_dissemination);
        subEconomy = findViewById(R.id.subject_economy);
        subEconomyStu = findViewById(R.id.subject_economy_study);
        subChinese = findViewById(R.id.subject_chinese);
        subFinance = findViewById(R.id.subject_finance);

        ivReturn.setOnClickListener(this);
        tvSave.setOnClickListener(this);

        subPolitics.setOnClickListener(this);
        subMathOne.setOnClickListener(this);
        subMathTwo.setOnClickListener(this);
        subMathThree.setOnClickListener(this);
        subEnglishOne.setOnClickListener(this);
        subEnglishTwo.setOnClickListener(this);

        subEducation.setOnClickListener(this);
        subHistory.setOnClickListener(this);
        subPsychology.setOnClickListener(this);
        subWestMedicine.setOnClickListener(this);
        subAccountantPro.setOnClickListener(this);
        subManagement.setOnClickListener(this);
        subEconomy.setOnClickListener(this);
        subChinese.setOnClickListener(this);

        subFinance.setOnClickListener(this);
        subNewsDissemination.setOnClickListener(this);
        subEducationPro.setOnClickListener(this);
        subManagementStu.setOnClickListener(this);
        subEconomyStu.setOnClickListener(this);

        tvPolitics = false;
        tvMathOne = false;
        tvMathTwo = false;
        tvMathThree = false;
        tvEnglishOne = false;
        tvEnglishTwo = false;
        tvEducation = false;
        tvHistory = false;
        tvPsychology = false;
        tvWestMedicine = false;
        tvAccountantPro = false;
        tvManagement = false;
        tvEconomy = false;
        tvChinese = false;
        tvNewsDissemination = false;
        tvEducationPro = false;
        tvManagementStu = false;
        tvEconomyStu = false;

        selectedSubject = new ArrayList<>();
        tvList = new ArrayList<TextView>();
        tvList.add(subPolitics);
        tvList.add(subMathOne);
        tvList.add(subMathTwo);
        tvList.add(subMathThree);
        tvList.add(subEnglishOne);
        tvList.add(subEnglishTwo);

        tvList.add(subEducation);
        tvList.add(subHistory);
        tvList.add(subPsychology);
        tvList.add(subWestMedicine);
        tvList.add(subAccountantPro);
        tvList.add(subManagement);
        tvList.add(subEconomy);
        tvList.add(subChinese);

        tvList.add(subFinance);
        tvList.add(subNewsDissemination);
        tvList.add(subEducationPro);
        tvList.add(subManagementStu);
        tvList.add(subEconomyStu);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.iv_return:
                onBackPressed();
                break;
            case R.id.tv_save:
                if (selectedSubject.size() < 1) {
                    Toast.makeText(this, "至少选择一项", Toast.LENGTH_SHORT).show();
                } else {
                    for (int i = 0; i < selectedSubject.size(); i++) {
                        if (i != selectedSubject.size() - 1) {
                            userSelectedSubject += selectedSubject.get(i) + ",";
                        } else {
                            userSelectedSubject += selectedSubject.get(i);
                        }
                    }
                    Intent intent = new Intent(ChangeSubject.this, MyDetail.class);
                    intent.putExtra("userExamSubject", userSelectedSubject);
                    setResult(SELECT_SUBJECT, intent);
                    onBackPressed();
                }

                break;
            case R.id.subject_politics:
                tvPolitics = clickEvent(subPolitics, tvPolitics);
                break;
            case R.id.subject_math_one:
                if (tvMathTwo == true) {
                    tvMathTwo = clickEvent(subMathTwo, tvMathTwo);
                } else if (tvMathThree == true) {
                    tvMathThree = clickEvent(subMathThree, tvMathThree);
                }
                tvMathOne = clickEvent(subMathOne, tvMathOne);
                break;
            case R.id.subject_math_two:
                if (tvMathOne == true) {
                    tvMathOne = clickEvent(subMathOne, tvMathOne);
                } else if (tvMathThree == true) {
                    tvMathThree = clickEvent(subMathThree, tvMathThree);
                }
                tvMathTwo = clickEvent(subMathTwo, tvMathTwo);

                break;
            case R.id.subject_math_three:
                if (tvMathOne == true) {
                    tvMathOne = clickEvent(subMathOne, tvMathOne);
                } else if (tvMathTwo == true) {
                    tvMathTwo = clickEvent(subMathTwo, tvMathTwo);
                }
                tvMathThree = clickEvent(subMathThree, tvMathThree);

                break;
            case R.id.subject_english_one:

                if (tvEnglishTwo == true) {
                    tvEnglishTwo = clickEvent(subEnglishTwo, tvEnglishTwo);
                }
                tvEnglishOne = clickEvent(subEnglishOne, tvEnglishOne);


                break;
            case R.id.subject_english_two:
                if (tvEnglishOne == true) {
                    tvEnglishOne = clickEvent(subEnglishOne, tvEnglishOne);
                }
                tvEnglishTwo = clickEvent(subEnglishTwo, tvEnglishTwo);

                break;
            case R.id.subject_education:
                if (tvHistory == true) {
                    tvHistory = clickEvent(subHistory, tvHistory);
                } else if (tvPsychology == true) {
                    tvPsychology = clickEvent(subPsychology, tvPsychology);

                } else if (tvWestMedicine == true) {
                    tvWestMedicine = clickEvent(subWestMedicine, tvWestMedicine);
                } else if (tvAccountantPro == true) {
                    tvAccountantPro = clickEvent(subAccountantPro, tvAccountantPro);
                } else if (tvChinese == true) {
                    tvChinese = clickEvent(subChinese, tvChinese);
                } else if (tvFinance == true) {
                    tvFinance = clickEvent(subFinance, tvFinance);
                } else if (tvManagementStu == true) {
                    tvManagementStu = clickEvent(subManagementStu, tvManagementStu);
                } else if (tvEconomyStu == true) {
                    tvEconomyStu = clickEvent(subEconomyStu, tvEconomyStu);
                } else if (tvEducationPro == true) {
                    tvEducationPro = clickEvent(subEducationPro, tvEducationPro);
                }
                tvEducation = clickEvent(subEducation, tvEducation);

                break;
            case R.id.subject_education_pro:
                if (tvEducation == true) {
                    tvEducation = clickEvent(subEducation, tvEducation);
                } else if (tvPsychology == true) {
                    tvPsychology = clickEvent(subPsychology, tvPsychology);

                } else if (tvWestMedicine == true) {
                    tvWestMedicine = clickEvent(subWestMedicine, tvWestMedicine);
                } else if (tvAccountantPro == true) {
                    tvAccountantPro = clickEvent(subAccountantPro, tvAccountantPro);
                } else if (tvChinese == true) {
                    tvChinese = clickEvent(subChinese, tvChinese);
                } else if (tvFinance == true) {
                    tvFinance = clickEvent(subFinance, tvFinance);
                } else if (tvManagementStu == true) {
                    tvManagementStu = clickEvent(subManagementStu, tvManagementStu);
                } else if (tvEconomyStu == true) {
                    tvEconomyStu = clickEvent(subEconomyStu, tvEconomyStu);
                } else if (tvHistory == true) {
                    tvHistory = clickEvent(subHistory, tvHistory);
                }
                tvEducationPro = clickEvent(subEducationPro, tvEducationPro);

                break;
            case R.id.subject_history:
                if (tvEducation == true) {
                    tvEducation = clickEvent(subEducation, tvEducation);
                } else if (tvPsychology == true) {
                    tvPsychology = clickEvent(subPsychology, tvPsychology);

                } else if (tvWestMedicine == true) {
                    tvWestMedicine = clickEvent(subWestMedicine, tvWestMedicine);
                } else if (tvAccountantPro == true) {
                    tvAccountantPro = clickEvent(subAccountantPro, tvAccountantPro);
                } else if (tvChinese == true) {
                    tvChinese = clickEvent(subChinese, tvChinese);
                } else if (tvFinance == true) {
                    tvFinance = clickEvent(subFinance, tvFinance);
                } else if (tvManagementStu == true) {
                    tvManagementStu = clickEvent(subManagementStu, tvManagementStu);
                } else if (tvEconomyStu == true) {
                    tvEconomyStu = clickEvent(subEconomyStu, tvEconomyStu);
                } else if (tvEducationPro == true) {
                    tvEducationPro = clickEvent(subEducationPro, tvEducationPro);
                }
                tvHistory = clickEvent(subHistory, tvHistory);

                break;
            case R.id.subject_psychology:
                if (tvEducation == true) {
                    tvEducation = clickEvent(subEducation, tvEducation);
                } else if (tvHistory == true) {
                    tvHistory = clickEvent(subHistory, tvHistory);

                } else if (tvWestMedicine == true) {
                    tvWestMedicine = clickEvent(subWestMedicine, tvWestMedicine);
                } else if (tvAccountantPro == true) {
                    tvAccountantPro = clickEvent(subAccountantPro, tvAccountantPro);
                } else if (tvChinese == true) {
                    tvChinese = clickEvent(subChinese, tvChinese);
                } else if (tvFinance == true) {
                    tvFinance = clickEvent(subFinance, tvFinance);
                } else if (tvManagementStu == true) {
                    tvManagementStu = clickEvent(subManagementStu, tvManagementStu);
                } else if (tvEconomyStu == true) {
                    tvEconomyStu = clickEvent(subEconomyStu, tvEconomyStu);
                } else if (tvEducationPro == true) {
                    tvEducationPro = clickEvent(subEducationPro, tvEducationPro);
                }
                tvPsychology = clickEvent(subPsychology, tvPsychology);
                break;
            case R.id.subject_west_medicine:
                if (tvEducation == true) {
                    tvEducation = clickEvent(subEducation, tvEducation);
                } else if (tvPsychology == true) {
                    tvPsychology = clickEvent(subPsychology, tvPsychology);

                } else if (tvHistory == true) {
                    tvHistory = clickEvent(subHistory, tvHistory);
                } else if (tvAccountantPro == true) {
                    tvAccountantPro = clickEvent(subAccountantPro, tvAccountantPro);
                } else if (tvChinese == true) {
                    tvChinese = clickEvent(subChinese, tvChinese);
                } else if (tvFinance == true) {
                    tvFinance = clickEvent(subFinance, tvFinance);
                } else if (tvManagementStu == true) {
                    tvManagementStu = clickEvent(subManagementStu, tvManagementStu);
                } else if (tvEconomyStu == true) {
                    tvEconomyStu = clickEvent(subEconomyStu, tvEconomyStu);
                } else if (tvEducationPro == true) {
                    tvEducationPro = clickEvent(subEducationPro, tvEducationPro);
                }

                tvWestMedicine = clickEvent(subWestMedicine, tvWestMedicine);
                break;
            case R.id.subject_accountant_pro:
                if (tvEducation == true) {
                    tvEducation = clickEvent(subEducation, tvEducation);
                } else if (tvPsychology == true) {
                    tvPsychology = clickEvent(subPsychology, tvPsychology);

                } else if (tvWestMedicine == true) {
                    tvWestMedicine = clickEvent(subWestMedicine, tvWestMedicine);
                } else if (tvHistory == true) {
                    tvHistory = clickEvent(subHistory, tvHistory);
                } else if (tvChinese == true) {
                    tvChinese = clickEvent(subChinese, tvChinese);
                } else if (tvFinance == true) {
                    tvFinance = clickEvent(subFinance, tvFinance);
                } else if (tvManagementStu == true) {
                    tvManagementStu = clickEvent(subManagementStu, tvManagementStu);
                } else if (tvEconomyStu == true) {
                    tvEconomyStu = clickEvent(subEconomyStu, tvEconomyStu);
                } else if (tvEducationPro == true) {
                    tvEducationPro = clickEvent(subEducationPro, tvEducationPro);
                }
                tvAccountantPro = clickEvent(subAccountantPro, tvAccountantPro);
                break;
            case R.id.subject_management:
                if (tvEconomy == true) {
                    tvEconomy = clickEvent(subEconomy, tvEconomy);
                }
                tvManagement = clickEvent(subManagement, tvManagement);
                break;
            case R.id.subject_management_study:
                if (tvEducation == true) {
                    tvEducation = clickEvent(subEducation, tvEducation);
                } else if (tvPsychology == true) {
                    tvPsychology = clickEvent(subPsychology, tvPsychology);

                } else if (tvWestMedicine == true) {
                    tvWestMedicine = clickEvent(subWestMedicine, tvWestMedicine);
                } else if (tvAccountantPro == true) {
                    tvAccountantPro = clickEvent(subAccountantPro, tvAccountantPro);
                } else if (tvChinese == true) {
                    tvChinese = clickEvent(subChinese, tvChinese);
                } else if (tvFinance == true) {
                    tvFinance = clickEvent(subFinance, tvFinance);
                } else if (tvHistory == true) {
                    tvHistory = clickEvent(subHistory, tvHistory);
                } else if (tvEconomyStu == true) {
                    tvEconomyStu = clickEvent(subEconomyStu, tvEconomyStu);
                } else if (tvEducationPro == true) {
                    tvEducationPro = clickEvent(subEducationPro, tvEducationPro);
                }
                tvManagementStu = clickEvent(subManagementStu, tvManagementStu);
                break;
            case R.id.subject_economy:
                if (tvManagement == true) {
                    tvManagement = clickEvent(subManagement, tvManagement);
                }
                tvEconomy = clickEvent(subEconomy, tvEconomy);
                break;
            case R.id.subject_economy_study:

                if (tvEducation == true) {
                    tvEducation = clickEvent(subEducation, tvEducation);
                } else if (tvPsychology == true) {
                    tvPsychology = clickEvent(subPsychology, tvPsychology);

                } else if (tvWestMedicine == true) {
                    tvWestMedicine = clickEvent(subWestMedicine, tvWestMedicine);
                } else if (tvAccountantPro == true) {
                    tvAccountantPro = clickEvent(subAccountantPro, tvAccountantPro);
                } else if (tvChinese == true) {
                    tvChinese = clickEvent(subChinese, tvChinese);
                } else if (tvFinance == true) {
                    tvFinance = clickEvent(subFinance, tvFinance);
                } else if (tvManagementStu == true) {
                    tvManagementStu = clickEvent(subManagementStu, tvManagementStu);
                } else if (tvHistory == true) {
                    tvHistory = clickEvent(subHistory, tvHistory);
                } else if (tvEducationPro == true) {
                    tvEducationPro = clickEvent(subEducationPro, tvEducationPro);
                }
                tvEconomyStu = clickEvent(subEconomyStu, tvEconomyStu);
                break;
            case R.id.subject_chinese:
                if (tvEducation == true) {
                    tvEducation = clickEvent(subEducation, tvEducation);
                } else if (tvPsychology == true) {
                    tvPsychology = clickEvent(subPsychology, tvPsychology);

                } else if (tvWestMedicine == true) {
                    tvWestMedicine = clickEvent(subWestMedicine, tvWestMedicine);
                } else if (tvAccountantPro == true) {
                    tvAccountantPro = clickEvent(subAccountantPro, tvAccountantPro);
                } else if (tvHistory == true) {
                    tvHistory = clickEvent(subHistory, tvHistory);
                } else if (tvFinance == true) {
                    tvFinance = clickEvent(subFinance, tvFinance);
                } else if (tvManagementStu == true) {
                    tvManagementStu = clickEvent(subManagementStu, tvManagementStu);
                } else if (tvEconomyStu == true) {
                    tvEconomyStu = clickEvent(subEconomyStu, tvEconomyStu);
                } else if (tvEducationPro == true) {
                    tvEducationPro = clickEvent(subEducationPro, tvEducationPro);
                }
                tvChinese = clickEvent(subChinese, tvChinese);
                break;
            case R.id.subject_finance:
                if (tvEducation == true) {
                    tvEducation = clickEvent(subEducation, tvEducation);
                } else if (tvPsychology == true) {
                    tvPsychology = clickEvent(subPsychology, tvPsychology);

                } else if (tvWestMedicine == true) {
                    tvWestMedicine = clickEvent(subWestMedicine, tvWestMedicine);
                } else if (tvAccountantPro == true) {
                    tvAccountantPro = clickEvent(subAccountantPro, tvAccountantPro);
                } else if (tvChinese == true) {
                    tvChinese = clickEvent(subChinese, tvChinese);
                } else if (tvHistory == true) {
                    tvHistory = clickEvent(subHistory, tvHistory);
                } else if (tvManagementStu == true) {
                    tvManagementStu = clickEvent(subManagementStu, tvManagementStu);
                } else if (tvEconomyStu == true) {
                    tvEconomyStu = clickEvent(subEconomyStu, tvEconomyStu);
                } else if (tvEducationPro == true) {
                    tvEducationPro = clickEvent(subEducationPro, tvEducationPro);
                }
                tvFinance = clickEvent(subFinance, tvFinance);
                break;
            case R.id.subject_news_dissemination:

                tvNewsDissemination = clickEvent(subNewsDissemination, tvNewsDissemination);
                break;
        }

    }


    public boolean clickEvent(TextView t, boolean b) {
        if (b == false) {
            b = true;
            t.setTextColor(COLOR);
            selectedSubject.add(t.getText().toString());
        } else {
            b = false;
            t.setTextColor(DEFAULT_COLOR);
            selectedSubject.remove(t.getText().toString());
        }
        return b;
    }

}
