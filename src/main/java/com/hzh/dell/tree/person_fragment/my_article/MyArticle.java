package com.hzh.dell.tree.person_fragment.my_article;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import com.hzh.dell.Bmob.entity.User;
import com.hzh.dell.tree.R;
import com.hzh.dell.tree.adapter.ViewPagerAdapter;
import com.hzh.dell.tree.utils.MagicIndicatorUtil;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;

import net.lucode.hackware.magicindicator.MagicIndicator;

import java.util.ArrayList;
import java.util.List;

import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.FindListener;

public class MyArticle extends AppCompatActivity implements View.OnClickListener {
    private static final String USER_NAME = "userName";
    private String userName = "";

    private User user;
    private ImageView ivReturn;//返回键
    private SmartRefreshLayout smartRefreshLayout;
    private List<String> titleList;
    private ViewPager viewPager;
    private List<Fragment> fragmentList;
    private Intent getInfoIntent;
    private MagicIndicator magicIndicator;
    private MagicIndicator magicIndicatorTitle;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.person_fragment_mine_article);
        initView();
    }


    private void initData() {
        ivReturn.setOnClickListener(this);
        titleList = new ArrayList<>();
        titleList.add("图文贴");
        titleList.add("投票贴");
        fragmentList = new ArrayList<>();
        Bundle bundle = new Bundle();
        bundle.putString(USER_NAME, user.getUserName());
        MyArticleMainFragmentList myArticleMainFragmentList_ = (MyArticleMainFragmentList) Fragment.instantiate(this, MyArticleMainFragmentList.class.getName(), bundle);
//        MyArticleReplyFragmentList myArticleReplyFragmentList = (MyArticleReplyFragmentList) Fragment.instantiate(this, MyArticleReplyFragmentList.class.getName(), bundle);

        fragmentList.add(myArticleMainFragmentList_);


        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager(), fragmentList, titleList);
        viewPager.setAdapter(viewPagerAdapter);
        viewPager.setOffscreenPageLimit(fragmentList.size());

        MagicIndicatorUtil magicIndicatorUtil = new MagicIndicatorUtil(
                magicIndicator, magicIndicatorTitle,
                getApplicationContext(),
                titleList, viewPager,
                2, 40, 3);
        magicIndicatorUtil.initMagicIndicatorTitle();
        magicIndicatorUtil.initMagicIndicator();
    }


    private void initView() {
        getInfoIntent = getIntent();
        userName = getInfoIntent.getStringExtra(USER_NAME);
        BmobQuery<User> userBmobQuery = new BmobQuery<>();
        userBmobQuery.addWhereEqualTo(USER_NAME, userName);
        userBmobQuery.findObjects(new FindListener<User>() {
            @Override
            public void done(List<com.hzh.dell.Bmob.entity.User> list, BmobException e) {
                if (list != null) {
                    user = list.get(0);
                    if (user != null) {
                        initData();
                    }
                }
            }
        });

        ivReturn = findViewById(R.id.iv_return);
        viewPager = findViewById(R.id.viewPager);

        magicIndicator = findViewById(R.id.magic_indicator);
        magicIndicatorTitle = findViewById(R.id.magic_indicator_title);
        smartRefreshLayout = findViewById(R.id.smartRefresh);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_return:
                onBackPressed();
                break;
        }
    }
}
