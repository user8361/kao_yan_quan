package com.hzh.dell.tree.person_fragment.change_info;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.hzh.dell.tree.R;
import com.hzh.dell.tree.database.MyGreenDaoApplication;
import com.hzh.dell.tree.entity.User;
import com.hzh.greendao.gen.UserDao;

/**
 * create by hzh
 * on 2019/4/9
 */
public class ChangeUserPassword extends AppCompatActivity implements View.OnClickListener {
    private UserDao userDao;
    private User user;
    private Intent getInfoIntent;
    private final String USER_NAME = "userName";

    private ImageView ivReturn;
    private EditText etOldPassword, etNewPassword, etConfirmPassword;
    private Button btnSaveChange;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.person_detail_info_change_user_password);
        initView();
    }

    private void initView() {
        getInfoIntent = getIntent();
        userDao = MyGreenDaoApplication.getInstance().getDaoSession().getUserDao();
        user = userDao.queryBuilder().where(UserDao.Properties.UserName.eq(getInfoIntent.getStringExtra(USER_NAME))).build().unique();

        ivReturn = findViewById(R.id.iv_return);
        etOldPassword = findViewById(R.id.et_old_password);
        etNewPassword = findViewById(R.id.et_new_password);
        etConfirmPassword = findViewById(R.id.et_confirm_password);
        btnSaveChange = findViewById(R.id.btn_save_change);

        ivReturn.setOnClickListener(this);
        btnSaveChange.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_return:
                onBackPressed();
                break;
            case R.id.btn_save_change:
                changeUserPassword();
                break;
        }
    }

    private void changeUserPassword() {
        String oldPwd = etOldPassword.getText().toString().trim();
        String newPwd = etNewPassword.getText().toString().trim();
        String confirmPwd = etConfirmPassword.getText().toString().trim();
        if (user != null) {
            if (oldPwd.equals(user.getUserPassword())) {//旧密码与数据库中密码相同
                if (newPwd.length() >= 6 && newPwd.length() <= 20) {//新密码长度正确
                    if (newPwd.equals(confirmPwd)) {//两次新密码相同
                        user.setUserPassword(etConfirmPassword.getText().toString().trim());
                        userDao.update(user);
                        Toast.makeText(this, "修改成功", Toast.LENGTH_SHORT).show();
                        onBackPressed();
                    } else {//两次新密码不相同
                        Toast.makeText(this, "新密码不匹配，请重新输入！", Toast.LENGTH_SHORT).show();

                    }
                } else {//新密码长度不正确
                    Toast.makeText(this, "密码长度为6-20个字符！", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(this, "原密码不正确，请重试！", Toast.LENGTH_SHORT).show();

            }

        }
    }

//    @Override
//    public void onBackPressed() {
//        super.onBackPressed();
//        Intent fatherIntent=new Intent(this,MyDetail.class);
//        fatherIntent.putExtra(USER_NAME,user.getUserName());
//        startActivity(fatherIntent);
//        finish();
//    }
}
