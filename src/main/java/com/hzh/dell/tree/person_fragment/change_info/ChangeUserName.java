package com.hzh.dell.tree.person_fragment.change_info;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.hzh.dell.Bmob.entity.User;
import com.hzh.dell.tree.R;
import com.hzh.dell.tree.login_register.UserManager;

import java.util.List;

import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.FindListener;
import cn.bmob.v3.listener.UpdateListener;

/**
 * create by hzh
 * on 2019/4/9
 */
public class ChangeUserName extends AppCompatActivity implements View.OnClickListener {
    private final String USER_NAME = "userName";
    private int CHANGE_NAME = 0x05;//修改名字
    //    private UserDao userDao;
    private User user;
    private Intent userIntent;
    private String userName = "";
    private TextView tvSave, tvHint;
    private EditText etUserName;
    private ImageView ivReturn;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.person_detail_info_change_user_name);

        initView();
        initData();
    }

    private void initView() {
        userIntent = getIntent();
        //获得用户名
        userName = userIntent.getStringExtra(USER_NAME);
        BmobQuery<User> userBmobQuery = new BmobQuery<>();
        userBmobQuery.addWhereEqualTo(USER_NAME, userName);
        userBmobQuery.findObjects(new FindListener<User>() {
            @Override
            public void done(List<User> list, BmobException e) {
                if (list != null) {
                    user = list.get(0);
                    initData();
                }
            }
        });
        tvSave = findViewById(R.id.tv_save);
        etUserName = findViewById(R.id.et_user_name);
        ivReturn = findViewById(R.id.iv_return);
        tvHint = findViewById(R.id.tv_hint);
    }

    private void initData() {

        if (user != null) {
            if (user.getUserName() != null) {
                etUserName.setText(user.getUserName());
                initEditTextChangeListener();//监听文本变化
                initClickListener();
            }
        }
    }

    public void initClickListener() {
        tvSave.setOnClickListener(this);
        ivReturn.setOnClickListener(this);
    }

    public void initEditTextChangeListener() {
        etUserName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                BmobQuery<User> userBmobQuery = new BmobQuery<>();
                userBmobQuery.addWhereEqualTo(USER_NAME, s.toString());
                userBmobQuery.findObjects(new FindListener<User>() {
                    @SuppressLint("ResourceAsColor")
                    @Override
                    public void done(List<User> list, BmobException e) {
                        if (list != null && list.size() == 1) {
                            tvHint.setText("用户名已被占用！");
                            tvHint.setTextColor(Color.parseColor("#ff3955"));
                            tvSave.setClickable(false);
                            tvSave.setTextColor(Color.parseColor("#4c4c4c"));
                        } else {
                            tvHint.setText("好的名字可以让你的朋友更容易记住你。");
                            tvHint.setTextColor(Color.parseColor("#c5c5c5"));
                            tvSave.setClickable(true);
                            tvSave.setTextColor(Color.parseColor("#353535"));
                        }
                    }
                });
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_return:
                onBackPressed();
                break;
            case R.id.tv_save:
                final String name = etUserName.getText().toString();
                user.setUserName(name);
                user.update(new UpdateListener() {
                    @Override
                    public void done(BmobException e) {
                        if (e == null) {
                            Toast.makeText(getApplicationContext(), "修改成功！", Toast.LENGTH_SHORT).show();
                            UserManager.getInstance().saveUserInfo(getApplicationContext(), name, user.getUserPassword());
                            Intent intent = new Intent(getApplicationContext(), MyDetail.class);
                            intent.putExtra(USER_NAME, user.getUserName());
                            Log.e(USER_NAME, user.getUserName());
                            setResult(CHANGE_NAME, intent);
                            onBackPressed();
                        } else {
                            Toast.makeText(getApplicationContext(), "修改失败！", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
        }
    }
}
