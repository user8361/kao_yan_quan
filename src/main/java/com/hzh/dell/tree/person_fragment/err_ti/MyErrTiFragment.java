package com.hzh.dell.tree.person_fragment.err_ti;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.hzh.dell.Bmob.entity.ErrTi;
import com.hzh.dell.tree.R;
import com.hzh.dell.tree.adapter.ListViewAdapter;
import com.hzh.dell.tree.custom.MyListView;

import java.util.List;

import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.FindListener;

/**
 * create by hzh
 * on 2019/5/13
 */
public class MyErrTiFragment extends Fragment {
    private View view;
    private MyListView listView;
    private String userId = "";
    private String errTiTitle = "";
    private Bundle bundle;
    private ListViewAdapter adapter;
    private String TAG = this.getClass().getSimpleName();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.show_err_ti_list, container, false);
        initView();
        return view;
    }

    private void initView() {
        listView = view.findViewById(R.id.listView);
        View emptyView = view.findViewById(R.id.empty_view);
        listView.setEmptyView(emptyView);
        initData();

    }

    private void initData() {
        bundle = getArguments();
        userId = bundle.getString("userId");
        errTiTitle = bundle.getString("errTiTitle");
        BmobQuery<ErrTi> errTiBmobQuery = new BmobQuery<>();
        errTiBmobQuery.addWhereEqualTo("userId", userId);
        errTiBmobQuery.addWhereEqualTo("errTiTitle", errTiTitle);
        errTiBmobQuery.findObjects(new FindListener<ErrTi>() {
            @Override
            public void done(final List<ErrTi> list, BmobException e) {
                if (list != null) {
                    String[] dataList = new String[list.size()];
                    for (int i = 0; i < list.size(); i++) {
                        ErrTi errTi = list.get(i);
                        dataList[i] = errTi.getErrTiKey() + " --- " + errTi.getCreatedAt().substring(0, 10);
                    }
                    initListViewData(list, dataList);
                } else {
                    Log.e("list", "错题为空");
                }
            }
        });
    }

    public void initListViewData(final List<ErrTi> list, String[] dataList) {
        adapter = new ListViewAdapter(getContext(), dataList);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String objectId = list.get(position).getObjectId();
                Log.e("id", objectId);
                Intent showDetailIntent = new Intent(getContext(), MyErrTiDetail.class);
                showDetailIntent.putExtra("objectId", objectId);
                startActivity(showDetailIntent);
            }
        });
    }

}
