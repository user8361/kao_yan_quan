package com.hzh.dell.tree.person_fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.hzh.dell.tree.R;
import com.hzh.dell.tree.database.MyGreenDaoApplication;
import com.hzh.dell.tree.entity.Article;
import com.hzh.dell.tree.entity.User;
import com.hzh.greendao.gen.ArticleDao;
import com.hzh.greendao.gen.UserDao;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.footer.ClassicsFooter;
import com.scwang.smartrefresh.layout.header.ClassicsHeader;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

public class MyArticleFragmentListItem extends Fragment implements View.OnClickListener {
    private static final String USER_NAME = "userName";
    private View view;
    private SmartRefreshLayout smartRefreshLayout;
    private UserDao userDao;
    private User user;
    private ArticleDao articleDao;
    private Article article;
    private String userName;
    private Intent getInfoIntent;
    private ImageView ivHeadImg;
    private TextView tvUserName, tvArticleTime, tvArticleTitle, tvArticleContent,
            tvArticleType, tvLikeNum, tvCollectNum, tvMsgNum;
    private GridView gridView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.person_fragment_mine_article_list_items, container, false);
        userName = getArguments().get(USER_NAME).toString();
        Toast.makeText(getContext(), userName, Toast.LENGTH_SHORT).show();
        if (userName != null) {
            initView();
            initData();
            refresh();
        }
        return view;
    }

    private void refresh() {
        /**
         * 刷新
         */
        smartRefreshLayout = view.findViewById(R.id.smartRefresh);
        smartRefreshLayout.setRefreshHeader(new ClassicsHeader(getContext()));
        smartRefreshLayout.setRefreshFooter(new ClassicsFooter(getContext()));
        smartRefreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshlayout) {
                refreshlayout.finishRefresh(2000);
            }
        });
        smartRefreshLayout.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                refreshLayout.finishLoadMore(1500);
            }
        });

    }

    private void initData() {


    }

    private void initView() {
        getInfoIntent = getActivity().getIntent();
        userDao = MyGreenDaoApplication.getInstance().getDaoSession().getUserDao();
        user = userDao.queryBuilder().where(UserDao.Properties.UserName.eq(userName)).build().unique();
        articleDao = MyGreenDaoApplication.getInstance().getDaoSession().getArticleDao();

        ivHeadImg = view.findViewById(R.id.person_headImg);
        tvUserName = view.findViewById(R.id.user_name);
        tvArticleTitle = view.findViewById(R.id.article_title);
        tvArticleTime = view.findViewById(R.id.user_submitTime);
        tvArticleContent = view.findViewById(R.id.article_content);
        tvArticleType = view.findViewById(R.id.article_type);
        tvCollectNum = view.findViewById(R.id.collect_text);
        tvLikeNum = view.findViewById(R.id.like_text);
        tvMsgNum = view.findViewById(R.id.msg_text);
        gridView = view.findViewById(R.id.gridView);

        tvUserName.setOnClickListener(this);
        tvArticleTime.setOnClickListener(this);
        tvArticleTitle.setOnClickListener(this);
        tvArticleContent.setOnClickListener(this);
        tvArticleType.setOnClickListener(this);
        tvLikeNum.setOnClickListener(this);
        tvCollectNum.setOnClickListener(this);
        tvMsgNum.setOnClickListener(this);
        gridView.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.user_headImg:
                Toast.makeText(getContext(), "HeadImg", Toast.LENGTH_SHORT).show();
                break;
            case R.id.user_name:
                Toast.makeText(getContext(), "NAME", Toast.LENGTH_SHORT).show();
        }
    }
}
