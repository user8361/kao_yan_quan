package com.hzh.dell.tree.person_fragment.my_strangeWord;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;

import com.hzh.dell.Bmob.entity.User;
import com.hzh.dell.Bmob.entity.UserWord;
import com.hzh.dell.Bmob.entity.Word;
import com.hzh.dell.tree.R;
import com.hzh.dell.tree.adapter.ListViewAdapter;
import com.hzh.dell.tree.common_search.CommonSearch;
import com.hzh.dell.tree.custom.MyListView;
import com.jaeger.library.StatusBarUtil;

import java.util.ArrayList;
import java.util.List;

import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.FindListener;

/**
 * create by hzh
 * on 2019/5/13
 */
public class StrangeWord extends AppCompatActivity implements View.OnClickListener {
    private final String USER_NAME = "userName";
    private String userName = "";
    private User user;
    private ImageView ivReturn, ivSearch;
    private MyListView listView;
    private ListViewAdapter adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_strange_word_list);
        StatusBarUtil.setTranslucent(this, 50);//透明度为50%
        userName = getIntent().getStringExtra(USER_NAME);
        BmobQuery<User> userBmobQuery = new BmobQuery<>();
        userBmobQuery.addWhereEqualTo(USER_NAME, userName);
        userBmobQuery.findObjects(new FindListener<User>() {
            @Override
            public void done(List<User> list, BmobException e) {
                if (list != null) {
                    Log.e("list", "1 ---> ！null");

                    user = list.get(0);
                    if (user != null) {
                        Log.e("list", "2 ---> ！null");

                        initView();
                    }
                } else {
                    Log.e("list", "user list ---> null");
                }
            }
        });

    }

    private void initView() {
        ivReturn = findViewById(R.id.iv_return);
        ivSearch = findViewById(R.id.iv_search);
        ivReturn.setOnClickListener(this);
        ivSearch.setOnClickListener(this);
        listView = findViewById(R.id.listView);

        View emptyView = findViewById(R.id.empty_view);
        listView.setEmptyView(emptyView);
        BmobQuery<UserWord> userWordBmobQuery = new BmobQuery<>();
        userWordBmobQuery.addWhereEqualTo("userId", user.getObjectId());
        userWordBmobQuery.order("updatedAt");
        userWordBmobQuery.addWhereEqualTo("strangeWordFlag", 1);
        userWordBmobQuery.findObjects(new FindListener<UserWord>() {
            @Override
            public void done(final List<UserWord> list, BmobException e) {
                if (list != null) {
                    final List<String> wordId = new ArrayList<>();
                    for (UserWord userWord : list) {
                        wordId.add(userWord.getWordId());
                    }
                    if (wordId.size() > 0) {
                        BmobQuery<Word> wordBmobQuery = new BmobQuery<>();
                        wordBmobQuery.findObjects(new FindListener<Word>() {
                            @Override
                            public void done(List<Word> list, BmobException e) {
                                if (list.size() > 0) {
                                    final List<Word> wordsList = new ArrayList<>();
                                    for (Word word : list) {
                                        if (wordId.contains(word.getObjectId())) {
                                            wordsList.add(word);
                                        }
                                    }
                                    if (wordsList.size() > 0) {
                                        String[] data = new String[wordsList.size()];
                                        for (int i = 0; i < data.length; i++) {
                                            Word word = wordsList.get(i);
                                            if (word.getPos().size() > 0) {
                                                data[i] = word.getKey() + " " + word.getPos().get(0) + " " + word.getAcceptationList().get(0);

                                            }
                                        }
                                        if (data.length > 0) {
                                            adapter = new ListViewAdapter(getApplicationContext(), data);
                                            listView.setAdapter(adapter);
                                            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                                @Override
                                                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                                    Intent wordDetailIntent = new Intent(getApplicationContext(), WordDetail.class);
                                                    wordDetailIntent.putExtra("wordId", wordsList.get(position).getObjectId());
                                                    wordDetailIntent.putExtra("userId", user.getObjectId());
                                                    startActivity(wordDetailIntent);
                                                }
                                            });

                                        }
                                    }
                                }
                            }
                        });
                    }
                } else {
                    Log.e("list", "userWord list --> null");
                }
            }
        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_return:
                onBackPressed();
                break;
            case R.id.iv_search:
                searchWord();
                break;


        }

    }

    private void searchWord() {
        Intent searchWord = new Intent(this, CommonSearch.class);
        startActivity(searchWord);
    }
}










