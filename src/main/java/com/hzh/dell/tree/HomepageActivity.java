package com.hzh.dell.tree;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.hzh.dell.tree.main_fragment.MainFragmentCommunity;
import com.hzh.dell.tree.main_fragment.MainFragmentHomePage;
import com.hzh.dell.tree.main_fragment.MainFragmentPerson;
import com.hzh.dell.tree.main_fragment.MainFragmentSchoolNew;
import com.jaeger.library.StatusBarUtil;

public class HomepageActivity extends AppCompatActivity {
    //    private UserDao userDao;
//    private User user;
    private MainFragmentHomePage mainFragmentHomePage;
    private MainFragmentCommunity mainFragmentCommunity;
    private MainFragmentSchoolNew mainFragmentSchoolNew;
    private MainFragmentPerson mainFragmentPerson;
    private Intent getInfoIntent;
    private String userName = "";
    private final String USER_NAME = "userName";
    Bundle bundle = new Bundle();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_homepage);
//        ViewTarget.setTagId(R.id.glide_tag);
//        StatusBarUtil.setColor(this, 0x5CC5B2);
        StatusBarUtil.setTranslucent(this, 50);//半透明

        getInfoIntent = getIntent();
        userName = getInfoIntent.getStringExtra(USER_NAME);


//        userDao = MyGreenDaoApplication.getInstance().getDaoSession().getUserDao();
//        user = userDao.queryBuilder().where(UserDao.Properties.UserName.eq(userName)).build().unique();

        initTab();//设置启动后的主页
        BottomNavigationView navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    }

    private void initTab() {
        bundle = new Bundle();
        bundle.putString(USER_NAME, /*user.getUserName()*/userName);
        mainFragmentHomePage = new MainFragmentHomePage();
        mainFragmentHomePage.setArguments(bundle);
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.main_content, mainFragmentHomePage);
        ft.commit();
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            FragmentManager fm = getSupportFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();

            switch (item.getItemId()) {
                case R.id.navigation_calendar:
                    if (mainFragmentHomePage == null) {
                        mainFragmentHomePage = new MainFragmentHomePage();
                    }
                    bundle.putString(USER_NAME, userName);

                    mainFragmentHomePage.setArguments(bundle);

                    ft.replace(R.id.main_content, mainFragmentHomePage);
                    ft.commit();
                    return true;
                case R.id.navigation_school:
                    if (mainFragmentSchoolNew == null) {
                        mainFragmentSchoolNew = new MainFragmentSchoolNew();
                    }
//                    if (user != null) {
//                        userName = user.getUserName();
//                        bundle.putString(USER_NAME, user.getUserName());
//                    }
                    bundle.putString(USER_NAME, userName);

                    mainFragmentSchoolNew.setArguments(bundle);

                    ft.replace(R.id.main_content, mainFragmentSchoolNew);
                    ft.commit();
                    return true;
                case R.id.navigation_community:
                    if (mainFragmentCommunity == null) {
                        mainFragmentCommunity = new MainFragmentCommunity();
                    }
//                    if (user != null) {
//                        userName = user.getUserName();
//                        bundle.putString(USER_NAME, user.getUserName());
//                    }
                    bundle.putString(USER_NAME, userName);

                    mainFragmentCommunity.setArguments(bundle);
                    ft.replace(R.id.main_content, mainFragmentCommunity);
                    ft.commit();
                    return true;
                case R.id.navigation_person:
                    if (mainFragmentPerson == null) {
                        mainFragmentPerson = new MainFragmentPerson();
                    }
//                    if (user != null) {
//                        userName = user.getUserName();
//                        bundle.putString(USER_NAME, user.getUserName());
//                    }
                    bundle.putString(USER_NAME, userName);

                    mainFragmentPerson.setArguments(bundle);
                    ft.replace(R.id.main_content, mainFragmentPerson);
                    ft.commit();
                    return true;
            }
            return false;
        }
    };


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }
}
