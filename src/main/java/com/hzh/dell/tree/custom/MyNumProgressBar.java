package com.hzh.dell.tree.custom;

import android.content.Context;

import com.daimajia.numberprogressbar.NumberProgressBar;

/**
 * create by hzh
 * on 2019/5/16
 */
public class MyNumProgressBar extends NumberProgressBar {
    public MyNumProgressBar(Context context) {
        super(context);
    }
}
