package com.hzh.dell.tree.listener;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;

public class MyTabListener implements android.support.v7.app.ActionBar.TabListener {
    private final Activity activity;//用于指定要加载Fragment的Activity
    private final Class aClass;//用于指定要加载的Fragment所对应的类
    private Fragment fragment;//定义Fragment对象

    public MyTabListener(Activity activity, Class aClass) {
        this.activity = activity;
        this.aClass = aClass;
    }

    /*** 被选择时 ***/
    @Override
    public void onTabSelected(ActionBar.Tab tab, android.support.v4.app.FragmentTransaction fragmentTransaction) {
        //判断fragment是否被初始化
        if (fragment == null) {
            //进行初始化，
            fragment = Fragment.instantiate(activity, aClass.getName());
            fragmentTransaction.add(android.R.id.content, fragment, null);
        }
        fragmentTransaction.attach(fragment);//显示新页面
    }

    /*** 退出时 ***/
    @Override
    public void onTabUnselected(ActionBar.Tab tab, android.support.v4.app.FragmentTransaction fragmentTransaction) {
        if (fragment != null) {
            fragmentTransaction.detach(fragment);//删除旧页面
        }
    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, android.support.v4.app.FragmentTransaction fragmentTransaction) {

    }
}
