package com.hzh.dell.tree.complete_info;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.hzh.dell.tree.R;

public class SelectMajorActivity extends AppCompatActivity implements View.OnClickListener {
    private ImageView ivReturn;//返回按钮
    private Button btn_xueshuo, btn_zhuanshuo;
    private Fragment fragment = null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.select_major);

        init();
    }

    private void init() {
        btn_xueshuo = findViewById(R.id.btn_xueshuo);
        btn_zhuanshuo = findViewById(R.id.btn_zhuanshuo);
        ivReturn = findViewById(R.id.iv_return);

        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        fragment = new XueShuoFragment();//初始化时显示学硕界面
        ft.replace(R.id.fragment, fragment);
        ft.commit();//提交事务

        ivReturn.setOnClickListener(this);
        btn_xueshuo.setOnClickListener(this);
        btn_zhuanshuo.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();

        switch (v.getId()) {
            case R.id.iv_return:
                onBackPressed();
                break;
            case R.id.btn_xueshuo:
                fragment = new XueShuoFragment();//学硕布局
                break;
            case R.id.btn_zhuanshuo:
                fragment = new ZhuanShuoFragment();//专硕布局
                break;
            default:
                fragment = new XueShuoFragment();//学硕布局
                break;
        }
        ft.replace(R.id.fragment, fragment);
        ft.commit();//提交事务
    }
}
