package com.hzh.dell.tree.complete_info;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.hzh.dell.tree.R;

/**
 * 完善信息类
 */
public class CompleteinfoActivity extends AppCompatActivity implements View.OnClickListener {
    private TextView select_year, select_major, select_school; //考研年份，专业，学校
    private ListView lv_select_year;//选择年份列表

    //获得返回的数据

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 0x123:
                Bundle getData = data.getExtras();
                String majorName = getData.getString("majorName");
                Log.d("majorName", majorName);
                select_major.setText(majorName);
                break;

        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_completeinfo);
//        StatusBarUtil.setTranslucent(this, 50);//半透明

        init();
//        Intent intent=getIntent();
//        String majorName=intent.getStringExtra("majorName");
//        Log.d("majorName",majorName);
//        select_major.setText(majorName);

    }

    //初始化
    private void init() {
        select_year = findViewById(R.id.select_year);
        select_major = findViewById(R.id.select_major);
        select_school = findViewById(R.id.select_school);
        lv_select_year = findViewById(R.id.lv_select_year);

        select_year.setOnClickListener(this);
        select_major.setOnClickListener(this);
        select_school.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.select_year://选择年份

                selectYear();
                break;
            case R.id.select_major://选择专业
                selectMajor();
                break;
            case R.id.select_school://选择学校
                selectSchool();
                break;
        }
    }

    //选择学校
    private void selectSchool() {

    }

    //选择专业
    private void selectMajor() {
        Intent intent = new Intent(this, SelectMajorActivity.class);
        startActivity(intent);
    }

    //选择年份
    private void selectYear() {
        final String[] data = new String[]{
                "2014", "2015", "2016", "2017", "2018", "2019", "2020", "2021", "2022", "2023"
        };
        AlertDialog.Builder select_year_list = new AlertDialog.Builder(this);
        select_year_list.setItems(data, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                select_year.setText(data[which]);
            }
        });
        select_year_list.show();
    }

}
