package com.hzh.dell.tree.complete_info;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.hzh.dell.tree.adapter.ListViewAdapter;
import com.hzh.dell.tree.R;


public class XueShuoFragment extends Fragment {
    private ListView lv_xueshuo;
    private TextView tv_select_major;
    //    private List<String> dataList=new ArrayList<>();
    private String[] dataList;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_xueshuo, container, false);
        lv_xueshuo = view.findViewById(R.id.lv_xueshuo);
        Resources resources = getResources();
        dataList = resources.getStringArray(R.array.xueshuo);
        lv_xueshuo.setAdapter(new ListViewAdapter(getActivity(), dataList));
        lv_xueshuo.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(view.getContext(), dataList[position], Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(getContext(), CompleteinfoActivity.class);
                intent.putExtra("majorName", dataList[position]);
                startActivityForResult(intent, 0x123);
            }
        });
        return view;
    }
}