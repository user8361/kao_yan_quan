package com.hzh.dell.tree.banner;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.hzh.dell.tree.R;

/**
 * create by hzh
 * on 2019/4/22
 */
public class WriteToFuture extends AppCompatActivity implements View.OnClickListener {

    private ImageView ivReturn, ivLetterImg, ivSendFuture;
    private TextView tvTitle, tvSeeOthers;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.banner_write_to_future);
        initView();
    }

    private void initView() {
        ivReturn = findViewById(R.id.iv_return);
        ivLetterImg = findViewById(R.id.iv_letter_img);
        ivSendFuture = findViewById(R.id.iv_send_future);

        tvTitle = findViewById(R.id.tv_title);
        tvSeeOthers = findViewById(R.id.tv_see_others);


        ivReturn.setOnClickListener(this);
        ivLetterImg.setOnClickListener(this);
        ivSendFuture.setOnClickListener(this);

        tvSeeOthers.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_return:
                onBackPressed();
                break;
            case R.id.iv_letter_img:

                break;
            case R.id.iv_send_future:

                break;
            case R.id.tv_see_others:

                break;

        }

    }
}
