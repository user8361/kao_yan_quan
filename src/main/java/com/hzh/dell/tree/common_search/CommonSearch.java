package com.hzh.dell.tree.common_search;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.hzh.dell.Bmob.entity.ErrTi;
import com.hzh.dell.Bmob.entity.User;
import com.hzh.dell.tree.R;
import com.hzh.dell.tree.adapter.recyclerview_adapter.adapter.CommonAdapter;
import com.hzh.dell.tree.adapter.recyclerview_adapter.adapter.MultiItemTypeAdapter;
import com.hzh.dell.tree.adapter.recyclerview_adapter.adapter.base.ViewHolder;
import com.hzh.dell.tree.database.MyGreenDaoApplication;
import com.hzh.dell.tree.entity.Search;
import com.hzh.greendao.gen.SearchDao;

import java.util.List;

import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.FindListener;


public class CommonSearch extends AppCompatActivity implements View.OnClickListener {
    private ImageView ivReturn, ivClear, ivSearch;
    private EditText etSearchText;
    private TextView tvClearHistory, tvRecord;
    private RecyclerView recyclerView;

    private SearchDao searchDao;
    private List<Search> searchList;
    private CommonAdapter<Search> adapter;
    private User user;
    private final static String USER_NAME = "userName";
    private String userName;


    private final static String SEARCH_TYPE = "searchType";
    private String searchType = "";


    public CommonSearch() {
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.common_search);
        searchType = getIntent().getStringExtra(SEARCH_TYPE);
        userName = getIntent().getStringExtra(USER_NAME);

        initView();
        initData();

    }

    private void initSearch() {
        if (userName.length() > 0) {
            Log.e("list", userName);
            if (searchType.equals("ErrTi")) {
                searchErrTiWithUser();
            } else if (searchType.equals("User")) {
                searchUserWithUser();
            } else if (searchType.equals("Article")) {
                searchArticleWithUser();
            } else if (searchType.equals("Word")) {
                searchWordWithUser();
            }
        }


    }

    private void searchWordWithUser() {

    }

    private void searchArticleWithUser() {
    }

    private void searchUserWithUser() {

    }

    private void searchErrTiWithUser() {
        BmobQuery<User> userBmobQuery = new BmobQuery<>();
        userBmobQuery.addWhereEqualTo(USER_NAME, userName);
        userBmobQuery.findObjects(new FindListener<User>() {
            @Override
            public void done(List<User> list, BmobException e) {
                if (list != null) {
                    user = list.get(0);
                    if (user != null) {
                        BmobQuery<ErrTi> errTiBmobQuery = new BmobQuery<>();
                        errTiBmobQuery.addWhereEqualTo("userId", user.getObjectId());
                        errTiBmobQuery.addWhereEqualTo("errTiTitle", etSearchText.getText());
                        errTiBmobQuery.findObjects(new FindListener<ErrTi>() {
                            @Override
                            public void done(List<ErrTi> list, BmobException e) {
                                if (list != null) {
                                    if (list.size() > 0) {
                                        CommonAdapter<ErrTi> adapterErrTi = new CommonAdapter<ErrTi>(getApplicationContext(), R.layout.search_item, list) {
                                            @Override
                                            protected void convert(ViewHolder holder, ErrTi errTi, int position) {
                                                holder.setText(R.id.tv_search_item, errTi.getErrTiTitle() + " -- " + errTi.getErrTiKey() + " -- " + errTi.getCreatedAt().substring(0, 10));
                                            }
                                        };
                                        recyclerView.setAdapter(adapterErrTi);
                                        Log.e("list", "list-->!null");
                                        tvClearHistory.setVisibility(View.GONE);
                                        tvRecord.setVisibility(View.GONE);


                                    } else {
                                        Log.e("list", "list.size ==0");
                                    }
                                } else {
                                    Log.e("list", "list-->null");
                                }
                            }
                        });

                    } else {
                        Log.e("list", "search user null");

                    }
                }
            }
        });

    }

    private void initView() {
        ivReturn = findViewById(R.id.iv_return);
        ivClear = findViewById(R.id.iv_clear);
        ivSearch = findViewById(R.id.iv_search);
        etSearchText = findViewById(R.id.et_search_text);
        tvClearHistory = findViewById(R.id.tv_clear_history);
        tvRecord = findViewById(R.id.tv_search_record_text);
        recyclerView = findViewById(R.id.recyclerView);
        ivReturn.setOnClickListener(this);
        ivSearch.setOnClickListener(this);
        ivClear.setOnClickListener(this);
        tvClearHistory.setOnClickListener(this);
        searchDao = MyGreenDaoApplication.getInstance().getDaoSession().getSearchDao();
        getList();
        //监听EditText文本变化
        etOnChangeListener();
    }

    //监听EditText文本变化
    private void etOnChangeListener() {

        TextWatcher textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                ivClear.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                ivClear.setVisibility(View.VISIBLE);
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (etSearchText.getText().length() != 0) {
                    ivClear.setVisibility(View.VISIBLE);
                } else {
                    ivClear.setVisibility(View.INVISIBLE);
                }
            }
        };
        etSearchText.addTextChangedListener(textWatcher);
    }

    private void getList() {
        searchList = searchDao.queryBuilder().orderDesc(SearchDao.Properties.Id).list();
    }

    private void initData() {
        if (searchList == null) {
            tvClearHistory.setVisibility(View.GONE);
        } else {
            adapter = new CommonAdapter<Search>(getApplicationContext(), R.layout.search_item, searchList) {
                @Override
                protected void convert(ViewHolder holder, Search search, int position) {
                    holder.setText(R.id.tv_search_item, search.getSearchTitle());
                }
            };
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            recyclerView.setAdapter(adapter);
            adapter.setOnItemClickListener(new MultiItemTypeAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(View view, RecyclerView.ViewHolder holder, int position) {
                    Toast.makeText(CommonSearch.this, position + "", Toast.LENGTH_SHORT).show();
                }

                @Override
                public boolean onItemLongClick(View view, RecyclerView.ViewHolder holder, int position) {
                    removeData(position);
                    return false;
                }
            });
        }
    }

    //点击事件
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_return:
                onBackPressed();
                break;
            case R.id.iv_search:
                insertData();
                break;
            case R.id.iv_clear:
                etSearchText.setText(null);
                ivClear.setVisibility(View.INVISIBLE);
                break;
            case R.id.tv_clear_history:
                searchDao.deleteAll();
                searchList.clear();
                adapter.notifyDataSetChanged();//清除所有
        }
    }

    //添加一条搜索记录
    private void insertData() {
        //添加搜索一条记录
        if (!TextUtils.isEmpty(etSearchText.getText())) {
            Search search = new Search();
            search.setSearchTitle(etSearchText.getText().toString());
            searchDao.insert(search);

            initSearch();
//            searchList.add(0, search);
//            adapter.notifyItemInserted(0);
//            adapter.notifyItemRangeChanged(0, adapter.getItemCount());
        }
    }

    //删除一条搜索记录
    private void removeData(int position) {
        Search search = searchDao.queryBuilder().where(SearchDao.Properties.Id.eq(searchList.get(position).getId())).unique();
        searchDao.delete(search);
        searchList.remove(position);
        adapter.notifyItemRemoved(position);
        adapter.notifyItemRangeChanged(position, adapter.getItemCount());
    }
}
