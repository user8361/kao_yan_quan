package com.hzh.dell.tree.homepage_fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.hzh.dell.tree.R;
import com.hzh.dell.tree.adapter.listview_adapter.CommonAdapter;
import com.hzh.dell.tree.bean.ListItemBean;
import com.hzh.dell.tree.adapter.listview_adapter.base.ViewHolder;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.footer.ClassicsFooter;
import com.scwang.smartrefresh.layout.header.ClassicsHeader;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.util.ArrayList;
import java.util.List;

public class CommonFragment extends Fragment {
    private SmartRefreshLayout smartRefreshLayout;
    private ListView mListView;
    private List<ListItemBean> mDatas;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.homepage_fragment_common_content_list, container, false);
        mListView = view.findViewById(R.id.homepage_common_listview);
        mDatas = new ArrayList<ListItemBean>();
        for (int i = 0; i < 10; i++) {
            ListItemBean bean = new ListItemBean();
            bean.setTitle("CommonPage_" + i);
            bean.setTabType("大学动态_" + i);
            bean.setSubmitTime(i + "小时前");
            bean.setImgId(R.drawable.example);
            mDatas.add(bean);
        }
        CommonAdapter<ListItemBean> mAdapter = new CommonAdapter<ListItemBean>(view.getContext(), mDatas, R.layout.homepage_fragment_content_list_items) {

            @Override
            public void convert(ViewHolder helper, ListItemBean item) {
                helper.setText(R.id.list_item_title, item.getTitle());
                helper.setText(R.id.list_item_tabtype, item.getTabType());
                helper.setText(R.id.list_item_time, item.getSubmitTime());
                helper.setImageResource(R.id.list_item_img, item.getImgId());
            }
        };
        mListView.setAdapter(mAdapter);
        /**
         * 刷新
         */
        smartRefreshLayout = view.findViewById(R.id.homepage_smartRefresh);
        smartRefreshLayout.setRefreshHeader(new ClassicsHeader(getContext()));
        smartRefreshLayout.setRefreshFooter(new ClassicsFooter(getContext()));
        smartRefreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshlayout) {
                refreshlayout.finishRefresh(2000);
            }
        });

        return view;

    }
}
