package com.hzh.dell.tree.homepage_fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.hzh.dell.Bmob.entity.Plan;
import com.hzh.dell.Bmob.entity.User;
import com.hzh.dell.tree.R;
import com.hzh.dell.tree.adapter.ListViewPlanAdapter;
import com.hzh.dell.tree.utils.DateUtil;
import com.mcsoft.timerangepickerdialog.RangeTimePickerDialog;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;

import java.util.ArrayList;
import java.util.List;

import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.FindListener;
import cn.bmob.v3.listener.SaveListener;


/**
 * create by hzh
 * on 2019/4/28
 */
public class EverydayPlan extends AppCompatActivity implements View.OnClickListener, RangeTimePickerDialog.ISelectedTime, ListViewPlanAdapter.InnerItemOnClickListener {
    private ImageView ivReturn, ivSelectTime;
    private TextView tvTitle, tvSave, tvReturnToday, tvSubmit;
    private MaterialCalendarView materialCalendarView;
    private FloatingActionButton fabButton;
    private LinearLayout bottomNavLayout, bottomWritePlanLayout;
    private EditText etWritePlan;
    private ListView mListView;
    private boolean flag = false;
    private String startTime, endTime;
    private User user;


    private final static String USER_NAME = "userName";
    private final static String USER_ID = "userId";
    private final static String CREATE_TIME = "createTime";
    private final static String CREATE_AT = "createdAt";
    private String userName = "";
    private String today = "";
    private List<Plan> planList;

    private ListViewPlanAdapter adapter;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.homepage_everyday_plan);
        initView();
    }

    private void initView() {
        ivReturn = findViewById(R.id.iv_return);
        ivSelectTime = findViewById(R.id.iv_select_time);
        tvTitle = findViewById(R.id.tv_title);
        tvSave = findViewById(R.id.tv_function);
        tvReturnToday = findViewById(R.id.tv_return_today);
        tvSubmit = findViewById(R.id.tv_submit);

        etWritePlan = findViewById(R.id.et_write_plan);

        materialCalendarView = findViewById(R.id.material_calendar);
        fabButton = findViewById(R.id.fab_btn);

        bottomNavLayout = findViewById(R.id.bottom_nav_layout);
        bottomWritePlanLayout = findViewById(R.id.bottom_write_plan_layout);
        mListView = findViewById(R.id.listView);
        View emptyView = findViewById(R.id.empty_view_content);
        mListView.setEmptyView(emptyView);


        planList = new ArrayList<>();
        Intent intent = getIntent();
        userName = intent.getStringExtra(USER_NAME);
        BmobQuery<User> userBmobQuery = new BmobQuery<>();
        userBmobQuery.addWhereEqualTo(USER_NAME, userName);
        userBmobQuery.findObjects(new FindListener<User>() {
            @Override
            public void done(List<User> list, BmobException e) {
                if (list != null) {
                    user = list.get(0);
                    if (user != null) {
                        initData();
                    }
                }
            }
        });

    }

    private void initData() {
        tvTitle.setText("每日计划");
        tvSave.setText("保存");
        materialCalendarView.setTopbarVisible(false);
        today = DateUtil.getFormattedDate(System.currentTimeMillis());

        BmobQuery<Plan> planBmobQuery = new BmobQuery<>();
        planBmobQuery.addWhereEqualTo(USER_ID, user.getObjectId());
        planBmobQuery.addWhereEqualTo(CREATE_TIME, today);
        planBmobQuery.order(CREATE_AT);
        planBmobQuery.findObjects(new FindListener<Plan>() {
            @Override
            public void done(List<Plan> list, BmobException e) {
                if (list != null) {
                    planList.clear();
                    planList.addAll(list);
                    adapter = new ListViewPlanAdapter(getLayoutInflater(), getApplicationContext(), planList);
                    mListView.setAdapter(adapter);
                    setAdapterOnInnerClick();
                    Animation animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_in_left);
                    LayoutAnimationController controller = new LayoutAnimationController(animation);
                    controller.setDelay(0.5f);
                    controller.setOrder(LayoutAnimationController.ORDER_NORMAL);
                    mListView.setAnimation(animation);
                }
            }
        });

        final String[] time = today.split("-");
        final CalendarDay day = CalendarDay.from(Integer.parseInt(time[0]), Integer.parseInt(time[1]), Integer.parseInt(time[2]));
        materialCalendarView.setSelectedDate(day);
        materialCalendarView.setOnDateChangedListener(new OnDateSelectedListener() {
            @Override
            public void onDateSelected(@NonNull MaterialCalendarView widget, @NonNull final CalendarDay date, boolean selected) {
                if (date.getDate().toString().equals(today)) {
                    tvSave.setVisibility(View.VISIBLE);
                    tvReturnToday.setVisibility(View.GONE);
                    fabButton.show();
                    fabButton.setClickable(true);
                } else {
                    fabButton.hide();
                    tvSave.setVisibility(View.GONE);
                    tvReturnToday.setVisibility(View.VISIBLE);
                    tvReturnToday.setText("回到\n今天");
                    tvReturnToday.setTextSize(12);
                    tvReturnToday.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            materialCalendarView.setSelectedDate(day);
                            materialCalendarView.getCurrentDate();

                            tvReturnToday.setVisibility(View.GONE);
                            tvSave.setVisibility(View.VISIBLE);
                            fabButton.show();
                            changeData(day);//显示数据变化
                        }
                    });
                }
                changeData(date);
            }
        });


        ivReturn.setOnClickListener(this);
        ivSelectTime.setOnClickListener(this);
        tvSave.setOnClickListener(this);
        tvSubmit.setOnClickListener(this);
        tvReturnToday.setOnClickListener(this);
        fabButton.setOnClickListener(this);
    }

    private void setAdapterOnInnerClick() {
        adapter.setOnInnerItemOnClickListener(this);
    }

    //改变ListView中显示的数据
    private void changeData(CalendarDay date) {
        planList.clear();//清空列表数据，等待查找新的数据
        adapter = new ListViewPlanAdapter(getLayoutInflater(), getApplicationContext(), planList);
        mListView.setAdapter(adapter);
        setAdapterOnInnerClick();//设置内部点击事件
        //开始查询新数据
        BmobQuery<Plan> planBmobQuery = new BmobQuery<>();
        //查询条件，用户id为当前用户的id
        planBmobQuery.addWhereEqualTo(USER_ID, user.getObjectId());
        //查询条件，查询日期为选中日期
        planBmobQuery.addWhereEqualTo(CREATE_TIME, date.getDate());
        planBmobQuery.order(CREATE_AT);//按照计划添加的时间来进行排序
        planBmobQuery.findObjects(new FindListener<Plan>() {
            @Override
            public void done(List<Plan> list, BmobException e) {
                //查找到的数据不为空，则表示当天制定了学习计划
                if (list != null) {
                    //将计划添加到列表中
                    planList.addAll(list);
                }
                //通知适配器数据已经更新
                adapter.notifyDataSetChanged();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_return:
                onBackPressed();
                break;
            case R.id.tv_function:
                final BmobQuery<Plan> planBmobQuery = new BmobQuery<>();
                planBmobQuery.addWhereEqualTo("userId", user.getObjectId());
                planBmobQuery.addWhereEqualTo("createTime", today);
                planBmobQuery.findObjects(new FindListener<Plan>() {
                    @Override
                    public void done(List<Plan> list, BmobException e) {
                        if (list.size() > 0) {
                            toast(list.size() + "");
                            if (planList.size() > 0) {
                                for (int i = 0; i < planList.size(); i++) {
                                    for (int j = 0; j < list.size(); j++) {
                                        if (list.get(j).getPlanContent().equals(planList.get(i))) {
                                            Log.e("list", " -->" + list.get(j).getPlanContent());
                                        } else {
                                            planList.get(i).save(new SaveListener<String>() {
                                                @Override
                                                public void done(String s, BmobException e) {
                                                    if (e == null) {
                                                        Log.e("save", "保存成功");
                                                    } else {
                                                        Log.e("save", "保存失败" + e.toString());

                                                    }
                                                }
                                            });
                                        }
                                    }

                                }

                            }
                        } else {
                            for (Plan plan : planList) {
                                plan.save(new SaveListener<String>() {
                                    @Override
                                    public void done(String s, BmobException e) {
                                        if (e == null) {
                                            Log.e("save", "done: 保存成功");
                                        } else {
                                            Log.e("save", "done: 保存失败");

                                        }
                                    }
                                });
                            }
                            Toast.makeText(getApplicationContext(), "保存成功！", Toast.LENGTH_SHORT).show();
                        }

                    }
                });


                onBackPressed();
                break;
            case R.id.fab_btn:
                if (flag == false) {
                    flag = true;
                    bottomWritePlanLayout.setVisibility(View.VISIBLE);
                    bottomNavLayout.setVisibility(View.GONE);

                } else {
                    flag = false;
                    bottomNavLayout.setVisibility(View.VISIBLE);
                    bottomWritePlanLayout.setVisibility(View.GONE);
                }
                break;
            case R.id.iv_select_time:
                selectTimeDialog();
                break;
            case R.id.tv_submit:
                addPlan();
                break;
        }

    }

    private void addPlan() {
        String text = etWritePlan.getText().toString().trim();
        if ("".equals(text) || text == null) {
            toast("计划不能为空！");
        } else {
            BmobQuery<Plan> planBmobQuery = new BmobQuery<>();
            planBmobQuery.addWhereEqualTo("planContent", text);
            planBmobQuery.addWhereEqualTo("createTime", today);
            planBmobQuery.findObjects(new FindListener<Plan>() {
                @Override
                public void done(List<Plan> list, BmobException e) {
                    if (list.size() > 0) {
                        Log.e("listSize", list.size() + " -- ");
                        toast("计划不能重复，您可以点击计划进行修改。");
                    } else {
                        Plan plan = new Plan();
                        plan.setCompleteState(0);
                        plan.setCreateTime(DateUtil.getFormattedDate(System.currentTimeMillis()));
                        plan.setPlanContent(etWritePlan.getText().toString());
                        plan.setCompleteState(0);//0表示未完成
                        if (startTime != null && endTime != null) {
                            plan.setTimeDuration(startTime + " -- " + endTime);
                        } else {
                            plan.setTimeDuration("");
                        }
                        plan.setUserId(user.getObjectId());

                        if (planList.size() == 0) {
                            planList.add(plan);
                            adapter = new ListViewPlanAdapter(getLayoutInflater(), getApplicationContext(), planList);
                            mListView.setAdapter(adapter);
                        } else {
                            planList.add(plan);
                            adapter.notifyDataSetChanged();
                        }
                        setAdapterOnInnerClick();//监听listView内部Item点击事件
                    }
                }
            });
        }


    }

    private void toast(String s) {
        Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
    }

    //选择时间
    private void selectTimeDialog() {
        RangeTimePickerDialog dialog = new RangeTimePickerDialog();
        dialog.newInstance();
        dialog.setRadiusDialog(5);
        dialog.setTextTabStart("开始时间");
        dialog.setTextTabEnd("结束时间");
        dialog.setTextBtnPositive("确定");
        dialog.setTextBtnNegative("取消");
        dialog.setMessageErrorRangeTime("结束时间应大于开始时间");
        dialog.setIs24HourView(true);
        dialog.setColorBackgroundHeader(R.color.colorPrimaryDark);
        dialog.setColorTextButton(R.color.colorPrimaryDark);
        android.app.FragmentManager fragmentManager = getFragmentManager();
        dialog.show(fragmentManager, "");
    }

    @Override
    public void onSelectedTime(int hourStart, int minuteStart, int hourEnd, int minuteEnd) {

        if (minuteStart < 10) {
            startTime = hourStart + ":" + "0" + minuteStart;
        } else {
            startTime = hourStart + ":" + minuteStart;
        }
        if (minuteEnd < 10) {
            endTime = hourEnd + ":" + "0" + minuteEnd;
        } else {
            endTime = hourEnd + ":" + minuteEnd;
        }
    }

    @Override
    public void itemClick(View v) {
        int position = (int) v.getTag();
        switch (v.getId()) {
            case R.id.iv_set_colock:
                toast(planList.get(position).getPlanContent());
                Intent setColock = new Intent(this, EverydayPlanItemsTimer.class);
                setColock.putExtra("title", planList.get(position).getPlanContent());
                startActivity(setColock);
                break;
            case R.id.tv_plan_content:
                TextView tvPlan = (TextView) v;

                break;
        }
    }
}
