package com.hzh.dell.tree.homepage_fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.hzh.dell.tree.R;

public class ToolBarMenu extends AppCompatActivity {
    private ImageView ivReturn;//返回按钮
    private TextView tvSave;//保存按钮
    private Toolbar toolbar;//toolbar
    private TabLayout tabLayout;//tablayout

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.homepage_fragment_toolbar_menu);
        initView();
        initData();
    }

    private void initData() {
        ivReturn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "返回", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void initView() {
        ivReturn = findViewById(R.id.iv_return);
        tvSave = findViewById(R.id.tv_save);
        toolbar = findViewById(R.id.toolBar);
        tabLayout = findViewById(R.id.tabLayout);
    }


}
