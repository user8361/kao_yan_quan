package com.hzh.dell.tree.homepage_fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import com.hzh.dell.tree.R;
import com.hzh.dell.tree.common_search.CommonSearch;
import com.hzh.dell.tree.database.MyGreenDaoApplication;
import com.hzh.greendao.gen.SchoolDao;

/**
 * create by hzh
 * on 2019/4/27
 */
public class MajorDetail extends AppCompatActivity implements View.OnClickListener {
    private SchoolDao schoolDao;
    private ImageView ivReturn, ivSearch;
    private TextView tvTitle;
    private WebView webView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.homepage_search_school);
        com.jaeger.library.StatusBarUtil.setTranslucent(this);

        initView();
        initData();

    }

    private void initView() {
        schoolDao = MyGreenDaoApplication.getInstance().getDaoSession().getSchoolDao();

        ivReturn = findViewById(R.id.iv_return);
        ivSearch = findViewById(R.id.iv_search);
        tvTitle = findViewById(R.id.tv_title);

        webView = findViewById(R.id.web_view);
        WebSettings webSettings = webView.getSettings();
        //如果访问的页面中要与Javascript交互，则webview必须设置支持Javascript
        webSettings.setJavaScriptEnabled(true);
// 若加载的 html 里有JS 在执行动画等操作，会造成资源浪费（CPU、电量）
// 在 onStop 和 onResume 里分别把 setJavaScriptEnabled() 给设置成 false 和 true 即可

//设置自适应屏幕，两者合用
        webSettings.setUseWideViewPort(true); //将图片调整到适合webview的大小
        webSettings.setLoadWithOverviewMode(true); // 缩放至屏幕的大小

//缩放操作
        webSettings.setSupportZoom(true); //支持缩放，默认为true。是下面那个的前提。
        webSettings.setBuiltInZoomControls(true); //设置内置的缩放控件。若为false，则该WebView不可缩放
        webSettings.setDisplayZoomControls(false); //隐藏原生的缩放控件

//其他细节操作
        webSettings.setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK); //关闭webview中缓存
        webSettings.setAllowFileAccess(true); //设置可以访问文件
        webSettings.setJavaScriptCanOpenWindowsAutomatically(true); //支持通过JS打开新窗口
        webSettings.setLoadsImagesAutomatically(true); //支持自动加载图片
        webSettings.setDefaultTextEncodingName("utf-8");//设置编码格式

        webView.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onReceivedTitle(WebView view, String title) {
                super.onReceivedTitle(view, title);

            }
        });
        ivReturn.setOnClickListener(this);
        ivSearch.setOnClickListener(this);
        tvTitle.setText("专业库");
    }

    private void initData() {
//        List<School> schoolList = new ArrayList<>();
//        List<School> spinnerlist = new ArrayList<>();
//        schoolList = schoolDao.queryBuilder().orderAsc().list();
        String major = "http://college.gaokao.com/touch/spesearch/";
        webView.loadUrl(major);
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

            @Override
            public void onPageFinished(WebView view, String url) {

                super.onPageFinished(view, url);
                //删除footer
                webView.loadUrl("javascript:function setTop()" +
                        "{" +
                        "var paras = document.getElementsByClassName('footer');" +
                        " for(i=0;i<paras.length;i++){\n" +
                        "    if (paras[i] != null)\n" +
                        "    paras[i].parentNode.removeChild(paras[i]);" +
                        "\n}"
                        + "}" +
                        "setTop();"
                );
            }
        });


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_return:
                onBackPressed();
                break;
            case R.id.iv_search:
                Intent searchSchool = new Intent(getApplicationContext(), CommonSearch.class);
                startActivity(searchSchool);
                break;
        }

    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK) && webView.canGoBack()) {
            webView.goBack();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }


}
