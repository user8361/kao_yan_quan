package com.hzh.dell.tree.homepage_fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.hzh.dell.dictionary.main.QueryWord;
import com.hzh.dell.tree.R;
import com.hzh.dell.tree.adapter.listview_adapter.CommonAdapter;
import com.hzh.dell.tree.adapter.listview_adapter.base.ViewHolder;
import com.hzh.dell.tree.banner.GlideImageLoader;
import com.hzh.dell.tree.banner.WriteToFuture;
import com.hzh.dell.tree.bean.ListItemBean;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.footer.ClassicsFooter;
import com.scwang.smartrefresh.layout.header.ClassicsHeader;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.youth.banner.Banner;
import com.youth.banner.listener.OnBannerListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.FindListener;

public class RecommendFragment extends Fragment implements View.OnClickListener {
    private SmartRefreshLayout smartRefreshLayout;
    private ListView mListView;
    private List<ListItemBean> mDatas;
    private Banner banner;
    private TextView tvSearchWord, tvSearchSchool, tvSearchMajor, tvEverydayPlan;
    private String USER_NAME = "userName";
    private String userName = "";
    private com.hzh.dell.Bmob.entity.User user;

    //    private UserDao userDao;
//    private User user;
    private Bundle bundle;

    private View view;
//    private SchoolDao schoolDao;
//    private List<School> schoolList;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.homepage_fragment_recommen_content_list, container, false);
        bundle = getArguments();
        userName = bundle.getString(USER_NAME);
        BmobQuery<com.hzh.dell.Bmob.entity.User> userBmobQuery = new BmobQuery<>();
        userBmobQuery.addWhereEqualTo(USER_NAME, userName);
        userBmobQuery.findObjects(new FindListener<com.hzh.dell.Bmob.entity.User>() {
            @Override
            public void done(List<com.hzh.dell.Bmob.entity.User> list, BmobException e) {
                if (list != null) {
                    user = list.get(0);
                    if (user != null) {
                        initData();
                        Log.e("USER_NAME", "done: --> " + user.getUserName());
                    } else {
                        Log.e("USER_NAME", "done: --> null ");
                    }
                } else {
                    Log.e("TAGTAGTAG", "null");
                }
            }
        });
//        userDao = MyGreenDaoApplication.getInstance().getDaoSession().getUserDao();
//        user = userDao.queryBuilder().where(UserDao.Properties.UserName.eq(userName)).unique();
//        schoolDao = MyGreenDaoApplication.getInstance().getDaoSession().getSchoolDao();
//        schoolList = new ArrayList<>();


        return view;
    }

    public void initData() {
        mListView = view.findViewById(R.id.homepage_listview);
        Integer[] imgs = {R.mipmap.icon_banner001, R.mipmap.icon_banner002, R.drawable.img03};
        View headerView = getLayoutInflater().inflate(R.layout.homepage_fragment_recommen_list_header, null);
        tvSearchWord = headerView.findViewById(R.id.tv_search_word);
        tvSearchWord.setOnClickListener(this);
        tvSearchSchool = headerView.findViewById(R.id.tv_search_school);
        tvSearchSchool.setOnClickListener(this);
        tvSearchMajor = headerView.findViewById(R.id.tv_search_major);
        tvSearchMajor.setOnClickListener(this);
        tvEverydayPlan = headerView.findViewById(R.id.tv_everyday_plan);
        tvEverydayPlan.setOnClickListener(this);
        mListView.addHeaderView(headerView);


        banner = headerView.findViewById(R.id.banner);
        banner.setFocusable(true);//获取焦点
        banner.setFocusableInTouchMode(true);//触摸是否能获取到焦点
        banner.requestFocus();//用于指定屏幕中的焦点View
        banner.setImageLoader(new GlideImageLoader());
        banner.setImages(Arrays.asList(imgs));
        banner.start();
        banner.setOnBannerListener(new OnBannerListener() {
            @Override
            public void OnBannerClick(int position) {
                switch (position) {
                    case 0:
                        Log.e("banner", "0");
                        break;
                    case 1:
                        Intent intent = new Intent(getContext(), WriteToFuture.class);
                        startActivity(intent);
                        break;
                }
            }
        });

        mDatas = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            ListItemBean bean = new ListItemBean();
            bean.setTitle("广州中医药大学2019年征兵信息");
            bean.setTabType("大学动态_" + i);
            bean.setSubmitTime(i + "小时前");
            bean.setImgId(R.drawable.example);
            mDatas.add(bean);
        }
        CommonAdapter<ListItemBean> mAdapter = new CommonAdapter<ListItemBean>(view.getContext(), mDatas, R.layout.homepage_fragment_content_list_items) {

            @Override
            public void convert(ViewHolder helper, ListItemBean item) {
                helper.setText(R.id.list_item_title, item.getTitle());
                helper.setText(R.id.list_item_tabtype, item.getTabType());
                helper.setText(R.id.list_item_time, item.getSubmitTime());
                helper.setImageResource(R.id.list_item_img, item.getImgId());
            }
        };
        mListView.setAdapter(mAdapter);


        /**
         * 刷新
         */
        smartRefreshLayout = view.findViewById(R.id.homepage_smartRefresh);
        smartRefreshLayout.setRefreshHeader(new ClassicsHeader(getContext()));
        smartRefreshLayout.setRefreshFooter(new ClassicsFooter(getContext()));
        smartRefreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshlayout) {
                refreshlayout.finishRefresh(2000);
            }
        });
    }

    private void toast(String msg) {
        Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_search_word:
                Intent searchWord = new Intent(getContext(), QueryWord.class);
                searchWord.putExtra(USER_NAME, user.getUserName());
                startActivity(searchWord);
                break;
            case R.id.tv_search_school:
//                addSchoolInfoToDB();
                Intent schoolDetailIntent = new Intent(getContext(), SchoolDetail.class);
                startActivity(schoolDetailIntent);
                break;
            case R.id.tv_search_major:
//                addSchoolInfoToDB();
                Intent majorDetailIntent = new Intent(getContext(), MajorDetail.class);
                startActivity(majorDetailIntent);
                break;
            case R.id.tv_everyday_plan:
                Intent planIntent = new Intent(getContext(), EverydayPlan.class);
                planIntent.putExtra(USER_NAME, user.getUserName());
                startActivity(planIntent);
                break;
        }
    }

    //子线程运行
//    public void addSchoolInfoToDB() {
//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                schoolList = schoolDao.queryBuilder().list();
//                if (schoolList.size() == 0) {
//                    try {
//                        InputStreamReader inputStreamReader = new InputStreamReader(getResources().getAssets().open("school.csv"));
//                        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
//                        bufferedReader.readLine();
//                        String line = "";
//                        while ((line = bufferedReader.readLine()) != null) {
//                            String buffer[] = line.split(",");
//                            School school = new School();
//                            school.setSchoolName(buffer[1]);
//                            school.setProvince(buffer[2]);
//                            school.setAddress(buffer[3]);
//                            school.setCity(buffer[4]);
//                            school.setIs211(buffer[5]);
//                            school.setIs985(buffer[6]);
//                            school.setPublicPrivate(buffer[7]);
//                            school.setSchoolCategory(buffer[8]);
//                            school.setDepartment(buffer[9]);
//                            school.setSchoolType(buffer[10]);
//                            schoolDao.insert(school);
//
//                        }
//                        bufferedReader.close();
//
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//                }
//            }
//        }).start();

//    }

}
