package com.hzh.dell.tree.homepage_fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Vibrator;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hzh.dell.tree.R;

/**
 * create by hzh
 * on 2019/5/7
 */
public class EverydayPlanItemsTimer extends AppCompatActivity implements View.OnClickListener {

    private ImageView ivStart, ivStop, ivPause, ivReturn;
    private TimeDown timeDown;
    private TextView tvHour, tvMinute, tvSecond, tvFifteenMinute, tvThirdyMinute, tvOneHour, tvCustom, tvTitle;
    private int hourNum, minuteNum, secondNum;//记录小时分钟秒
    private long millisInFuture;//记录秒自动变化

    private LinearLayout selectTimeLayout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.everyday_plan_countdown_timer);
        com.jaeger.library.StatusBarUtil.setTranslucent(this);
        initView();
    }


    private void initView() {
        tvTitle = findViewById(R.id.tv_title);
        tvTitle.setText(getIntent().getStringExtra("title"));

        hourNum = 0;
        minuteNum = 15;
        millisInFuture = 60000;
        secondNum = (int) millisInFuture / 1000;
        ivReturn = findViewById(R.id.iv_return);
        ivStart = findViewById(R.id.iv_start);
        ivPause = findViewById(R.id.iv_pause);
        ivStop = findViewById(R.id.iv_stop);
        tvSecond = findViewById(R.id.tvSecond);
        tvHour = findViewById(R.id.tvHour);
        tvMinute = findViewById(R.id.tvMinute);
        tvCustom = findViewById(R.id.tvCustom);

        tvFifteenMinute = findViewById(R.id.fifteenMinute);
        tvThirdyMinute = findViewById(R.id.thirtyMinute);
        tvOneHour = findViewById(R.id.oneHour);


        selectTimeLayout = findViewById(R.id.ll_select_time);
        timeDown = new TimeDown(millisInFuture, 1000);
        ivStart.setOnClickListener(this);
        ivPause.setOnClickListener(this);
        ivStop.setOnClickListener(this);
        ivReturn.setOnClickListener(this);

        tvFifteenMinute.setOnClickListener(this);
        tvThirdyMinute.setOnClickListener(this);
        tvOneHour.setOnClickListener(this);
        tvCustom.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_return:
                onBackPressed();
                break;
            case R.id.iv_start:
                selectTimeLayout.setVisibility(View.INVISIBLE);
                ivStart.setVisibility(View.GONE);
                ivPause.setVisibility(View.VISIBLE);
                ivStop.setVisibility(View.VISIBLE);
                timeDown = new TimeDown(millisInFuture, 1000);
                timeDown.start();
                break;
            case R.id.iv_pause:
                ivStart.setVisibility(View.VISIBLE);
                ivPause.setVisibility(View.GONE);
                ivStop.setVisibility(View.VISIBLE);
                timeDown.cancel();
                break;
            case R.id.iv_stop:
                timeDown.cancel();
                hourNum = 0;
                minuteNum = 15;
                secondNum = 0;
                millisInFuture = 60000;
                ivStart.setVisibility(View.VISIBLE);
                ivPause.setVisibility(View.GONE);
                ivStop.setVisibility(View.GONE);
                selectTimeLayout.setVisibility(View.VISIBLE);
                tvHour.setText("00");
                tvMinute.setText("15");
                tvSecond.setText("00");
                break;
            case R.id.fifteenMinute:
                hourNum = 0;
                minuteNum = 15;
                secondNum = 0;
                tvHour.setText("00");
                tvMinute.setText(minuteNum + "");
                tvSecond.setText(secondNum + "0");
                break;
            case R.id.thirtyMinute:
                hourNum = 0;
                minuteNum = 30;
                secondNum = 0;
                tvHour.setText("00");
                tvMinute.setText(minuteNum + "");
                tvSecond.setText(secondNum + "0");
                break;
            case R.id.oneHour:
                hourNum = 1;
                minuteNum = 0;
                secondNum = 0;
                tvHour.setText("01");
                tvMinute.setText("00");
                tvSecond.setText("00");
                break;
            case R.id.tvCustom:
                setCustomTime();
                break;
        }
    }


    class TimeDown extends CountDownTimer {

        /**
         * @param millisInFuture    The number of millis in the future from the call
         *                          to {@link #start()} until the countdown is done and {@link #onFinish()}
         *                          is called.
         * @param countDownInterval The interval along the way to receive
         *                          {@link #onTick(long)} callbacks.
         */
        public TimeDown(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onTick(long millisUntilFinished) {
            millisInFuture = millisUntilFinished;
            secondNum = (int) millisUntilFinished / 1000;
            int tmp = 0;
            if (minuteNum == 1) {
                tmp = 0;
                tvMinute.setText("00");
            } else {
                tmp = minuteNum;

            }
            if (minuteNum > 0) {
                if (minuteNum < 10) {
                    tvMinute.setText("0" + tmp);
                } else {
                    tvMinute.setText(tmp + "");
                }
            } else if (minuteNum == 0) {
                if (hourNum > 0) {
                    tvMinute.setText("59");
                    if (hourNum < 10) {
                        tvHour.setText("0" + (hourNum - 1));
                    } else {
                        tvHour.setText((hourNum - 1) + "");
                    }
                } else if (hourNum == 0) {
                    tvHour.setText("00");
                    tvMinute.setText("00");
                }
            }

            if (secondNum > 9) {
                tvSecond.setText(secondNum + "");
            } else if (secondNum >= 0) {
                tvSecond.setText("0" + secondNum);
            }

            Log.e("timeLeft", " ---> hourNum:" + hourNum + " minuteNum:" + minuteNum + " secondNum:" + secondNum);
        }

        @Override
        public void onFinish() {


            if (minuteNum == 1) {
                minuteNum = 0;
                if (hourNum > 0) {
                    minuteNum = 60;
                    hourNum--;//小时数减1
                    tvMinute.setText(minuteNum + "");
                    if (hourNum < 10) {
                        tvHour.setText("0" + hourNum);
                    } else {
                        tvHour.setText(hourNum + "");
                    }
                    timeDown.start();//开始计时
                } else if (hourNum == 0) {
                    minuteNum = 0;
                    tvHour.setText("00");
                    tvMinute.setText("00");
                    tvSecond.setText("00");
                    timeDown.cancel();

                    toast("任务完成！");
                    ivStop.setVisibility(View.GONE);
                    ivPause.setVisibility(View.GONE);
                    ivStart.setVisibility(View.VISIBLE);
                    selectTimeLayout.setVisibility(View.VISIBLE);
                    hourNum = 0;
                    minuteNum = 15;
                    secondNum = 0;
                    tvHour.setText("00");
                    tvMinute.setText("15");
                    tvSecond.setText("00");
                    startVibrator();
                }


            } else if (minuteNum > 1) {
                Log.e("timeLeft", minuteNum + "");
                minuteNum--;
                if (minuteNum <= 10) {
                    tvMinute.setText("0" + minuteNum);
                } else {
                    tvMinute.setText(minuteNum + "");
                }
                timeDown.start();
            } else if (minuteNum == 0) {
                if (hourNum > 0) {
                    minuteNum = 59;
                    hourNum--;//小时数减1
                    tvMinute.setText(minuteNum + "");
                    if (hourNum < 10) {
                        tvHour.setText("0" + hourNum);
                    } else {
                        tvHour.setText(hourNum + "");
                    }
                    timeDown.start();//开始计时
                } else if (hourNum == 0) {
                    minuteNum = 0;
                    tvHour.setText("00");
                    tvMinute.setText("00");
                    tvSecond.setText("00");
                    timeDown.cancel();

                    toast("任务完成！");
                    ivStop.setVisibility(View.GONE);
                    ivPause.setVisibility(View.GONE);
                    ivStart.setVisibility(View.VISIBLE);
                    selectTimeLayout.setVisibility(View.VISIBLE);
                    hourNum = 0;
                    minuteNum = 15;
                    secondNum = 0;
                    tvHour.setText("00");
                    tvMinute.setText("15");
                    tvSecond.setText("00");
                    startVibrator();
                }
            }


//            if (minuteNum>0){
////                minuteNum--;
//                timeDown.start();
//            }else if (minuteNum==0&&hourNum>0){
//                minuteNum=59;
//                hourNum--;
//                timeDown.start();
//            }else

//            if (hourNum == 0 && minuteNum == 0 && secondNum == 0) {
//                timeDown.cancel();
//                toast("任务完成！");
//                ivStop.setVisibility(View.GONE);
//                ivPause.setVisibility(View.GONE);
//                ivStart.setVisibility(View.VISIBLE);
//                hourNum = 0;
//                minuteNum = 15;
//                secondNum = 0;
//                tvHour.setText("00");
//                tvMinute.setText("15");
//                tvSecond.setText("00");
//                startVibrator();
//            }
        }
    }

    private void toast(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    private void startVibrator() {
        /**
         * 想设置震动大小可以通过改变pattern来设定，如果开启时间太短，震动效果可能感觉不到
         *
         */
        Vibrator vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        long[] pattern = {500, 2000, 500, 2000}; // 停止 开启 停止 开启
        vibrator.vibrate(pattern, 0);//默认为-1重复一次，0表示两次
    }

    private void setCustomTime() {
        final View view = getLayoutInflater().inflate(R.layout.dialog_set_custom_time, null);
        final EditText etHour = view.findViewById(R.id.et_hour);
        final EditText etMinute = view.findViewById(R.id.et_minute);
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("请输入学习的时长").setView(view)
                .setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (!"".equals(etHour.getText().toString())) {
                            int etHourNum = Integer.valueOf(etHour.getText().toString());
                            if (!"".equals(etHourNum)) {
                                if (etHourNum < 0 && etHourNum > 24) {
                                    toast("您输入的小时数格式不正确");
                                } else {
                                    hourNum = etHourNum;
                                    if (hourNum < 10) {
                                        tvHour.setText("0" + hourNum);
                                    } else {
                                        tvHour.setText(hourNum + "");
                                    }
                                }
                            }
                        }

                        if (!"".equals(etMinute.getText().toString())) {
                            int etMinuteNum = Integer.valueOf(etMinute.getText().toString());
                            if (!"".equals(etMinuteNum)) {
                                if (etMinuteNum < 0 && etMinuteNum > 60) {
                                    toast("您输入的分钟数格式不正确");
                                } else {
                                    minuteNum = etMinuteNum;
                                    if (minuteNum < 10) {
                                        tvMinute.setText("0" + minuteNum);
                                    } else {
                                        tvMinute.setText(minuteNum + "");
                                    }
                                }
                            }
                        }
                        secondNum = 0;
                        tvSecond.setText("00");
                    }
                })
                .setNegativeButton("取消", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).create().show();
    }


}
