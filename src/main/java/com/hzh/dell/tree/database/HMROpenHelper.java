package com.hzh.dell.tree.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.hzh.greendao.gen.DaoMaster;

public class HMROpenHelper extends DaoMaster.DevOpenHelper {
    public HMROpenHelper(Context context, String name, SQLiteDatabase.CursorFactory factory) {
        super(context, name, factory);
    }
}
