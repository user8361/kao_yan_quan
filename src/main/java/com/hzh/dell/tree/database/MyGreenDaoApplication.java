package com.hzh.dell.tree.database;

import android.app.Application;
import android.database.sqlite.SQLiteDatabase;

import com.hzh.greendao.gen.DaoMaster;
import com.hzh.greendao.gen.DaoSession;

public class MyGreenDaoApplication extends Application {
    private SQLiteDatabase db;//数据库对象
    private DaoMaster mDaoMaster;//DaoMaster对象
    private DaoSession mDaoSession;//DaoSession对象
    private static MyGreenDaoApplication instance;//实例

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        setDatabase();

    }

    private void setDatabase() {
        HMROpenHelper mHelper = new HMROpenHelper(this, "teefpsGDDB", null);
        db = mHelper.getWritableDatabase();
        mDaoMaster = new DaoMaster(db);
        mDaoSession = mDaoMaster.newSession();
    }

    public static MyGreenDaoApplication getInstance() {
        return instance;
    }

    public DaoSession getDaoSession() {
        return mDaoSession;
    }

    public SQLiteDatabase getDb() {
        return db;
    }
}
