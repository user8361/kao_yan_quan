package com.hzh.dell.tree.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import java.util.List;
import java.util.Map;

//数据库管理类
public class DBUtils extends DBOpenHelper {
    public DBUtils(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public void insertData(SQLiteDatabase readableDatabase, String tbName, List<Map<String, String>> list) {
        ContentValues values = new ContentValues();
        values.put(list.toString(), list.toString());
        readableDatabase.insert(tbName, null, values);
    }
}
