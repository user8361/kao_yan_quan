package com.hzh.dell.tree.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBOpenHelper extends SQLiteOpenHelper {
    final String CREATE_USER_TABLE_SQL = "create table if not exists " +
            "user(_id integer primary key autoincrement," +
            "u_name varchar(20)," +//用户名
            "u_phone varchar(11)," +//用户手机号
            "u_pwd varchar(20)," +//用户密码
            "u_address varchar(30)," +//用户地址
            "u_school varchar(20)," +//用户学校
            "u_targetSchool varchar(20)," +//用户目标学校
            "u_major varchar(20)," +//用户专业
            "u_year varchar(4)," +//用户考研时间
            "u_subject varchar(30))";//用户专业

    public DBOpenHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, null, version);//重写构造方法，并且设置工厂为nul
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_USER_TABLE_SQL);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
