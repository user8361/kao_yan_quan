package com.hzh.dell.tree.target_fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import com.hzh.dell.Bmob.entity.Grade;
import com.hzh.dell.Bmob.entity.User;
import com.hzh.dell.tree.R;
import com.hzh.dell.tree.adapter.GridViewShowGradeItemsAdapter;

import java.util.ArrayList;
import java.util.List;

import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.FindListener;
import lecho.lib.hellocharts.gesture.ContainerScrollType;
import lecho.lib.hellocharts.gesture.ZoomType;
import lecho.lib.hellocharts.model.Axis;
import lecho.lib.hellocharts.model.AxisValue;
import lecho.lib.hellocharts.model.Line;
import lecho.lib.hellocharts.model.LineChartData;
import lecho.lib.hellocharts.model.PointValue;
import lecho.lib.hellocharts.model.ValueShape;
import lecho.lib.hellocharts.model.Viewport;
import lecho.lib.hellocharts.view.LineChartView;

/**
 * create by hzh
 * on 2019/5/8
 */
public class TargetGrade extends Fragment {
    private GridView gridView;
    private String userName = "";
    private String USER_NAME = "userName";
    private User user;
    private GridViewShowGradeItemsAdapter adapter;
    private View view;
    private Bundle bundle;
    private LineChartView lineChartView;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.target_grade, container, false);
        initView();
        return view;
    }
    private void initView() {
        Log.e("refresh", "刷新数据");
        bundle = getArguments();
        userName = bundle.getString(USER_NAME);
        gridView = view.findViewById(R.id.gridView);
        View emptyView = view.findViewById(R.id.empty_view_content);
        gridView.setEmptyView(emptyView);
        BmobQuery<User> userBmobQuery = new BmobQuery<>();
        userBmobQuery.addWhereEqualTo(USER_NAME, userName);
        userBmobQuery.findObjects(new FindListener<User>() {
            @Override
            public void done(List<User> list, BmobException e) {
                if (list != null) {
                    user = list.get(0);
                    if (user != null) {
                        BmobQuery<Grade> gradeBmobQuery = new BmobQuery<>();
                        gradeBmobQuery.order("examTime");
                        gradeBmobQuery.addWhereEqualTo("userId", user.getObjectId());
                        gradeBmobQuery.addWhereEqualTo("examName", bundle.getString("examName"));
                        gradeBmobQuery.findObjects(new FindListener<Grade>() {
                            @Override
                            public void done(List<Grade> list, BmobException e) {
                                if (list != null) {
                                    adapter = new GridViewShowGradeItemsAdapter(list, getContext(), getLayoutInflater());
                                    gridView.setAdapter(adapter);
                                    ArrayList<String> examNameList = new ArrayList<>();
                                    ArrayList<Float> targetGradeList = new ArrayList<>();
                                    ArrayList<Float> nowGradeList = new ArrayList<>();
                                    List<String> dateList = new ArrayList<>();
                                    for (Grade g : list) {
                                        examNameList.add(g.getExamName());
                                        targetGradeList.add((float) g.getExamTargetGrade());
                                        nowGradeList.add((float) g.getExamNowGrade());
                                        dateList.add(g.getExamTime());
                                    }

                                    //设置折线图
                                    initHelloChart(dateList, targetGradeList, nowGradeList);
                                }
                            }
                        });
                    }
                }
            }
        });
    }

    /**
     * 绘制折线图
     *
     * @param dateList        x轴日期标签
     * @param targetGradeList 目标成绩数据
     * @param nowGradeList    当前成绩数据
     */
    public void initHelloChart(List<String> dateList, List<Float> targetGradeList, List<Float> nowGradeList) {
        lineChartView = view.findViewById(R.id.line_chart_view);
        //折线条数
        List<Line> lines = new ArrayList<>();
        //获取x轴坐标
        List<AxisValue> axisValues = new ArrayList<>();
        for (int i = 0; i < dateList.size(); i++) {
            //设置x轴坐标标签的个数
            axisValues.add(new AxisValue(i).setLabel(dateList.get(i)));
        }
        //设置目标成绩要显示图表的点
        List<PointValue> targetGradePointValueList = new ArrayList<>();
        setPoint(targetGradePointValueList, targetGradeList);
        //目标成绩折线的颜色
        Line targetGradeLine = new Line(targetGradePointValueList).setColor(getResources().getColor(R.color.colorWarn));
        //设置目标成绩折线的样式
        setLineStyle(targetGradeLine);
        lines.add(targetGradeLine);//将目标成绩折线添加到将要绘制的折线列表中
        //显示图表的点 当前成绩
        List<PointValue> nowGradePointValueList = new ArrayList<>();
        setPoint(nowGradePointValueList, nowGradeList);
        //当前成绩折线的颜色
        Line nowGradeLine = new Line(nowGradePointValueList).setColor(getResources().getColor(R.color.mainBlue));
        setLineStyle(nowGradeLine);//当前成绩折线的样式
        lines.add(nowGradeLine);//将当前成绩折线添加到将要绘制的折线列表中
        LineChartData lineChartData = new LineChartData();
        lineChartData.setLines(lines);//加入线
        setXYAxisStyle(lineChartData, axisValues);//设置x轴，y轴 显示的数据
        setBehaviorAction(lineChartView, lineChartData);//设置手势动作
    }


    /**
     * 设置折线的属性 shape、平滑、折线、填充、点击数据显示、是否显示线、是否显示圆点、圆点半径
     *
     * @param line
     */
    private void setLineStyle(Line line) {

        //折线图上每个数据点的形状  这里是圆形 （有三种 ：ValueShape.SQUARE  ValueShape.CIRCLE  ValueShape.DIAMOND）
        line.setShape(ValueShape.CIRCLE);
        line.setCubic(true);//曲线是否平滑，即是曲线还是折线
        line.setFilled(false);//是否填充曲线的面积
//        targetGradeLine.setHasLabels(true);//曲线的数据坐标是否加上备注
        line.setHasLabelsOnlyForSelected(true);//点击数据坐标提示数据（设置了这个line.setHasLabels(true);就无效）
        line.setHasLines(true);//是否用线显示。如果为false 则没有曲线只有点显示
        line.setHasPoints(true);//是否显示圆点 如果为false 则没有原点只有点显示（每个数据点都是个大的圆点）
        line.setPointRadius(2);//圆的半径

    }

    /**
     * 设置手势
     *
     * @param view
     * @param data
     */
    public void setBehaviorAction(LineChartView view, LineChartData data) {
        //设置行为属性，支持缩放、滑动以及平移
        view.setInteractive(true);
        view.setZoomType(ZoomType.HORIZONTAL);
        view.setMaxZoom((float) 2);//最大方法比例
        view.setContainerScrollEnabled(true,
                ContainerScrollType.HORIZONTAL);
        view.setLineChartData(data);

        Viewport v = new Viewport(view.getMaximumViewport());
        v.left = 0;
        v.right = 3;
        view.setCurrentViewport(v);
    }

    /**
     * 设置横纵坐标样式
     *
     * @param data
     * @param list
     */
    public void setXYAxisStyle(LineChartData data, List<AxisValue> list) {
        Axis axisX = new Axis(); //X轴
        axisX.setHasTiltedLabels(false);
        //X坐标轴字体是斜的显示还是直的，true是斜的显示
        axisX.setTextColor(Color.BLACK);  //设置字体颜色
        axisX.setName("日期");  //表格名称
        axisX.setTextSize(12);//设置字体大小
        axisX.setMaxLabelChars(5);//设置x轴标签的间距

        axisX.setValues(list);  //填充X轴的坐标名称
        data.setAxisXBottom(axisX); //x 轴在底部
        //data.setAxisXTop(axisX);  //x 轴在顶部
        axisX.setHasLines(true); //x 轴分割线

        Axis axisY = new Axis();  //Y轴
        axisY.setName("");//y轴标注
        axisY.setTextSize(12);//设置字体大小
        axisY.setTextColor(Color.BLACK);
        data.setAxisYLeft(axisY);  //Y轴设置在左边
        axisY.setName("成绩");
    }


    /**
     * 设置线的圆点坐标
     *
     * @param valueList
     * @param gradeList
     */
    public void setPoint(List<PointValue> valueList, List<Float> gradeList) {
        //显示图表的点 目标成绩
        for (int i = 0; i < gradeList.size(); i++) {
            valueList.add(new PointValue(i, gradeList.get(i)));
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        initView();
        Log.e("refresh", "刷新");
    }
}
