package com.hzh.dell.tree.target_fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hzh.dell.Bmob.entity.User;
import com.hzh.dell.tree.R;
import com.hzh.dell.tree.common_university.CommonUniversity;

import java.util.List;

import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.FindListener;
import cn.bmob.v3.listener.UpdateListener;

/**
 * create by hzh
 * on 2019/4/28
 */
public class TargetSchool extends Fragment implements View.OnClickListener {
    private WebView webView;
    private View view;
    private TextView tvTargetSchool;
    private LinearLayout targrtSchoolLayout;
    private String SCHOOL = "SCHOOL";
    private int TARGET_SCHOOL = 0x03;
    private String USER_NAME = "userName";
    private String userName = "";
    private Bundle bundle;
    private User user;
    private View emptyView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.target_school, container, false);
        initView();
        return view;
    }

    private void initView() {
        webView = view.findViewById(R.id.web_view);
        tvTargetSchool = view.findViewById(R.id.tv_user_target_school);
        targrtSchoolLayout = view.findViewById(R.id.user_target_school_layout);
        targrtSchoolLayout.setOnClickListener(this);
        emptyView = view.findViewById(R.id.empty_view);

        bundle = getArguments();
        userName = bundle.getString(USER_NAME, "");

        BmobQuery<User> userBmobQuery = new BmobQuery<>();
        userBmobQuery.addWhereEqualTo(USER_NAME, userName);
        userBmobQuery.findObjects(new FindListener<User>() {
            @Override
            public void done(List<User> list, BmobException e) {
                if (list != null) {
                    user = list.get(0);
                    if (user != null) {
                        if (user.getUserTargetSchool().length() > 0) {
                            tvTargetSchool.setText(user.getUserTargetSchool());

                            initWebView();
                        }
                    }
                }
            }
        });
    }


    private void initWebView() {

        webView.setVisibility(View.VISIBLE);
        emptyView.setVisibility(View.GONE);
        String url = "http://m.college.koolearn.com/mobile/universityInfo?universityId=263";
        webView.loadUrl(url);
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);

            }
        });

        WebSettings webSettings = webView.getSettings();
        //如果访问的页面中要与Javascript交互，则webview必须设置支持Javascript
        webSettings.setJavaScriptEnabled(true);
        // 若加载的 html 里有JS 在执行动画等操作，会造成资源浪费（CPU、电量）
        // 在 onStop 和 onResume 里分别把 setJavaScriptEnabled() 给设置成 false 和 true 即可
        //设置自适应屏幕，两者合用
        webSettings.setUseWideViewPort(true); //将图片调整到适合webview的大小
        webSettings.setLoadWithOverviewMode(true); // 缩放至屏幕的大小
        //缩放操作
        webSettings.setSupportZoom(true); //支持缩放，默认为true。是下面那个的前提。
        webSettings.setBuiltInZoomControls(true); //设置内置的缩放控件。若为false，则该WebView不可缩放
        webSettings.setDisplayZoomControls(false); //隐藏原生的缩放控件
        //其他细节操作
        webSettings.setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK); //关闭webview中缓存
        webSettings.setAllowFileAccess(true); //设置可以访问文件
        webSettings.setJavaScriptCanOpenWindowsAutomatically(true); //支持通过JS打开新窗口
        webSettings.setLoadsImagesAutomatically(true); //支持自动加载图片
        webSettings.setDefaultTextEncodingName("utf-8");//设置编码格式
        webView.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onReceivedTitle(WebView view, String title) {
                super.onReceivedTitle(view, title);
            }
        });
    }



    public boolean onKeyDown(int keyCode, KeyEvent event) {
        return (keyCode == KeyEvent.KEYCODE_BACK) && webView.canGoBack();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.user_target_school_layout:
                Intent targetSchoolIntent = new Intent(getActivity(), CommonUniversity.class);
                targetSchoolIntent.putExtra(SCHOOL, TARGET_SCHOOL);
                startActivityForResult(targetSchoolIntent, TARGET_SCHOOL);//
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == TARGET_SCHOOL && resultCode == TARGET_SCHOOL) {
            String result_value = data.getStringExtra("userTargetSchool");
            tvTargetSchool.setText(result_value);
            if (user != null) {
                user.setUserTargetSchool(result_value);
                webView.setVisibility(View.VISIBLE);

                emptyView.setVisibility(View.GONE);
                user.update(new UpdateListener() {
                    @Override
                    public void done(BmobException e) {
                        if (e == null) {
                            Log.e("userInfo", "用户数据已更新");
                        } else {
                            Log.e("userInfo", "用户数据更新失败");
                        }
                    }
                });
            }

        }
    }
}
