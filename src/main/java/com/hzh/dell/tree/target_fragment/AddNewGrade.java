package com.hzh.dell.tree.target_fragment;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.hzh.dell.Bmob.entity.Grade;
import com.hzh.dell.Bmob.entity.User;
import com.hzh.dell.tree.R;
import com.hzh.dell.tree.utils.DateUtil;
import com.jaeger.library.StatusBarUtil;

import java.util.List;

import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.FindListener;
import cn.bmob.v3.listener.SaveListener;

/**
 * create by hzh
 * on 2019/5/8
 */
public class AddNewGrade extends AppCompatActivity implements View.OnClickListener {
    private ImageView ivReturn;
    private EditText etExamName, etExamGrade, etTargetGrade;
    private TextView tvSave, tvExamTime;
    private User user;
    private final static String USER_NAME = "userName";
    private String userName = "";
    private final String TAG = this.getClass().getSimpleName();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.target_grade_add_new);
        StatusBarUtil.setTranslucent(this, 50);//半透明
        initView();

    }

    private void initView() {
        ivReturn = findViewById(R.id.iv_return);
        tvSave = findViewById(R.id.tv_function);
        etExamName = findViewById(R.id.et_exam_name);
        etExamGrade = findViewById(R.id.et_exam_grade);
        etTargetGrade = findViewById(R.id.et_target_grade);
        tvExamTime = findViewById(R.id.tv_exam_time);
        tvExamTime.setText(DateUtil.getFormattedDate(System.currentTimeMillis()));

        ivReturn.setOnClickListener(this);
        tvSave.setOnClickListener(this);
        tvExamTime.setOnClickListener(this);

        userName = getIntent().getStringExtra(USER_NAME);
        BmobQuery<User> userBmobQuery = new BmobQuery<>();
        userBmobQuery.addWhereEqualTo(USER_NAME, userName);
        userBmobQuery.findObjects(new FindListener<User>() {
            @Override
            public void done(List<User> list, BmobException e) {
                if (list != null) {
                    user = list.get(0);
                    if (user != null) {

                    } else {
                        Log.e("list", TAG + " --> user null");

                    }
                } else {
                    Log.e("list", TAG + " --> list null");
                }
            }
        });


    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.iv_return:
                onBackPressed();
                break;
            case R.id.tv_function:
                if (etExamName != null && etExamGrade != null && etTargetGrade != null) {
                    int targetGrade = Integer.valueOf(etTargetGrade.getText().toString().trim());
                    int nowGrade = Integer.valueOf(etExamGrade.getText().toString().trim());
                    int difGrade = nowGrade - targetGrade;


                    Grade grade = new Grade();
                    grade.setUserId(user.getObjectId());
                    grade.setExamName(etExamName.getText().toString().trim());
                    grade.setExamTime(tvExamTime.getText().toString());
                    grade.setExamTargetGrade(targetGrade);
                    grade.setExamNowGrade(nowGrade);
                    grade.setExamDifGrade(difGrade);
                    grade.save(new SaveListener<String>() {
                        @Override
                        public void done(String s, BmobException e) {
                            if (e == null) {
                                toast("保存成功！");
                                onBackPressed();
                            } else {
                                toast(e.toString());
                            }
                        }
                    });
                }
                break;
            case R.id.tv_exam_time:
                selectTime();
                break;
        }

    }

    private void toast(String s) {
        Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
    }

    private void selectTime() {
        View view = getLayoutInflater().inflate(R.layout.target_grade_add_new_select_exam_time, null);
        final DatePicker dtSelectTime = view.findViewById(R.id.dp_select_exam_time);
        final String[] selectTime = {""};

        dtSelectTime.setOnDateChangedListener(new DatePicker.OnDateChangedListener() {
            @Override
            public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                selectTime[0] = String.valueOf(year) + "-" + String.valueOf(monthOfYear + 1) + "-" + String.valueOf(dayOfMonth);
            }
        });
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(view)
                .setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Log.e("selectTime", selectTime[0]);
                        tvExamTime.setText(DateUtil.getFormattedDate(DateUtil.getExamTimestamp(selectTime[0])));
                    }
                })
                .setNegativeButton("取消", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                }).create().show();


    }


}
