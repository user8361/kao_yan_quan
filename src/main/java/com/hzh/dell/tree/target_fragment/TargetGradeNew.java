package com.hzh.dell.tree.target_fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hzh.dell.Bmob.entity.Grade;
import com.hzh.dell.Bmob.entity.User;
import com.hzh.dell.tree.R;
import com.hzh.dell.tree.adapter.ViewPagerAdapter;
import com.hzh.dell.tree.null_fragment.EmptyFragment;
import com.hzh.dell.tree.utils.MagicIndicatorUtil;

import net.lucode.hackware.magicindicator.MagicIndicator;

import java.util.ArrayList;
import java.util.List;

import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.FindListener;

/**
 * create by hzh
 * on 2019/5/8
 */
public class TargetGradeNew extends Fragment {
    private String userName = "";
    private String USER_NAME = "userName";
    private User user;
    private View view;
    private Bundle bundle;
    private Toolbar toolbar;
    private MagicIndicator magicIndicator;
    private MagicIndicator magicIndicatorTitle;
    private ViewPager viewPager;
    private List<String> titles;
    private List<Fragment> fragmentList;
    private Context context;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.target_grade_new, container, false);
        initView();
        initToobar();

        return view;
    }


    private void initView() {
        bundle = getArguments();
        userName = bundle.getString(USER_NAME);
        context = this.getContext();
        toolbar = view.findViewById(R.id.toolBar);
        viewPager = view.findViewById(R.id.viewPager);
        magicIndicator = view.findViewById(R.id.magic_indicator);
        magicIndicatorTitle = view.findViewById(R.id.magic_indicator_title);

    }

    private void initToobar() {
        toolbar.setTitle("");
        titles = new ArrayList<>();
        fragmentList = new ArrayList<>();

        BmobQuery<User> userBmobQuery = new BmobQuery<>();
        userBmobQuery.addWhereEqualTo(USER_NAME, userName);
        userBmobQuery.findObjects(new FindListener<User>() {
            @Override
            public void done(List<User> list, BmobException e) {
                if (list != null) {
                    user = list.get(0);
                    if (user != null) {
                        BmobQuery<Grade> gradeBmobQuery = new BmobQuery<>();
                        gradeBmobQuery.addWhereEqualTo("userId", user.getObjectId());
                        gradeBmobQuery.findObjects(new FindListener<Grade>() {
                            @Override
                            public void done(List<Grade> list, BmobException e) {
                                if (list != null) {
                                    for (Grade g : list) {
                                        if (!titles.contains(g.getExamName())) {
                                            titles.add(g.getExamName());
                                        }
                                    }
                                }
                                if (titles.size() > 0) {
                                    for (int i = 0; i < titles.size(); i++) {
                                        Bundle bundle = new Bundle();
                                        bundle.putString("examName", titles.get(i));
                                        bundle.putString(USER_NAME, userName);
                                        TargetGrade targetGrade = (TargetGrade) Fragment.instantiate(context, TargetGrade.class.getName(), bundle);
                                        fragmentList.add(targetGrade);
                                    }

                                } else {
                                    titles.add("请先添加成绩");
                                    fragmentList.add(new EmptyFragment());
                                }

                                MagicIndicatorUtil magicIndicatorUtil =
                                        new MagicIndicatorUtil(
                                                magicIndicator, magicIndicatorTitle,
                                                context, titles, viewPager,
                                                2, 20, 3);
                                magicIndicatorUtil.initMagicIndicator();
                                magicIndicatorUtil.initMagicIndicatorTitle();
                                viewPager.setAdapter(new ViewPagerAdapter(getChildFragmentManager(), fragmentList, titles));

                            }
                        });
                    }
                } else {
                    Log.e("USER", "null");
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        initToobar();
    }
}
