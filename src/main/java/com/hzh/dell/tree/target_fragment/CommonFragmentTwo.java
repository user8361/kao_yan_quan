package com.hzh.dell.tree.target_fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.hzh.dell.tree.R;

public class CommonFragmentTwo extends Fragment {
    private View view;

    //    private
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.schoolpage_fragment_school_detail, container, false);
        initView();

        return view;
    }

    private void initView() {
        Toast.makeText(getContext(), "init", Toast.LENGTH_SHORT).show();
    }
}
