package com.hzh.dell.tree.target_fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;

import com.hzh.dell.tree.R;
import com.hzh.dell.tree.adapter.ViewPagerAdapter;
import com.hzh.dell.tree.bean.ListItemBean;

import java.util.ArrayList;
import java.util.List;

public class CommonFragmentOne extends Fragment implements View.OnClickListener {
    private View view;
    private ImageView imageView;
    private Toolbar toolbarTwo;
    private TabLayout tabTwo;
    private ViewPager viewPagerTwo;
    private ListView mListView;
    private List<ListItemBean> listTwo;
    private List<Fragment> fragmentListTwo;
    private List<String> titleTwo;

    //    private
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.schoolpage_fragment_school_major, container, false);
        initView();
        initData();
        return view;
    }

    private void initData() {
        imageView.setOnClickListener(this);
        listTwo = new ArrayList<ListItemBean>();
        ListItemBean schoolInfoImg = new ListItemBean();
        schoolInfoImg.setImgId(R.drawable.school);
        titleTwo = new ArrayList<>();
        titleTwo.add("子标题一");
        titleTwo.add("子标题二");
        titleTwo.add("子标题三");

        for (int i = 0; i < titleTwo.size(); i++) {

            CommonFragmentTwo commonTwo = new CommonFragmentTwo();
            fragmentListTwo.add(commonTwo);
        }

        ViewPagerAdapter viewPagerAdapterTwo = new ViewPagerAdapter(getChildFragmentManager(),
                fragmentListTwo, titleTwo);

        viewPagerTwo.setAdapter(viewPagerAdapterTwo);


        tabTwo.setupWithViewPager(viewPagerTwo);


        tabTwo.setTabGravity(Gravity.CENTER);


        tabTwo.setTabMode(TabLayout.MODE_FIXED);
    }

    private void initView() {
        imageView = view.findViewById(R.id.imageView);
        toolbarTwo = view.findViewById(R.id.toolBar_two);

        tabTwo = view.findViewById(R.id.tab_two);
        viewPagerTwo = view.findViewById(R.id.viewPager_two);
        fragmentListTwo = new ArrayList<>();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imageView:
                Uri uri = Uri.parse("https://www.tsinghua.edu.cn/publish/thu2018/index.html");
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);

        }

    }
}
