package com.hzh.dell.tree.settings;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.hzh.dell.tree.R;
import com.hzh.dell.tree.dialog.MyMenuDialog;
import com.hzh.dell.tree.login_register.UserManager;

/**
 * create by hzh
 * on 2019/4/11
 */
public class Settings extends AppCompatActivity implements View.OnClickListener {
    private LinearLayout logoffLayout;
    private ImageView ivReturn;
    private int LIGHTBLUE = 0x9506a6e6;
    private int GRAY = 0x8A000000;
    private MyMenuDialog dialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings);

        initView();
    }

    private void initView() {
        logoffLayout = findViewById(R.id.log_off_layout);
        ivReturn = findViewById(R.id.iv_return);
        ivReturn.setOnClickListener(this);
        logoffLayout.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_return:
                onBackPressed();
                break;
            case R.id.log_off_layout:
                logoffDialog();
                break;
        }
    }

    private void logoffDialog() {


        String[] names = {"温馨提示", "您确定要退出吗?", "确定", "取消"};
        dialog = new MyMenuDialog(this, names, true);
        int[] colors = new int[]{GRAY, GRAY, LIGHTBLUE, LIGHTBLUE};
        dialog.setTextColor2Items(colors);
//        dialog.setTextColor2AllItems(LIGHTBLUE);


        dialog.setOnClickListener2LastTwoItems(new MyMenuDialog.OnClickListener2LastTwoItem() {
            @Override
            public void onClickListener2LastItem() {//取消按钮
                dialog.dismiss();
            }

            @Override
            public void onClickListener2SecondLastItem() {//确定按钮
                dialog.dismiss();
                UserManager.getInstance().saveUserInfo(getApplicationContext(), "", "");
                finish();
            }
        });
        dialog.show();
    }
}
