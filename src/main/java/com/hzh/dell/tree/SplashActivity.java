package com.hzh.dell.tree;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.hzh.dell.tree.login_register.EnterActivity;
import com.hzh.dell.tree.login_register.UserManager;
import com.hzh.dell.tree.utils.DateUtil;
import com.robinhood.ticker.TickerView;

import cn.bmob.v3.Bmob;
import cn.bmob.v3.BmobConfig;

/**
 * create by hzh
 * on 2019/4/13
 */
public class SplashActivity extends AppCompatActivity {
    private static final int GO_HOME = 0;//去主页
    private static final int GO_LOGIN = 1;//去登录页
    private final String USER_NAME = "userName";
    private TickerView tickerView;
    private String MONTH_DAY = "-12-22";
    private final String AppID = "9be0ce96212e3b81fac411b1f9a0bbea";

    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        initBomb();
        initView();

        //自动判断登录
        if (UserManager.getInstance().hasUserInfo(this)) {
            mHandler.sendEmptyMessageDelayed(GO_HOME, 2000);
        } else {
            mHandler.sendEmptyMessageDelayed(GO_LOGIN, 2000);
        }
    }

    private void initBomb() {
        //法一：默认初始化
//        Bmob.initialize(this, "9be0ce96212e3b81fac411b1f9a0bbea");

        //第二：自v3.4.7版本开始,设置BmobConfig,允许设置请求超时时间、文件分片上传时每片的大小、文件的过期时间(单位为秒)，
        BmobConfig config = new BmobConfig.Builder(this)
                ////设置appkey
                .setApplicationId(AppID)
                ////请求超时时间（单位为秒）：默认15s
                .setConnectTimeout(30)
                ////文件分片上传时每片的大小（单位字节），默认512*1024
                .setUploadBlockSize(1024 * 1024)
                ////文件的过期时间(单位为秒)：默认1800s
                .setFileExpiration(2500)
                .build();
        Bmob.initialize(config);

    }

    private void initView() {
        tickerView = findViewById(R.id.tv_rest_day);

        long nowTime = System.currentTimeMillis();//现在的时间戳
        String year = DateUtil.getFormattedDate(nowTime).substring(0, 4);//获得年份
        long examTime = DateUtil.getExamTimestamp(year + MONTH_DAY);//2019-12-22
        String restDay = String.valueOf((examTime - nowTime) / 1000 / 60 / 60 / 24);//剩余天数

        //判断本地文件是否含有数据
        preferences = getSharedPreferences("restDay", Context.MODE_PRIVATE);
        editor = preferences.edit();
        if (preferences.getBoolean("flag", false)) {
            //如果文件中flag==true，则文本要显示的时间为记录中的时间
            String oldRestDay = preferences.getString("restDay", "365");
            tickerView.setCharacterLists(oldRestDay);
            tickerView.setText(oldRestDay);
            editor.putString("restDay", restDay);
            editor.commit();
            tickerView.setAnimationDelay(500);//延迟500毫秒
            tickerView.setAnimationDuration(1000);//持续2秒


        } else {
            //文件中为false,或是不存在

            editor.putBoolean("flag", true);
            editor.putString("restDay", restDay);
            editor.commit();
        }
        tickerView.setCharacterLists(restDay);
        tickerView.setText(restDay);
    }



    /**
     * 跳转的判断
     */
    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case GO_HOME:
                    Intent intent = new Intent(SplashActivity.this, HomepageActivity.class);
                    intent.putExtra(USER_NAME, UserManager.getInstance().getUserInfo(getApplicationContext()).getUserName());
                    startActivity(intent);
                    finish();
                    break;
                case GO_LOGIN:
                    Intent intent1 = new Intent(SplashActivity.this, EnterActivity.class);
                    startActivity(intent1);
                    finish();
                    break;
            }

        }
    };
}
