package com.hzh.dell.tree.bean;

import java.util.List;

/**
 * create by hzh
 * on 2019/4/10
 */
public class UniversityJsonBean {
    private String province;
    private List<University> schools;

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public List<University> getSchools() {
        return schools;
    }

    public void setSchools(List<University> schools) {
        this.schools = schools;
    }

    public class University {
        private String name;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

    }
}
