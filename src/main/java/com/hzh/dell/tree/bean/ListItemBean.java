package com.hzh.dell.tree.bean;

public class ListItemBean {
    private String title;//标题
    private String tabType;//所属的tab类型
    private String submitTime;//提交的时间
    private int imgId;

    public ListItemBean() {
        super();
    }

    public ListItemBean(String title, String tabType, String submitTime, int imgId) {
        this.title = title;
        this.tabType = tabType;
        this.submitTime = submitTime;
        this.imgId = imgId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTabType() {
        return tabType;
    }

    public void setTabType(String tabType) {
        this.tabType = tabType;
    }

    public String getSubmitTime() {
        return submitTime;
    }

    public void setSubmitTime(String submitTime) {
        this.submitTime = submitTime;
    }

    public int getImgId() {
        return imgId;
    }

    public void setImgId(int imgId) {
        this.imgId = imgId;
    }
}
