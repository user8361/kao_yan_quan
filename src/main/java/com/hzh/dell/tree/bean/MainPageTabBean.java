package com.hzh.dell.tree.bean;

/**
 * 首页tab标题
 */
public class MainPageTabBean {
    private String tabName;

    public MainPageTabBean(String tabName) {
        this.tabName = tabName;
    }

    public String getTabName() {
        return tabName;
    }

    public void setTabName(String tabName) {
        this.tabName = tabName;
    }
}
