package com.hzh.dell.tree.bean;

public class ListBean {
    private int imgLeftId;
    private String itemTitle;
    private int imgRightId;

    public ListBean(int imgLeftId, String itemTitle, int imgRightId) {
        this.imgLeftId = imgLeftId;
        this.itemTitle = itemTitle;
        this.imgRightId = imgRightId;
    }

    public int getImgLeftId() {
        return imgLeftId;
    }

    public void setImgLeftId(int imgLeftId) {
        this.imgLeftId = imgLeftId;
    }

    public String getItemTitle() {
        return itemTitle;
    }

    public void setItemTitle(String itemTitle) {
        this.itemTitle = itemTitle;
    }

    public int getImgRightId() {
        return imgRightId;
    }

    public void setImgRightId(int imgRightId) {
        this.imgRightId = imgRightId;
    }
}
