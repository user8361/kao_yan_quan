package com.hzh.dell.tree.entity;

/**
 * create by hzh
 * on 2019/5/3
 */
//@Entity
public class Vote {
    //    @Id
    private Long id;//id
    private Long userId;//用户Id
    private String voteContent;//主题内容
    private String voteItems;//投票内容
    private long createTime;//发表时间
    private int isMultipleSelection;//是否可以多选。
    private int likeNum;//喜欢数目
    private int replyNum;//回复数目
}
