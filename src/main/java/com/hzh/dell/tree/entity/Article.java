package com.hzh.dell.tree.entity;

import com.hzh.greendao.gen.ArticleDao;
import com.hzh.greendao.gen.DaoSession;
import com.hzh.greendao.gen.UserDao;

import org.greenrobot.greendao.DaoException;
import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.ToMany;
import org.greenrobot.greendao.annotation.ToOne;

import java.util.List;

@Entity(nameInDb = "ARTICLE")
public class Article {
    @Id(autoincrement = true)
    private Long articleId;//帖子id
    //ToOne注解使用的joinPropety="userId"指的是Article中自定义的这个userId;
    private Long userId;//发帖用户Id
    //在insert时，将userId同一对一关联的User的主键id绑定
    @ToOne(joinProperty = "userId")
    private User user;
    @ToMany(referencedJoinProperty = "userId")
    private List<User> commentUserList;//评论用户Id
    @ToMany(referencedJoinProperty = "userId")
    private List<User> likeUserList;//点赞用户id
    @ToMany(referencedJoinProperty = "userId")
    private List<User> collectUserList;//收藏用户id
    private String articleTitle;//帖子标题
    private String articleType;//帖子类型
    private String articleContent;//帖子内容
    private String articleImgId;//帖子中的图片
    private long createTime;//发帖时间
    private int replyNum;//回复数目(一对多)
    private int likeNum;//红心数目
    private int collectNum;//收藏数目（一对多）
    private int likeFlag;//喜欢flag
    private int collectFlag;//收藏flag

    private String articleCollectImgId;//收藏图标id
    private String articleLikeImgId;//赞图标id
    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#refresh(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 1942392019)
    public void refresh() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.refresh(this);
    }
    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#update(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 713229351)
    public void update() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.update(this);
    }
    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#delete(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 128553479)
    public void delete() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.delete(this);
    }

    /**
     * Resets a to-many relationship, making the next get call to query for a fresh result.
     */
    @Generated(hash = 195170769)
    public synchronized void resetCollectUserList() {
        collectUserList = null;
    }

    /**
     * To-many relationship, resolved on first access (and after reset).
     * Changes to to-many relations are not persisted, make changes to the target entity.
     */
    @Generated(hash = 1893625119)
    public List<User> getCollectUserList() {
        if (collectUserList == null) {
            final DaoSession daoSession = this.daoSession;
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            UserDao targetDao = daoSession.getUserDao();
            List<User> collectUserListNew = targetDao._queryArticle_CollectUserList(articleId);
            synchronized (this) {
                if (collectUserList == null) {
                    collectUserList = collectUserListNew;
                }
            }
        }
        return collectUserList;
    }

    /**
     * Resets a to-many relationship, making the next get call to query for a fresh result.
     */
    @Generated(hash = 1702923514)
    public synchronized void resetLikeUserList() {
        likeUserList = null;
    }

    /**
     * To-many relationship, resolved on first access (and after reset).
     * Changes to to-many relations are not persisted, make changes to the target entity.
     */
    @Generated(hash = 1299092138)
    public List<User> getLikeUserList() {
        if (likeUserList == null) {
            final DaoSession daoSession = this.daoSession;
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            UserDao targetDao = daoSession.getUserDao();
            List<User> likeUserListNew = targetDao._queryArticle_LikeUserList(articleId);
            synchronized (this) {
                if (likeUserList == null) {
                    likeUserList = likeUserListNew;
                }
            }
        }
        return likeUserList;
    }

    /** Resets a to-many relationship, making the next get call to query for a fresh result. */
    @Generated(hash = 1909585047)
    public synchronized void resetCommentUserList() {
        commentUserList = null;
    }
    /**
     * To-many relationship, resolved on first access (and after reset).
     * Changes to to-many relations are not persisted, make changes to the target entity.
     */
    @Generated(hash = 1256902428)
    public List<User> getCommentUserList() {
        if (commentUserList == null) {
            final DaoSession daoSession = this.daoSession;
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            UserDao targetDao = daoSession.getUserDao();
            List<User> commentUserListNew = targetDao._queryArticle_CommentUserList(articleId);
            synchronized (this) {
                if (commentUserList == null) {
                    commentUserList = commentUserListNew;
                }
            }
        }
        return commentUserList;
    }

    /** called by internal mechanisms, do not call yourself. */
    @Generated(hash = 496399742)
    public void setUser(User user) {
        synchronized (this) {
            this.user = user;
            userId = user == null ? null : user.getUserId();
            user__resolvedKey = userId;
        }
    }

    /** To-one relationship, resolved on first access. */
    @Generated(hash = 859885876)
    public User getUser() {
        Long __key = this.userId;
        if (user__resolvedKey == null || !user__resolvedKey.equals(__key)) {
            final DaoSession daoSession = this.daoSession;
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            UserDao targetDao = daoSession.getUserDao();
            User userNew = targetDao.load(__key);
            synchronized (this) {
                user = userNew;
                user__resolvedKey = __key;
            }
        }
        return user;
    }
    @Generated(hash = 251390918)
    private transient Long user__resolvedKey;

    /** called by internal mechanisms, do not call yourself. */
    @Generated(hash = 2112142041)
    public void __setDaoSession(DaoSession daoSession) {
        this.daoSession = daoSession;
        myDao = daoSession != null ? daoSession.getArticleDao() : null;
    }

    /** Used for active entity operations. */
    @Generated(hash = 434328755)
    private transient ArticleDao myDao;
    /** Used to resolve relations */
    @Generated(hash = 2040040024)
    private transient DaoSession daoSession;

    public String getArticleLikeImgId() {
        return this.articleLikeImgId;
    }

    public void setArticleLikeImgId(String articleLikeImgId) {
        this.articleLikeImgId = articleLikeImgId;
    }

    public String getArticleCollectImgId() {
        return this.articleCollectImgId;
    }

    public void setArticleCollectImgId(String articleCollectImgId) {
        this.articleCollectImgId = articleCollectImgId;
    }
    public int getCollectNum() {
        return this.collectNum;
    }
    public void setCollectNum(int collectNum) {
        this.collectNum = collectNum;
    }
    public int getLikeNum() {
        return this.likeNum;
    }
    public void setLikeNum(int likeNum) {
        this.likeNum = likeNum;
    }
    public int getReplyNum() {
        return this.replyNum;
    }
    public void setReplyNum(int replyNum) {
        this.replyNum = replyNum;
    }
    public long getCreateTime() {
        return this.createTime;
    }
    public void setCreateTime(long createTime) {
        this.createTime = createTime;
    }
    public String getArticleImgId() {
        return this.articleImgId;
    }
    public void setArticleImgId(String articleImgId) {
        this.articleImgId = articleImgId;
    }
    public String getArticleContent() {
        return this.articleContent;
    }
    public void setArticleContent(String articleContent) {
        this.articleContent = articleContent;
    }
    public String getArticleType() {
        return this.articleType;
    }
    public void setArticleType(String articleType) {
        this.articleType = articleType;
    }
    public String getArticleTitle() {
        return this.articleTitle;
    }
    public void setArticleTitle(String articleTitle) {
        this.articleTitle = articleTitle;
    }
    public Long getUserId() {
        return this.userId;
    }
    public void setUserId(Long userId) {
        this.userId = userId;
    }
    public Long getArticleId() {
        return this.articleId;
    }
    public void setArticleId(Long articleId) {
        this.articleId = articleId;
    }

    public int getCollectFlag() {
        return this.collectFlag;
    }

    public void setCollectFlag(int collectFlag) {
        this.collectFlag = collectFlag;
    }

    public int getLikeFlag() {
        return this.likeFlag;
    }

    public void setLikeFlag(int likeFlag) {
        this.likeFlag = likeFlag;
    }

    @Generated(hash = 1894395622)
    public Article(Long articleId, Long userId, String articleTitle, String articleType,
                   String articleContent, String articleImgId, long createTime, int replyNum, int likeNum,
                   int collectNum, int likeFlag, int collectFlag, String articleCollectImgId,
                   String articleLikeImgId) {
        this.articleId = articleId;
        this.userId = userId;
        this.articleTitle = articleTitle;
        this.articleType = articleType;
        this.articleContent = articleContent;
        this.articleImgId = articleImgId;
        this.createTime = createTime;
        this.replyNum = replyNum;
        this.likeNum = likeNum;
        this.collectNum = collectNum;
        this.likeFlag = likeFlag;
        this.collectFlag = collectFlag;
        this.articleCollectImgId = articleCollectImgId;
        this.articleLikeImgId = articleLikeImgId;
    }
    @Generated(hash = 742516792)
    public Article() {
    }


}
