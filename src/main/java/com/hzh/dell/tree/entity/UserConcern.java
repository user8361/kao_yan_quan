package com.hzh.dell.tree.entity;

import com.hzh.greendao.gen.DaoSession;
import com.hzh.greendao.gen.UserConcernDao;
import com.hzh.greendao.gen.UserDao;

import org.greenrobot.greendao.DaoException;
import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.NotNull;
import org.greenrobot.greendao.annotation.ToMany;

import java.util.List;

/**
 * create by hzh
 * on 2019/4/15
 */
@Entity
public class UserConcern {

    @Id(autoincrement = true)
    private Long id;
    @NotNull//用户本人id
    private Long userId;
    private Long concernId;
    private Long funId;

    //关注用户
    @ToMany(referencedJoinProperty = "userId")
    private List<User> concernList;

    //粉丝用户
    @ToMany(referencedJoinProperty = "userId")
    private List<User> funList;

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#refresh(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 1942392019)
    public void refresh() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.refresh(this);
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#update(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 713229351)
    public void update() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.update(this);
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#delete(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 128553479)
    public void delete() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.delete(this);
    }

    /**
     * Resets a to-many relationship, making the next get call to query for a fresh result.
     */
    @Generated(hash = 65951437)
    public synchronized void resetFunList() {
        funList = null;
    }

    /**
     * To-many relationship, resolved on first access (and after reset).
     * Changes to to-many relations are not persisted, make changes to the target entity.
     */
    @Generated(hash = 345831229)
    public List<User> getFunList() {
        if (funList == null) {
            final DaoSession daoSession = this.daoSession;
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            UserDao targetDao = daoSession.getUserDao();
            List<User> funListNew = targetDao._queryUserConcern_FunList(id);
            synchronized (this) {
                if (funList == null) {
                    funList = funListNew;
                }
            }
        }
        return funList;
    }

    /**
     * Resets a to-many relationship, making the next get call to query for a fresh result.
     */
    @Generated(hash = 1800045579)
    public synchronized void resetConcernList() {
        concernList = null;
    }

    /**
     * To-many relationship, resolved on first access (and after reset).
     * Changes to to-many relations are not persisted, make changes to the target entity.
     */
    @Generated(hash = 871366578)
    public List<User> getConcernList() {
        if (concernList == null) {
            final DaoSession daoSession = this.daoSession;
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            UserDao targetDao = daoSession.getUserDao();
            List<User> concernListNew = targetDao._queryUserConcern_ConcernList(id);
            synchronized (this) {
                if (concernList == null) {
                    concernList = concernListNew;
                }
            }
        }
        return concernList;
    }

    /**
     * called by internal mechanisms, do not call yourself.
     */
    @Generated(hash = 647432771)
    public void __setDaoSession(DaoSession daoSession) {
        this.daoSession = daoSession;
        myDao = daoSession != null ? daoSession.getUserConcernDao() : null;
    }

    /**
     * Used for active entity operations.
     */
    @Generated(hash = 1193942269)
    private transient UserConcernDao myDao;
    /**
     * Used to resolve relations
     */
    @Generated(hash = 2040040024)
    private transient DaoSession daoSession;

    public Long getFunId() {
        return this.funId;
    }

    public void setFunId(Long funId) {
        this.funId = funId;
    }

    public Long getConcernId() {
        return this.concernId;
    }

    public void setConcernId(Long concernId) {
        this.concernId = concernId;
    }

    public Long getUserId() {
        return this.userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Generated(hash = 65111479)
    public UserConcern(Long id, @NotNull Long userId, Long concernId, Long funId) {
        this.id = id;
        this.userId = userId;
        this.concernId = concernId;
        this.funId = funId;
    }

    @Generated(hash = 177492842)
    public UserConcern() {
    }

}
