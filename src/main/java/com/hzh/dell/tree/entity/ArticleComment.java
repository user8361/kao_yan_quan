package com.hzh.dell.tree.entity;

public class ArticleComment {
    private Long id;//自增id
    private Long hostUserId;//发帖用户id
    private Long guestUserId;//评论用户id
    private String comContent;//评论内容
    private int comNum;//评论数目
    private long comTime;//评论时间
}
