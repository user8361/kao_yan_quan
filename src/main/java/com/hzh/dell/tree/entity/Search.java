package com.hzh.dell.tree.entity;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.NotNull;
import org.greenrobot.greendao.annotation.Generated;

/**
 * create by hzh
 * on 2019/4/19
 */
@Entity
public class Search {
    @Id(autoincrement = true)
    private Long id;
    @NotNull
    private String searchTitle;

    public String getSearchTitle() {
        return this.searchTitle;
    }

    public void setSearchTitle(String searchTitle) {
        this.searchTitle = searchTitle;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Generated(hash = 146928749)
    public Search(Long id, @NotNull String searchTitle) {
        this.id = id;
        this.searchTitle = searchTitle;
    }

    @Generated(hash = 1644193961)
    public Search() {
    }
}
