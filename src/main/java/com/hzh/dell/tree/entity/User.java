package com.hzh.dell.tree.entity;

import com.hzh.greendao.gen.ArticleDao;
import com.hzh.greendao.gen.DaoSession;
import com.hzh.greendao.gen.UserDao;

import org.greenrobot.greendao.DaoException;
import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Index;
import org.greenrobot.greendao.annotation.NotNull;
import org.greenrobot.greendao.annotation.ToMany;

import java.util.List;

@Entity(
        nameInDb = "USER",
        indexes = {
                @Index(value = "userName ASC")
        }
)
public class User {
    @Id(autoincrement = true)
    private Long userId;//id自动生成

    private String userName;//用户名（账号）
    @NotNull
    private String userPhone;//用户手机号
    @NotNull
    private String userPassword;//用户密码
    private String userHeadImgId;//用户头像ID
    private String userAddress;//用户地址
    private String userCurrentSchool;//用户本科学校
    private String userTargetSchool;//用户目标学校
    private String userExamYear;//用户考研年份
    private String userMajor;//用户目标专业
    private String userExamSubject;//用户考试科目
    //此处的userId是在Article中定义的一个变量
    @ToMany(referencedJoinProperty = "userId")
    private List<Article> articleList;//发表的帖子集合

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#refresh(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 1942392019)
    public void refresh() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.refresh(this);
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#update(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 713229351)
    public void update() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.update(this);
    }

    /**
     * Resets a to-many relationship, making the next get call to query for a fresh result.
     */
    @Generated(hash = 1410160217)
    public synchronized void resetArticleList() {
        articleList = null;
    }

    /**
     * To-many relationship, resolved on first access (and after reset).
     * Changes to to-many relations are not persisted, make changes to the target entity.
     */
    @Generated(hash = 1057344985)
    public List<Article> getArticleList() {
        if (articleList == null) {
            final DaoSession daoSession = this.daoSession;
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            ArticleDao targetDao = daoSession.getArticleDao();
            List<Article> articleListNew = targetDao._queryUser_ArticleList(userId);
            synchronized (this) {
                if (articleList == null) {
                    articleList = articleListNew;
                }
            }
        }
        return articleList;
    }

    /**
     * called by internal mechanisms, do not call yourself.
     */
    @Generated(hash = 2059241980)
    public void __setDaoSession(DaoSession daoSession) {
        this.daoSession = daoSession;
        myDao = daoSession != null ? daoSession.getUserDao() : null;
    }

    /**
     * Used for active entity operations.
     */
    @Generated(hash = 1507654846)
    private transient UserDao myDao;

    /**
     * Used to resolve relations
     */
    @Generated(hash = 2040040024)
    private transient DaoSession daoSession;

    public String getUserExamSubject() {
        return this.userExamSubject;
}
public void setUserExamSubject(String userExamSubject) {
        this.userExamSubject = userExamSubject;
}

    public String getUserMajor() {
        return this.userMajor;
    }

    public void setUserMajor(String userMajor) {
        this.userMajor = userMajor;
    }

    public String getUserExamYear() {
        return this.userExamYear;
}
public void setUserExamYear(String userExamYear) {
        this.userExamYear = userExamYear;
}

    public String getUserTargetSchool() {
        return this.userTargetSchool;
}
public void setUserTargetSchool(String userTargetSchool) {
    this.userTargetSchool = userTargetSchool;
}

    public String getUserCurrentSchool() {
        return this.userCurrentSchool;
}
public void setUserCurrentSchool(String userCurrentSchool) {
    this.userCurrentSchool = userCurrentSchool;
}

    public String getUserAddress() {
        return this.userAddress;
}
public void setUserAddress(String userAddress) {
        this.userAddress = userAddress;
}

    public String getUserHeadImgId() {
        return this.userHeadImgId;
}
public void setUserHeadImgId(String userHeadImgId) {
        this.userHeadImgId = userHeadImgId;
}

    public String getUserPassword() {
        return this.userPassword;
}
public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
}

    public String getUserPhone() {
        return this.userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public String getUserName() {
        return this.userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Long getUserId() {
        return this.userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#delete(Object)}.
     * Entity must attached to an entity context.
     */

    @Generated(hash = 128553479)
    public void delete() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.delete(this);
    }

    @Generated(hash = 1554996404)
    public User(Long userId, String userName, @NotNull String userPhone,
                @NotNull String userPassword, String userHeadImgId,
                String userAddress, String userCurrentSchool,
                String userTargetSchool, String userExamYear, String userMajor,
                String userExamSubject) {
        this.userId = userId;
        this.userName = userName;
        this.userPhone = userPhone;
        this.userPassword = userPassword;
        this.userHeadImgId = userHeadImgId;
        this.userAddress = userAddress;
        this.userCurrentSchool = userCurrentSchool;
        this.userTargetSchool = userTargetSchool;
        this.userExamYear = userExamYear;
        this.userMajor = userMajor;
        this.userExamSubject = userExamSubject;
}
@Generated(hash = 586692638)
public User() {
}

    
}
