package com.hzh.dell.tree.entity;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;

/**
 * create by hzh
 * on 2019/5/2
 */
@Entity
public class Plan {
    @Id(autoincrement = true)
    private Long planId;//计划id
    private Long userId;//用户id
    private String createTime;//创建时间
    private String timeDuration;//时间段
    private String planContent;//计划内容
    private int completeState;//完成状态

    public int getCompleteState() {
        return this.completeState;
    }

    public void setCompleteState(int completeState) {
        this.completeState = completeState;
    }

    public String getPlanContent() {
        return this.planContent;
    }

    public void setPlanContent(String planContent) {
        this.planContent = planContent;
    }

    public String getTimeDuration() {
        return this.timeDuration;
    }

    public void setTimeDuration(String timeDuration) {
        this.timeDuration = timeDuration;
    }

    public String getCreateTime() {
        return this.createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public Long getUserId() {
        return this.userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getPlanId() {
        return this.planId;
    }

    public void setPlanId(Long planId) {
        this.planId = planId;
    }

    @Generated(hash = 1346538222)
    public Plan(Long planId, Long userId, String createTime, String timeDuration,
                String planContent, int completeState) {
        this.planId = planId;
        this.userId = userId;
        this.createTime = createTime;
        this.timeDuration = timeDuration;
        this.planContent = planContent;
        this.completeState = completeState;
    }

    @Generated(hash = 592612124)
    public Plan() {
    }

}
