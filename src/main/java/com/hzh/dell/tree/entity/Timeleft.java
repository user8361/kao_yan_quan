package com.hzh.dell.tree.entity;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;

/**
 * create by hzh
 * on 2019/4/22
 */
@Entity
public class Timeleft {
    @Id
    private Long id;//
    private String timeLeft;//剩余时间
    private String year;//考研年份

    public String getYear() {
        return this.year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getTimeLeft() {
        return this.timeLeft;
    }

    public void setTimeLeft(String timeLeft) {
        this.timeLeft = timeLeft;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Generated(hash = 408936951)
    public Timeleft(Long id, String timeLeft, String year) {
        this.id = id;
        this.timeLeft = timeLeft;
        this.year = year;
    }

    @Generated(hash = 1608307830)
    public Timeleft() {
    }
}
