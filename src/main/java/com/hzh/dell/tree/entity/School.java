package com.hzh.dell.tree.entity;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;

/**
 * create by hzh
 * on 2019/4/27
 */
@Entity
public class School {

    @Id(autoincrement = true)
    private Long schoolId;//学校id
    private String schoolName;//学校名称
    private String province;//所在省份
    private String address;//地址
    private String city;//城市
    private String is211;//是否是211
    private String is985;//是否是985
    private String publicPrivate;//公办民办
    private String schoolCategory;//学校分类信息
    private String department;//隶属
    private String schoolType;//学校类型

    public String getSchoolType() {
        return this.schoolType;
    }

    public void setSchoolType(String schoolType) {
        this.schoolType = schoolType;
    }

    public String getDepartment() {
        return this.department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getSchoolCategory() {
        return this.schoolCategory;
    }

    public void setSchoolCategory(String schoolCategory) {
        this.schoolCategory = schoolCategory;
    }

    public String getPublicPrivate() {
        return this.publicPrivate;
    }

    public void setPublicPrivate(String publicPrivate) {
        this.publicPrivate = publicPrivate;
    }

    public String getIs985() {
        return this.is985;
    }

    public void setIs985(String is985) {
        this.is985 = is985;
    }

    public String getIs211() {
        return this.is211;
    }

    public void setIs211(String is211) {
        this.is211 = is211;
    }

    public String getCity() {
        return this.city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddress() {
        return this.address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getProvince() {
        return this.province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getSchoolName() {
        return this.schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public Long getSchoolId() {
        return this.schoolId;
    }

    public void setSchoolId(Long schoolId) {
        this.schoolId = schoolId;
    }

    @Generated(hash = 806486184)
    public School(Long schoolId, String schoolName, String province,
                  String address, String city, String is211, String is985,
                  String publicPrivate, String schoolCategory, String department,
                  String schoolType) {
        this.schoolId = schoolId;
        this.schoolName = schoolName;
        this.province = province;
        this.address = address;
        this.city = city;
        this.is211 = is211;
        this.is985 = is985;
        this.publicPrivate = publicPrivate;
        this.schoolCategory = schoolCategory;
        this.department = department;
        this.schoolType = schoolType;
    }

    @Generated(hash = 1579966795)
    public School() {
    }


}
