package com.hzh.dell.tree.main_fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.hzh.dell.tree.R;
import com.hzh.dell.tree.adapter.ViewPagerAdapter;
import com.hzh.dell.tree.target_fragment.AddNewGrade;
import com.hzh.dell.tree.target_fragment.TargetGradeNew;
import com.hzh.dell.tree.target_fragment.TargetSchool;
import com.hzh.dell.tree.utils.MagicIndicatorUtil;

import net.lucode.hackware.magicindicator.MagicIndicator;

import java.util.ArrayList;
import java.util.List;

//import com.hzh.dell.tree.article.third_nav_tab_article.CommonArticleDisplay_News;

public class MainFragmentSchoolNew extends Fragment {
    private Toolbar toolbar;
    private List<Fragment> fragmentList;
    private List<String> titles;
    private ViewPager viewPager;
    private View view;

    private final String USER_NAME = "userName";
    private String userName;

    private MagicIndicator magicIndicator;
    private MagicIndicator magicIndicatorTitle;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.main_fragment_school_new, container, false);
        Bundle bundle = getArguments();
        userName = bundle.getString(USER_NAME);
        initView();
        initToolbar();
        return view;
    }

    private void initView() {
        toolbar = view.findViewById(R.id.toolBar);
        viewPager = view.findViewById(R.id.nav_community_viewPager);

        magicIndicator = view.findViewById(R.id.magic_indicator);
        magicIndicatorTitle = view.findViewById(R.id.magic_indicator_title);

        setHasOptionsMenu(true);
    }

    private void initToolbar() {
        toolbar.setTitle("");
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        titles = new ArrayList<>();
        titles.add("院校");
        titles.add("成绩");


        fragmentList = new ArrayList<>();
        Bundle b = new Bundle();
        b.putString(USER_NAME, userName);
        TargetSchool targetSchool = (TargetSchool) Fragment.instantiate(getContext(), TargetSchool.class.getName(), b);
//        TargetGrade targetGrade = (TargetGrade) Fragment.instantiate(getContext(), TargetGrade.class.getName(), b);
        TargetGradeNew targetGradeNew = (TargetGradeNew) Fragment.instantiate(getContext(), TargetGradeNew.class.getName(), b);



        fragmentList.add(targetSchool);
        fragmentList.add(targetGradeNew);


        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getChildFragmentManager(),
                fragmentList, titles);
        viewPager.setAdapter(viewPagerAdapter);
        viewPager.setOffscreenPageLimit(10);
        MagicIndicatorUtil magicIndicatorUtil =
                new MagicIndicatorUtil(
                        magicIndicator, magicIndicatorTitle,
                        getContext(), titles, viewPager,
                        2, 20, 3);
        magicIndicatorUtil.initMagicIndicator();
        magicIndicatorUtil.initMagicIndicatorTitle();
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.navigation_add_school_grade, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.add_school:

                break;
            case R.id.add_grade:
                Intent addGradeIntent = new Intent(getContext(), AddNewGrade.class);
                addGradeIntent.putExtra(USER_NAME, userName);
                startActivity(addGradeIntent);
                break;


        }
        return super.onOptionsItemSelected(item);

    }
}
