package com.hzh.dell.tree.main_fragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hzh.dell.tree.R;
import com.hzh.dell.tree.adapter.ViewPagerAdapter;
import com.hzh.dell.tree.common_search.CommonSearch;
import com.hzh.dell.tree.custom.WaterBallView;
import com.hzh.dell.tree.homepage_fragment.CommonFragment;
import com.hzh.dell.tree.homepage_fragment.RecommendFragment;
import com.hzh.dell.tree.homepage_fragment.ToolBarMenu;
import com.hzh.dell.tree.utils.DateUtil;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;

import java.util.ArrayList;
import java.util.List;

public class MainFragmentHomePage extends Fragment implements View.OnClickListener {
//    private UserDao userDao;
//    private User user;

    private String USER_NAME = "userName";
    private String userName;
    private Bundle bundle;
    private Toolbar toolbar;
    private List<Fragment> fragmentList;
    private List<String> titles;
    private String[] data;
    private ViewPager viewPager;
    private TabLayout tabLayout;
    private TextView tvSearch;
    private MaterialCalendarView materialCalendarView;
    //    private MagicIndicator magicIndicator;
//    private MagicIndicator magicIndicatorTitle;
//    private WaveView waveView;
    private WaterBallView waterBallView;
    private View view;

    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.main_fragment_homepage_new, container, false);
        bundle = getArguments();
        userName = bundle.getString(USER_NAME);
//        userDao = MyGreenDaoApplication.getInstance().getDaoSession().getUserDao();
//        user = userDao.queryBuilder().where(UserDao.Properties.UserName.eq(userName)).unique();

        toolbar = view.findViewById(R.id.toolBar);
        viewPager = view.findViewById(R.id.viewPager);
        tabLayout = view.findViewById(R.id.tabLayout);


        preferences = getContext().getSharedPreferences("restDay", Context.MODE_PRIVATE);
        final float restDay = Float.valueOf(preferences.getString("restDay", "0"));
        //水球
        waterBallView = view.findViewById(R.id.waterBall);
        waterBallView.change(restDay);
        waterBallView.moveWaterLine();
        waterBallView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                waterBallView.change(restDay);
            }
        });


        //设置日历
        materialCalendarView = view.findViewById(R.id.material_calendar);

        materialCalendarView.setTopbarVisible(false);
        String today = DateUtil.getFormattedDate(System.currentTimeMillis());
        String[] time = today.split("-");
        CalendarDay day = CalendarDay.from(Integer.parseInt(time[0]), Integer.parseInt(time[1]), Integer.parseInt(time[2]));
        materialCalendarView.setSelectedDate(day);


        tvSearch = view.findViewById(R.id.tv_search);
        tvSearch.setOnClickListener(this);

        initToolbar();
        setHasOptionsMenu(true);
        return view;
    }

    private void initToolbar() {


        Resources res = getResources();
        toolbar.setTitle("");
//        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        titles = new ArrayList<>();
        fragmentList = new ArrayList<>();
        data = res.getStringArray(R.array.tab_lable);
        titles.add(data[0]);

        Bundle bundle = new Bundle();
        bundle.putString(USER_NAME, /*user.getUserName()*/userName);
        RecommendFragment recommendFragment = (RecommendFragment) Fragment.instantiate(getContext(), RecommendFragment.class.getName(), bundle);
        fragmentList.add(recommendFragment);
        for (int i = 1; i < data.length; i++) {
            titles.add(data[i]);
            Fragment common = new CommonFragment();
            fragmentList.add(common);
        }


        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getChildFragmentManager(),
                fragmentList, titles);
        viewPager.setAdapter(viewPagerAdapter);
        viewPager.setOffscreenPageLimit(titles.size());


//        magicIndicator=view.findViewById(R.id.magic_indicator);
//        magicIndicatorTitle=view.findViewById(R.id.magic_indicator_title);
//        MagicIndicatorUtil magicIndicatorUtil=new MagicIndicatorUtil(magicIndicator,magicIndicatorTitle,getContext(),titles,viewPager,2,20,3);
//        magicIndicatorUtil.initMagicIndicator();
//        magicIndicatorUtil.initMagicIndicatorTitle();
        viewPager.setOffscreenPageLimit(tabLayout.getTabCount());
//        将TabLayout和ViewPager关联起来。
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setTabGravity(Gravity.CENTER);
//        设置可以滑动
        tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);

    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.homepage_toolbar_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.tool_bar_menu:
                Intent intent = new Intent(getContext(), ToolBarMenu.class);
                startActivity(intent);
                break;
        }
        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_search:
                Intent intent = new Intent(getContext(), CommonSearch.class);
                startActivity(intent);
        }

    }
}
