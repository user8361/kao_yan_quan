package com.hzh.dell.tree.main_fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.hzh.dell.tree.R;
import com.hzh.dell.tree.adapter.ViewPagerAdapter;
import com.hzh.dell.tree.article.add_article.AddErrTi;
import com.hzh.dell.tree.article.add_article.AddVoteArticle;
import com.hzh.dell.tree.article.add_article.AddWordPicArticle;
import com.hzh.dell.tree.article.third_nav_tab_article.CommonArticleDisplay_Concerns;
import com.hzh.dell.tree.article.third_nav_tab_article.CommonArticleDisplay_Hot;
import com.hzh.dell.tree.article.third_nav_tab_article.CommonArticleDisplay_News;
import com.hzh.dell.tree.common_search.CommonSearch;
import com.hzh.dell.tree.utils.MagicIndicatorUtil;

import net.lucode.hackware.magicindicator.MagicIndicator;

import java.util.ArrayList;
import java.util.List;

//import com.hzh.dell.tree.article.third_nav_tab_article.CommonArticleDisplay_Local;
//import com.hzh.dell.tree.article.third_nav_tab_article.CommonArticleDisplay_News;

public class MainFragmentCommunity extends Fragment implements View.OnClickListener {
    private Toolbar toolbar;
    private List<Fragment> fragmentList;
    private List<String> titles;
    private ViewPager viewPager;
    //    private TabLayout tabLayout;
    private View view;
    private FloatingActionMenu fabMenu;
    private FloatingActionButton addWordFab, addVoteFab, addErrFab;
    private final String USER_NAME = "userName";
    private String userName;

    private MagicIndicator magicIndicator;
    private MagicIndicator magicIndicatorTitle;



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.main_fragment_community, container, false);

        Bundle bundle = getArguments();
        userName = bundle.getString(USER_NAME);
        initView();

        initToolbar();


        fabMenu.setClosedOnTouchOutside(true);
        addWordFab.setOnClickListener(this);
        addVoteFab.setOnClickListener(this);
        addErrFab.setOnClickListener(this);


        setHasOptionsMenu(true);
        return view;
    }

    private void initView() {
        toolbar = view.findViewById(R.id.toolBar);
        viewPager = view.findViewById(R.id.nav_community_viewPager);
        fabMenu = view.findViewById(R.id.fab_menu);
        addWordFab = view.findViewById(R.id.fab_add_word_article);
        addVoteFab = view.findViewById(R.id.fab_add_vote_article);
        addErrFab = view.findViewById(R.id.fab_add_err_ti);
        magicIndicator = view.findViewById(R.id.magic_indicator);
        magicIndicatorTitle = view.findViewById(R.id.magic_indicator_title);
    }

    private void initToolbar() {
        toolbar.setTitle("");
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        titles = new ArrayList<>();
        titles.add("最新");
        titles.add("热门");
        titles.add("关注");
        titles.add("本地");

        fragmentList = new ArrayList<>();
        Bundle b = new Bundle();
        b.putString(USER_NAME, userName);
        CommonArticleDisplay_News commonArticleDisplay_news = (CommonArticleDisplay_News) Fragment.instantiate(getContext(), CommonArticleDisplay_News.class.getName(), b);
        CommonArticleDisplay_Hot commonArticleDisplay_hot = (CommonArticleDisplay_Hot) Fragment.instantiate(getContext(), CommonArticleDisplay_Hot.class.getName(), b);
//        MyArticleMainFragmentList myArticleMainFragmentList = (MyArticleMainFragmentList) Fragment.instantiate(getContext(), MyArticleMainFragmentList.class.getName(), b);

        CommonArticleDisplay_Concerns commonArticleDisplay_concerns = (CommonArticleDisplay_Concerns) Fragment.instantiate(getContext(), CommonArticleDisplay_Concerns.class.getName(), b);
//        CommonArticleDisplay_Local commonArticleDisplay_local = (CommonArticleDisplay_Local) Fragment.instantiate(getContext(), CommonArticleDisplay_Local.class.getName(), b);


        fragmentList.add(commonArticleDisplay_news);
        fragmentList.add(new Fragment());
        fragmentList.add(commonArticleDisplay_concerns);

        fragmentList.add(new Fragment());
//        fragmentList.add(myArticleMainFragmentList);

//        fragmentList.add(commonArticleDisplay_local);


        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getChildFragmentManager(),
                fragmentList, titles);
        viewPager.setAdapter(viewPagerAdapter);
        viewPager.setOffscreenPageLimit(10);
        MagicIndicatorUtil magicIndicatorUtil =
                new MagicIndicatorUtil(
                        magicIndicator, magicIndicatorTitle,
                        getContext(), titles, viewPager,
                        2, 20, 3);


        magicIndicatorUtil.initMagicIndicator();
        magicIndicatorUtil.initMagicIndicatorTitle();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.community_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        String tip = "";
        switch (item.getItemId()) {
            case R.id.search:
                Intent intent = new Intent(getActivity(), CommonSearch.class);
                startActivity(intent);
                break;
            case R.id.email:
                tip = "email";
                break;
        }
        Toast.makeText(getContext(), tip, Toast.LENGTH_SHORT).show();
        return true;
    }

//    //    设置tab下面的下划线宽度
//    public void setIndicator(TabLayout tabs, int leftDip, int rightDip) {
//        Class<?> tabLayout = tabs.getClass();
//        Field tabStrip = null;
//        try {
//            tabStrip = tabLayout.getDeclaredField("slidingTabIndicator");//tabStrip是SlidingTabStrip的一个对象，SlidingTabStrip是继承LinearLayout的
//        } catch (NoSuchFieldException e) {
//            e.printStackTrace();
//        }
//
//        tabStrip.setAccessible(true);
//        LinearLayout llTab = null;
//        try {
//            llTab = (LinearLayout) tabStrip.get(tabs);
//        } catch (IllegalAccessException e) {
//            e.printStackTrace();
//        }
//
//        int left = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, leftDip, Resources.getSystem().getDisplayMetrics());
//        int right = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, rightDip, Resources.getSystem().getDisplayMetrics());
//
//        for (int i = 0; i < llTab.getChildCount(); i++) {
//            View child = llTab.getChildAt(i);
//            child.setPadding(0, 0, 0, 0);
//            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 1);
//            params.leftMargin = left;
//            params.rightMargin = right;
//            child.setLayoutParams(params);
//            child.invalidate();
//        }
//    }

    //步骤一：在onDestroyView方法内把Fragment的RootView从ViewPager中remove
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (null != view) {
            ((ViewGroup) view.getParent()).removeView(view);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.fab_add_word_article:
                Intent addWordArticleIntent = new Intent(getContext(), AddWordPicArticle.class);
                addWordArticleIntent.putExtra(USER_NAME, userName);
                startActivity(addWordArticleIntent);
                break;
            case R.id.fab_add_vote_article:
                Intent addVoteArticleIntent = new Intent(getContext(), AddVoteArticle.class);
                addVoteArticleIntent.putExtra(USER_NAME, userName);
                startActivity(addVoteArticleIntent);
                break;
            case R.id.fab_add_err_ti:
                Intent addErrIntent = new Intent(getContext(), AddErrTi.class);
                addErrIntent.putExtra(USER_NAME, userName);
                startActivity(addErrIntent);
                break;
        }

    }
}
