package com.hzh.dell.tree.main_fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hzh.dell.Bmob.entity.User;
import com.hzh.dell.Bmob.entity.UserRelation;
import com.hzh.dell.tree.R;
import com.hzh.dell.tree.person_fragment.change_info.MyDetail;
import com.hzh.dell.tree.person_fragment.concern_fan.MyConcern;
import com.hzh.dell.tree.person_fragment.err_ti.MyErrTiContent;
import com.hzh.dell.tree.person_fragment.my_article.MyArticle;
import com.hzh.dell.tree.person_fragment.my_strangeWord.StrangeWord;
import com.hzh.dell.tree.settings.Settings;
import com.hzh.dell.tree.utils.ImgUtils;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.header.ClassicsHeader;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.util.List;

import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.FindListener;


public class MainFragmentPerson extends Fragment implements View.OnClickListener {
    private int CHANGE_INFO = 0X99;
    private final String USER_NAME = "userName";
    private View view;
    private ImageView headImg;
    private RelativeLayout personDetail;
    private SmartRefreshLayout refreshLayout;
    private TextView tvUserName, tvMyArticle, tvMyConcernNum, tvMyFanNum, tvSettings,
            tvErrTi, tvStrangeWord;
    private LinearLayout concernLayout, fanLayout;

    private String userName = "";
    private User user;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.main_fragment_person, container, false);
        userName = getArguments().getString(USER_NAME);
        initView();


        return view;
    }


    private void initView() {
        tvUserName = view.findViewById(R.id.user_name);
        tvMyConcernNum = view.findViewById(R.id.tv_concern_num);
        tvMyFanNum = view.findViewById(R.id.tv_fun_num);
        tvMyArticle = view.findViewById(R.id.person_article);
        tvSettings = view.findViewById(R.id.settings);
        tvStrangeWord = view.findViewById(R.id.tv_strange_word);
        tvErrTi = view.findViewById(R.id.tv_err_ti);
        personDetail = view.findViewById(R.id.person_detail);
        headImg = view.findViewById(R.id.person_headImg);

        concernLayout = view.findViewById(R.id.concern_layout);
        fanLayout = view.findViewById(R.id.fun_layout);


        if (userName != null) {

            BmobQuery<User> userBmobQuery = new BmobQuery<>();
            userBmobQuery.addWhereEqualTo(USER_NAME, userName);
            userBmobQuery.findObjects(new FindListener<User>() {
                @Override
                public void done(List<User> list, BmobException e) {
                    if (list != null) {
                        user = list.get(0);
                        if (user != null) {
                            initClickListener();
                            tvUserName.setText(user.getUserName());
                            headImg.setImageBitmap(ImgUtils.stringToBitmap(user.getUserHeadImgPath()));
                            initConcernInfo();
                            refresh();
                        }

                    }
                }
            });
        }
    }

    private void initConcernInfo() {
        BmobQuery<UserRelation> userRelationBmobQuery = new BmobQuery<>();
        userRelationBmobQuery.addWhereEqualTo("fansId", user.getObjectId());
        userRelationBmobQuery.findObjects(new FindListener<UserRelation>() {
            @Override
            public void done(List<UserRelation> list, BmobException e) {
                if (list != null) {
                    tvMyConcernNum.setText(list.size() + "");
                }
            }
        });
        BmobQuery<UserRelation> userRelationBmobQuery1 = new BmobQuery<>();
        userRelationBmobQuery1.addWhereEqualTo("followedId", user.getObjectId());
        userRelationBmobQuery1.findObjects(new FindListener<UserRelation>() {
            @Override
            public void done(List<UserRelation> list, BmobException e) {
                if (list != null) {
                    tvMyFanNum.setText(list.size() + "");
                }
            }
        });

    }


    private void initClickListener() {
        tvMyArticle.setOnClickListener(this);
        concernLayout.setOnClickListener(this);
        fanLayout.setOnClickListener(this);
        tvErrTi.setOnClickListener(this);
        tvSettings.setOnClickListener(this);
        personDetail.setOnClickListener(this);
        tvStrangeWord.setOnClickListener(this);
    }

    private void refresh() {
        /**
         * 下拉刷新
         */
        refreshLayout = view.findViewById(R.id.person_smartRefresh);
        refreshLayout.setRefreshHeader(new ClassicsHeader(getContext()));
        refreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshlayout) {
                initView();
                refreshlayout.finishRefresh(2000/*,false*/);//传入false表示刷新失败
            }
        });

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.person_article://我的帖子
                Intent myArticleIntent = new Intent(getContext(), MyArticle.class);
                myArticleIntent.putExtra(USER_NAME, userName);
                startActivityForResult(myArticleIntent, CHANGE_INFO);
                break;
            case R.id.person_detail://详细信息
                Intent myDatilIntent = new Intent(getContext(), MyDetail.class);
                myDatilIntent.putExtra(USER_NAME, userName);
                startActivityForResult(myDatilIntent, CHANGE_INFO);
                break;
            case R.id.settings://设置
                Intent settingIntent = new Intent(getContext(), Settings.class);
                startActivity(settingIntent);
                break;
            case R.id.concern_layout:
                Intent myConcernIntent = new Intent(getContext(), MyConcern.class);
                myConcernIntent.putExtra(USER_NAME, userName);
                startActivity(myConcernIntent);
                break;
            case R.id.tv_err_ti:
                Intent myErrTiIntent = new Intent(getContext(), MyErrTiContent.class);
                myErrTiIntent.putExtra(USER_NAME, user.getUserName());
                startActivity(myErrTiIntent);
                break;
            case R.id.tv_strange_word:
                Intent strangeWordIntent = new Intent(getContext(), StrangeWord.class);
                strangeWordIntent.putExtra(USER_NAME, user.getUserName());
                startActivity(strangeWordIntent);
                break;
            default:
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CHANGE_INFO && resultCode == 0) {
            tvUserName.setText(user.getUserName());
            Log.e("result", "result");
            headImg.setImageBitmap(ImgUtils.stringToBitmap(user.getUserHeadImgPath()));
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        initView();
    }
}
