package com.hzh.dell.tree.main_fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.hzh.dell.tree.R;
import com.hzh.dell.tree.adapter.ViewPagerAdapter;
import com.hzh.dell.tree.bean.ListItemBean;
import com.hzh.dell.tree.target_fragment.CommonFragmentOne;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;

import java.util.ArrayList;
import java.util.List;

public class MainFragmentSchool extends Fragment {
    private SmartRefreshLayout smartRefreshLayout;
    private Toolbar toolbarOne;
    private TabLayout tabOne;
    private ViewPager viewPagerOne;
    private View view;
    private ListView mListView;
    private List<ListItemBean> listOne;
    private List<Fragment> fragmentListOne;
    private List<String> titleOne;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.main_fragment_school, container, false);
        initView();
        initData();
        return view;
    }

    private void initData() {
        listOne = new ArrayList<ListItemBean>();
        ListItemBean schoolInfoImg = new ListItemBean();
        schoolInfoImg.setImgId(R.drawable.school);
        titleOne = new ArrayList<>();
        titleOne.add("目标院校");
        titleOne.add("目标成绩");


        for (int i = 0; i < titleOne.size(); i++) {
            CommonFragmentOne commonOne = new CommonFragmentOne();
            fragmentListOne.add(commonOne);
        }

        ViewPagerAdapter viewPagerAdapterOne = new ViewPagerAdapter(getChildFragmentManager(),
                fragmentListOne, titleOne);

        viewPagerOne.setAdapter(viewPagerAdapterOne);


        tabOne.setupWithViewPager(viewPagerOne);


        tabOne.setTabGravity(Gravity.LEFT);


        tabOne.setTabMode(TabLayout.MODE_FIXED);

    }


    private void initView() {
        smartRefreshLayout = view.findViewById(R.id.smartRefresh);
        toolbarOne = view.findViewById(R.id.toolBar_one);

        tabOne = view.findViewById(R.id.tab_one);
        viewPagerOne = view.findViewById(R.id.viewpager_one);
        fragmentListOne = new ArrayList<>();
    }
}
