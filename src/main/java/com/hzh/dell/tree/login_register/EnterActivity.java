package com.hzh.dell.tree.login_register;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.hzh.dell.tree.HomepageActivity;
import com.hzh.dell.tree.R;
import com.hzh.dell.tree.complete_info.CompleteinfoActivity;
import com.hzh.dell.tree.database.MyGreenDaoApplication;
import com.hzh.dell.tree.entity.User;
import com.hzh.greendao.gen.UserDao;
import com.jaeger.library.StatusBarUtil;

import java.util.List;

import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.FindListener;


public class EnterActivity extends AppCompatActivity implements View.OnClickListener {
    private final int CODE = 0x110;
    private EditText editPerson, editPassword;//用户名密码的输入框
    private TextView textViewR;//跳转注册的按钮
    private Button btnLogin, btnFindpwd;//登录按钮,忘记密码按钮
    private String currentUserName, currentPassword;//用于加载完成后的用户名和密码
    private ImageView qq, wechat, weibo, guest;//qq 微信 微博登录按钮
    private final String USER_NAME = "userName";
    private String userName;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_enter);
        StatusBarUtil.setTranslucent(this, 50);//半透明
        initView();//初始化方法
    }

    private void initView() {


        btnLogin = findViewById(R.id.login_btn);
        btnFindpwd = findViewById(R.id.findpwd_btn);
        editPerson = findViewById(R.id.et_username);

        editPassword = findViewById(R.id.et_password);
        textViewR = findViewById(R.id.tv_register);
        qq = findViewById(R.id.iv_qq_login);
        wechat = findViewById(R.id.iv_wechat_login);
        weibo = findViewById(R.id.iv_weibo_login);
        guest = findViewById(R.id.iv_try_login);

        btnLogin.setOnClickListener(this);
        btnFindpwd.setOnClickListener(this);
        textViewR.setOnClickListener(this);
        qq.setOnClickListener(this);
        wechat.setOnClickListener(this);
        weibo.setOnClickListener(this);
        guest.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.login_btn:
                login();//登录
                break;
            case R.id.findpwd_btn:
                forgetPwd();//找回密码
                break;
            case R.id.tv_register:
                register();//注册
                break;
            case R.id.iv_qq_login:
            case R.id.iv_wechat_login:
            case R.id.iv_weibo_login:
                info();
                break;
            case R.id.iv_try_login:
                completeInfo();
                break;
        }
    }

    private void forgetPwd() {
        Intent intent = new Intent(this, FindpwdActivity.class);
        startActivity(intent);
    }

    private void completeInfo() {
        Intent intent = new Intent(this, CompleteinfoActivity.class);
        startActivity(intent);

    }

    private void info() {
        Toast.makeText(this, "第三方登录暂不可用", Toast.LENGTH_SHORT).show();
    }

    //注册事件
    private void register() {
        Intent intent = new Intent(this, RegisterActivity.class);
        startActivityForResult(intent, CODE);
    }

    //直接登录事件
    private void login() {
        currentUserName = editPerson.getText().toString().trim();//去除空格,获取用户名
        currentPassword = editPassword.getText().toString().trim();//去除空格,获取密码

        if (TextUtils.isEmpty(currentUserName)) {
            Toast.makeText(this, "用户名不能为空", Toast.LENGTH_SHORT).show();
        } else if (TextUtils.isEmpty(currentPassword)) {
            Toast.makeText(this, "密码不能为空", Toast.LENGTH_SHORT).show();
        } else {
            BmobQuery<com.hzh.dell.Bmob.entity.User> userBmobQuery = new BmobQuery<>();
            userBmobQuery.findObjects(new FindListener<com.hzh.dell.Bmob.entity.User>() {
                @Override
                public void done(List<com.hzh.dell.Bmob.entity.User> list, BmobException e) {
                    if (e == null) {
                        String name;
                        String password;
                        String phone;
                        for (com.hzh.dell.Bmob.entity.User u : list) {
                            name = u.getUserName();
                            phone = u.getUserPhone();
                            password = u.getUserPassword();
                            if (name.equals(currentUserName) || phone.equals(currentUserName)) {
                                if (password.equals(currentPassword)) {
                                    UserManager.getInstance().saveUserInfo(getApplicationContext(), name, password);
                                    Intent intent = new Intent(EnterActivity.this, HomepageActivity.class);
                                    intent.putExtra(USER_NAME, name);
                                    startActivity(intent);//跳转到主界面
                                    finish();//销毁当前界面
                                }
                            }
                        }
                    } else {
                        Log.e("err", e.toString());
                        toast("用户不存在！" + e.toString());
                    }
                }
            });
        }
    }

    private void toast(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CODE && resultCode == CODE) {
            userName = data.getStringExtra(USER_NAME);
            Log.e(USER_NAME, userName);
            editPerson.setText(userName);
        }
    }
}
