package com.hzh.dell.tree.login_register;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.hzh.dell.Bmob.entity.User;
import com.hzh.dell.tree.R;
import com.hzh.dell.tree.utils.ImgUtils;
import com.hzh.dell.tree.utils.NotificationUtils;
import com.hzh.dell.tree.utils.RandomCode;
import com.jaeger.library.StatusBarUtil;

import java.util.List;

import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.FindListener;
import cn.bmob.v3.listener.SaveListener;
import cn.bmob.v3.listener.UpdateListener;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener {

    private final int RESULT_CODE = 0x110;
    //标题栏两个按钮
    private ImageView ivReturn;//返回按钮
    private TextView tvEnter;//登陆按钮
    //注册框
    private EditText etPhoneNum;//手机号
    private Button btnSmsCode;//获取验证码按钮
    private EditText etSmsCode;//验证码
    private EditText etPassword;//密码
    private Button btnRegister;//注册按钮
    private String currentPhoneNum, currentPassword, currentSmsCode;//当前的手机号,短信验证码,密码
    private boolean flag = false;//设置是否获得验证码
    private String getCode;//获得的验证码
    //自动登录传值
    private final static String USER_NAME = "userName";
    private TimeLeft timeLeft;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_register);
        StatusBarUtil.setTranslucent(this, 50);//半透明

        init();//初始化
    }

    //初始化
    private void init() {

        ivReturn = findViewById(R.id.iv_return);
        tvEnter = findViewById(R.id.tv_enter);
        etPhoneNum = findViewById(R.id.et_phone_num);
        btnSmsCode = findViewById(R.id.btn_sms_code);
        etSmsCode = findViewById(R.id.et_sms_code);
        etPassword = findViewById(R.id.et_password);
        btnRegister = findViewById(R.id.register_btn);

        ivReturn.setOnClickListener(this);
        tvEnter.setOnClickListener(this);
        btnSmsCode.setOnClickListener(this);
        btnRegister.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_return://返回按钮
                onBackPressed();//返回夫界面
                break;
            case R.id.tv_enter://登陆按钮
                bmobLogin();
                break;
            case R.id.btn_sms_code:
                getCode();//获得验证码
                break;
            case R.id.register_btn:
                register();//注册
                break;
        }
    }

    //获取验证码，以通知（notification）方式显示
    private void getCode() {

        currentPhoneNum = etPhoneNum.getText().toString().trim();
        if (TextUtils.isEmpty(currentPhoneNum)) {
            Toast.makeText(this, "手机号不能为空", Toast.LENGTH_SHORT).show();
        } else if (currentPhoneNum.length() != 11) {
            Toast.makeText(this, "手机号格式不正确", Toast.LENGTH_SHORT).show();
        } else {
            timeLeft = new TimeLeft(60000, 1000);
            timeLeft.start();
            BmobQuery<User> userQuery = new BmobQuery<>();
            userQuery.addWhereEqualTo("userPhone", currentPhoneNum);
            userQuery.findObjects(new FindListener<com.hzh.dell.Bmob.entity.User>() {
                @Override
                public void done(List<com.hzh.dell.Bmob.entity.User> list, BmobException e) {
                    if (list != null) {//查询到
                        Log.e("List111", list.get(0).getUserName());
                        toast("用户已注册");
                        onBackPressed();

                    } else {//未查询到
                        getCode = RandomCode.getSixCode();
                        //获取验证码
                        NotificationUtils.setNotificationChannel(getApplicationContext());
                        NotificationUtils.show(getApplicationContext(), "考研圈·SIM1", "验证码：" + getCode, null);
                    }
                }
            });
        }
        flag = true;//获取过了验证码
    }

    //注册按钮，进行注册
    private void register() {
        currentPhoneNum = etPhoneNum.getText().toString().trim();//获取手机号
        currentPassword = etPassword.getText().toString().trim();//获取密码
        currentSmsCode = etSmsCode.getText().toString().trim();//获取验证码
        if (TextUtils.isEmpty(currentPhoneNum)) {
            Toast.makeText(this, "手机号不能为空", Toast.LENGTH_SHORT).show();
        } else if (TextUtils.isEmpty(currentSmsCode)) {
            Toast.makeText(this, "验证码不能为空", Toast.LENGTH_SHORT).show();
        } else if (!currentSmsCode.equals(getCode)) {
            Toast.makeText(this, "验证码不正确", Toast.LENGTH_SHORT).show();
        } else if (TextUtils.isEmpty(currentPassword)) {
            Toast.makeText(this, "密码不能为空", Toast.LENGTH_SHORT).show();
        } else if (currentPassword.length() < 6 || currentPassword.length() > 20) {
            Toast.makeText(this, "密码长度为6-20个字符", Toast.LENGTH_SHORT).show();
        } else {
            bmobAddUser(currentPhoneNum, currentPassword);

        }
    }

    /**
     * 跳转登录
     */
    private void bmobLogin() {
        Intent intent = new Intent(this, EnterActivity.class);
        startActivity(intent);
    }

    /**
     * bmob后端，添加用户
     *
     * @param userPhone
     * @param userPassword
     */
    private void bmobAddUser(String userPhone, String userPassword) {
        final com.hzh.dell.Bmob.entity.User u = new com.hzh.dell.Bmob.entity.User();
        Bitmap headImg = ((BitmapDrawable) getResources().getDrawable(R.drawable.default_head_img)).getBitmap();
        u.setUserName(userPhone);
        u.setUserPassword(userPassword);
        u.setUserGender("");
        u.setUserAddress("");
        u.setUserCurrentSchool("");
        u.setUserExamSubject("");
        u.setUserExamYear("");
        u.setUserHeadImgPath(ImgUtils.bitmapToString(headImg));
        u.setUserMajor("");
        u.setUserPhone(userPhone);
        u.setUserTargetSchool("");

        u.save(new SaveListener<String>() {
            @Override
            public void done(String s, BmobException e) {
                if (e == null) {
                    u.setUserId(s);
                    u.update(new UpdateListener() {
                        @Override
                        public void done(BmobException e) {
                            if (e == null) {
                                final Intent intent = new Intent(getApplicationContext(), EnterActivity.class);
                                intent.putExtra(USER_NAME, u.getUserName());
                                setResult(RESULT_CODE, intent);
                                toast("注册成功！");
                                onBackPressed();
                            }
                        }
                    });
                }
            }
        });
    }


    private void toast(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }


    //计时器类
    class TimeLeft extends CountDownTimer {

        /**
         * @param millisInFuture    The number of millis in the future from the call
         *                          to {@link #start()} until the countdown is done and {@link #onFinish()}
         *                          is called.
         * @param countDownInterval The interval along the way to receive
         *                          {@link #onTick(long)} callbacks.
         */
        public TimeLeft(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onTick(long millisUntilFinished) {
            btnSmsCode.setTextSize(8);
            btnSmsCode.setTextColor(Color.BLACK);
            btnSmsCode.setText(millisUntilFinished / 1000 + "s" + "后" + "重新获取验证码");
            btnSmsCode.setBackgroundColor(Color.parseColor("#ededed"));
            btnSmsCode.setClickable(false);
        }

        @Override
        public void onFinish() {
            btnSmsCode.setText("重新获取验证码");
            btnSmsCode.setTextSize(12);
            btnSmsCode.setTextColor(Color.WHITE);
            btnSmsCode.setBackgroundColor(Color.parseColor("#4EB84A"));
            btnSmsCode.setClickable(true);
        }
    }

}
