package com.hzh.dell.tree.login_register;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

/**
 * create by hzh
 * on 2019/4/13
 */
public class UserManager {
    private final String USER_NAME = "USER_NAME";
    private final String USER_PASSWORD = "USER_PASSWORD";
    private static UserManager instance;

    private UserManager() {
    }

    public static UserManager getInstance() {
        if (instance == null) {
            instance = new UserManager();
        }
        return instance;
    }

    /**
     * 保存自动登录的信息
     *
     * @param context
     * @param userName
     * @param userPassword
     */
    public void saveUserInfo(Context context, String userName, String userPassword) {
        //Context.MODE_PRIVATE表示只有应用自己可以访问到userInfo文件
        SharedPreferences sp = context.getSharedPreferences("userInfo", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(USER_NAME, userName);
        editor.putString(USER_PASSWORD, userPassword);
        editor.commit();
    }


    /**
     * 获取用户信息
     *
     * @param context
     * @return
     */
    public UserInfo getUserInfo(Context context) {
        SharedPreferences sp = context.getSharedPreferences("userInfo", Context.MODE_PRIVATE);
        UserInfo userInfo = new UserInfo();
        userInfo.setUserName(sp.getString(USER_NAME, null));
        userInfo.setUserPassword(sp.getString(USER_PASSWORD, null));
        return userInfo;
    }

    /**
     * 判断userInfo中是否有数据
     *
     * @param context
     * @return
     */
    public boolean hasUserInfo(Context context) {
        UserInfo userInfo = getUserInfo(context);
//        String userName=userInfo.getUserName();
//        String userPassword=userInfo.getUserPassword();
        if (userInfo != null) {
            if (!TextUtils.isEmpty(userInfo.getUserName()) && !TextUtils.isEmpty(userInfo.getUserPassword())) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    /**
     * 用户信息model
     */
    public class UserInfo {
        private String userName;
        private String userPassword;

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public String getUserPassword() {
            return userPassword;
        }

        public void setUserPassword(String userPassword) {
            this.userPassword = userPassword;
        }
    }
}
