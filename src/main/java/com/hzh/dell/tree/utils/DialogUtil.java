package com.hzh.dell.tree.utils;

import android.app.Dialog;
import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;

import com.hzh.dell.tree.R;
import com.scwang.smartrefresh.layout.util.DensityUtil;

/**
 * create by hzh
 * on 2019/5/15
 */
public class DialogUtil {
    private Context context;
    private View contentView;
    private Dialog dialog;

    public DialogUtil(Context context, View contentView, Dialog dialog) {
        this.context = context;
        this.contentView = contentView;
        this.dialog = dialog;
    }

    public void showTwoOption() {
        //设置dialog样式，
        dialog = new Dialog(context, R.style.BottomDialog);

        dialog.setContentView(contentView);

        ViewGroup.LayoutParams layoutParams = contentView.getLayoutParams();
        layoutParams.width = context.getResources().getDisplayMetrics().widthPixels - DensityUtil.dp2px(25f);
        contentView.setLayoutParams(layoutParams);
        dialog.getWindow().setGravity(Gravity.BOTTOM);
        dialog.getWindow().setWindowAnimations(R.style.BottomDialog_Animation);
        dialog.show();
    }

    public void showCenterDialog() {
        dialog = new Dialog(context, R.style.CenterDialog);
        dialog.setContentView(contentView);
        ViewGroup.LayoutParams layoutParams = contentView.getLayoutParams();
        layoutParams.width = context.getResources().getDisplayMetrics().widthPixels - DensityUtil.dp2px(50f);
        contentView.setLayoutParams(layoutParams);
        dialog.getWindow().setGravity(Gravity.CENTER);
        dialog.create();
        dialog.show();
    }

    public void dismissDialog() {
        dialog.dismiss();
    }


}
