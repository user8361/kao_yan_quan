package com.hzh.dell.tree.utils;

import android.content.Context;
import android.util.Log;

import com.hzh.dell.Bmob.entity.User;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import cn.bmob.v3.datatype.BmobFile;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.DownloadFileListener;
import cn.bmob.v3.listener.UploadBatchListener;
import cn.bmob.v3.listener.UploadFileListener;

/**
 * create by hzh
 * on 2019/5/9
 */
public class FileUtil {
    private Context context;
    private User user;
    private List<String> filePathList;
    private final static String TAG = "UPLOAD";


    public FileUtil(Context context, User user, List<String> filePathList) {
        this.context = context;
        this.user = user;
        this.filePathList = filePathList;
    }

    public void uploadFile(String path) {
        BmobFile file = new BmobFile(new File(path));
        file.uploadblock(new UploadFileListener() {
            @Override
            public void done(BmobException e) {
                logInfo(e);
            }
        });
    }

    public List<BmobFile> uploadFiles(List<String> filePathList) {
        final List<BmobFile> fileList = new ArrayList<>();
        for (String path : filePathList) {
            final BmobFile file = new BmobFile(new File(path));
            file.uploadblock(new UploadFileListener() {
                @Override
                public void done(BmobException e) {
                    if (e == null) {
                        fileList.add(file);
                    }
                    logInfo(e);
                }
            });
        }
        return fileList;
    }

    public List<BmobFile> uploadFilesWithProgress(List<String> filePathList) {
        final List<BmobFile> fileList = new ArrayList<>();
        String[] paths = new String[filePathList.size()];
        for (int i = 0; i < filePathList.size(); i++) {
            paths[i] = filePathList.get(i);
        }
        BmobFile.uploadBatch(paths, new UploadBatchListener() {
            @Override
            public void onSuccess(List<BmobFile> list, List<String> list1) {
                Log.e(TAG, list.size() + " - " + list1.size());
                fileList.addAll(list);
            }

            @Override
            public void onProgress(int i, int i1, int i2, int i3) {
                Log.e(TAG, " --> i:" + i + " i1:" + i1 + " i2:" + i2 + " i3:" + i3);
            }

            @Override
            public void onError(int i, String s) {
                Log.e(TAG, "第" + i + "个 上传失败");
            }
        });


        return fileList;
    }

    public void downLoadFile(String path) {
        BmobFile file = new BmobFile(new File(path));
        file.download(new DownloadFileListener() {
            @Override
            public void done(String s, BmobException e) {
                logInfo(e);
            }

            @Override
            public void onProgress(Integer integer, long l) {
                Log.e(TAG, "下载进度：" + integer + " long:" + l);
            }
        });
    }

    public void downloadFiles(List<String> filePathList) {
        for (String path : filePathList) {
            BmobFile file = new BmobFile(new File(path));
            file.download(new DownloadFileListener() {
                @Override
                public void done(String s, BmobException e) {
                    logInfo(e);
                }

                @Override
                public void onProgress(Integer integer, long l) {
                    Log.e(TAG, "下载进度：" + integer + " long:" + l);
                }
            });
        }
    }

    private void logInfo(BmobException e) {
        if (e == null) {
            Log.e(TAG, "upload success ");
        } else {
            Log.e(TAG, "upload failure , err info: " + e.toString());

        }
    }


}
