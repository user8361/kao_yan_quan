package com.hzh.dell.tree.utils;

import android.media.MediaPlayer;

import java.io.IOException;

/**
 * create by hzh
 * on 2019/4/27
 */
public class MediaUtil {
    public MediaPlayer mediaPlayer;

    public void play(String address) {
        mediaPlayer = new MediaPlayer();
        try {
            mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    mediaPlayer.release();
                    mediaPlayer = null;
                }
            });
            mediaPlayer.setDataSource(address);
            mediaPlayer.prepare();
            mediaPlayer.start();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
