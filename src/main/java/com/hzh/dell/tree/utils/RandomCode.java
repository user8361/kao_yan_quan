package com.hzh.dell.tree.utils;

/**
 * create by hzh
 * on 2019/4/9
 */
public class RandomCode {
    private static String code;

    public static String getFourCode() {
        code = String.valueOf((int) ((Math.random() * 9 + 1) * 1000));
        return code;
    }

    public static String getSixCode() {
        code = String.valueOf((int) ((Math.random() * 9 + 1) * 100000));
        return code;
    }
}
