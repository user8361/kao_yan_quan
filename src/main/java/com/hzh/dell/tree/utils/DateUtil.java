package com.hzh.dell.tree.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtil {

    // 2019-04-01
    public static String getFormattedDate(long timestamp) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");//定义格式化的格式
        return formatter.format(timestamp);//返回格式化后的数据

    }

    //12:00:00
    public static String getFormattedTime(long timestamp) {
//        timestamp = System.currentTimeMillis();//获得时间戳
        SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss");//定义格式化的格式
        return formatter.format(new Date(timestamp));//返回格式化后的数据
    }

    // 2019-04-01 12:00:00
    public static String getFormattedDateTime(long timestamp) {

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//定义格式化的格式
        return formatter.format(new Date(timestamp));//返回格式化后的数据
    }

    /**
     * 判断发表时间的长短
     *
     * @param createTime
     * @return
     */
    public static String getDayHourMinute(long createTime) {
        long difTimestamp = System.currentTimeMillis() - createTime;
        String time = "";
        if (difTimestamp / 1000 / 60 / 60 / 24 > 1) {
            time = DateUtil.getFormattedDate(createTime);//天
        } else if (difTimestamp / 1000 / 60 / 60 >= 1) {
            time = String.valueOf(difTimestamp / 1000 / 60 / 60) + "小时前";
        } else {
            time = String.valueOf(difTimestamp / 1000 / 60) + "分钟前";
        }
        return time;
    }

    /**
     * 返回1分钟 1小时 1天前的时间
     *
     * @param i
     * @return
     */
    public static long getDifTime(int i) {
        long now = System.currentTimeMillis();
        long result = 0;
        switch (i) {
            case 1://需要返回1分钟前的时间
                result = now - 60 * 1000;
                break;
            case 60://需要返回1小时前的时间
                result = now - 60 * 1000 * 60;
                break;
            case 24://需要返回24小时，一天前的时间
                result = now - 60 * 1000 * 60 * 24;
                break;
        }

        return result;
    }

    public static long getExamTimestamp(String yyyy_MM_dd) {
        //设置pattern
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        long timestamp = new Date().getTime();//初始化timestamp
        try {
            Date date = df.parse(yyyy_MM_dd);//转化日期为timestamp
            Calendar cal = Calendar.getInstance();//后的日历实例
            cal.setTime(date);//设置日历时间
            timestamp = cal.getTimeInMillis();//获得日历的时间戳
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return timestamp;//返回时间戳
    }

}
