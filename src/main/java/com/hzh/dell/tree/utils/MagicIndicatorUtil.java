package com.hzh.dell.tree.utils;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;

import com.hzh.dell.individualproj.view.ColorFlipPagerTitleView;
import com.hzh.dell.tree.R;

import net.lucode.hackware.magicindicator.MagicIndicator;
import net.lucode.hackware.magicindicator.ViewPagerHelper;
import net.lucode.hackware.magicindicator.buildins.UIUtil;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.CommonNavigator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.CommonNavigatorAdapter;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerTitleView;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.indicators.LinePagerIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.titles.SimplePagerTitleView;

import java.util.List;

/**
 * create by hzh
 * on 2019/4/20
 */
public class MagicIndicatorUtil {
    private MagicIndicator magicIndicator;
    private MagicIndicator magicIndicatorTitle;
    private Context mContext;
    private List<String> mDataList;
    private ViewPager mViewPager;
    private int lineHeight;
    private int lineWidth;
    private int roundRadius;


    /**
     * 带参构造函数
     *
     * @param magicIndicator      下划线
     * @param magicIndicatorTitle 标题
     * @param mContext            上下文
     * @param mDataList           标题数组
     * @param mViewPager          viewPager
     * @param lineHeight          线高
     * @param lineWidth           线宽
     * @param roundRadius         圆角
     */
    public MagicIndicatorUtil(MagicIndicator magicIndicator, MagicIndicator magicIndicatorTitle, Context mContext, List<String> mDataList, ViewPager mViewPager, int lineHeight, int lineWidth, int roundRadius) {
        this.magicIndicator = magicIndicator;
        this.magicIndicatorTitle = magicIndicatorTitle;
        this.mContext = mContext;
        this.mDataList = mDataList;
        this.mViewPager = mViewPager;
        this.lineHeight = lineHeight;
        this.lineWidth = lineWidth;
        this.roundRadius = roundRadius;
    }


    public void initMagicIndicator() {
        CommonNavigator commonNavigator = new CommonNavigator(mContext);
        commonNavigator.setScrollPivotX(0.65f);
        commonNavigator.setAdjustMode(true);
        commonNavigator.setAdapter(new CommonNavigatorAdapter() {
            @Override
            public int getCount() {
                return mDataList == null ? 0 : mDataList.size();
            }

            @Override
            public IPagerTitleView getTitleView(Context context, final int i) {
                SimplePagerTitleView simplePagerTitleView = new ColorFlipPagerTitleView(context);
                simplePagerTitleView.setText(mDataList.get(i));
                simplePagerTitleView.setNormalColor(ContextCompat.getColor(mContext, R.color.mainBlack));
                simplePagerTitleView.setSelectedColor(ContextCompat.getColor(mContext, R.color.colorThemeFill));
                simplePagerTitleView.setTextSize(18);
                simplePagerTitleView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mViewPager.setCurrentItem(i, false);
                    }
                });
                return simplePagerTitleView;
            }

            @Override
            public IPagerIndicator getIndicator(Context context) {
                LinePagerIndicator indicator = new LinePagerIndicator(context);
                indicator.setMode(LinePagerIndicator.MODE_EXACTLY);
                indicator.setLineHeight(UIUtil.dip2px(context, lineHeight));
                indicator.setLineWidth(UIUtil.dip2px(context, lineWidth));
                indicator.setRoundRadius(UIUtil.dip2px(context, roundRadius));
                indicator.setStartInterpolator(new AccelerateInterpolator());
                indicator.setEndInterpolator(new DecelerateInterpolator(2.0f));
                indicator.setColors(ContextCompat.getColor(mContext, R.color.colorThemeFill));
                return indicator;
            }
        });
        magicIndicator.setNavigator(commonNavigator);
        ViewPagerHelper.bind(magicIndicator, mViewPager);
    }

    public void initMagicIndicatorTitle() {
        CommonNavigator commonNavigator = new CommonNavigator(mContext);
        commonNavigator.setScrollPivotX(0.65f);
        commonNavigator.setAdjustMode(true);
        commonNavigator.setAdapter(new CommonNavigatorAdapter() {
            @Override
            public int getCount() {
                return mDataList == null ? 0 : mDataList.size();
            }

            @Override
            public IPagerTitleView getTitleView(Context context, final int index) {
                SimplePagerTitleView simplePagerTitleView = new ColorFlipPagerTitleView(context);
                simplePagerTitleView.setText(mDataList.get(index));
                simplePagerTitleView.setNormalColor(ContextCompat.getColor(mContext, R.color.mainBlack));
                simplePagerTitleView.setSelectedColor(ContextCompat.getColor(mContext, R.color.mainBlack));
                simplePagerTitleView.setTextSize(18);
                simplePagerTitleView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mViewPager.setCurrentItem(index, false);
                    }
                });
                return simplePagerTitleView;
            }

            @Override
            public IPagerIndicator getIndicator(Context context) {
                LinePagerIndicator indicator = new LinePagerIndicator(context);
                indicator.setMode(LinePagerIndicator.MODE_EXACTLY);
                indicator.setLineHeight(UIUtil.dip2px(context, lineHeight));
                indicator.setLineWidth(UIUtil.dip2px(context, lineWidth));
                indicator.setRoundRadius(UIUtil.dip2px(context, roundRadius));
                indicator.setStartInterpolator(new AccelerateInterpolator());
                indicator.setEndInterpolator(new DecelerateInterpolator(2.0f));
                indicator.setColors(ContextCompat.getColor(mContext, R.color.colorTheme));
                return indicator;
            }
        });
        magicIndicatorTitle.setNavigator(commonNavigator);
        ViewPagerHelper.bind(magicIndicatorTitle, mViewPager);
    }
}
