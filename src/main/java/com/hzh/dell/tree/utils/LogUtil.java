package com.hzh.dell.tree.utils;

import android.util.Log;

import cn.bmob.v3.exception.BmobException;

/**
 * create by hzh
 * on 2019/5/9
 */
public class LogUtil {

    private BmobException e;
    private String tag;

    public LogUtil(String tag, BmobException e) {
        this.e = e;
        this.tag = tag;
    }

    public void logSaveState() {
        if (e == null) {
            Log.e(tag, "保存成功");
        } else {
            Log.e(tag, "保存失败，错误信息：" + e.toString());
        }
    }


}
