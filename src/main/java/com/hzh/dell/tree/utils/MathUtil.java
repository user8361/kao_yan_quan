package com.hzh.dell.tree.utils;

/**
 * create by hzh
 * on 2019/4/27
 */
public class MathUtil {
    public static int min(int x, int y) {
        return x < y ? x : y;
    }

    public static int max(int x, int y) {
        return x > y ? x : y;
    }
}
