package com.hzh.dell.tree.adapter;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.hzh.dell.tree.R;
import com.hzh.dell.tree.utils.UriFileUtil;

import java.io.File;
import java.util.List;

/**
 * create by hzh
 * on 2019/5/14
 */
public class ListViewAdapterForFile extends BaseAdapter {
    private List<File> fileList;
    private Context context;
    private UriFileUtil uriFileUtil;

    public ListViewAdapterForFile(Context context, List<File> fileList) {
        this.fileList = fileList;
        this.context = context;
    }

    @Override
    public int getCount() {
        return fileList.size();
    }

    @Override
    public Object getItem(int position) {
        return fileList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = LayoutInflater.from(context).inflate(R.layout.upload_or_download_file_item, null);
            viewHolder.ivFileType = convertView.findViewById(R.id.iv_file_type);
            viewHolder.tvFileName = convertView.findViewById(R.id.tv_file_name);
            viewHolder.tvFileSize = convertView.findViewById(R.id.tv_file_size);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        final int currentPosition = position;
        File file = fileList.get(currentPosition);
        uriFileUtil = new UriFileUtil(context, Uri.fromFile(file));

        //获得文件的大小
        String size = uriFileUtil.formatFileSize(uriFileUtil.getFileSizes(file));
        String end = file.getName().substring(file.getName().lastIndexOf(".") + 1, file.getName().length()).toLowerCase();
        viewHolder.tvFileName.setText(file.getName());
        viewHolder.tvFileSize.setText(size);
        if (end.equals("jpg")) {
            viewHolder.ivFileType.setImageResource(R.mipmap.icon_office_jpg);


        } else if (end.equals("ppt") || end.equals("pptx")) {

            viewHolder.ivFileType.setImageResource(R.mipmap.icon_office_ppt);
        } else if (end.equals("gif")) {

            viewHolder.ivFileType.setImageResource(R.mipmap.icon_office_gif);
        } else if (end.equals("png")) {

            viewHolder.ivFileType.setImageResource(R.mipmap.icon_office_png);

        } else if (end.equals("bmp") || end.equals("jpeg") || end.equals("webp")) {

            viewHolder.ivFileType.setImageResource(R.mipmap.icon_office_pic);
        } else if (end.equals("doc")) {

            viewHolder.ivFileType.setImageResource(R.mipmap.icon_office_word);
        } else if (end.equals("xls") || end.equals("xlsx")) {

            viewHolder.ivFileType.setImageResource(R.mipmap.icon_office_excel);
        } else if (end.equals("pdf")) {

            viewHolder.ivFileType.setImageResource(R.mipmap.icon_office_pdf);
        } else if (end.equals("txt")) {

            viewHolder.ivFileType.setImageResource(R.mipmap.icon_office_txt);
        } else if (end.equals("zip")) {

            viewHolder.ivFileType.setImageResource(R.mipmap.icon_office_zip);
        } else if (end.equals("rar")) {

            viewHolder.ivFileType.setImageResource(R.mipmap.icon_office_rar);
        }

        return convertView;
    }

    class ViewHolder {
        private ImageView ivFileType;
        private TextView tvFileName, tvFileSize;
    }
}
