package com.hzh.dell.tree.adapter.recyclerview_adapter.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.hzh.dell.Bmob.entity.User;
import com.hzh.dell.Bmob.entity.UserWord;
import com.hzh.dell.Bmob.entity.Word;
import com.hzh.dell.tree.R;
import com.hzh.dell.tree.utils.MathUtil;

import java.util.List;

import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.FindListener;

/**
 * create by hzh
 * on 2019/4/26
 */
public class RecyclerListAdapterForWordItems extends RecyclerView.Adapter<RecyclerListAdapterForWordItems.ViewHolder> implements View.OnClickListener {
    private List<Word> wordList;
    private Context mContext;
    private LayoutInflater mInflater;
    private User user;

    public RecyclerListAdapterForWordItems(List<Word> wordList, Context mContext, LayoutInflater mInflater) {
        this.wordList = wordList;
        this.mContext = mContext;
        this.mInflater = mInflater;
    }

    public RecyclerListAdapterForWordItems(List<Word> wordList, Context mContext, LayoutInflater mInflater, User user) {
        this.wordList = wordList;
        this.mContext = mContext;
        this.mInflater = mInflater;
        this.user = user;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = mInflater.inflate(R.layout.homepage_search_word_detail_item, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, int i) {
        Word word = wordList.get(i);
        if (word != null) {
            viewHolder.tvWordKey.setText(word.getKey());
            viewHolder.tvWordKey.setTag(i);
            viewHolder.ivAddStrangeWord.setTag(i);

            if (user != null) {
                final BmobQuery<UserWord> userWordBmobQuery = new BmobQuery<>();
                userWordBmobQuery.addWhereEqualTo("userId", user.getObjectId());
                userWordBmobQuery.addWhereEqualTo("wordId", word.getObjectId());
                userWordBmobQuery.findObjects(new FindListener<UserWord>() {
                    @Override
                    public void done(List<UserWord> list, BmobException e) {
                        if (list != null) {
                            UserWord userWord = list.get(0);
                            if (userWord.getStrangeWordFlag() == 0) {
                                viewHolder.ivAddStrangeWord.setImageResource(R.mipmap.icon_add_to_strange_word);
                            } else {
                                viewHolder.ivAddStrangeWord.setImageResource(R.mipmap.icon_remove_from_strange_word);
                            }
                        }
                    }
                });
            }


            String acceptationText = "";
            String instanceText = "";


            if (isNotNull(word.getPsE())) {
                viewHolder.tvPsE.setText("英 " + "['" + word.getPsE() + "']");
                viewHolder.tvPsA.setText("美 " + "['" + word.getPsA() + "']");
                viewHolder.tvPsE.setTag(i);
                viewHolder.tvPsA.setTag(i);
            }


            if (isNotNull(word.getPos())) {
                for (int m = 0; m < MathUtil.min(word.getPos().size(), word.getAcceptationList().size()); ) {
                    acceptationText = acceptationText + word.getPos().get(m) + word.getAcceptationList().get(m) + "\n";
                    m++;
                }
                viewHolder.tvAcceptation.setText(acceptationText);
            }


            if (isNotNull(word.getSentTransList())) {
                for (int m = 1, n = 0; n < MathUtil.min(word.getSentOrigList().size(), word.getSentTransList().size()); m++) {
                    instanceText = instanceText + m + ". " + word.getSentOrigList().get(n) + "\n" + word.getSentTransList().get(n) + "\n";
                    n++;
                }
                viewHolder.tvInstance.setText(instanceText);
            }
        } else {
            Toast.makeText(mContext, "网络错误...", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public int getItemCount() {
        return wordList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvWordKey, tvPsE, tvPsA, tvAcceptation, tvInstance;
        private ImageView ivAddStrangeWord;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvWordKey = itemView.findViewById(R.id.tv_word_key);
            tvPsE = itemView.findViewById(R.id.tv_psE);
            tvPsA = itemView.findViewById(R.id.tv_psA);
            tvAcceptation = itemView.findViewById(R.id.tv_acceptation);
            tvInstance = itemView.findViewById(R.id.tv_instances);

            ivAddStrangeWord = itemView.findViewById(R.id.iv_add_strange_word);

            ivAddStrangeWord.setOnClickListener(RecyclerListAdapterForWordItems.this);
            tvPsE.setOnClickListener(RecyclerListAdapterForWordItems.this);
            tvPsA.setOnClickListener(RecyclerListAdapterForWordItems.this);

        }
    }

    //自定义一个回调接口实现Click
    public interface InnerItemOnClickListener {
        void itemClick(View v, int position);
    }

    //声明自定义的接口
    public InnerItemOnClickListener mInnerItemOnClickListener;

    //定义方法给外界使用
    public void setInnerItemOnClickListener(InnerItemOnClickListener listener) {
        this.mInnerItemOnClickListener = listener;
    }

    @Override
    public void onClick(View v) {
        if (v.getTag() != null) {
            int position = (int) v.getTag();
            mInnerItemOnClickListener.itemClick(v, position);
        }

    }

    private boolean isNotNull(Object o) {
        if (o == null) {
            return false;
        }
        return true;
    }
}
