package com.hzh.dell.tree.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.hzh.dell.Bmob.entity.Grade;
import com.hzh.dell.tree.R;

import java.util.List;

/**
 * create by hzh
 * on 2019/4/17
 */
public class GridViewShowGradeItemsAdapter extends BaseAdapter {
    private Context context;
    private LayoutInflater inflater;
    private List<Grade> gradeList;

    public GridViewShowGradeItemsAdapter(List<Grade> grades, Context context, LayoutInflater inflater) {
        this.gradeList = grades;
        this.context = context;
        this.inflater = inflater;
    }
    @Override
    public int getCount() {
        return gradeList.size();
    }
    @Override
    public Object getItem(int position) {
        return gradeList.get(position);
    }
    @Override
    public long getItemId(int position) {
        return position;
    }
    @SuppressLint("ResourceAsColor")
    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.target_grade_and_now_grade_items, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        Grade grade = gradeList.get(position);
        viewHolder.tvExamDate.setTag(position);
        viewHolder.tvExamDate.setText(gradeList.get(position).getExamTime());
        viewHolder.tvTargetGrade.setTag(position);
        viewHolder.tvTargetGrade.setText(grade.getExamTargetGrade().toString());
        viewHolder.tvNowGrade.setTag(position);
        viewHolder.tvNowGrade.setText(grade.getExamNowGrade().toString());
        viewHolder.tvDifGrade.setTag(position);
        viewHolder.tvDifGrade.setText(grade.getExamDifGrade().toString());
        if (gradeList.get(position).getExamDifGrade() > 0) {
            viewHolder.tvDifGrade.setTextColor(Color.parseColor("#339c5f"));
        } else {
            viewHolder.tvDifGrade.setTextColor(Color.parseColor("#ff3955"));
        }
        return convertView;
    }
    public class ViewHolder {
        public final TextView tvExamDate, tvTargetGrade, tvNowGrade, tvDifGrade;
        public final View root;

        public ViewHolder(View root) {
            this.tvExamDate = root.findViewById(R.id.tv_exam_date);
            this.tvTargetGrade = root.findViewById(R.id.tv_target_grade);
            this.tvNowGrade = root.findViewById(R.id.tv_now_grade);
            this.tvDifGrade = root.findViewById(R.id.tv_dif_grade);
            this.root = root;
        }
    }
}
