package com.hzh.dell.tree.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.hzh.dell.tree.R;


/**
 * ListView 适配器
 */
public class ListViewAdapter extends BaseAdapter {
    private Context context;//上下文对象
//    private List<String> dataList;//ListView显示的数据

    private String[] dataList;
    private ViewHolder viewHolder;

    /**
     * 构造器
     *
     * @param context
     * @param dataList
     */
    public ListViewAdapter(Context context, String[] dataList) {
        this.context = context;
        this.dataList = dataList;
    }

    @Override
    public int getCount() {
        return dataList == null ? 0 : dataList.length;
    }

    @Override
    public Object getItem(int position) {
        return dataList[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        //判断是否有缓存
        if (convertView == null) {
//            convertView=LayoutInflater.from(context).inflate(R.layout.listview_items,null);
            convertView = LayoutInflater.from(this.context).inflate(R.layout.listview_items, null);
            viewHolder = new ViewHolder(convertView, position);
            viewHolder.contentTV.setTextSize(15);

        } else {
            //得到缓存的布局
//            viewHolder= (ViewHolder) convertView.getTag();
//            Log.d("HUANCUN",viewHolder.contentTV.toString());
            viewHolder = new ViewHolder(convertView, position);

        }

        return convertView;
    }

    private class ViewHolder {
        private ImageView pictureImg;//图片
        private TextView contentTV;//内容

        /**
         * 构造器，创建对象时调用
         *
         * @param view 视图组件（ListView的子项视图）
         */
        ViewHolder(View view, int position) {
            pictureImg = view.findViewById(R.id.img_picture);
            pictureImg.setImageResource(R.drawable.icon_select);
            contentTV = view.findViewById(R.id.tv_content);
            contentTV.setText(dataList[position]);
            contentTV.setLines(1);
        }
    }
}
