package com.hzh.dell.tree.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.hzh.dell.tree.R;

import java.util.ArrayList;
import java.util.List;

/**
 * create by hzh
 * on 2019/4/17
 */
public class GridViewShowImgArticleDetailAdapter extends BaseAdapter implements View.OnClickListener {
    private List<String> urls;
    private Context context;
    private LayoutInflater inflater;
    private List<String> tmpList = new ArrayList<>();
    private InnerItemOnClickListener mListener;


    private int maxImages = 9;

    public int getMaxImages() {
        return maxImages;
    }

    public void setMaxImages(int maxImages) {
        this.maxImages = maxImages;
    }

    public GridViewShowImgArticleDetailAdapter(List<String> urls, Context context, LayoutInflater inflater) {
        this.urls = urls;
        this.context = context;
        this.inflater = inflater;
    }


    /**
     * 让gridview中的数据项目加1后，显示+号
     * 到达最大后不在显示加号
     *
     * @return 返回GridView中的数量
     */
    @Override
    public int getCount() {
        int count = (urls == null) ? 1 : urls.size() + 1;
        if (count >= maxImages) {
            return maxImages;
        }
        return count;
    }

    @Override
    public Object getItem(int position) {
        return urls.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        final ViewHolder viewHolder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.article_detail_show_word_pic_item, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        if (urls != null && position < urls.size()) {
            if (urls.size() == 1) {//如果只有示例图片，则不显示
                viewHolder.ivAddImg.setVisibility(View.GONE);
            }
            Glide.with(convertView).load(urls.get(position)).into(viewHolder.ivAddImg);
        } else if (position == urls.size()) {//如果position 等于 urls.size的大小则不显示最后一张图片（示例图）
            viewHolder.ivAddImg.setVisibility(View.GONE);
        }


        return convertView;
    }

    public class ViewHolder {
        public final ImageView ivAddImg;
        public final View root;

        public ViewHolder(View root) {
            this.ivAddImg = root.findViewById(R.id.iv_add_pic);
            this.root = root;
        }
    }

    public interface InnerItemOnClickListener {
        void itemClick(View v);
    }

    public void setOnInnerItemOnClickListener(InnerItemOnClickListener listener) {
        mListener = listener;
    }

    @Override
    public void onClick(View v) {
        mListener.itemClick(v);
    }
}
