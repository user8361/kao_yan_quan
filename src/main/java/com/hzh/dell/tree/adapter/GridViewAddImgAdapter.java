package com.hzh.dell.tree.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.hzh.dell.tree.R;

import java.util.ArrayList;
import java.util.List;

/**
 * create by hzh
 * on 2019/4/17
 */
public class GridViewAddImgAdapter extends BaseAdapter {
    private List<String> urls;
    private Context context;
    private LayoutInflater inflater;
    private List<String> tmpList = new ArrayList<>();
    private int tmp;
    private boolean flag = false;

    private int maxImages = 9;

    public int getMaxImages() {
        return maxImages;
    }

    public void setMaxImages(int maxImages) {
        this.maxImages = maxImages;
    }

    public GridViewAddImgAdapter(List<String> urls, Context context, LayoutInflater inflater) {
        this.urls = urls;
        this.context = context;
        this.inflater = inflater;


        tmp = tmpList.size();
        tmpList.addAll(urls);

        Log.d("tmpList.size()------->", tmpList.size() + "");
    }


    /**
     * 让gridview中的数据项目加1后，显示+号
     * 到达最大后不在显示加号
     *
     * @return 返回GridView中的数量
     */
    @Override
    public int getCount() {
        int count = (urls == null) ? 1 : urls.size() + 1;
        if (count >= maxImages) {
            return maxImages;
        }
        return count;
    }

    @Override
    public Object getItem(int position) {
        return urls.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.article_add_word_pic_item, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        if (!flag) {
            flag = true;
            tmpList.clear();
        }
        if (urls != null && position < urls.size()) {

            Glide.with(convertView).load("file://" + urls.get(position)).into(viewHolder.ivAddImg);
            viewHolder.ivAddImg.setVisibility(View.VISIBLE);
            viewHolder.ivDelImg.setVisibility(View.VISIBLE);
            viewHolder.ivDelImg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    mItemOnClickListener.itemOnClickListener(v);
                    urls.remove(position);
                    tmpList.clear();
                    tmpList.addAll(urls);
                    urls.clear();
                    ;
                    urls.addAll(tmpList);
                    notifyDataSetChanged();
                    Log.e("TAGTAGTAGTAG", "------->  删除数据后  " + tmpList.size());

                }

            });


        } else {
            Glide.with(context).load(R.mipmap.find_add_img).into(viewHolder.ivAddImg);
            if (position == 0) {
                viewHolder.ivAddImg.setVisibility(View.GONE);
            } else {
                viewHolder.ivAddImg.setScaleType(ImageView.ScaleType.FIT_XY);

            }
            viewHolder.ivDelImg.setVisibility(View.GONE);

        }


        return convertView;
    }

    public void notifyDataSetChanged(List<String> datas) {
        this.urls.addAll(datas);
        tmpList.clear();
        tmpList.addAll(urls);
        urls.clear();
        urls.addAll(tmpList);
        Log.e("TAGTAGTAGTAG", "------->  再次添加数据后  " + tmpList.size());
        this.notifyDataSetChanged();
    }

    private ItemOnClickListener mItemOnClickListener;

    public void setItemOnClickListener(ItemOnClickListener listener) {
        this.mItemOnClickListener = listener;
    }

    public interface ItemOnClickListener {
        /**
         * 传递点击的view
         *
         * @param view
         */
        public void itemOnClickListener(View view);
    }


    public class ViewHolder {
        public final ImageView ivAddImg;
        public final ImageView ivDelImg;
        public final View root;

        public ViewHolder(View root) {
            this.ivAddImg = root.findViewById(R.id.iv_add_pic);
            this.ivDelImg = root.findViewById(R.id.iv_del_pic);
            this.root = root;
        }
    }
}
