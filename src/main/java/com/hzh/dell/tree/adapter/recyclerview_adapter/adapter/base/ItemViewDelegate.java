package com.hzh.dell.tree.adapter.recyclerview_adapter.adapter.base;

/**
 * /**
 * create by hzh
 * on 2019/4/20
 */
public interface ItemViewDelegate<T> {

    int getItemViewLayoutId();

    boolean isForViewType(T item, int position);

    void convert(ViewHolder holder, T t, int position);

}