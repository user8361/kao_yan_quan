package com.hzh.dell.tree.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.hzh.dell.Bmob.entity.Plan;
import com.hzh.dell.tree.R;

import java.util.List;

//import com.hzh.dell.tree.entity.Plan;

/**
 * create by hzh
 * on 2019/5/2
 */
public class ListViewPlanAdapter extends BaseAdapter implements View.OnClickListener {
    private LayoutInflater inflater;
    private Context context;
    private ViewHolder viewHolder;
    private List<Plan> planList;
    private InnerItemOnClickListener mListener;

    public ListViewPlanAdapter(LayoutInflater inflater, Context context, List<Plan> planList) {
        this.inflater = inflater;
        this.context = context;
        this.planList = planList;
    }

    @Override
    public int getCount() {
        return planList.size();
    }

    @Override
    public Object getItem(int position) {
        return planList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = inflater.inflate(R.layout.homepage_everyday_plan_items, null);
            viewHolder.tvPlanContent = convertView.findViewById(R.id.tv_plan_content);
            viewHolder.tvTimeDuration = convertView.findViewById(R.id.tv_time_duration);
            viewHolder.ivColock = convertView.findViewById(R.id.iv_set_colock);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.tvPlanContent.setText(planList.get(position).getPlanContent());
        viewHolder.tvPlanContent.setTag(position);
        viewHolder.tvTimeDuration.setText(planList.get(position).getTimeDuration());
        viewHolder.tvTimeDuration.setTag(position);
        viewHolder.ivColock.setTag(position);

        viewHolder.tvPlanContent.setOnClickListener(this);
        viewHolder.tvTimeDuration.setOnClickListener(this);
        viewHolder.ivColock.setOnClickListener(this);


        return convertView;
    }

    final public static class ViewHolder {
        private TextView tvPlanContent, tvTimeDuration;
        private ImageView ivColock;
    }

    public interface InnerItemOnClickListener {
        void itemClick(View v);
    }

    public void setOnInnerItemOnClickListener(InnerItemOnClickListener listener) {
        mListener = listener;
    }

    @Override
    public void onClick(View v) {
        mListener.itemClick(v);
    }

}
