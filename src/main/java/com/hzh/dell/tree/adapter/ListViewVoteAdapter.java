package com.hzh.dell.tree.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;

import com.hzh.dell.tree.R;

import java.util.List;

/**
 * create by hzh
 * on 2019/5/3
 */
public class ListViewVoteAdapter extends BaseAdapter implements View.OnClickListener {
    private LayoutInflater inflater;
    private Context context;
    private List<String> datas;
    private InnerItemOnClickListener mListener;

    public ListViewVoteAdapter(LayoutInflater inflater, Context context, List<String> datas) {
        this.inflater = inflater;
        this.context = context;
        this.datas = datas;
    }

    @Override
    public int getCount() {
        return datas.size();
    }

    @Override
    public Object getItem(int position) {
        return datas.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = inflater.inflate(R.layout.article_add_vote_list_item, null);
            viewHolder.et_vote_add_item = convertView.findViewById(R.id.et_add_item);
            viewHolder.iv_vote_delete_item = convertView.findViewById(R.id.iv_delete_item);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.iv_vote_delete_item.setTag(position);
        if (datas.size() > 2) {
            viewHolder.iv_vote_delete_item.setVisibility(View.VISIBLE);
            viewHolder.iv_vote_delete_item.setOnClickListener(this);
        } else {
            viewHolder.iv_vote_delete_item.setVisibility(View.GONE);
        }


        viewHolder.et_vote_add_item.setTag(position);
        CharSequence ch = "选项" + (position + 1) + "（20字以内）";
        viewHolder.et_vote_add_item.setHint(ch);

        viewHolder.et_vote_add_item.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Log.e("vieview", v.getId() + "");
                return false;
            }
        });


        return convertView;
    }


    public class ViewHolder {
        private EditText et_vote_add_item;
        private ImageView iv_vote_delete_item;
    }

    public interface InnerItemOnClickListener {
        void itemClick(View v);
    }

    public void setOnInnerItemOnClickListener(InnerItemOnClickListener listener) {
        this.mListener = listener;
    }

    @Override
    public void onClick(View v) {
        mListener.itemClick(v);
    }
}
