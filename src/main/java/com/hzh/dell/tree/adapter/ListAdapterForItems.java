package com.hzh.dell.tree.adapter;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hzh.dell.Bmob.entity.Article;
import com.hzh.dell.Bmob.entity.Click;
import com.hzh.dell.Bmob.entity.User;
import com.hzh.dell.tree.R;
import com.hzh.dell.tree.article.show_article_detail.ArticleDetail;
import com.hzh.dell.tree.custom.MyGridView;
import com.hzh.dell.tree.utils.ImgUtils;

import java.util.ArrayList;
import java.util.List;

import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.datatype.BmobFile;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.FindListener;

public class ListAdapterForItems extends BaseAdapter implements View.OnClickListener {

    private Context mContext;
    private List<com.hzh.dell.Bmob.entity.Article> articleList;
    private InnerItemOnClickListener mListener;
    private LayoutInflater mInflater;
    private User articleUser;
    private User appUser;


    public ListAdapterForItems(Context mContext, List<Article> articleList, LayoutInflater inflater) {
        this.mContext = mContext;
        this.articleList = articleList;
        this.mInflater = inflater;

    }

    public ListAdapterForItems(Context mContext, List<Article> articleList, LayoutInflater inflater, User user) {
        this.mContext = mContext;
        this.articleList = articleList;
        this.mInflater = inflater;
        this.appUser = user;
    }

    @Override
    public int getCount() {
        return articleList.size();
    }

    @Override
    public Object getItem(int position) {
        return articleList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = LayoutInflater.from(mContext).inflate(R.layout.person_fragment_mine_article_list_items, null);
            viewHolder.ivHeadImg = convertView.findViewById(R.id.user_headImg);
            viewHolder.ivCollectImg = convertView.findViewById(R.id.collect_img);
            viewHolder.ivLikeImg = convertView.findViewById(R.id.like_img);
            viewHolder.ivMsgImg = convertView.findViewById(R.id.msg_img);

            viewHolder.tvUserName = convertView.findViewById(R.id.user_name);
            viewHolder.tvArticleTitle = convertView.findViewById(R.id.article_title);

            viewHolder.tvArticleContent = convertView.findViewById(R.id.article_content);
            viewHolder.gridView = convertView.findViewById(R.id.gridView);


            viewHolder.tvSubmitTime = convertView.findViewById(R.id.user_submitTime);
            viewHolder.tvArticleType = convertView.findViewById(R.id.article_type);
            viewHolder.tvCollectNum = convertView.findViewById(R.id.collect_text);
            viewHolder.tvLikeNum = convertView.findViewById(R.id.like_text);
            viewHolder.tvMsgNum = convertView.findViewById(R.id.msg_text);

            viewHolder.contentLayout = convertView.findViewById(R.id.content_layout);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        final int currentPosition = position;


        /**
         * 数据显示延迟  帖子用户
         */
        final Article thisArticle = articleList.get(currentPosition);
        BmobQuery<User> userBmobQuery = new BmobQuery<>();
        userBmobQuery.addWhereEqualTo("objectId", thisArticle.getAuthor().getObjectId());
        userBmobQuery.findObjects(new FindListener<User>() {
            @Override
            public void done(List<User> list, BmobException e) {
                if (e == null) {
                    if (list != null) {
                        articleUser = list.get(0);
                        //异步执行导致显示的慢
                        viewHolder.tvUserName.setText(articleUser.getUserName());
                        viewHolder.tvUserName.setTag(currentPosition);
                        viewHolder.ivHeadImg.setTag(currentPosition);
                        viewHolder.ivHeadImg.setImageBitmap(ImgUtils.stringToBitmap(articleUser.getUserHeadImgPath()));

                    }
                }
            }
        });


        /******/
        viewHolder.ivCollectImg.setTag(currentPosition);
        viewHolder.ivLikeImg.setTag(currentPosition);
        viewHolder.ivMsgImg.setTag(currentPosition);
        viewHolder.tvLikeNum.setTag(currentPosition);
        viewHolder.tvCollectNum.setTag(currentPosition);
        viewHolder.tvMsgNum.setTag(currentPosition);

        viewHolder.tvArticleTitle.setTag(currentPosition);
        viewHolder.tvArticleContent.setTag(currentPosition);
        viewHolder.tvSubmitTime.setTag(currentPosition);
        viewHolder.tvArticleType.setTag(currentPosition);
        viewHolder.contentLayout.setTag(currentPosition);


        BmobQuery<Click> clickBmobQuery = new BmobQuery<>();
        clickBmobQuery.addWhereEqualTo("userId",/*appUser.getObjectId()*/appUser.getObjectId());
        clickBmobQuery.addWhereEqualTo("itemId", thisArticle.getObjectId());//itemId此时未文章的id
        clickBmobQuery.findObjects(new FindListener<Click>() {
            @Override
            public void done(List<Click> list, BmobException e) {

                if (list != null) {
                    if (list.size() > 0) {//用户与本贴有关系
                        Click click = list.get(0);
                        if (click.getLikeState() == 0) {
                            viewHolder.ivLikeImg.setImageResource(R.mipmap.heart_gray);
                        } else {
                            viewHolder.ivLikeImg.setImageResource(R.mipmap.heart_pink);
                        }
                        if (click.getCollectState() == 0) {
                            viewHolder.ivCollectImg.setImageResource(R.mipmap.collect_gray);
                        } else {
                            viewHolder.ivCollectImg.setImageResource(R.mipmap.collect_pink);
                        }
                    } else {//用户与本贴无关
                        viewHolder.ivCollectImg.setImageResource(R.mipmap.collect_gray);
                        viewHolder.ivLikeImg.setImageResource(R.mipmap.heart_gray);
                    }
                } else {//用户与本贴无关
                    viewHolder.ivCollectImg.setImageResource(R.mipmap.collect_gray);
                    viewHolder.ivLikeImg.setImageResource(R.mipmap.heart_gray);
                }
                viewHolder.ivMsgImg.setImageResource(R.mipmap.message);
                viewHolder.tvArticleTitle.setText(articleList.get(currentPosition).getArticleTitle());
                viewHolder.tvArticleContent.setText(articleList.get(currentPosition).getArticleContent());
                viewHolder.tvSubmitTime.setText(articleList.get(currentPosition).getCreatedAt().substring(0, 10));
                viewHolder.tvArticleType.setText(articleList.get(currentPosition).getArticleTheme());
                viewHolder.tvCollectNum.setText(articleList.get(currentPosition).getCollectNum() + "");
                viewHolder.tvLikeNum.setText(articleList.get(currentPosition).getLikeNum() + "");
                viewHolder.tvMsgNum.setText(articleList.get(currentPosition).getCommentNum() + "");


                List<BmobFile> imgFile = thisArticle.getArticleImgFile();


                if (imgFile != null) {
                    if (imgFile.size() == 0) {
                        viewHolder.gridView.setVisibility(View.GONE);
                    } else {
                        List<String> urlList = new ArrayList<>();
                        for (BmobFile file : imgFile) {
                            urlList.add(file.getFileUrl());
                        }
                        viewHolder.gridView.setTag(currentPosition);

                        final int urlTag = (int) viewHolder.gridView.getTag();
                        if (!TextUtils.isEmpty(String.valueOf(urlTag)) && urlTag == currentPosition) {
                            GridViewShowImgAdapter adapter = new GridViewShowImgAdapter(urlList, mContext, mInflater);
                            viewHolder.gridView.setAdapter(adapter);//设置数据源
                            viewHolder.gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                    Intent detail = new Intent(mContext, ArticleDetail.class);
                                    detail.putExtra("articleId", articleList.get(currentPosition).getArticleId());
                                    detail.putExtra("userName", articleList.get(currentPosition).getAuthor().getUserName());
                                    mContext.startActivity(detail);
                                }
                            });
                        }

                    }
                }
            }
        });






        viewHolder.ivHeadImg.setOnClickListener(this);
        viewHolder.ivCollectImg.setOnClickListener(this);
        viewHolder.ivLikeImg.setOnClickListener(this);
        viewHolder.ivMsgImg.setOnClickListener(this);
        viewHolder.tvUserName.setOnClickListener(this);
        viewHolder.tvSubmitTime.setOnClickListener(this);
        viewHolder.tvArticleType.setOnClickListener(this);
        viewHolder.tvCollectNum.setOnClickListener(this);
        viewHolder.tvLikeNum.setOnClickListener(this);
        viewHolder.tvMsgNum.setOnClickListener(this);
        viewHolder.contentLayout.setOnClickListener(this);

        return convertView;
    }


    public final static class ViewHolder {
        ImageView ivHeadImg, ivCollectImg, ivLikeImg, ivMsgImg;
        TextView tvUserName, tvArticleTitle, tvArticleContent, tvSubmitTime, tvArticleType, tvCollectNum, tvLikeNum, tvMsgNum;
        LinearLayout contentLayout;
        MyGridView gridView;
    }


    public interface InnerItemOnClickListener {
        void itemClick(View v);
    }

    public void setOnInnerItemOnClickListener(InnerItemOnClickListener listener) {
        mListener = listener;
    }

    @Override
    public void onClick(View v) {
        mListener.itemClick(v);
    }
}
