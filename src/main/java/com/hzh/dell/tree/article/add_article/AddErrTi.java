package com.hzh.dell.tree.article.add_article;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.hzh.dell.Bmob.entity.ErrTi;
import com.hzh.dell.Bmob.entity.User;
import com.hzh.dell.tree.R;
import com.hzh.dell.tree.adapter.GridViewAddErrTiImgAdapter;
import com.hzh.dell.tree.utils.GlideLoadEngineUtil;
import com.jaeger.library.StatusBarUtil;
import com.zhihu.matisse.Matisse;
import com.zhihu.matisse.MimeType;
import com.zhihu.matisse.filter.Filter;
import com.zhihu.matisse.internal.entity.CaptureStrategy;
import com.zhihu.matisse.internal.entity.IncapableCause;
import com.zhihu.matisse.internal.entity.Item;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.datatype.BmobFile;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.FindListener;
import cn.bmob.v3.listener.SaveListener;
import cn.bmob.v3.listener.UploadBatchListener;

/**
 * create by hzh
 * on 2019/5/8
 */
public class AddErrTi extends Activity implements View.OnClickListener {
    private ImageView ivReturn;
    private TextView tvSave;
    private EditText etErrTiSubject, etErrTiKey;
    private GridView gridView;
    private final static String USER_NAME = "userName";
    private String userName = "";
    private User user;
    private GridViewAddErrTiImgAdapter addErrTiImgAdapter;
    private List<String> pathList;
    private int deletePosition;
    private List<String> tmpPathList = new ArrayList<>();
    private int tmpCount;
    private int num;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_err_ti);
        StatusBarUtil.setTranslucent(this, 50);//半透明

        initView();

    }


    private void initView() {
        pathList = new ArrayList<>();
        ivReturn = findViewById(R.id.iv_return);
        tvSave = findViewById(R.id.tv_function);
        etErrTiSubject = findViewById(R.id.et_err_ti_subject);
        etErrTiKey = findViewById(R.id.et_err_ti_key);
        gridView = findViewById(R.id.gridView);

        userName = getIntent().getStringExtra(USER_NAME);
        BmobQuery<User> userBmobQuery = new BmobQuery<>();
        userBmobQuery.addWhereEqualTo(USER_NAME, userName);
        userBmobQuery.findObjects(new FindListener<User>() {
            @Override
            public void done(List<User> list, BmobException e) {
                if (list != null) {
                    user = list.get(0);
                    if (user != null) {
                        initGridView();
                        initGridView();
                        initClickEvent();
                    }
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_return:
                onBackPressed();
                break;
            case R.id.tv_function:
                saveErrTi();
                break;
        }

    }

    private void saveErrTi() {
        final ErrTi errTi = new ErrTi();
        String subject = etErrTiSubject.getText().toString();
        String key = etErrTiKey.getText().toString();
        if (subject.length() < 2) {
            toast("科目至少输入2个字符");
        } else if (key.length() < 4) {
            toast("关键字至少输入4个字符");
        } else {
            errTi.setErrTiTitle(subject);
            errTi.setErrTiKey(key);
            errTi.setUserId(user.getObjectId());
            if (tmpPathList.size() > 0) {
                final String[] imgPath = new String[tmpPathList.size()];
                for (int i = 0; i < tmpPathList.size(); i++) {
                    imgPath[i] = tmpPathList.get(i);
                }

                final ProgressDialog dialog = new ProgressDialog(this);
                dialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                dialog.setMax(100);
                dialog.setTitle("发布中,请稍等...");
                dialog.setCanceledOnTouchOutside(true);
                dialog.show();

                BmobFile.uploadBatch(imgPath, new UploadBatchListener() {
                    @Override
                    public void onSuccess(List<BmobFile> list, List<String> list1) {
                        if (list.size() == imgPath.length) {

                            errTi.setImgList(list);
                            errTi.save(new SaveListener<String>() {
                                @Override
                                public void done(String s, BmobException e) {
                                    if (e == null) {
                                        toast("保存成功");
                                        dialog.dismiss();
                                        onBackPressed();
                                    } else {
                                        toast("保存失败 , 错误信息" + e.toString());

                                    }
                                }
                            });
                        }

                    }

                    @Override
                    public void onProgress(int i, int i1, int i2, int i3) {
                        dialog.setProgress(i3);
                    }

                    @Override
                    public void onError(int i, String s) {

                    }
                });
            } else {
                errTi.save(new SaveListener<String>() {
                    @Override
                    public void done(String s, BmobException e) {
                        if (e == null) {
                            toast("保存成功");
                            onBackPressed();
                        } else {
                            toast("保存失败 , 错误信息" + e.toString());

                        }
                    }
                });
            }


        }


    }

    private void toast(String s) {
        Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
    }


    private void initClickEvent() {
        ivReturn.setOnClickListener(this);
        tvSave.setOnClickListener(this);
    }

    private void initGridView() {
        addErrTiImgAdapter = new GridViewAddErrTiImgAdapter(pathList, getApplicationContext(), getLayoutInflater());
        gridView.setAdapter(addErrTiImgAdapter);
        onClick();
    }

    private void addPic() {
        num = 9 - tmpCount;
        if (tmpPathList.size() > 0) {
            if (num > 0) {
                useMatisse(num, 2);//调用添加图片的方法，第二次添加
            } else {
                toast("请先删除需要替换掉的图片!");
            }
        } else {
            useMatisse(9, 1);//调用添加图片的方法，第一次添加
        }
    }

    /**
     * 调用Matisse第三方库添加图片
     *
     * @param num         可以选择的图片数目
     * @param requestCode 判断是不是第一次添加图片
     */
    public void useMatisse(int num, int requestCode) {
        Matisse.from(this).choose(MimeType.ofImage(), false)
                .countable(true)
                .capture(true)
                .captureStrategy(new CaptureStrategy(true, "com.hzh.dell.tree.article.fileprovider"))
                .maxSelectable(num)
                .addFilter(new Filter() {
                    @Override
                    protected Set<MimeType> constraintTypes() {
                        return new HashSet<MimeType>() {
                            {
                                add(MimeType.PNG);
                            }
                        };
                    }

                    @Override
                    public IncapableCause filter(Context context, Item item) {
                        try {

                            InputStream inputStream = getContentResolver().openInputStream(item.getContentUri());
                            BitmapFactory.Options options = new BitmapFactory.Options();
                            options.inJustDecodeBounds = true;
                            BitmapFactory.decodeStream(inputStream, null, options);

                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }
                        return null;
                    }
                })
                .originalEnable(true)
                .maxOriginalSize(10)
                .theme(R.style.Matisse_Dracula)//添加主题样式
                .gridExpectedSize((int) getResources().getDimension(R.dimen.grid_expected_size))
                .restrictOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT)
                .thumbnailScale(0.87f)
                .imageEngine(new GlideLoadEngineUtil())
                .forResult(requestCode);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode == RESULT_OK) {
            List<String> pathList = Matisse.obtainPathResult(data);
            tmpPathList.addAll(pathList);
            tmpCount = tmpPathList.size();
            addErrTiImgAdapter = new GridViewAddErrTiImgAdapter(pathList, this, getLayoutInflater());
            gridView.setAdapter(addErrTiImgAdapter);
            onClick();//判断首次添加数据后删除数据

        } else if (requestCode == 2 && resultCode == RESULT_OK) {

            List<String> pathList2 = Matisse.obtainPathResult(data);
            tmpPathList.addAll(pathList2);
            tmpCount = tmpPathList.size();
            addErrTiImgAdapter.notifyDataSetChanged(pathList2);
            onClick();//判断删除数据

        }
    }

    //点击内部ite的动作
    public void onClick() {
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position == parent.getChildCount() - 1) {
                    addPic();
                } else {
                    deletePosition = position;
                }
            }
        });

        addErrTiImgAdapter.setItemOnClickListener(new GridViewAddErrTiImgAdapter.ItemOnClickListener() {
            //
            @Override
            public void itemOnClickListener(View view) {
                switch (view.getId()) {
                    case R.id.iv_del_pic:
                        tmpPathList.remove(deletePosition);
                        tmpCount = tmpPathList.size();
                        break;
                }
            }
        });
    }

}
