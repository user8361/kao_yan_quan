package com.hzh.dell.tree.article.add_article;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.hzh.dell.tree.R;
import com.hzh.dell.tree.adapter.ListViewVoteAdapter;
import com.hzh.dell.tree.custom.DrawableCenterTextView;
import com.jaeger.library.StatusBarUtil;
import com.kyleduo.switchbutton.SwitchButton;

import java.util.ArrayList;
import java.util.List;

/**
 * create by hzh
 * on 2019/5/3
 */
public class AddVoteArticle extends AppCompatActivity implements View.OnClickListener {
    private ImageView ivReturn, ivSubNum, ivAddNum;
    private TextView tvSent, tvSelectNum;
    private ListView listView;
    private DrawableCenterTextView tvAddNewItem;
    private SwitchButton switchButton;
    private LinearLayout llSelectNum;
    private ListViewVoteAdapter adapter;
    private List<String> list;
    private int num;

    private boolean isMultiCheckable = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.article_add_vote);
        StatusBarUtil.setTranslucent(this, 50);//半透明

        initView();
        initData();
    }

    private void initView() {
        ivReturn = findViewById(R.id.iv_return);
        ivSubNum = findViewById(R.id.iv_sub_num);
        ivAddNum = findViewById(R.id.iv_add_num);
        tvSent = findViewById(R.id.tv_sent);
        tvSelectNum = findViewById(R.id.tv_select_num);
        listView = findViewById(R.id.listView);

        tvAddNewItem = findViewById(R.id.tv_add_new_item);
        switchButton = findViewById(R.id.btn_switch);
        llSelectNum = findViewById(R.id.ll_select_num);
        llSelectNum.setVisibility(View.GONE);

        ivReturn.setOnClickListener(this);
        tvSent.setOnClickListener(this);
        tvAddNewItem.setOnClickListener(this);
        ivSubNum.setOnClickListener(this);
        ivAddNum.setOnClickListener(this);
        switchButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    isMultiCheckable = true;
                    llSelectNum.setVisibility(View.VISIBLE);
                } else {
                    isMultiCheckable = false;
                    llSelectNum.setVisibility(View.GONE);
                }
            }
        });
    }

    private void initData() {
        list = new ArrayList<>();
        list.add("");
        list.add("");
        adapter = new ListViewVoteAdapter(getLayoutInflater(), getApplicationContext(), list);
        listView.setAdapter(adapter);
        adapter.setOnInnerItemOnClickListener(new ListViewVoteAdapter.InnerItemOnClickListener() {
            @Override
            public void itemClick(View v) {
                num = Integer.valueOf(tvSelectNum.getText().toString());
                if (num == list.size()) {
                    num--;
                    tvSelectNum.setText(String.valueOf(num));
                }
                int position = (int) v.getTag();
                list.remove(position);
                adapter.notifyDataSetChanged();
                if (list.size() < 10) {
                    tvAddNewItem.setVisibility(View.VISIBLE);
                }
            }
        });
    }


    @Override
    public void onClick(View v) {
        num = Integer.valueOf(tvSelectNum.getText().toString());
        switch (v.getId()) {
            case R.id.iv_return:
                onBackPressed();
                break;
            case R.id.tv_add_new_item:
                addNewItem();
                break;

            case R.id.btn_switch:
                if (switchButton.isChecked()) {
                    llSelectNum.setVisibility(View.VISIBLE);
                } else {
                    llSelectNum.setVisibility(View.GONE);
                }
                break;

            case R.id.iv_sub_num:

                if (list.size() > 2 && num > 2) {
                    num--;
                    tvSelectNum.setText(String.valueOf(num));
                }
                break;
            case R.id.iv_add_num:
                if (num < list.size()) {
                    num++;
                    tvSelectNum.setText(String.valueOf(num));
                }
                break;
            case R.id.tv_sent:
                String str = "";
                for (int i = 0; i < list.size(); i++) {

                }
                break;
            case R.id.et_add_item:
                Log.e("item", "click");
                break;
        }
    }

    //添加子项
    private void addNewItem() {
        list.add("hello");
        adapter.notifyDataSetChanged();

        if (list.size() >= 10) {
            tvAddNewItem.setVisibility(View.GONE);
        }
    }
}
