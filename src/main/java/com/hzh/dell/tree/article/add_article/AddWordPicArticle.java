package com.hzh.dell.tree.article.add_article;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.numberprogressbar.NumberProgressBar;
import com.hzh.dell.Bmob.entity.Article;
import com.hzh.dell.Bmob.entity.Click;
import com.hzh.dell.Bmob.entity.User;
import com.hzh.dell.tree.R;
import com.hzh.dell.tree.adapter.GridViewAddImgAdapter;
import com.hzh.dell.tree.adapter.ListViewAdapterForFile;
import com.hzh.dell.tree.custom.MyListView;
import com.hzh.dell.tree.utils.DialogUtil;
import com.hzh.dell.tree.utils.GlideLoadEngineUtil;
import com.hzh.dell.tree.utils.ListViewShowAllUtil;
import com.hzh.dell.tree.utils.UriFileUtil;
import com.jaeger.library.StatusBarUtil;
import com.zhihu.matisse.Matisse;
import com.zhihu.matisse.MimeType;
import com.zhihu.matisse.filter.Filter;
import com.zhihu.matisse.internal.entity.CaptureStrategy;
import com.zhihu.matisse.internal.entity.IncapableCause;
import com.zhihu.matisse.internal.entity.Item;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.datatype.BmobFile;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.FindListener;
import cn.bmob.v3.listener.SaveListener;
import cn.bmob.v3.listener.UploadBatchListener;

/**
 * create by hzh
 * on 2019/4/12
 */
public class AddWordPicArticle extends AppCompatActivity implements View.OnClickListener {
    private final static int ZERO = 0;
    private final static int GET_FILE_FROM_CATEGORY = 0x111;
    private UriFileUtil uriFileUtil;
    private DialogUtil dialogUtil;
    private final Dialog uploadDialog = null;



    private ImageView ivReturn, ivPic, ivFile, ivFriend, ivLink, ivLook;
    private TextView tvSent, tvAddType;
    private GridView gridView;
    private ImageView imageView;
    private EditText etTitle, etContent;
    private Intent getInfoIntent;
    private final String USER_NAME = "userName";
    private String userName = "";

    private GridViewAddImgAdapter gridViewAdapter;
    private List<String> tmpPathList = new ArrayList<>();
    private int num;
    private int tmpCount;
    private int deletePosition;
    private MyListView fileListView;
    private ListViewAdapterForFile adapterForFile;
    private List<File> fileList;

    private com.hzh.dell.Bmob.entity.User user = new com.hzh.dell.Bmob.entity.User();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.article_add_word_pic);
        StatusBarUtil.setTranslucent(this, 50);//半透明

        initView();
    }

    private void initView() {

        getInfoIntent = getIntent();
        userName = getInfoIntent.getStringExtra(USER_NAME);
        BmobQuery<User> userBmobQuery = new BmobQuery<>();
        userBmobQuery.addWhereEqualTo(USER_NAME, userName);
        userBmobQuery.findObjects(new FindListener<User>() {
            @Override
            public void done(List<User> list, BmobException e) {
                if (list != null) {
                    if (list.size() > 0) {
                        user = list.get(0);
                        initClickListener();
                    }
                }

            }
        });

        ivReturn = findViewById(R.id.iv_return);
        ivPic = findViewById(R.id.article_add_picture);
        ivFile = findViewById(R.id.article_add_file);
        ivFriend = findViewById(R.id.article_add_friend);
        ivLink = findViewById(R.id.add_article_link);
        ivLook = findViewById(R.id.article_add_look);

        fileListView = findViewById(R.id.fileListView);

        gridView = findViewById(R.id.gridView);
        imageView = findViewById(R.id.imageView);

        tvSent = findViewById(R.id.tv_sent);
        tvAddType = findViewById(R.id.article_add_type);

        etTitle = findViewById(R.id.article_title);
        etContent = findViewById(R.id.article_content);

        ivReturn.setOnClickListener(this);


    }

    private void initClickListener() {
        fileList = new ArrayList<>();
        adapterForFile = new ListViewAdapterForFile(getApplicationContext(), fileList);
        fileListView.setAdapter(adapterForFile);
        ListViewShowAllUtil.setListViewHeightBasedOnChildren(fileListView);
        ivPic.setOnClickListener(this);
        ivFile.setOnClickListener(this);
        ivFriend.setOnClickListener(this);
        ivLink.setOnClickListener(this);
        ivLook.setOnClickListener(this);

        tvSent.setOnClickListener(this);
        tvAddType.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        String etTitleText = etTitle.getText().toString();
        String etContentText = etContent.getText().toString();

        switch (v.getId()) {
            case R.id.iv_return:
                if (etTitleText != null || etContentText != null) {
                    Toast.makeText(this, "已保存到草稿", Toast.LENGTH_SHORT).show();
                }
                onBackPressed();
                break;
            case R.id.tv_sent:
                if (etTitleText.length() < 4 || etContentText.length() < 4) {
                    Toast.makeText(this, "标题、内容至少包含4个字符", Toast.LENGTH_SHORT).show();
                } else {
                    sentArticle();
                }
                break;
            case R.id.article_add_picture:

                addPicture();
                break;
            case R.id.article_add_file:
                Intent getFileIntent = new Intent(Intent.ACTION_GET_CONTENT);
                getFileIntent.setType("*/*");
                getFileIntent.addCategory(Intent.CATEGORY_OPENABLE);
                startActivityForResult(getFileIntent, GET_FILE_FROM_CATEGORY);

                break;
            case R.id.article_add_friend:
                Toast.makeText(this, "添加朋友", Toast.LENGTH_SHORT).show();
                break;
            case R.id.add_article_link:
                Toast.makeText(this, "添加链接", Toast.LENGTH_SHORT).show();
                break;
            case R.id.article_add_look:


                Toast.makeText(this, "添加表情", Toast.LENGTH_SHORT).show();
                break;
            case R.id.article_add_type:
                Toast.makeText(this, "添加类型", Toast.LENGTH_SHORT).show();
                break;
            default:
                break;
        }
    }

    private void addPicture() {
        Log.e("TAGTAGTAGTAG", "Main 调用添加图片方法初始时tmpPathList.size() = " + tmpCount);
        num = 9 - tmpCount;
        if (tmpPathList.size() > 0) {
            if (num > 0) {
                useMatisse(num, 2);//调用添加图片的方法，第二次添加
            } else {
                Toast.makeText(this, "请先删除需要替换掉的图片！", Toast.LENGTH_SHORT).show();
            }
        } else {
            useMatisse(9, 1);//调用添加图片的方法，第一次添加
        }
        Log.e("TAGTAGTAGTAG", "Main 调用添加图片方法结束时tmpPathList.size() = " + tmpCount);

    }

    /**
     * 调用Matisse第三方库添加图片
     *
     * @param num         可以选择的图片数目
     * @param requestCode 判断是不是第一次添加图片
     */
    public void useMatisse(int num, int requestCode) {
        Matisse.from(this).choose(MimeType.ofImage(), false)
                .countable(true)
                .capture(true)
                .captureStrategy(new CaptureStrategy(true, "com.hzh.dell.tree.article.fileprovider"))
                .maxSelectable(num)
                .addFilter(new Filter() {
                    @Override
                    protected Set<MimeType> constraintTypes() {
                        return new HashSet<MimeType>() {
                            {
                                add(MimeType.PNG);
                            }
                        };
                    }

                    @Override
                    public IncapableCause filter(Context context, Item item) {
                        try {

                            InputStream inputStream = getContentResolver().openInputStream(item.getContentUri());
                            BitmapFactory.Options options = new BitmapFactory.Options();
                            options.inJustDecodeBounds = true;
                            BitmapFactory.decodeStream(inputStream, null, options);

                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }
                        return null;
                    }
                })
                .originalEnable(true)
                .maxOriginalSize(10)
                .theme(R.style.Matisse_Dracula)//添加主题样式
                .gridExpectedSize((int) getResources().getDimension(R.dimen.grid_expected_size))
                .restrictOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT)
                .thumbnailScale(0.87f)
                .imageEngine(new GlideLoadEngineUtil())
                .forResult(requestCode);
    }

    //点击内部ite的动作
    public void onClick() {
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                if (position == parent.getChildCount() - 1) {
                    addPicture();
                } else {
                    deletePosition = position;
                }
            }
        });
        gridViewAdapter.setItemOnClickListener(new GridViewAddImgAdapter.ItemOnClickListener() {
            @Override
            public void itemOnClickListener(View view) {
                switch (view.getId()) {
                    case R.id.iv_del_pic:
                        tmpPathList.remove(deletePosition);
                        Log.e("TAGTAGTAGTAG", " ---delete--- ");
//                            tmpPathList=gridViewAdapter.get
                        tmpCount = tmpPathList.size();
                        Log.e("TAGTAGTAGTAG", " ---tmpCount--- " + tmpCount);
                        break;
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode == RESULT_OK) {
            List<String> pathList = Matisse.obtainPathResult(data);
            tmpPathList.addAll(pathList);
            tmpCount = tmpPathList.size();
            Log.e("TAGTAGTAGTAG", "Main 第一次添加数据后剩余 " + tmpPathList.size());
            gridViewAdapter = new GridViewAddImgAdapter(pathList, this, getLayoutInflater());
            gridView.setAdapter(gridViewAdapter);
            onClick();//判断首次添加数据后删除数据

        } else if (requestCode == 2 && resultCode == RESULT_OK) {

            List<String> pathList2 = Matisse.obtainPathResult(data);
            tmpPathList.addAll(pathList2);
            tmpCount = tmpPathList.size();
            Log.e("TAGTAGTAGTAG", "Main 再次添加照片时tmpList.size() = " + tmpCount);
            gridViewAdapter.notifyDataSetChanged(pathList2);
            onClick();//判断删除数据

        } else if (requestCode == GET_FILE_FROM_CATEGORY && resultCode == RESULT_OK) {
            if (data != null) {
                final Uri uri = data.getData();
                uriFileUtil = new UriFileUtil(this, uri);
                //这里要调用工具类的getPath()，不能直接使用uri.getPath;
                //如果选择的是图片的话直接调用得到 的path不是图片的本身路径
                String path = uriFileUtil.getPath(this, uri);
            /*这里要调用这个getPath方法来能过uri获取路径不能直接使用uri.getPath。
            因为如果选的图片的话直接使用得到的path不是图片的本身路径*/
                File file = new File(path);
                String end = file.getName().substring(file.getName().lastIndexOf(".") + 1, file.getName().length()).toLowerCase();
                if (end.equals("jpg")
                        || end.equals("png")
                        || end.equals("gif")
                        || end.equals("bmp")
                        || end.equals("jpeg")
                        || end.equals("webp")
                        || end.equals("doc")
                        || end.equals("pdf")
                        || end.equals("ppt")
                        || end.equals("pptx")
                        || end.equals("xls")
                        || end.equals("xlsx")
                        || end.equals("txt")
                        || end.equals("zip")
                        || end.equals("rar")
                        ) {
                    fileList.add(file);

                } else {
                    Toast.makeText(getApplicationContext(), "仅支持office类型文件与压缩包上传", Toast.LENGTH_SHORT).show();
                }
                adapterForFile.notifyDataSetChanged();


                fileListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                    @Override
                    public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
                        final AlertDialog.Builder builder = new AlertDialog.Builder(parent.getContext());
                        builder.setTitle("温馨提示");
                        builder.setMessage("确定要删除吗？");
                        builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                fileList.remove(position);
                                adapterForFile.notifyDataSetChanged();
                                Log.e("position", "position " + position);
                                dialog.dismiss();
                            }
                        });
                        builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        builder.create().show();
                        return true;
                    }
                });
            }
        }
    }

    private void sentArticle() {

        Log.e("tmpListSize", " ---> " + tmpPathList.size() + "");
        final String[] imgPath = new String[tmpPathList.size()];
        for (int i = 0; i < tmpPathList.size(); i++) {
            imgPath[i] = tmpPathList.get(i);
        }

        if (user == null) {
            Toast.makeText(this, "请先登录", Toast.LENGTH_SHORT).show();
        } else {
            String articleTitle = etTitle.getText().toString();
            String articleContent = etContent.getText().toString();
            final com.hzh.dell.Bmob.entity.Article article = new com.hzh.dell.Bmob.entity.Article();
            article.setArticleTitle(articleTitle);
            article.setArticleContent(articleContent);
            String userTargetSchool = user.getUserTargetSchool();
            String articleTheme;
            if (!"".equals(userTargetSchool)) {
                articleTheme = "#" + userTargetSchool + "#";
            } else {
                articleTheme = "#2020考研#";
            }
            article.setArticleTheme(articleTheme);
            article.setArticleType(0);
            article.setAuthor(user);
            article.setCollectNum(0);
            article.setLikeNum(0);
            article.setCommentNum(0);
            //文件地址
            final String[] filePath = new String[fileList.size()];
            for (int i = 0; i < fileList.size(); i++) {
                filePath[i] = fileList.get(i).getPath();
            }
            if (imgPath.length == 0 && filePath.length == 0) {
                //没有上传任何文件
                article.save(new SaveListener<String>() {
                    @Override
                    public void done(String s, BmobException e) {
                        if (e == null) {
                            saveArticleAndClick(user, article, e);
                            toast("保存成功");
                        }
                    }
                });
            } else {
                View contentView = getLayoutInflater().inflate(R.layout.dialog_show_progress, null);
                dialogUtil = new DialogUtil(this, contentView, uploadDialog);
                dialogUtil.showCenterDialog();
                final NumberProgressBar picProgress = contentView.findViewById(R.id.progress_pic);
                final NumberProgressBar fileProgress = contentView.findViewById(R.id.progress_file);
                LinearLayout llPicProgress = contentView.findViewById(R.id.ll_save_pic);
                LinearLayout llFileProgress = contentView.findViewById(R.id.ll_save_file);

                if (imgPath.length > 0 && filePath.length > 0) {
                    //有图片上传
                    //上传的有图片还有文件
                    llPicProgress.setVisibility(View.VISIBLE);
                    llFileProgress.setVisibility(View.VISIBLE);
                    //先上传图片资源
                    BmobFile.uploadBatch(imgPath, new UploadBatchListener() {
                        @Override
                        public void onSuccess(List<BmobFile> list, List<String> list1) {
                            if (list.size() == imgPath.length) {
                                article.setArticleImgFile(list);
                                if (article.getArticleImgFile().size() == imgPath.length && article.getArticleOtherFile().size() == filePath.length) {
                                    article.save(new SaveListener<String>() {
                                        @Override
                                        public void done(String s, BmobException e) {
                                            if (e == null) {
                                                saveArticleAndClick(user, article, e);
                                            }
                                        }
                                    });
                                }
                            }
                        }

                        @Override
                        public void onProgress(int i, int i1, int i2, int i3) {
                            picProgress.setProgress(i3);//图片进度
                        }

                        @Override
                        public void onError(int i, String s) {
                            Log.e("img", "上传图片失败 " + s);
                        }
                    });
                    BmobFile.uploadBatch(filePath, new UploadBatchListener() {
                        @Override
                        public void onSuccess(List<BmobFile> list, List<String> list1) {
                            if (list.size() == filePath.length) {
                                //资源文件上传成功
                                article.setArticleOtherFile(list);
                                if (article.getArticleImgFile().size() == imgPath.length && article.getArticleOtherFile().size() == filePath.length) {
                                    article.save(new SaveListener<String>() {
                                        @Override
                                        public void done(String s, BmobException e) {
                                            if (e == null) {
                                                saveArticleAndClick(user, article, e);
                                            }
                                        }
                                    });
                                }
                            }
                        }

                        @Override
                        public void onProgress(int i, int i1, int i2, int i3) {
                            fileProgress.setProgress(i3);//进度
                        }

                        @Override
                        public void onError(int i, String s) {
                            Log.e("errinfo", "上传文件失败 " + s);
                        }


                    });
                }
                //上上传的只有图片
                else if (imgPath.length > 0 && fileList.size() == 0) {
                    llFileProgress.setVisibility(View.GONE);
                    llPicProgress.setVisibility(View.VISIBLE);
                    BmobFile.uploadBatch(imgPath, new UploadBatchListener() {
                        @Override
                        public void onSuccess(List<BmobFile> list, List<String> list1) {
                            if (list.size() == imgPath.length) {
                                article.save(new SaveListener<String>() {
                                    @Override
                                    public void done(String s, BmobException e) {
                                        if (e == null) {
                                            saveArticleAndClick(user, article, e);
                                            toast("保存成功");
                                        }
                                    }
                                });
                            }
                        }

                        @Override
                        public void onProgress(int i, int i1, int i2, int i3) {
                            picProgress.setProgress(i3);
                        }

                        @Override
                        public void onError(int i, String s) {
                            Log.e("img", " 错误信息 " + s);
                        }
                    });

                }
                //上传的只有文件
                else if (imgPath.length == 0 && filePath.length > 0) {
                    llPicProgress.setVisibility(View.GONE);
                    llFileProgress.setVisibility(View.VISIBLE);
                    //保存文件
                    BmobFile.uploadBatch(filePath, new UploadBatchListener() {
                        @Override
                        public void onSuccess(List<BmobFile> list, List<String> list1) {
                            if (list.size() == filePath.length) {
                                article.save(new SaveListener<String>() {
                                    @Override
                                    public void done(String s, BmobException e) {
                                        if (e == null) {
                                            article.save(new SaveListener<String>() {
                                                @Override
                                                public void done(String s, BmobException e) {
                                                    if (e == null) {
                                                        saveArticleAndClick(user, article, e);
                                                        toast("保存成功");
                                                    }
                                                }
                                            });
                                        }
                                    }
                                });
                            }
                        }

                        @Override
                        public void onProgress(int i, int i1, int i2, int i3) {
                            fileProgress.setProgress(i3);
                        }

                        @Override
                        public void onError(int i, String s) {
                            Log.e("file", " 保存,第 " + i + " 个文件失败,错误信息 " + s);
                        }
                    });
                }
            }

        }
    }


    private void logE(String tag, String msg) {
        Log.e(tag, msg);
    }

    private void toast(String s) {
        Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
    }

    public void saveArticleAndClick(User user, final Article article, BmobException e) {
        if (e == null) {
            Click click = new Click();
            click.setUserId(user.getObjectId());
            click.setItemId(article.getObjectId());
            click.setLikeState(ZERO);
            click.setCollectState(ZERO);
            click.setCommentState(ZERO);
            click.setItemType(ZERO);//表示为图文贴
            click.save(new SaveListener<String>() {
                @Override
                public void done(String s, BmobException e) {
                    if (e != null) {
                        Log.e("click_save", "click 保存失败 " + e.toString());
                    } else {
                        logE("save", "保存成功 " + article.getArticleOtherFile().size() + " " + article.getArticleImgFile().size());
                        Toast.makeText(getApplicationContext(), "发布成功！", Toast.LENGTH_SHORT).show();
                    }
                }
            });
            dialogUtil.dismissDialog();
            onBackPressed();
        } else {
            logE("article_save", "article 保存失败 " + e.toString());
        }
    }


    public void uploadImg(final Article article, final String[] imgPath) {
        BmobFile.uploadBatch(imgPath, new UploadBatchListener() {
            @Override
            public void onSuccess(List<BmobFile> list, List<String> list1) {
                //上传成功
                if (list.size() == imgPath.length) {
                    article.setArticleImgFile(list);
                }
            }

            @Override
            public void onProgress(int i, int i1, int i2, int i3) {
                Log.e("upload", "img progress ---> " + i3);
            }

            @Override
            public void onError(int i, String s) {

            }
        });
    }

    public void uploadFile(final Article article, final List<File> fileList) {
        String[] filePath = new String[fileList.size()];
        for (int i = 0; i < fileList.size(); i++) {
            filePath[i] = fileList.get(i).getPath();
        }
        BmobFile.uploadBatch(filePath, new UploadBatchListener() {
            @Override
            public void onSuccess(List<BmobFile> list, List<String> list1) {
                if (list.size() == fileList.size()) {
                    article.setArticleOtherFile(list);
                }
            }

            @Override
            public void onProgress(int i, int i1, int i2, int i3) {
                Log.e("upload", "file progress ---> " + i3);
            }

            @Override
            public void onError(int i, String s) {
                logE("errInfo", s);
            }
        });
    }
}
