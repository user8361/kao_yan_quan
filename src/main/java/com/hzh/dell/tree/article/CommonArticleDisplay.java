//package com.hzh.dell.tree.article;
//
//import android.graphics.Bitmap;
//import android.graphics.drawable.BitmapDrawable;
//import android.os.Bundle;
//import android.support.annotation.NonNull;
//import android.support.annotation.Nullable;
//import android.support.v4.app.Fragment;
//import android.util.Log;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.ImageView;
//import android.widget.ListView;
//
//import com.hzh.dell.tree.R;
//import com.hzh.dell.tree.adapter.ListAdapterForItems;
//import com.hzh.dell.tree.database.MyGreenDaoApplication;
//import com.hzh.dell.tree.entity.Article;
//import com.hzh.dell.tree.entity.User;
//import com.hzh.dell.tree.utils.ImgUtils;
//import com.hzh.greendao.gen.ArticleDao;
//import com.hzh.greendao.gen.UserDao;
//import com.scwang.smartrefresh.layout.SmartRefreshLayout;
//import com.scwang.smartrefresh.layout.api.RefreshLayout;
//import com.scwang.smartrefresh.layout.footer.ClassicsFooter;
//import com.scwang.smartrefresh.layout.header.ClassicsHeader;
//import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
//import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
//
//import java.util.ArrayList;
//import java.util.List;
//
//public class CommonArticleDisplay extends Fragment implements ListAdapterForItems.InnerItemOnClickListener {
//    private static final String USER_NAME = "userName";
//    private final long ONE_MINUTE = 1 * 60 * 1000;
//    private View view;
//    private ListView mListView;
//    private List<Article> articleList;
//    private List<Article> lists;
//    private SmartRefreshLayout smartRefreshLayout;
//    private UserDao userDao;
//    private User user;
//    private ArticleDao articleDao;
//    private Article article;
//    private Bundle bundle;
//    private String userName = "";
//
//    boolean flag = false;
//    private int OFFSET = 4;
//    private int LodeCount = 0;
//    private int newCount = 0;
//    private ListAdapterForItems mAdapter;
//
//
//    @Nullable
//    @Override
//    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        view = inflater.inflate(R.layout.person_fragment_mine_article_list, container, false);
//
//        initView();
//        initData();//初始化数据
//        setAdapter();//设置adapter
//        refresh();
//        return view;
//    }
//
//    private void refresh() {
//        /**
//         * 刷新
//         */
//        smartRefreshLayout = view.findViewById(R.id.smartRefresh);
//        smartRefreshLayout.setRefreshHeader(new ClassicsHeader(getContext()));
//        smartRefreshLayout.setRefreshFooter(new ClassicsFooter(getContext()));
//        smartRefreshLayout.setEnableAutoLoadMore(false);//使上拉加载具有弹性效果
//        smartRefreshLayout.setEnableOverScrollDrag(true);//禁止越界拖动（1.0.4以上版本）
//        smartRefreshLayout.setEnableOverScrollBounce(true);//关闭越界回弹功能
//        smartRefreshLayout.setEnableLoadMore(true); //不隐藏加载,使用底部加载
//        smartRefreshLayout.setOnRefreshListener(new OnRefreshListener() {
//            @Override
//            public void onRefresh(RefreshLayout refreshlayout) {
//                refreshMore();
//                refreshlayout.finishRefresh(1000);
//            }
//        });
//        smartRefreshLayout.setOnLoadMoreListener(new OnLoadMoreListener() {
//            @Override
//            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
//                loadMore();
//                refreshLayout.finishLoadMore(1000);
//            }
//        });
//
//    }
//
//    private void initData() {
//        lists = new ArrayList<>();
//        articleList = articleDao.queryBuilder()
//                .orderDesc(ArticleDao.Properties.ArticleId)
//                .limit(OFFSET)
//                .where(ArticleDao.Properties.UserId.eq(user.getUserId()),
//                        ArticleDao.Properties.CreateTime.lt(getDifTime()))//用户发表并且发表时间大于一分钟的
//                .list();//初始时显示的数据
//        lists.addAll(articleList);//将初始数据添加到列表中
//        Log.d("TAGTAGTAG", "---> 1. 初始化数据时  : lists.size()= " + lists.size());
//    }
//
//    private void setAdapter() {
////        if (articleList.size() > 0) {
//        mAdapter = new ListAdapterForItems(view.getContext(), articleList, getLayoutInflater());
//        mAdapter.setOnInnerItemOnClickListener(this);
//        mListView.setAdapter(mAdapter);
////        }
//    }
//
//    //刷新更多
//    private void refreshMore() {
//        Log.d("TAGTAGTAG", "---> 2. 进入下拉刷新时  : lists.size()= " + lists.size());
//        //列表中最新的，等待判断
//        List<Article> tmpLists = articleDao.queryBuilder()
//                .orderDesc(ArticleDao.Properties.ArticleId)//倒序，id越大发表的时间越晚
//                .where(ArticleDao.Properties.UserId.eq(user.getUserId()),
//                        ArticleDao.Properties.CreateTime.gt(getDifTime()))//发表时间在一分钟之内的
//                .list();
//        List<Article> newList = new ArrayList<Article>();
//        newList.addAll(tmpLists);
//        //判断article的id是否在列表中
//        for (Article i : tmpLists) {
//            for (Article j : lists) {
//                if (i.getArticleId().equals(j.getArticleId())) {
//                    newList.remove(i);//原列表中存在就移除
//                }
//            }
//        }
//        newCount = newList.size();
//        articleList.clear();//清空数据源
//        articleList.addAll(newList);//添加最新的数据
//        articleList.addAll(lists);//添加原数据
//        mAdapter.notifyDataSetChanged();
//        lists.clear();//原数据清空
//        lists.addAll(articleList);//将新的数据添加到列表中
//        Log.d("TAGTAGTAG", "---> 3. 下拉刷新结束时  : lists.size()= " + lists.size());
//
//    }
//
//    //加载更多
//    private void loadMore() {
//        Log.d("TAGTAGTAG", "---> 4. 进入上拉加载时  : lists.size()= " + lists.size());
//
////        LodeCount++;
//        articleList.addAll(articleDao.queryBuilder()
//                .orderDesc(ArticleDao.Properties.ArticleId)
//                .offset(lists.size()).limit(4)//忽略刷新加入的和以前的数据，并且限制每次加载四条数据
//                .where(ArticleDao.Properties.UserId.eq(user.getUserId()))
//                .list());
//
//        if (articleList.size() > 0) {
//            mAdapter.notifyDataSetChanged();
//        }
//        List<Article> tmp = new ArrayList<Article>();
//        tmp.addAll(articleList);
//        for (Article i : lists) {
//            for (Article j : articleList) {
//                if (j.getArticleId().equals(i.getArticleId())) {
//                    tmp.remove(j);
//                }
//            }
//        }
//        lists.addAll(tmp);
//        Log.d("TAGTAGTAG", "---> 5. 上拉加载结束时  : lists.size()= " + lists.size());
//
//    }
//
//    //一分钟时差
//    private long getDifTime() {
//        return System.currentTimeMillis() - ONE_MINUTE;
//    }
//
//    private void initView() {
//        bundle = getArguments();
//        userName = bundle.getString(USER_NAME);
//        userDao = MyGreenDaoApplication.getInstance().getDaoSession().getUserDao();
//        user = userDao.queryBuilder().where(UserDao.Properties.UserName.eq(userName)).build().unique();
//        articleDao = MyGreenDaoApplication.getInstance().getDaoSession().getArticleDao();
//        mListView = view.findViewById(R.id.listView);
//    }
//
//    @Override
//    public void itemClick(View v) {
//        int position = (int) v.getTag();
//        article = articleDao.queryBuilder().where(ArticleDao.Properties.ArticleId.eq(articleList.get(position).getArticleId())).build().unique();
//        switch (v.getId()) {
//            case R.id.user_headImg:
//                Log.e("内部item--1-->", position + "");
//                break;
//            case R.id.user_name:
//                Log.e("内部item--2-->", position + "");
//                break;
//            case R.id.user_submitTime:
//                Log.e("内部item--3-->", position + "");
//                break;
//            case R.id.content_layout:
//                Log.e("内部item--4-->", position + "");
//                break;
//            case R.id.article_type:
//                Log.e("内部item--5-->", position + "");
//                break;
//            case R.id.collect_img:
//                Log.e("内部item--6-->", position + "");
//                break;
//            case R.id.like_img:
//                ImageView img = v.findViewWithTag(position).findViewById(R.id.like_img);
//                Bitmap likeImgBitmap = ((BitmapDrawable) getResources().getDrawable(R.mipmap.heart_pink)).getBitmap();
//                Bitmap unLikeImgBitmap = ((BitmapDrawable) getResources().getDrawable(R.mipmap.heart_gray)).getBitmap();
//                if (flag == false) {
//                    flag = true;
//                    article.setArticleLikeImgId(ImgUtils.bitmapToString(likeImgBitmap));
//                    articleDao.update(article);
//                    img.setImageBitmap(ImgUtils.stringToBitmap(article.getArticleLikeImgId()));
//                } else {
//                    flag = false;
//                    article.setArticleLikeImgId(ImgUtils.bitmapToString(unLikeImgBitmap));
//                    articleDao.update(article);
//                    img.setImageBitmap(ImgUtils.stringToBitmap(article.getArticleLikeImgId()));
//                }
//                Log.e("内部item--7-->", position + "");
//                break;
//            case R.id.msg_img:
//                Log.e("内部item--8-->", position + "");
//                break;
//            default:
//                break;
//        }
//    }
//}
