package com.hzh.dell.tree.article.show_article_detail;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.RemoteViews;
import android.widget.TextView;
import android.widget.Toast;

import com.hzh.dell.Bmob.entity.Article;
import com.hzh.dell.Bmob.entity.Click;
import com.hzh.dell.Bmob.entity.Comment;
import com.hzh.dell.Bmob.entity.User;
import com.hzh.dell.Bmob.entity.UserRelation;
import com.hzh.dell.individualproj.MainActivity;
import com.hzh.dell.tree.R;
import com.hzh.dell.tree.adapter.GridViewShowImgArticleDetailAdapter;
import com.hzh.dell.tree.adapter.ListViewAdapterForFile;
import com.hzh.dell.tree.adapter.listview_adapter.CommonAdapter;
import com.hzh.dell.tree.adapter.listview_adapter.base.ViewHolder;
import com.hzh.dell.tree.custom.DrawableCenterTextView;
import com.hzh.dell.tree.custom.MyGridView;
import com.hzh.dell.tree.custom.MyListView;
import com.hzh.dell.tree.utils.DialogUtil;
import com.hzh.dell.tree.utils.EditTextShowInputUtil;
import com.hzh.dell.tree.utils.ImgUtils;
import com.hzh.dell.tree.utils.ListViewShowAllUtil;
import com.hzh.dell.tree.utils.NotificationUtils;
import com.jaeger.library.StatusBarUtil;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;

import java.util.ArrayList;
import java.util.List;

import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.datatype.BmobFile;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.DownloadFileListener;
import cn.bmob.v3.listener.FindListener;
import cn.bmob.v3.listener.QueryListener;
import cn.bmob.v3.listener.SaveListener;
import cn.bmob.v3.listener.UpdateListener;

/**
 * 帖子详情页
 * create by hzh
 * on 2019/4/14
 */
public class ArticleDetail extends AppCompatActivity implements View.OnClickListener {
    private final String TAG = "myCustomTag";
    private ImageView ivReturn, ivHeadImg, ivCollectImg, ivLikeImg, ivMsgImg;
    private TextView tvConcern, tvConcerned, tvUserName, tvCreateTime, tvArticleTitle,
            tvArticleContent, tvArticleType, tvCollectNum, tvLikeNum, tvMsgNum;
    private EditText etComment;
    private DrawableCenterTextView tvSend;
    private MyListView commentListView, fileListView;
    private MyGridView imgGridView;
    private Intent getInfoIntent;
    private String articleId;
    private static final String ARTICLE_ID = "articleId";
    private final String USER_NAME = "userName";
    private String userName = "";
    private User appUser;//app使用用户
    private User articleUser;//发表帖子的用户
    private SmartRefreshLayout smartRefreshLayout;
    private GridViewShowImgArticleDetailAdapter adapter;
    private Article article;
    private Click click;
    private CommonAdapter<Comment> commentCommonAdapter;
    private ListViewAdapterForFile adapterForFile;
    private List<Comment> commentList;
    private List<User> commentUserList;
    private boolean clickCommentItem = false;
    private int clickPosition = 0;
    private UserRelation appUserArticleUserRelation;
    private NotificationUtils notificationUtils;//通知 工具类


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.article_detail_content);
        StatusBarUtil.setTranslucent(this, 50);//半透明
        initView();
        initData();
//        refresh();
    }

    /**
     * 初始化组件，不需要再查看
     */
    private void initView() {
        getInfoIntent = getIntent();
        articleId = getInfoIntent.getStringExtra(ARTICLE_ID);//获取传过来的id
        userName = getInfoIntent.getStringExtra(USER_NAME);//传过来的用户名字
        commentList = new ArrayList<>();
        commentUserList = new ArrayList<>();


        etComment = findViewById(R.id.et_comment);
        etComment.clearFocus();//清除进入时的焦点

        commentListView = findViewById(R.id.commentListView);
        fileListView = findViewById(R.id.fileListView);
        tvSend = findViewById(R.id.tv_send);
        ivReturn = findViewById(R.id.iv_return);
        ivHeadImg = findViewById(R.id.user_headImg);
        ivCollectImg = findViewById(R.id.collect_img);
        ivLikeImg = findViewById(R.id.like_img);
        ivMsgImg = findViewById(R.id.msg_img);
        tvConcern = findViewById(R.id.tv_concern);
        tvConcerned = findViewById(R.id.tv_concerned);
        tvArticleTitle = findViewById(R.id.article_title);
        tvArticleContent = findViewById(R.id.article_content);
        tvArticleType = findViewById(R.id.article_type);
        tvCreateTime = findViewById(R.id.user_submitTime);
        tvCollectNum = findViewById(R.id.collect_text);
        tvLikeNum = findViewById(R.id.like_text);
        tvMsgNum = findViewById(R.id.msg_text);
        smartRefreshLayout = findViewById(R.id.smartRefresh);
        tvUserName = findViewById(R.id.user_name);
        imgGridView = findViewById(R.id.imgGridView);

        ivReturn.setOnClickListener(this);

    }

    /**
     * 初始化点击事件
     */
    public void initClickEvent() {

        tvConcern.setOnClickListener(this);
        tvConcerned.setOnClickListener(this);

        ivHeadImg.setOnClickListener(this);
        tvUserName.setOnClickListener(this);

        ivLikeImg.setOnClickListener(this);
        ivCollectImg.setOnClickListener(this);
        ivMsgImg.setOnClickListener(this);
        tvArticleType.setOnClickListener(this);
        tvSend.setOnClickListener(this);
    }

    /**
     * 初始化数据
     */
    private void initData() {

/*******************查询帖子************************************************/
        final BmobQuery<Article> articleQuery = new BmobQuery<>();
        articleQuery.addWhereEqualTo("objectId", articleId);
        articleQuery.findObjects(new FindListener<Article>() {
            @Override
            public void done(List<Article> list, BmobException e) {
                if (list != null) {
                    Log.e("size", list.size() + "");
                    article = list.get(0);
                    if (article != null) {
/******************查询帖子用户***********************************************/
                        BmobQuery<User> articleUserQuery = new BmobQuery<>();
                        articleUserQuery.addWhereEqualTo("objectId", article.getAuthor().getObjectId());
                        articleUserQuery.findObjects(new FindListener<User>() {
                            @Override
                            public void done(List<User> list, BmobException e) {
                                if (list != null) {
                                    articleUser = list.get(0);
                                    if (articleUser != null) {
/******************查询app用户***********************************************/
                                        BmobQuery<User> appUserQuery = new BmobQuery<>();
                                        appUserQuery.addWhereEqualTo("userName", userName);
                                        appUserQuery.findObjects(new FindListener<User>() {
                                            @Override
                                            public void done(List<User> list, BmobException e) {
                                                if (list != null) {
                                                    appUser = list.get(0);
                                                    if (appUser != null) {
/******************查询当前帖子的状态信息***********************************************/
                                                        BmobQuery<Click> clickBmobQuery = new BmobQuery<>();
                                                        clickBmobQuery.addWhereEqualTo("userId", appUser.getObjectId());
                                                        clickBmobQuery.addWhereEqualTo("itemId", article.getObjectId());
                                                        clickBmobQuery.findObjects(new FindListener<Click>() {
                                                            @Override
                                                            public void done(List<Click> list, BmobException e) {
                                                                if (list != null) {
                                                                    click = list.get(0);
                                                                    if (click != null) {
                                                                        showDetailInfo();//其中会初始化评论列表的数据
                                                                        initClickEvent();
                                                                    } else {
                                                                        ivLikeImg.setImageResource(R.mipmap.heart_gray);
                                                                        ivCollectImg.setImageResource(R.mipmap.collect_gray);
                                                                        ivMsgImg.setImageResource(R.mipmap.message);
                                                                        showDetailInfo();//其中会初始化评论列表的数据
                                                                        initClickEvent();
                                                                    }
                                                                }
                                                            }
                                                        });
                                                    }
                                                }
                                            }
                                        });

                                    }
                                }
                            }
                        });
                    }
                }
            }
        });
    }

    private void showDetailInfo() {
        ivHeadImg.setImageBitmap(ImgUtils.stringToBitmap(articleUser.getUserHeadImgPath()));
        tvUserName.setText(articleUser.getUserName());
        tvArticleTitle.setText(article.getArticleTitle());
        tvArticleContent.setText(article.getArticleContent());
        tvArticleType.setText(article.getArticleTheme());
        tvCreateTime.setText(article.getCreatedAt());
        tvLikeNum.setText(article.getLikeNum() + "");
        tvCollectNum.setText(article.getCollectNum() + "");
        tvMsgNum.setText(article.getCommentNum() + "");

        etComment.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                tvSend.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().trim().length() > 0) {
                    tvSend.setVisibility(View.VISIBLE);
                } else {
                    tvSend.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        if (click.getLikeState() == 0) {
            ivLikeImg.setImageResource(R.mipmap.heart_gray);
        } else {
            ivLikeImg.setImageResource(R.mipmap.heart_pink);
        }

        if (click.getCollectState() == 0) {
            ivCollectImg.setImageResource(R.mipmap.collect_gray);
        } else {
            ivCollectImg.setImageResource(R.mipmap.collect_pink);
        }

        final List<BmobFile> fileList = article.getArticleOtherFile();
        List<java.io.File> fileList1 = new ArrayList<>();
        if (fileList.size() > 0) {
            for (BmobFile bmobFile : fileList) {
                java.io.File file = new java.io.File(bmobFile.getFileUrl());
                fileList1.add(file);
            }
            adapterForFile = new ListViewAdapterForFile(getApplicationContext(), fileList1);
            fileListView.setAdapter(adapterForFile);
            fileListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(final AdapterView<?> parent, View view, final int position, long id) {
                    Dialog dialog = null;
                    View bottomDialogView = LayoutInflater.from(parent.getContext()).inflate(R.layout.dialog_show_two_options, null);
                    final DialogUtil dialogUtil = new DialogUtil(parent.getContext(), bottomDialogView, dialog);
                    dialogUtil.showTwoOption();
                    bottomDialogView.findViewById(R.id.tv_option_one).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialogUtil.dismissDialog();
                            notificationUtils = new NotificationUtils();
                            View contentView = View.inflate(getApplicationContext(), R.layout.notification_show_progress, null);

                            final RemoteViews remoteViews = new RemoteViews(getPackageName(), R.layout.notification_show_progress);
                            notificationUtils.show(getApplicationContext(), "", "", remoteViews, null);


                            fileList.get(position).download(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), new DownloadFileListener() {
                                @Override
                                public void done(String s, BmobException e) {
                                    toast("文件保存目录：" + s);
                                }

                                @Override
                                public void onProgress(Integer integer, long l) {
                                    remoteViews.setProgressBar(R.id.progress_download_file, 100, integer, false);
//                                    Log.e("progress",numberProgressBar.getProgress()+"");
                                }
                            });
                        }
                    });
                    bottomDialogView.findViewById(R.id.tv_option_two).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialogUtil.dismissDialog();
                        }
                    });

                }
            });
        } else {
            fileListView.setVisibility(View.GONE);
        }


        List<BmobFile> imgList = article.getArticleImgFile();
        if (imgList.size() > 0) {
            imgGridView.setVisibility(View.VISIBLE);
            List<String> imgPathList = new ArrayList<>();
            for (int i = 0; i < imgList.size(); i++) {
                imgPathList.add(imgList.get(i).getFileUrl());
            }
            adapter = new GridViewShowImgArticleDetailAdapter(imgPathList, getApplicationContext(), getLayoutInflater());
            setGridViewMatchParent(imgGridView);
            imgGridView.setAdapter(adapter);
        } else {
            imgGridView.setVisibility(View.GONE);
        }

        //关注状态
        BmobQuery<UserRelation> userRelationBmobQuery = new BmobQuery<>();
        userRelationBmobQuery.addWhereEqualTo("userId", appUser.getObjectId());
        userRelationBmobQuery.addWhereEqualTo("followedId", articleUser.getObjectId());
        userRelationBmobQuery.findObjects(new FindListener<UserRelation>() {
            @Override
            public void done(List<UserRelation> list, BmobException e) {
                if (list != null) {

                    appUserArticleUserRelation = list.get(0);
                    if (appUserArticleUserRelation != null) {
                        if (appUserArticleUserRelation.getRelationFlag() == 1) {
                            tvConcerned.setVisibility(View.VISIBLE);
                            tvConcern.setVisibility(View.GONE);
                        } else {
                            tvConcern.setVisibility(View.VISIBLE);
                            tvConcerned.setVisibility(View.GONE);
                        }

                    } else {
                        tvConcern.setVisibility(View.VISIBLE);
                        tvConcerned.setVisibility(View.GONE);
                    }
                } else {
                    tvConcern.setVisibility(View.VISIBLE);
                    tvConcerned.setVisibility(View.GONE);
                }
                ivMsgImg.setImageResource(R.mipmap.message);

            }
        });


        initCommentListView();
    }

    //设置GridView的高度自适应
    public static void setGridViewMatchParent(GridView gridView) {
        // 获取gridView的adapter
        ListAdapter adapter = gridView.getAdapter();
        if (adapter == null) {
            return;
        }
        // 固定列宽，有多少列
        int col = 1;// imgGridView.getNumColumns();
        int totalHeight = 0;
        // i每次加2，相当于adapter.getCount()小于等于2时 循环一次，计算一次item的高度， adapter.getCount()小于等于8时计算两次高度相加
        for (int i = 0; i < adapter.getCount(); i += col) {
            // 获取listview的每一个item
            View listItem = adapter.getView(i, null, gridView);
            listItem.measure(0, 0);
            // 获取item的高度和
            totalHeight += listItem.getMeasuredHeight();
        }

        // 获取gridView的布局参数
        ViewGroup.LayoutParams params = gridView.getLayoutParams();
        // 设置高度
        params.height = totalHeight;
        // 设置margin
        ((ViewGroup.MarginLayoutParams) params).setMargins(10, 10, 10, 10);
        // 设置参数
        gridView.setLayoutParams(params);
    }


    private void toast(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

//    private void refresh() {
//        /**
//         * 刷新
//         */
//        smartRefreshLayout = findViewById(R.id.smartRefresh);
//        smartRefreshLayout.setRefreshHeader(new ClassicsHeader(this));
//        smartRefreshLayout.setRefreshFooter(new ClassicsFooter(this));
//        smartRefreshLayout.setEnableAutoLoadMore(false);//使上拉加载具有弹性效果
//        smartRefreshLayout.setEnableOverScrollDrag(true);//禁止越界拖动（1.0.4以上版本）
//        smartRefreshLayout.setEnableOverScrollBounce(false);//关闭越界回弹功能
//        smartRefreshLayout.setEnableLoadMore(true); //不隐藏加载,使用底部加载
//        smartRefreshLayout.setOnRefreshListener(new OnRefreshListener() {
//            @Override
//            public void onRefresh(RefreshLayout refreshlayout) {
////                refreshMore();//下拉刷新
//                initView();
//                initData();
//                initClickEvent();
//                refreshlayout.finishRefresh(1000);
//            }
//        });
//        smartRefreshLayout.setOnLoadMoreListener(new OnLoadMoreListener() {
//            @Override
//            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
////                loadMore(refreshLayout);//上拉加载
//                refreshLayout.finishLoadMore(1000);
//            }
//        });
//
//    }


    /**
     * 更新帖子 信息
     *
     * @param article
     */
    public void updateArticle(final Article article) {
        article.update(new UpdateListener() {
            @Override
            public void done(BmobException e) {
                logInfo("update", e);
                tvCollectNum.setText(article.getCollectNum() + "");
                tvLikeNum.setText(article.getLikeNum() + "");
            }
        });
    }

    /**
     * 点击 后更新事件
     *
     * @param click
     */
    public void updateClick(Click click) {
        click.update(new UpdateListener() {
            @Override
            public void done(BmobException e) {
                logInfo("click", e);
            }
        });
    }

    /**
     * 异常处理
     *
     * @param tag
     * @param e
     */
    public void logInfo(String tag, BmobException e) {
        if (e == null) {
            Log.e(tag, "无异常");
        } else {
            Log.e(tag, "错误信息：" + e.toString());
        }
    }

    @Override
    public void onClick(final View v) {
        BmobQuery<Click> clickBmobQuery = new BmobQuery<>();
        clickBmobQuery.addWhereEqualTo("userId", appUser.getObjectId());
        clickBmobQuery.addWhereEqualTo("itemId", article.getObjectId());
        clickBmobQuery.findObjects(new FindListener<Click>() {
            @Override
            public void done(List<Click> list, BmobException e) {
                if (list != null) {
                    if (list.size() > 0) {
                        Click click = list.get(0);
                        switch (v.getId()) {
                            case R.id.iv_return:
                                onBackPressed();
                                break;
                            case R.id.user_headImg:
                            case R.id.user_name:
                                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                intent.putExtra(USER_NAME, articleUser.getUserName());
                                startActivity(intent);
                                break;
                            case R.id.tv_concern:
                                if (appUserArticleUserRelation != null) {
                                    appUserArticleUserRelation.setRelationFlag(1);
                                    appUserArticleUserRelation.update(new UpdateListener() {
                                        @Override
                                        public void done(BmobException e) {
                                            if (e == null) {
                                                tvConcern.setVisibility(View.GONE);
                                                tvConcerned.setVisibility(View.VISIBLE);
                                                Toast.makeText(ArticleDetail.this, "关注成功！", Toast.LENGTH_SHORT).show();
                                            } else {
                                                Toast.makeText(ArticleDetail.this, "关注失败！错误信息：" + e.toString(), Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    });
                                } else {
                                    appUserArticleUserRelation = new UserRelation();
                                    appUserArticleUserRelation.setUserId(appUser.getObjectId());//当前
                                    appUserArticleUserRelation.setFollowedId(articleUser.getObjectId());
                                    appUserArticleUserRelation.setRelationFlag(1);
                                    appUserArticleUserRelation.save(new SaveListener<String>() {
                                        @Override
                                        public void done(String s, BmobException e) {
                                            if (e == null) {

                                                UserRelation userRelation2 = new UserRelation();
                                                userRelation2.setUserId(articleUser.getObjectId());
                                                userRelation2.setFansId(appUser.getObjectId());
                                                userRelation2.setRelationFlag(1);
                                                userRelation2.save(new SaveListener<String>() {
                                                    @Override
                                                    public void done(String s, BmobException e) {
                                                        if (e == null) {
                                                            tvConcern.setVisibility(View.GONE);
                                                            tvConcerned.setVisibility(View.VISIBLE);
                                                            Toast.makeText(ArticleDetail.this, "关注成功！", Toast.LENGTH_SHORT).show();
                                                        } else {
                                                            Toast.makeText(ArticleDetail.this, "关注失败！错误信息2：" + e.toString(), Toast.LENGTH_SHORT).show();
                                                        }
                                                    }
                                                });

                                            } else {
                                                Toast.makeText(ArticleDetail.this, "关注失败！错误信息1：" + e.toString(), Toast.LENGTH_SHORT).show();

                                            }
                                        }
                                    });
                                }
                                break;
                            case R.id.tv_concerned:
                                if (appUserArticleUserRelation != null) {
                                    appUserArticleUserRelation.setRelationFlag(0);
                                    appUserArticleUserRelation.update(new UpdateListener() {
                                        @Override
                                        public void done(BmobException e) {
                                            if (e == null) {
                                                tvConcerned.setVisibility(View.GONE);
                                                tvConcern.setVisibility(View.VISIBLE);
                                                Toast.makeText(ArticleDetail.this, "已取消关注", Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    });
                                }
                                break;
                            case R.id.collect_img:
                                if (click.getCollectState() == 0) {
                                    click.setCollectState(1);
                                    ivCollectImg.setImageResource(R.mipmap.collect_pink);
                                    article.setCollectNum(article.getCollectNum() + 1);
                                } else {
                                    click.setCollectState(0);
                                    article.setCollectNum(article.getCollectNum() - 1);
                                    ivCollectImg.setImageResource(R.mipmap.collect_gray);
                                }
                                updateClick(click);//更新点击事物
                                updateArticle(article);//更新文章，并更新界面数据
                                break;
                            case R.id.like_img:
                                if (click.getLikeState() == 0) {
                                    click.setLikeState(1);
                                    article.setLikeNum(article.getLikeNum() + 1);
                                    ivLikeImg.setImageResource(R.mipmap.heart_pink);
                                } else {
                                    click.setLikeState(0);
                                    article.setLikeNum(article.getLikeNum() - 1);
                                    ivLikeImg.setImageResource(R.mipmap.heart_gray);
                                }
                                updateClick(click);//更新点击事物
                                updateArticle(article);//更新文章，刷新界面
                                break;
                            case R.id.msg_img:
                                writeComment();//写评论，暂未实现
                                break;
                            case R.id.tv_send:
                                if (clickCommentItem) {
                                    replyComment();
                                } else {
                                    sentComment();
                                }
                                break;
                            default:
                                break;
                        }

                    } else {
                        final Click click = new Click();
                        click.setUserId(appUser.getObjectId());//设置当前用户的id
                        click.setItemId(article.getObjectId());//设置点击事物的id
                        click.setItemType(0);//0表示为图文贴 1：投票贴 2 错题
                        click.setLikeState(0);//设置初始值不喜欢
                        click.setCollectState(0);//未收藏
                        click.setCommentState(0);//未评论
                        click.save(new SaveListener<String>() {
                            @Override
                            public void done(String s, BmobException e) {
                                if (e == null) {//点击事件已经保存
                                    switch (v.getId()) {
                                        case R.id.user_headImg:
                                        case R.id.user_name:
                                            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                            intent.putExtra(USER_NAME, articleUser.getUserName());
                                            startActivity(intent);
                                            break;
                                        case R.id.article_type:
                                            break;
                                        case R.id.collect_img:
                                            click.setCollectState(1);//收藏
                                            updateClick(click);//更新点击事件
                                            ivCollectImg.setImageResource(R.mipmap.collect_pink);
                                            article.setCollectNum(article.getCollectNum() + 1);
                                            updateArticle(article);//更新文章，并更新界面数据
                                            break;
                                        case R.id.like_img:
                                            click.setLikeState(1);
                                            ivLikeImg.setImageResource(R.mipmap.heart_pink);
                                            updateClick(click);//更新点击事件
                                            article.setLikeNum(article.getLikeNum() + 1);
                                            updateArticle(article);//更新文章，刷新界面
                                            break;
                                        case R.id.msg_img:
                                            writeComment();//写评论
                                            break;
                                        case R.id.tv_send:
                                            sentComment();//发表评论
                                            break;
                                        default:
                                            break;
                                    }
                                } else {
                                    logInfo("click ,errInfo： ", e);
                                }
                            }
                        });
                    }
                } else {
                    logInfo("click ,errInfo： ", e);
                }
            }
        });
    }

    //写评论
    private void writeComment() {

    }


    /**
     * 难点
     */
    public void initCommentListView() {
        final View emptyView = findViewById(R.id.empty_view_content);
        commentListView.setEmptyView(emptyView);

        //这里只是设置样式，不能成功设置数据，此时commentList.size仍然是空的数据。
        commentCommonAdapter = new CommonAdapter<Comment>(getApplicationContext(), commentList, R.layout.comment_item) {
            @Override
            public void convert(final ViewHolder helper, final Comment item) {

                BmobQuery<User> getCommentUser = new BmobQuery<>();
                getCommentUser.getObject(item.getUser().getObjectId(), new QueryListener<User>() {
                    @Override
                    public void done(User user, BmobException e) {
                        int position = 0;

                        for (int i = 0; i < commentList.size(); i++) {
                            if (item.getObjectId().equals(commentList.get(i).getObjectId())) {
                                position = i + 1;
                                Log.e(TAG, "convert: position == " + position);
                            }
                        }
                        if (user != null) {
                            commentUserList.add(user);
//                            Log.e(TAG, "done: user:" + user.getUserName());
                            helper.setText(R.id.tv_user_name, user.getUserName());
                            helper.setImageBitmap(R.id.iv_head_img, ImgUtils.stringToBitmap(user.getUserHeadImgPath()));
                            helper.setText(R.id.tv_comment_time, item.getCreatedAt());
                            helper.setText(R.id.tv_comment, item.getContent());
                            helper.setImageResource(R.id.iv_like_img, R.mipmap.heart_gray);
                            helper.setText(R.id.tv_floor, position + "楼");
                            helper.setText(R.id.tv_comment_like_num, "10");
                        } else {
                            Log.e(TAG, "done: user == null");
                        }
                    }
                });
            }
        };
        commentListView.setAdapter(commentCommonAdapter);
        ListViewShowAllUtil.setListViewHeightBasedOnChildren(commentListView);
        //查询帖子评论列表
        BmobQuery<Comment> comQuery = new BmobQuery<>();
        comQuery.addWhereEqualTo("itemId", article.getObjectId());
        comQuery.findObjects(new FindListener<Comment>() {
            @Override
            public void done(List<Comment> list, BmobException e) {
                if (list != null) {
                    commentList.addAll(list);
                    commentCommonAdapter.notifyDataSetChanged();//通知更新数据源
                    commentListView.setAdapter(commentCommonAdapter);
                }
            }
        });
        commentItemOnClickListener();//监听评论点击事件

    }


    /**
     * 监听点击事件,获取焦点
     */
    public void commentItemOnClickListener() {
        commentListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                clickCommentItem = true;
                clickPosition = position;
                Log.e(TAG, "onItemClick: " + clickPosition + " -- " + commentUserList.get(clickPosition).getUserName());
                etComment.setHint("回复" + commentUserList.get(clickPosition).getUserName());//设置输入框中的提示信息
                etComment.setFocusable(true);//EditText可以获得焦点
                etComment.setFocusableInTouchMode(true);//EditText可输入模式
                etComment.requestFocus();//EditText请求获得焦点
                //弹出软键盘工具类
                EditTextShowInputUtil.showSoftInputFromWindow(etComment);
            }
        });
    }

    /**
     * 保存评论
     */
    public void sentComment() {
        Log.e(TAG, "sentComment: 开始");
        String commentContent = etComment.getText().toString();
        final Comment comment = new Comment();
        comment.setItemId(article.getObjectId());
        comment.setUser(appUser);
        comment.setContent(commentContent);
        comment.save(new SaveListener<String>() {
            @Override
            public void done(String s, BmobException e) {
                if (e == null) {
                    article.setCommentNum(article.getCommentNum() + 1);
                    article.update(new UpdateListener() {
                        @Override
                        public void done(BmobException e) {
                            if (e == null) {
                                tvMsgNum.setText(article.getCommentNum() + "");
                            }
                        }
                    });
                    etComment.setText("");
                    tvSend.setVisibility(View.INVISIBLE);
                    etComment.clearFocus();
                    commentList.add(comment);
                    commentCommonAdapter.notifyDataSetChanged();

                    Log.e(TAG, "--------> sentComment: 评论成功，更新数据");
                }
            }
        });
        Log.e(TAG, "sentComment: 结束");
    }

    /**
     * 保存对评论的评论
     */
    public void replyComment() {
        String replyComment = "回复" + commentUserList.get(clickPosition).getUserName() + ": ";
        final Comment childComment = new Comment();
        childComment.setUser(appUser);
        childComment.setItemId(articleId);
        childComment.setContent(replyComment + etComment.getText());
        childComment.save(new SaveListener<String>() {
            @Override
            public void done(String s, BmobException e) {
                if (e == null) {
                    etComment.setText("");
                    tvSend.setVisibility(View.INVISIBLE);
                    etComment.clearFocus();
                    commentList.add(childComment);
                    commentCommonAdapter.notifyDataSetChanged();
                    clickCommentItem = false;
                }
            }
        });
    }
}
