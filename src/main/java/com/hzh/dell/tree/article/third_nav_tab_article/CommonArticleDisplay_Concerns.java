package com.hzh.dell.tree.article.third_nav_tab_article;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.hzh.dell.Bmob.entity.Article;
import com.hzh.dell.Bmob.entity.Click;
import com.hzh.dell.Bmob.entity.User;
import com.hzh.dell.individualproj.MainActivity;
import com.hzh.dell.tree.R;
import com.hzh.dell.tree.adapter.ListAdapterForItems;
import com.hzh.dell.tree.article.show_article_detail.ArticleDetail;
import com.hzh.dell.tree.utils.DateUtil;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.footer.ClassicsFooter;
import com.scwang.smartrefresh.layout.header.ClassicsHeader;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.datatype.BmobDate;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.FindListener;
import cn.bmob.v3.listener.SaveListener;
import cn.bmob.v3.listener.UpdateListener;

public class CommonArticleDisplay_Concerns extends Fragment implements ListAdapterForItems.InnerItemOnClickListener {
    private static final String TAG = "mTag";
    private static final String USER_NAME = "userName";
    private static final String ARTICLE_ID = "articleId";
    private static final String CREATED_AT = "createdAt";
    private View view;
    private ListView mListView;
    private List<Article> articleList;
    private SmartRefreshLayout smartRefreshLayout;
    private User user;
    private Article article;

    private Bundle bundle;
    private String userName = "";
    private ListAdapterForItems mAdapter;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.person_fragment_mine_article_list, container, false);
        initView();
        return view;
    }

    private void setAdapter(List<Article> lists) {
        mAdapter = new ListAdapterForItems(view.getContext(), lists, getLayoutInflater(), user);
        mListView.setAdapter(mAdapter);
        mAdapter.setOnInnerItemOnClickListener(this);
    }

    private void initData() {

        BmobQuery<com.hzh.dell.Bmob.entity.Article> articleListQuery = new BmobQuery<>();
        articleListQuery.order("-createdAt").setLimit(8);//倒序,限制四条数据
        articleListQuery.findObjects(new FindListener<com.hzh.dell.Bmob.entity.Article>() {
            @Override
            public void done(List<com.hzh.dell.Bmob.entity.Article> list, BmobException e) {
                articleList = new ArrayList<>();
                if (list != null) {
                    articleList.addAll(list);
                    setAdapter(articleList);
                }
            }
        });
    }


    private void initView() {
        bundle = getArguments();
        userName = bundle.getString(USER_NAME);
        BmobQuery<User> userBmobQuery = new BmobQuery<>();
        userBmobQuery.addWhereEqualTo(USER_NAME, userName);
        userBmobQuery.findObjects(new FindListener<User>() {
            @Override
            public void done(List<User> list, BmobException e) {
                if (list != null) {
                    if (list.size() > 0) {
                        user = list.get(0);
                        initData();
                        refresh();
                    }
                }
            }
        });

        mListView = view.findViewById(R.id.listView);
        View emptyView = view.findViewById(R.id.empty_view_content);
        mListView.setEmptyView(emptyView);

    }

    //下拉刷新更多
    private void refreshMore() {
        final List<Article> tmpList = new ArrayList<>();

        long now = System.currentTimeMillis();//现在的时间
        long oneMinuteBefore = (now - 1 * 60 * 1000);//1分钟前的时间

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String createAtEnd = DateUtil.getFormattedDateTime(now);
        String createAtStart = DateUtil.getFormattedDateTime(oneMinuteBefore);
        try {
            Date createAtDateStart = sdf.parse(createAtStart);
            Date createAtDateEnd = sdf.parse(createAtEnd);
            BmobDate bmobDateStart = new BmobDate(createAtDateStart);
            BmobDate bmobDateEnd = new BmobDate(createAtDateEnd);


            BmobQuery<com.hzh.dell.Bmob.entity.Article> articleStart = new BmobQuery<>();
            articleStart.addWhereGreaterThanOrEqualTo(CREATED_AT, bmobDateStart);//比开始时间大
            BmobQuery<com.hzh.dell.Bmob.entity.Article> articleEnd = new BmobQuery<>();
            articleEnd.addWhereLessThanOrEqualTo(CREATED_AT, bmobDateEnd);//比结束时间小
            List<BmobQuery<Article>> queries = new ArrayList<>();
            queries.add(articleStart);
            queries.add(articleEnd);

            final BmobQuery<Article> articleBmobQuery = new BmobQuery<>();
            articleBmobQuery.and(queries).order("-createdAt").setLimit(4);//倒序,显示四条数据
            articleBmobQuery.findObjects(new FindListener<Article>() {
                @Override
                public void done(List<Article> list, BmobException e) {
                    if (e == null) {
                        if (list != null) {
                            tmpList.clear();
                            tmpList.addAll(articleList);
                            for (Article i : articleList) {
                                for (Article j : list) {
                                    if (j.getObjectId().equals(i.getObjectId())) {
                                        tmpList.remove(i);//原列表中存在就删除了
                                    }
                                }
                            }
                            articleList.clear();//清空元数据
                            articleList.addAll(list);//添加最新的数据
                            articleList.addAll(tmpList);//添加原来的 数据(已经删除了重复的数据)
                            mAdapter.notifyDataSetChanged();//通知变化
                        }
                    } else {
                        Log.e(TAG, "done: " + e.toString());
                    }
                }
            });
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }

    //
//    //上拉刷新,加载更多
    private void loadMore(final RefreshLayout refreshLayout) {
        List<Article> tmpList = new ArrayList<>();
        tmpList.addAll(articleList);
        if (tmpList.size() > 0) {
            BmobQuery<com.hzh.dell.Bmob.entity.Article> articleListQuery = new BmobQuery<>();
            articleListQuery.order("-createdAt").setLimit(8).setSkip(tmpList.size());//跳过
            articleListQuery.findObjects(new FindListener<com.hzh.dell.Bmob.entity.Article>() {
                @Override
                public void done(List<com.hzh.dell.Bmob.entity.Article> list, BmobException e) {
                    if (list != null) {
                        if (list.size() == 0) {
                            refreshLayout.setNoMoreData(true);
                        }
                        articleList.addAll(list);
                        mAdapter.notifyDataSetChanged();
                    }
                }
            });

        }

    }

    @Override
    public void itemClick(final View v) {
        final int position = (int) v.getTag();
        article = articleList.get(position);


        BmobQuery<Click> clickBmobQuery = new BmobQuery<>();
        clickBmobQuery.addWhereEqualTo("userId", user.getObjectId());
        clickBmobQuery.addWhereEqualTo("itemId", article.getObjectId());
        clickBmobQuery.findObjects(new FindListener<Click>() {
            @Override
            public void done(List<Click> list, BmobException e) {
                if (list != null) {
                    if (list.size() > 0) {
                        Click click = list.get(0);
                        switch (v.getId()) {
                            case R.id.user_headImg:
                            case R.id.user_name:
                                Intent intent = new Intent(getContext(), MainActivity.class);
                                intent.putExtra(USER_NAME, user.getUserName());
                                startActivity(intent);
                                break;
                            case R.id.user_submitTime:
                            case R.id.content_layout:
                                Intent detail = new Intent(getContext(), ArticleDetail.class);
                                detail.putExtra(ARTICLE_ID, article.getObjectId());
                                detail.putExtra(USER_NAME, user.getUserName());
                                startActivity(detail);
                                break;
                            case R.id.article_type:
                                Log.e("内部item--5-->", position + "");
                                break;
                            case R.id.collect_img:
                                ImageView collectImg = v.findViewWithTag(position).findViewById(R.id.collect_img);
                                if (click.getCollectState() == 0) {
                                    click.setCollectState(1);
                                    collectImg.setImageResource(R.mipmap.collect_pink);
                                    article.setCollectNum(article.getCollectNum() + 1);
                                } else {
                                    click.setCollectState(0);
                                    article.setCollectNum(article.getCollectNum() - 1);
                                    collectImg.setImageResource(R.mipmap.collect_gray);
                                }
                                updateClick(click);//更新点击事物
                                updateArticle(article);//更新文章，并更新界面数据
                                Log.e("collect", " -->  collect");
                                break;
                            case R.id.like_img:
                                ImageView likeImg = v.findViewWithTag(position).findViewById(R.id.like_img);
                                if (click.getLikeState() == 0) {
                                    click.setLikeState(1);
                                    article.setLikeNum(article.getLikeNum() + 1);
                                    likeImg.setImageResource(R.mipmap.heart_pink);
                                } else {
                                    click.setLikeState(0);
                                    article.setLikeNum(article.getLikeNum() - 1);
                                    likeImg.setImageResource(R.mipmap.heart_gray);
                                }
                                updateClick(click);//更新点击事物
                                updateArticle(article);//更新文章，刷新界面
                                Log.e("内部item--7-->", position + "");
                                break;
                            case R.id.msg_img:
                                //预留--   评论后更新click数据
                                Log.e("内部item--8-->", position + "");
                                break;
                            default:
                                break;
                        }

                    } else {
                        final Click click = new Click();
                        click.setUserId(user.getObjectId());//设置当前用户的id
                        click.setItemId(article.getObjectId());//设置点击事物的id
                        click.setItemType(0);//0表示为图文贴 1：投票贴 2 错题
                        click.setLikeState(0);//设置初始值不喜欢
                        click.setCollectState(0);//未收藏
                        click.setCommentState(0);//未评论
                        click.save(new SaveListener<String>() {
                            @Override
                            public void done(String s, BmobException e) {
                                if (e == null) {//点击事件已经保存
                                    switch (v.getId()) {
                                        case R.id.user_headImg:
                                        case R.id.user_name:
                                            Intent intent = new Intent(getContext(), MainActivity.class);
                                            intent.putExtra(USER_NAME, user.getUserName());
                                            startActivity(intent);
                                            break;
                                        case R.id.user_submitTime:
                                        case R.id.content_layout:
                                            Intent detail = new Intent(getContext(), ArticleDetail.class);
                                            detail.putExtra(ARTICLE_ID, article.getObjectId());
                                            detail.putExtra(USER_NAME, user.getUserName());
                                            startActivity(detail);
                                            break;
                                        case R.id.article_type:
                                            Log.e("内部item--5-->", position + "");
                                            break;
                                        case R.id.collect_img:
                                            ImageView collectImg = v.findViewWithTag(position).findViewById(R.id.collect_img);
                                            click.setCollectState(1);//收藏
                                            updateClick(click);//更新点击事件
                                            collectImg.setImageResource(R.mipmap.collect_pink);

                                            article.setCollectNum(article.getCollectNum() + 1);
                                            updateArticle(article);//更新文章，并更新界面数据
                                            break;
                                        case R.id.like_img:
                                            ImageView likeImg = v.findViewWithTag(position).findViewById(R.id.like_img);
                                            click.setLikeState(1);
                                            likeImg.setImageResource(R.mipmap.heart_pink);
                                            updateClick(click);//更新点击事件
                                            article.setLikeNum(article.getLikeNum() + 1);
                                            updateArticle(article);//更新文章，刷新界面
                                            Log.e("内部item--7-->", position + "");
                                            break;
                                        case R.id.msg_img:
                                            //预留--   评论后更新click数据
                                            Log.e("内部item--8-->", position + "");
                                            break;
                                        default:
                                            break;
                                    }
                                } else {
                                    logInfo("click", e);
                                }
                            }
                        });
                    }
                } else {
                    logInfo("click list = null", e);
                }


            }
        });


    }

    private void toast(String msg) {
        Toast.makeText(getContext(), msg, Toast.LENGTH_SHORT).show();
    }


    private void refresh() {
        /**
         * 刷新
         */
        smartRefreshLayout = view.findViewById(R.id.smartRefresh);
        smartRefreshLayout.setRefreshHeader(new ClassicsHeader(getContext()));
        smartRefreshLayout.setRefreshFooter(new ClassicsFooter(getContext()));
        smartRefreshLayout.setEnableAutoLoadMore(false);//使上拉加载具有弹性效果
        smartRefreshLayout.setEnableOverScrollDrag(true);//禁止越界拖动（1.0.4以上版本）
        smartRefreshLayout.setEnableOverScrollBounce(false);//关闭越界回弹功能
        smartRefreshLayout.setEnableLoadMore(true); //不隐藏加载,使用底部加载
        smartRefreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshlayout) {
                refreshMore();//下拉刷新
                refreshlayout.finishRefresh(1000);
            }
        });
        smartRefreshLayout.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                loadMore(refreshLayout);//上拉加载
                refreshLayout.finishLoadMore(1000);
            }
        });

    }


    /**
     * 更新帖子 信息
     *
     * @param article
     */
    public void updateArticle(Article article) {
        article.update(new UpdateListener() {
            @Override
            public void done(BmobException e) {
                if (e == null) {
                    logInfo("update", e);
                    mAdapter.notifyDataSetChanged();
                } else {
                    logInfo("update", e);
                }
            }
        });
    }

    /**
     * 点击 后更新事件
     *
     * @param click
     */
    public void updateClick(Click click) {
        click.update(new UpdateListener() {
            @Override
            public void done(BmobException e) {
                if (e == null) {
                    logInfo("click", e);
                    mAdapter.notifyDataSetChanged();
                } else {
                    logInfo("click", e);
                }
            }
        });
    }

    /**
     * 异常处理
     *
     * @param tag
     * @param e
     */
    public void logInfo(String tag, BmobException e) {
        if (e == null) {
            Log.e(tag, "无异常");
        } else {
            Log.e(tag, "错误信息：" + e.toString());
        }
    }


}
