package com.hzh.dell.tree.common_university;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.hzh.dell.tree.R;
import com.hzh.dell.tree.bean.UniversityJsonBean;
import com.hzh.dell.tree.person_fragment.change_info.MyDetail;
import com.hzh.dell.tree.utils.GetJsonDataUtil;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;


/**
 * create by hzh
 * on 2019/4/10
 */
public class CommonUniversity extends AppCompatActivity {
    private final static String TAG = "UNIVERSITY";
    private final static int MAX = 200;
    private int CURRENT_SCHOOL = 0x02;//选择当前学校
    private int TARGET_SCHOOL = 0x03;//选择目标院校
    private ExpandableListView expandableListView;
    private ArrayList<UniversityJsonBean> provinceList = new ArrayList<>();//省
    public String[] groupString;
    public String[][] childString;
    private String userCurrentSchool;
    private String userTargetSchool;
    private Intent getInfoIntent;
    private int getCode;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.common_university);
        getInfoIntent = getIntent();
        getCode = getInfoIntent.getIntExtra("SCHOOL", 0);
        initView();
    }

    private void initView() {
        String JsonData = new GetJsonDataUtil().getJson(this, "university.json");
        provinceList = parseData(JsonData);
        groupString = new String[provinceList.size()];
        childString = new String[provinceList.size()][MAX - 1];
        for (int i = 0; i < provinceList.size(); i++) {
//            Log.d(TAG,provinceList.get(i).getProvince()+"_"+provinceList.get(i).getSchools().size()+"\n");
            groupString[i] = provinceList.get(i).getProvince();
            for (int c = 0; c < provinceList.get(i).getSchools().size(); c++) {
                childString[i][c] = provinceList.get(i).getSchools().get(c).getName();
            }
        }
        expandableListView = findViewById(R.id.common_university_content_list);
        expandableListView.setAdapter(new MyExtendableListViewAdapter(groupString, childString));
        expandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView expandableListView, View view, int i, long l) {
                return false;
            }
        });
        expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView expandableListView, View view, int i, int i1, long l) {
                Toast.makeText(CommonUniversity.this, childString[i][i1], Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(CommonUniversity.this, MyDetail.class);
                if (getCode == CURRENT_SCHOOL) {
                    intent.putExtra("userCurrentSchool", childString[i][i1]);
                    setResult(CURRENT_SCHOOL, intent);
                } else if (getCode == TARGET_SCHOOL) {
                    intent.putExtra("userTargetSchool", childString[i][i1]);
                    setResult(TARGET_SCHOOL, intent);
                }

                onBackPressed();
                return false;
            }
        });
    }

    public ArrayList<UniversityJsonBean> parseData(String result) {
        ArrayList<UniversityJsonBean> detail = new ArrayList<>();
        try {
            JSONArray data = new JSONArray(result);
            Gson gson = new Gson();
            for (int i = 0; i < data.length(); i++) {
                UniversityJsonBean entity1 = gson.fromJson(data.optJSONObject(i).toString(), UniversityJsonBean.class);
                detail.add(entity1);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return detail;
    }
}
