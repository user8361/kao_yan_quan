package com.hzh.dell.dictionary.main;

import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.hzh.dell.Bmob.entity.User;
import com.hzh.dell.Bmob.entity.UserWord;
import com.hzh.dell.Bmob.entity.Word;
import com.hzh.dell.dictionary.util.DictQuery;
import com.hzh.dell.tree.R;
import com.hzh.dell.tree.adapter.recyclerview_adapter.adapter.CommonAdapter;
import com.hzh.dell.tree.adapter.recyclerview_adapter.adapter.MultiItemTypeAdapter;
import com.hzh.dell.tree.adapter.recyclerview_adapter.adapter.RecyclerListAdapterForWordItems;
import com.hzh.dell.tree.adapter.recyclerview_adapter.adapter.base.ViewHolder;
import com.hzh.dell.tree.utils.MathUtil;
import com.jaeger.library.StatusBarUtil;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.FindListener;
import cn.bmob.v3.listener.QueryListener;
import cn.bmob.v3.listener.SaveListener;
import cn.bmob.v3.listener.UpdateListener;

/**
 * create by hzh
 * on 2019/4/24
 */
public class QueryWord extends AppCompatActivity implements View.OnClickListener {
    private ImageView ivReturn, ivClear, ivSearch;
    private EditText etSearchWord;
    private TextView tvSearchHistory, tvClearHistory;
    private RecyclerView recyclerView;
    private List<UserWord> historyList;
    private CommonAdapter<UserWord> historyAdapter;
    private User user;
    private Word word;
    private UserWord getUserWord;
    private List<Word> wordList;//单词
    private String USER_NAME = "userName";
    private String userName = "";
    private Intent getInfoIntent;
    private MediaPlayer mediaPlayer;
    private Context context;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.homepage_search_word);
        StatusBarUtil.setTranslucent(this, 50);//半透明
        context = this;
        initView();
    }
    private void initData() {
        BmobQuery<UserWord> userWordBmobQuery = new BmobQuery<>();
        userWordBmobQuery.addWhereEqualTo("userId", user.getObjectId());
        userWordBmobQuery.addWhereEqualTo("historyRecordFlag", 1);
        userWordBmobQuery.findObjects(new FindListener<UserWord>() {
            @Override
            public void done(List<UserWord> list, BmobException e) {
                if (list != null) {
                    historyList = list;
                    showHistory();
                }
            }
        });
    }
    private void initView() {
        getInfoIntent = getIntent();
        userName = getInfoIntent.getStringExtra(USER_NAME);
        BmobQuery<User> userBmobQuery = new BmobQuery<>();
        userBmobQuery.addWhereEqualTo(USER_NAME, userName);
        userBmobQuery.findObjects(new FindListener<User>() {
            @Override
            public void done(List<User> list, BmobException e) {
                if (list != null) {
                    user = list.get(0);
                    wordList = new ArrayList<>();
                    initData();

                }
            }
        });
        ivReturn = findViewById(R.id.iv_return);
        ivClear = findViewById(R.id.iv_clear);
        ivSearch = findViewById(R.id.iv_search);
        etSearchWord = findViewById(R.id.et_search_word);
        tvSearchHistory = findViewById(R.id.tv_search_history);
        tvClearHistory = findViewById(R.id.tv_clear_history);
        recyclerView = findViewById(R.id.recyclerView);
        ivReturn.setOnClickListener(this);
        ivSearch.setOnClickListener(this);
        ivClear.setOnClickListener(this);
        tvClearHistory.setOnClickListener(this);
        //监听EditText文本变化
        etOnChangeListener();
    }
    //监听EditText文本变化
    private void etOnChangeListener() {
        TextWatcher textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                ivClear.setVisibility(View.INVISIBLE);
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                ivClear.setVisibility(View.VISIBLE);
            }
            @Override
            public void afterTextChanged(Editable s) {
                if (etSearchWord.getText().length() != 0) {
                    ivClear.setVisibility(View.VISIBLE);
                } else {
                    ivClear.setVisibility(View.INVISIBLE);
                }
            }
        };
        etSearchWord.addTextChangedListener(textWatcher);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_return:
                onBackPressed();
                break;
            case R.id.iv_search:
                getSearchWord();
                break;
            case R.id.iv_clear:
                etSearchWord.setText(null);
                ivClear.setVisibility(View.INVISIBLE);
                break;
            case R.id.tv_clear_history:
                clearHistory();
                break;
        }
    }


    //子线程获得搜索的单词
    private void getSearchWord() {
        final String searchWord = etSearchWord.getText().toString().toLowerCase().trim();
        final Handler handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                if (!"".equals(searchWord) && searchWord != null) {//文本输入框中数据不为空
                    //在单词表中查询单词
                    final BmobQuery<Word> wordBmobQuery = new BmobQuery<>();
                    wordBmobQuery.addWhereEqualTo("key", searchWord);//根据输入的单词进行查询
                    wordBmobQuery.findObjects(new FindListener<Word>() {
                        @Override
                        public void done(List<Word> list, BmobException e) {
                            //如果单词存在
                            if (list != null) {
                                word = list.get(0);///获得查询到的第一个数据也是唯一一个数据
                                //查询用户词典中是否存在这个单词
                                final BmobQuery<UserWord> userWordBmobQuery = new BmobQuery<>();
                                userWordBmobQuery.addWhereEqualTo("wordId", word.getObjectId());//单词id
                                userWordBmobQuery.addWhereEqualTo("userId", user.getObjectId());//用户id
                                userWordBmobQuery.findObjects(new FindListener<UserWord>() {//判断用户是否已经查询过该单词
                                    @Override
                                    public void done(List<UserWord> list, BmobException e) {
                                        if (list != null) {
                                            //如果存在就更新
                                            getUserWord = list.get(0);
                                            getUserWord.setHistoryRecordFlag(1);//设置查询历史标志为1并更新用户词典
                                            getUserWord.update(new UpdateListener() {
                                                @Override
                                                public void done(BmobException e) {
                                                    if (e == null) {//更新成功，单词将在查询历史记录中显示
                                                        Log.e("record", "userWord update ---> success");
                                                    } else {//更新失败，打印失败原因
                                                        Log.e("record", "userWord update ---> failure and errInfo:" + e.toString());
                                                    }
                                                }
                                            });
                                        } else {//用户未查询过该单词，将单词加入到 用户词典
                                            getUserWord = new UserWord();
                                            getUserWord.setUserId(user.getObjectId());
                                            getUserWord.setWordId(word.getObjectId());
                                            getUserWord.setHistoryRecordFlag(1);//设置查询历史记录标志位为1，表示查询过
                                            getUserWord.setStrangeWordFlag(0);//设置陌生词标志位为0，表示未添加到生词本
                                            getUserWord.save(new SaveListener<String>() {
                                                @Override
                                                public void done(String s, BmobException e) {
                                                    if (e == null) {
                                                        Log.e("record", "userWord save ---> success" + s);
                                                    } else {
                                                        Log.e("record", "userWord save ---> failure and errInfo:" + e.toString());
                                                    }
                                                }
                                            });
                                        }
                                    }
                                });
                            } else {
                                Log.e("characters", "查询失败 list == null");
                                //因Bmob查询为异步查询，会出现展示详细信息的方法在查询到结果前调用，
                                //所以当查询不到数据的时候再次递归调用一次查询单词的方法。
                                getSearchWord();
                            }
                            //显示查询的结果
                            showWordDetail(searchWord);//显示单词详细信息
                        }
                    });
                }
            }
        };
        new Thread(new Runnable() {
            @Override
            public void run() {
                Word getWord = new DictQuery().getWordFromInternet(searchWord);//导入数据库新的单词
                Message msg = Message.obtain();
                handler.sendMessage(msg);
            }
        }).start();
    }


    /**
     * 显示查询到的数据详细信息
     *
     * @param wordStr
     */
    private void showWordDetail(final String wordStr) {
        tvSearchHistory.setVisibility(View.GONE);
        tvClearHistory.setVisibility(View.GONE);
        wordList.clear();
        BmobQuery<Word> wordBmobQuery = new BmobQuery<>();
        wordBmobQuery.addWhereEqualTo("key", wordStr);
        wordBmobQuery.findObjects(new FindListener<Word>() {
            @Override
            public void done(List<Word> list, BmobException e) {
                if (list != null) {
                    final Word tmpWord = list.get(0);
                    wordList.addAll(list);
                    if (wordList.size() > 0) {
                        RecyclerListAdapterForWordItems adapterForItems = new RecyclerListAdapterForWordItems(wordList, context, getLayoutInflater(),user);
                        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                        recyclerView.setAdapter(adapterForItems);
                        adapterForItems.setInnerItemOnClickListener(new RecyclerListAdapterForWordItems.InnerItemOnClickListener() {
                            @Override
                            public void itemClick(final View v, int i) {
                                switch (v.getId()) {
                                    case R.id.iv_add_strange_word:
                                        BmobQuery<UserWord> userWordBmobQuery = new BmobQuery<>();
                                        userWordBmobQuery.addWhereEqualTo("wordId", tmpWord.getObjectId());
                                        userWordBmobQuery.findObjects(new FindListener<UserWord>() {
                                            @Override
                                            public void done(List<UserWord> list, BmobException e) {
                                                if (list != null) {
                                                    UserWord userWord = list.get(0);
                                                    if (userWord != null) {
                                                        if (userWord.getStrangeWordFlag() == 0) {
                                                            userWord.setStrangeWordFlag(1);
                                                            userWord.update(new UpdateListener() {
                                                                @Override
                                                                public void done(BmobException e) {
                                                                    if (e == null) {
                                                                        ((ImageView) v).setImageResource(R.mipmap.icon_remove_from_strange_word);

                                                                        Toast.makeText(QueryWord.this, "已添加到生词本", Toast.LENGTH_SHORT).show();
                                                                    }
                                                                }
                                                            });
                                                        } else {
                                                            userWord.setStrangeWordFlag(0);
                                                            userWord.update(new UpdateListener() {
                                                                @Override
                                                                public void done(BmobException e) {
                                                                    if (e == null) {
                                                                        ((ImageView) v).setImageResource(R.mipmap.icon_add_to_strange_word);

                                                                        Toast.makeText(QueryWord.this, "已移除生词本", Toast.LENGTH_SHORT).show();
                                                                    }
                                                                }
                                                            });
                                                        }

                                                    }
                                                }
                                            }
                                        });
                                        break;
                                    case R.id.tv_psE:
                                        if (tmpWord.getPronE() != null) {
                                            mediaPlayer = new MediaPlayer();
                                            try {
                                                mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                                                    @Override
                                                    public void onCompletion(MediaPlayer mp) {
                                                        if (mediaPlayer.isPlaying()) {
                                                            mediaPlayer.stop();
                                                            mediaPlayer.release();
                                                        }
                                                        mediaPlayer = null;
                                                    }
                                                });
                                                mediaPlayer.setDataSource(tmpWord.getPronE());
                                                mediaPlayer.prepare();
                                                mediaPlayer.start();

                                            } catch (IOException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                        break;
                                    case R.id.tv_psA:
                                        if (tmpWord.getPronE() != null) {
                                            mediaPlayer = new MediaPlayer();
                                            try {
                                                mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                                                    @Override
                                                    public void onCompletion(MediaPlayer mp) {
                                                        mediaPlayer.release();
                                                        mediaPlayer = null;
                                                    }
                                                });
                                                mediaPlayer.setDataSource(tmpWord.getPronA());
                                                mediaPlayer.prepare();
                                                mediaPlayer.start();
                                            } catch (IOException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                        break;
                                }

                            }
                        });
                    }
                } else {
                    Log.e("showDetail", "查询为空");
                }
            }
        });
    }

    //历史记录
    private void showHistory() {
        tvSearchHistory.setVisibility(View.VISIBLE);
        tvClearHistory.setVisibility(View.VISIBLE);
        if (historyList == null) {
            tvClearHistory.setVisibility(View.GONE);
        } else {
            historyAdapter = new CommonAdapter<UserWord>(getApplicationContext(), R.layout.search_item, historyList) {
                @Override
                protected void convert(final ViewHolder holder, UserWord userWord, int position) {
                    BmobQuery<Word> wordBmobQuery = new BmobQuery<>();
                    wordBmobQuery.addWhereEqualTo("objectId", userWord.getWordId());
                    wordBmobQuery.findObjects(new FindListener<Word>() {
                        @Override
                        public void done(List<Word> list, BmobException e) {
                            if (list != null) {
                                word = list.get(0);
                                String str = "";
                                if (isNotNull(word.getPos())) {
                                    for (int i = 0; i < MathUtil.min(word.getPos().size(), word.getAcceptationList().size()); ) {
                                        str += " " + word.getPos().get(i) + word.getAcceptationList().get(i);
                                        i++;
                                    }
                                }
                                holder.setText(R.id.tv_search_item, word.getKey() + " " + str);
                            } else {
                                Log.e("history", "历史为空");
                            }
                        }
                    });
                }
            };
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            recyclerView.setAdapter(historyAdapter);
            historyAdapter.setOnItemClickListener(new MultiItemTypeAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(View view, RecyclerView.ViewHolder holder, int position) {
                    BmobQuery<Word> wordBmobQuery = new BmobQuery<>();
                    wordBmobQuery.getObject(historyList.get(position).getWordId(), new QueryListener<Word>() {
                        @Override
                        public void done(Word word, BmobException e) {
                            showWordDetail(word.getKey());//数据库中已经存在
                        }
                    });
                }

                @Override
                public boolean onItemLongClick(View view, RecyclerView.ViewHolder holder, final int position) {
                    historyList.get(position).setHistoryRecordFlag(0);
                    historyList.get(position).update(new UpdateListener() {
                        @Override
                        public void done(BmobException e) {
                            if (e == null) {
                                historyList.remove(position);
                                historyAdapter.notifyItemRemoved(position);
                                historyAdapter.notifyItemRangeChanged(position, historyAdapter.getItemCount());
                            }
                        }
                    });
                    return false;
                }
            });
        }
    }

    //删除所有记录
    private void clearHistory() {
        for (UserWord u : historyList) {
            u.setHistoryRecordFlag(0);
//            userWordDao.update(u);
            u.update(new UpdateListener() {
                @Override
                public void done(BmobException e) {
                    if (e == null) {
                        Log.e("update", "修改成功");
                    } else {
                        Log.e("update", "修改失败");

                    }
                }
            });
        }
        historyList.clear();
        historyAdapter.notifyDataSetChanged();//清除所有
    }

    private boolean isNotNull(Object o) {
        if (o == null) {
            return false;
        }
        return true;
    }

}
