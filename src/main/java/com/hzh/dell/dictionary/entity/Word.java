package com.hzh.dell.dictionary.entity;


import com.hzh.dell.dictionary.util.StringConverter;

import org.greenrobot.greendao.annotation.Convert;
import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;

import java.util.List;

import org.greenrobot.greendao.annotation.Generated;

/**
 * create by hzh
 * on 2019/4/23
 */
@Entity
public class Word {

    @Id
    private Long id;


    private String key;//查询的单词
    private String psE;//查询单词英式发音
    private String pronE;//英式发音mp3地址
    private String psA;//美式发音
    private String pronA;//美式发音地址

    @Convert(columnType = String.class, converter = StringConverter.class)
    private List<String> pos;//词性
    @Convert(columnType = String.class, converter = StringConverter.class)
    private List<String> acceptationList;//翻译
    @Convert(columnType = String.class, converter = StringConverter.class)
    private List<String> sentOrigList;//英语例句
    @Convert(columnType = String.class, converter = StringConverter.class)
    private List<String> sentTransList;//汉语翻译

    public List<String> getSentTransList() {
        return this.sentTransList;
    }

    public void setSentTransList(List<String> sentTransList) {
        this.sentTransList = sentTransList;
    }

    public List<String> getSentOrigList() {
        return this.sentOrigList;
    }

    public void setSentOrigList(List<String> sentOrigList) {
        this.sentOrigList = sentOrigList;
    }

    public List<String> getAcceptationList() {
        return this.acceptationList;
    }

    public void setAcceptationList(List<String> acceptationList) {
        this.acceptationList = acceptationList;
    }

    public List<String> getPos() {
        return this.pos;
    }

    public void setPos(List<String> pos) {
        this.pos = pos;
    }

    public String getPronA() {
        return this.pronA;
    }

    public void setPronA(String pronA) {
        this.pronA = pronA;
    }

    public String getPsA() {
        return this.psA;
    }

    public void setPsA(String psA) {
        this.psA = psA;
    }

    public String getPronE() {
        return this.pronE;
    }

    public void setPronE(String pronE) {
        this.pronE = pronE;
    }

    public String getPsE() {
        return this.psE;
    }

    public void setPsE(String psE) {
        this.psE = psE;
    }

    public String getKey() {
        return this.key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Generated(hash = 1753085314)
    public Word(Long id, String key, String psE, String pronE, String psA,
                String pronA, List<String> pos, List<String> acceptationList,
                List<String> sentOrigList, List<String> sentTransList) {
        this.id = id;
        this.key = key;
        this.psE = psE;
        this.pronE = pronE;
        this.psA = psA;
        this.pronA = pronA;
        this.pos = pos;
        this.acceptationList = acceptationList;
        this.sentOrigList = sentOrigList;
        this.sentTransList = sentTransList;
    }

    @Generated(hash = 3342184)
    public Word() {
    }


}
