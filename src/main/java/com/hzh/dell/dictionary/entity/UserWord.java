package com.hzh.dell.dictionary.entity;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;

/**
 * create by hzh
 * on 2019/4/25
 */
@Entity
public class UserWord {

    @Id(autoincrement = true)
    private Long id;
    private Long userId;//用户id
    private Long wordId;//单词id
    private int historyRecordFlag;//搜索历史记录,值为1表示显示在搜索历史中，值为0表示不显示
    private int strangeWordFlag;//生词本，值为1表示加入生词本，值为0表示不在生词本

    public int getStrangeWordFlag() {
        return this.strangeWordFlag;
    }

    public void setStrangeWordFlag(int strangeWordFlag) {
        this.strangeWordFlag = strangeWordFlag;
    }

    public int getHistoryRecordFlag() {
        return this.historyRecordFlag;
    }

    public void setHistoryRecordFlag(int historyRecordFlag) {
        this.historyRecordFlag = historyRecordFlag;
    }

    public Long getWordId() {
        return this.wordId;
    }

    public void setWordId(Long wordId) {
        this.wordId = wordId;
    }

    public Long getUserId() {
        return this.userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Generated(hash = 1178438779)
    public UserWord(Long id, Long userId, Long wordId, int historyRecordFlag,
                    int strangeWordFlag) {
        this.id = id;
        this.userId = userId;
        this.wordId = wordId;
        this.historyRecordFlag = historyRecordFlag;
        this.strangeWordFlag = strangeWordFlag;
    }

    @Generated(hash = 183045641)
    public UserWord() {
    }
}
