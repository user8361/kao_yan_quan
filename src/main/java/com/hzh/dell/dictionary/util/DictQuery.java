package com.hzh.dell.dictionary.util;


import com.hzh.dell.Bmob.entity.Word;

import org.xml.sax.InputSource;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * create by hzh
 * on 2019/4/24
 */
public class DictQuery {
    InputStream in;//输入流
    String tmpUrl = "";//临时url
    public Word getWordFromInternet(String searchWord) {
        Word word = null;
        String tmpWord = searchWord;
        if (tmpWord == null || tmpWord.equals("")) {
            return null;
        } else {
            char[] array = tmpWord.toCharArray();
            if (array[0] > 256) {
                tmpWord = "_" + URLEncoder.encode(tmpWord);
            }
            tmpUrl = NetOperator.iCiBaURL + tmpWord + NetOperator.iCiBaKEY;
            in = NetOperator.getInputStreamByUrl(tmpUrl);
            if (in != null) {
                XMLParser xmlParser = new XMLParser();
                try {
                    InputStreamReader reader = new InputStreamReader(in, "utf-8");
                    SAXParseHandler contentHandler = new SAXParseHandler();
                    xmlParser.parseJinShanXml(contentHandler, new InputSource(reader));
                    word = new Word();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
            return word;
        }
    }
}
