package com.hzh.dell.dictionary.util;

import org.greenrobot.greendao.converter.PropertyConverter;

import java.util.Arrays;
import java.util.List;

/**
 * create by hzh
 * on 2019/4/24
 */
public class StringConverter implements PropertyConverter<List<String>, String> {
    @Override
    public List<String> convertToEntityProperty(String databaseValue) {
        return databaseValue == null ? null : Arrays.asList(databaseValue.split("##"));
    }
    @Override
    public String convertToDatabaseValue(List<String> entityProperty) {
        if (entityProperty == null) {
            return null;
        } else {
            StringBuilder sb = new StringBuilder();
            for (String link : entityProperty) {
                sb.append(link);
                sb.append("##");
            }
            return sb.toString();
        }
    }
}
