package com.hzh.dell.dictionary.util;

import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * create by hzh
 * on 2019/4/24
 */

//网络访问工具类
public class NetOperator {
    public final static String iCiBaURL = "http://dict-co.iciba.com/api/dictionary.php?w=";
    public final static String iCiBaKEY = "&key=4E9F73D06B97F71D198E51EEB08F5A3E";

    public static InputStream getInputStreamByUrl(String urlStr) {
        InputStream tmpInput = null;
        URL url = null;
        HttpURLConnection connection = null;

        try {
            url = new URL(urlStr);
            connection = (HttpURLConnection) url.openConnection();
            connection.setConnectTimeout(8000);
            connection.setReadTimeout(10000);
            tmpInput = connection.getInputStream();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (tmpInput == null) {

            Log.e("tmpInput", "null");
            return null;
        }
        return tmpInput;
    }
}
