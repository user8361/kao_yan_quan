package com.hzh.dell.dictionary.util;

import android.util.Log;

import com.hzh.dell.Bmob.entity.Word;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.List;

import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.FindListener;
import cn.bmob.v3.listener.SaveListener;
/**
 * create by hzh
 * on 2019/4/24
 */
//用于解析XML
public class SAXParseHandler extends DefaultHandler {
    private String currentTag;//记录当前解析到的节点名称
    private Word word;
    private List<String> psList;//英美音标
    private List<String> pronList;//英美发音地址
    private List<String> posList;//词性
    private List<String> acceptionList;//词性对应的汉语解析
    private List<String> origList;//英文例句
    private List<String> transList;//例句解释


    public SAXParseHandler() {
        word = new Word();
        psList = new ArrayList<>();
        pronList = new ArrayList<>();
        posList = new ArrayList<>();
        acceptionList = new ArrayList<>();
        origList = new ArrayList<>();
        transList = new ArrayList<>();
    }

    public Word getWord() {
        return word;
    }

    @Override
    public void startDocument() throws SAXException {
        super.startDocument();
    }

    @Override
    public void endDocument() throws SAXException {
        super.endDocument();
        BmobQuery<Word> wordBmobQuery = new BmobQuery<>();
        wordBmobQuery.addWhereEqualTo("key", word.getKey());
        wordBmobQuery.findObjects(new FindListener<Word>() {
            @Override
            public void done(List<Word> list, BmobException e) {
                if (list == null) {//数据库中没有查询到单词
                    if (sizeGTzero(psList)) {
                        if (psList.size() > 1) {
                            word.setPsE(psList.get(0));//英式音标
                            word.setPronE(pronList.get(0));//英式音频
                            word.setPsA(psList.get(1));//美式音标
                            word.setPronA(pronList.get(1));//美式音频
                        } else {
                            word.setPsE(psList.get(0));//英式音标
                            word.setPronE(pronList.get(0));//英式音频
                            word.setPsA(psList.get(0));//美式音标
                            word.setPronA(pronList.get(0));//美式音频
                        }
                    } else {
                        word.setPsE(null);//英式音标
                        word.setPronE(null);//英式音频
                        word.setPsA(null);//美式音标
                        word.setPronA(null);//美式音频
                    }
                    if (sizeGTzero(posList)) {
                        word.setPos(posList);//词性
                        word.setAcceptationList(acceptionList);//词性解释

                    } else {
                        word.setPos(null);//词性
                        word.setAcceptationList(null);//词性解释
                    }
                    if (sizeGTzero(origList)) {
                        word.setSentOrigList(origList);//例句
                        word.setSentTransList(transList);//例句解析
                    } else {
                        word.setSentOrigList(null);//例句
                        word.setSentTransList(null);//例句解析
                    }
                    word.save(new SaveListener<String>() {
                        @Override
                        public void done(String s, BmobException e) {
                            if (e == null) {
                                Log.e("characters", "--- 查询列表信息为:null 插入一条新数据 ---");
                            } else {
                                Log.e("record", "SAX ---> " + e.toString());
                            }
                        }
                    });
                } else {
                    Log.e("characters", "查询列表信息为:" + list.get(0).getKey());
                }
            }
        });
    }

    //解析xml元素
    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        super.startElement(uri, localName, qName, attributes);
        currentTag = localName;//记录当前的标签
    }
    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        super.endElement(uri, localName, qName);
        currentTag = null;
    }
    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        super.characters(ch, start, length);
        String value = new String(ch, start, length);
        //解析的文件中包含换行符，判断非换行符
        if (!"\n".equals(value)) {
            if (currentTag.equals("key")) {
                word.setKey(value);
            } else if (currentTag.equals("ps")) {
                psList.add(value);
            } else if (currentTag.equals("pron")) {
                pronList.add(value);
            } else if (currentTag.equals("pos")) {
                posList.add(value);
            } else if (currentTag.equals("acceptation")) {
                acceptionList.add(value);
            } else if (currentTag.equals("orig")) {
                origList.add(value);
            } else if (currentTag.equals("trans")) {
                transList.add(value);
            }
        }
    }
    //判断非空
    public boolean sizeGTzero(List<String> list) {
        if (list.size() > 0) {
            return true;
        }
        return false;
    }
}
