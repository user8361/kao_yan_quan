package com.hzh.dell.dictionary.util;

import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.SAXParserFactory;

/**
 * create by hzh
 * on 2019/4/24
 */
public class XMLParser {
    public SAXParserFactory factory = null;
    public XMLReader reader = null;

    public XMLParser() {

        try {
            factory = SAXParserFactory.newInstance();
            reader = factory.newSAXParser().getXMLReader();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void parseJinShanXml(DefaultHandler content, InputSource inSource) {
        if (inSource == null)
            return;
        try {
            reader.setContentHandler(content);
            reader.parse(inSource);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    //每日一句
//    public void parseDailySentenceXml(DailySentContentHandler contentHandler, InputSource inSource){
//        if(inSource==null)
//            return;
//        try {
//            reader.setContentHandler(contentHandler);
//            reader.parse(inSource);
//        } catch (Exception e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        }
//    }
}
