package com.hzh.dell.Bmob.entity;

import cn.bmob.v3.BmobObject;

/**
 * create by hzh
 * on 2019/5/8
 */
public class Grade extends BmobObject {
    private String userId;
    private String examName;
    private Integer examNowGrade;
    private Integer examTargetGrade;
    private Integer examDifGrade;
    private String examTime;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getExamName() {
        return examName;
    }

    public void setExamName(String examName) {
        this.examName = examName;
    }

    public Integer getExamNowGrade() {
        return examNowGrade;
    }

    public void setExamNowGrade(Integer examNowGrade) {
        this.examNowGrade = examNowGrade;
    }

    public Integer getExamTargetGrade() {
        return examTargetGrade;
    }

    public void setExamTargetGrade(Integer examTargetGrade) {
        this.examTargetGrade = examTargetGrade;
    }

    public Integer getExamDifGrade() {
        return examDifGrade;
    }

    public void setExamDifGrade(Integer examDifGrade) {
        this.examDifGrade = examDifGrade;
    }

    public String getExamTime() {
        return examTime;
    }

    public void setExamTime(String examTime) {
        this.examTime = examTime;
    }
}
