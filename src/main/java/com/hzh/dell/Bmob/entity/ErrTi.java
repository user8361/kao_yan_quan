package com.hzh.dell.Bmob.entity;

import java.util.List;

import cn.bmob.v3.BmobObject;
import cn.bmob.v3.datatype.BmobFile;

/**
 * create by hzh
 * on 2019/5/8
 */
public class ErrTi extends BmobObject {
    private String userId;//用户id
    private String errTiTitle;//错题名称
    private String errTiKey;//错题关键字
    private BmobFile errTiImg;//错题照片
    private List<BmobFile> imgList;//错题照片文件

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getErrTiTitle() {
        return errTiTitle;
    }

    public void setErrTiTitle(String errTiTitle) {
        this.errTiTitle = errTiTitle;
    }

    public String getErrTiKey() {
        return errTiKey;
    }

    public void setErrTiKey(String errTiKey) {
        this.errTiKey = errTiKey;
    }

    public BmobFile getErrTiImg() {
        return errTiImg;
    }

    public void setErrTiImg(BmobFile errTiImg) {
        this.errTiImg = errTiImg;
    }

    public List<BmobFile> getImgList() {
        return imgList;
    }

    public void setImgList(List<BmobFile> imgList) {
        this.imgList = imgList;
    }
}
