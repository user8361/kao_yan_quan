package com.hzh.dell.Bmob.entity;

import java.util.List;

import cn.bmob.v3.BmobObject;

/**
 * create by hzh
 * on 2019/5/6
 */
public class Word extends BmobObject {
    private String key;//查询的单词
    private String psE;//查询单词英式发音
    private String pronE;//英式发音mp3地址
    private String psA;//美式发音
    private String pronA;//美式发音地址

    private List<String> pos;
    private List<String> acceptationList;//翻译
    private List<String> sentOrigList;//英语例句
    private List<String> sentTransList;//汉语翻译


    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getPsE() {
        return psE;
    }

    public void setPsE(String psE) {
        this.psE = psE;
    }

    public String getPronE() {
        return pronE;
    }

    public void setPronE(String pronE) {
        this.pronE = pronE;
    }

    public String getPsA() {
        return psA;
    }

    public void setPsA(String psA) {
        this.psA = psA;
    }

    public String getPronA() {
        return pronA;
    }

    public void setPronA(String pronA) {
        this.pronA = pronA;
    }

    public List<String> getPos() {
        return pos;
    }

    public void setPos(List<String> pos) {
        this.pos = pos;
    }

    public List<String> getAcceptationList() {
        return acceptationList;
    }

    public void setAcceptationList(List<String> acceptationList) {
        this.acceptationList = acceptationList;
    }

    public List<String> getSentOrigList() {
        return sentOrigList;
    }

    public void setSentOrigList(List<String> sentOrigList) {
        this.sentOrigList = sentOrigList;
    }

    public List<String> getSentTransList() {
        return sentTransList;
    }

    public void setSentTransList(List<String> sentTransList) {
        this.sentTransList = sentTransList;
    }
}
