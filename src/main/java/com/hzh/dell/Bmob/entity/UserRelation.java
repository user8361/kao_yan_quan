package com.hzh.dell.Bmob.entity;

import cn.bmob.v3.BmobObject;

/**
 * create by hzh
 * on 2019/5/5
 */
public class UserRelation extends BmobObject {
    private String userId;//用户id
    private String followedId;//关注用户id
    private String fansId;//粉丝id
    private Integer relationFlag;//状态信息
    public String getUserId() {
        return userId;

    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFollowedId() {
        return followedId;
    }

    public void setFollowedId(String followedId) {
        this.followedId = followedId;
    }

    public String getFansId() {
        return fansId;
    }

    public void setFansId(String fansId) {
        this.fansId = fansId;
    }


    public Integer getRelationFlag() {
        return relationFlag;
    }

    public void setRelationFlag(Integer relationFlag) {
        this.relationFlag = relationFlag;
    }
}
