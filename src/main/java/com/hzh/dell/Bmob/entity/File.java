package com.hzh.dell.Bmob.entity;

import cn.bmob.v3.BmobObject;

/**
 * create by hzh
 * on 2019/5/14
 */
public class File extends BmobObject {
    private String fileName;//文件名
    private String userId;//用户Id
    private Integer fileType;//0上传 1下载
    private float fileSize;//文件大小
    private String fileUri;//文件的uri

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Integer getFileType() {
        return fileType;
    }

    public void setFileType(Integer fileType) {
        this.fileType = fileType;
    }

    public float getFileSize() {
        return fileSize;
    }

    public void setFileSize(float fileSize) {
        this.fileSize = fileSize;
    }

    public String getFileUri() {
        return fileUri;
    }

    public void setFileUri(String fileUri) {
        this.fileUri = fileUri;
    }
}
