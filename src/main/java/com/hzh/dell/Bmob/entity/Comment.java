package com.hzh.dell.Bmob.entity;

import cn.bmob.v3.BmobObject;

/**
 * create by hzh
 * on 2019/5/4
 */
public class Comment extends BmobObject {

    private String content;//评论内容
    private User user;//评论的用户
    private String itemId;//评论的item的id,0表示帖子，1表示其他的评论

    public String getContent() {

        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }
}
