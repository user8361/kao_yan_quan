package com.hzh.dell.Bmob.entity;

import cn.bmob.v3.BmobObject;

/**
 * create by hzh
 * on 2019/5/6
 */
public class Plan extends BmobObject {
    private String userId;//用户id
    private String createTime;//创建时间
    private String timeDuration;//时间段
    private String planContent;//计划内容
    private int completeState;//完成状态


    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getTimeDuration() {
        return timeDuration;
    }

    public void setTimeDuration(String timeDuration) {
        this.timeDuration = timeDuration;
    }

    public String getPlanContent() {
        return planContent;
    }

    public void setPlanContent(String planContent) {
        this.planContent = planContent;
    }

    public int getCompleteState() {
        return completeState;
    }

    public void setCompleteState(int completeState) {
        this.completeState = completeState;
    }
}
