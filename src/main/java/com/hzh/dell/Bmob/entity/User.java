package com.hzh.dell.Bmob.entity;

import cn.bmob.v3.BmobObject;
import cn.bmob.v3.datatype.BmobFile;

/**
 * create by hzh
 * on 2019/5/3
 */
public class User extends BmobObject {
    private String userId;//id自动生成
    private String userName;//用户名（账号）
    private String userPhone;//用户手机号
    private String userPassword;//用户密码
    private String userGender;//用户性别
    private String userHeadImgPath;//用户头像路径
    private String userAddress;//用户地址
    private String userCurrentSchool;//用户本科学校
    private String userTargetSchool;//用户目标学校
    private String userExamYear;//用户考研年份
    private String userMajor;//用户目标专业
    private String userExamSubject;//用户考试科目

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getUserGender() {
        return userGender;
    }

    public void setUserGender(String userGender) {
        this.userGender = userGender;
    }

    public String getUserHeadImgPath() {
        return userHeadImgPath;
    }

    public void setUserHeadImgPath(String userHeadImgPath) {
        this.userHeadImgPath = userHeadImgPath;
    }

    public String getUserAddress() {
        return userAddress;
    }

    public void setUserAddress(String userAddress) {
        this.userAddress = userAddress;
    }

    public String getUserCurrentSchool() {
        return userCurrentSchool;
    }

    public void setUserCurrentSchool(String userCurrentSchool) {
        this.userCurrentSchool = userCurrentSchool;
    }

    public String getUserTargetSchool() {
        return userTargetSchool;
    }

    public void setUserTargetSchool(String userTargetSchool) {
        this.userTargetSchool = userTargetSchool;
    }

    public String getUserExamYear() {
        return userExamYear;
    }

    public void setUserExamYear(String userExamYear) {
        this.userExamYear = userExamYear;
    }

    public String getUserMajor() {
        return userMajor;
    }

    public void setUserMajor(String userMajor) {
        this.userMajor = userMajor;
    }

    public String getUserExamSubject() {
        return userExamSubject;
    }

    public void setUserExamSubject(String userExamSubject) {
        this.userExamSubject = userExamSubject;
    }
}
