package com.hzh.dell.Bmob.entity;

import java.util.List;

import cn.bmob.v3.BmobObject;
import cn.bmob.v3.datatype.BmobFile;

/**
 * create by hzh
 * on 2019/5/3
 */
public class Article extends BmobObject {
    private String articleId;//帖子id
    private User author;//作者
    private Integer articleType;//帖子类型 0图文 1投票
    private String articleTitle;//帖子标题
    private String articleContent;//帖子内容
    private List<BmobFile> articleImgFile;//帖子中的图片地址
    private List<BmobFile> articleOtherFile;//帖子上传的文件地址
    private String articleTheme;//帖子主题

    private Integer likeNum;//喜欢数目
    private Integer collectNum;//收藏数目
    private Integer commentNum;//评论数目

    public String getArticleId() {
        return articleId;
    }

    public void setArticleId(String articleId) {
        this.articleId = articleId;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public Integer getArticleType() {
        return articleType;
    }

    public void setArticleType(Integer articleType) {
        this.articleType = articleType;
    }

    public String getArticleTitle() {
        return articleTitle;
    }

    public void setArticleTitle(String articleTitle) {
        this.articleTitle = articleTitle;
    }

    public String getArticleContent() {
        return articleContent;
    }

    public void setArticleContent(String articleContent) {
        this.articleContent = articleContent;
    }

    public List<BmobFile> getArticleImgFile() {
        return articleImgFile;
    }

    public void setArticleImgFile(List<BmobFile> articleImgFile) {
        this.articleImgFile = articleImgFile;
    }


    public String getArticleTheme() {
        return articleTheme;
    }

    public void setArticleTheme(String articleTheme) {
        this.articleTheme = articleTheme;
    }

    public Integer getLikeNum() {
        return likeNum;
    }

    public void setLikeNum(Integer likeNum) {
        this.likeNum = likeNum;
    }

    public Integer getCollectNum() {
        return collectNum;
    }

    public void setCollectNum(Integer collectNum) {
        this.collectNum = collectNum;
    }

    public Integer getCommentNum() {
        return commentNum;
    }

    public void setCommentNum(Integer commentNum) {
        this.commentNum = commentNum;
    }

    public List<BmobFile> getArticleOtherFile() {
        return articleOtherFile;
    }

    public void setArticleOtherFile(List<BmobFile> articleOtherFile) {
        this.articleOtherFile = articleOtherFile;
    }
}
