package com.hzh.dell.Bmob.entity;

import cn.bmob.v3.BmobObject;

/**
 * 喜欢与收藏 文章
 * create by hzh
 * on 2019/5/9
 */
public class Click extends BmobObject {
    private String userId;//用户id
    private String itemId;//点击事物id
    private Integer itemType;//点击事物类型 0 图文贴 ， 1 投票贴 ，2 错题
    private Integer likeState;//喜欢为1 不喜欢为0
    private Integer collectState;//收藏为1 为收藏为0
    private Integer commentState;//评论为1 为评论为0

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public Integer getItemType() {
        return itemType;
    }

    public void setItemType(Integer itemType) {
        this.itemType = itemType;
    }

    public Integer getLikeState() {
        return likeState;
    }

    public void setLikeState(Integer likeState) {
        this.likeState = likeState;
    }

    public Integer getCollectState() {
        return collectState;
    }

    public void setCollectState(Integer collectState) {
        this.collectState = collectState;
    }

    public Integer getCommentState() {
        return commentState;
    }

    public void setCommentState(Integer commentState) {
        this.commentState = commentState;
    }
}
