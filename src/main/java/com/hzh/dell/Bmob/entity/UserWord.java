package com.hzh.dell.Bmob.entity;

import cn.bmob.v3.BmobObject;

/**
 * create by hzh
 * on 2019/5/6
 */
public class UserWord extends BmobObject {
    private String userId;//用户id
    private String wordId;//单词id
    private int historyRecordFlag;//搜索历史记录,值为1表示显示在搜索历史中，值为0表示不显示
    private int strangeWordFlag;//生词本，值为1表示加入生词本，值为0表示不在生词本

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getWordId() {
        return wordId;
    }

    public void setWordId(String wordId) {
        this.wordId = wordId;
    }

    public int getHistoryRecordFlag() {
        return historyRecordFlag;
    }

    public void setHistoryRecordFlag(int historyRecordFlag) {
        this.historyRecordFlag = historyRecordFlag;
    }

    public int getStrangeWordFlag() {
        return strangeWordFlag;
    }

    public void setStrangeWordFlag(int strangeWordFlag) {
        this.strangeWordFlag = strangeWordFlag;
    }
}
