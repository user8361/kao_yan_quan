package com.hzh.greendao.gen;

import java.util.List;
import java.util.ArrayList;
import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;

import org.greenrobot.greendao.AbstractDao;
import org.greenrobot.greendao.Property;
import org.greenrobot.greendao.internal.SqlUtils;
import org.greenrobot.greendao.internal.DaoConfig;
import org.greenrobot.greendao.database.Database;
import org.greenrobot.greendao.database.DatabaseStatement;
import org.greenrobot.greendao.query.Query;
import org.greenrobot.greendao.query.QueryBuilder;

import com.hzh.dell.tree.entity.User;

import com.hzh.dell.tree.entity.Article;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.

/**
 * DAO for table "ARTICLE".
 */
public class ArticleDao extends AbstractDao<Article, Long> {

    public static final String TABLENAME = "ARTICLE";

    /**
     * Properties of entity Article.<br/>
     * Can be used for QueryBuilder and for referencing column names.
     */
    public static class Properties {
        public final static Property ArticleId = new Property(0, Long.class, "articleId", true, "_id");
        public final static Property UserId = new Property(1, Long.class, "userId", false, "USER_ID");
        public final static Property ArticleTitle = new Property(2, String.class, "articleTitle", false, "ARTICLE_TITLE");
        public final static Property ArticleType = new Property(3, String.class, "articleType", false, "ARTICLE_TYPE");
        public final static Property ArticleContent = new Property(4, String.class, "articleContent", false, "ARTICLE_CONTENT");
        public final static Property ArticleImgId = new Property(5, String.class, "articleImgId", false, "ARTICLE_IMG_ID");
        public final static Property CreateTime = new Property(6, long.class, "createTime", false, "CREATE_TIME");
        public final static Property ReplyNum = new Property(7, int.class, "replyNum", false, "REPLY_NUM");
        public final static Property LikeNum = new Property(8, int.class, "likeNum", false, "LIKE_NUM");
        public final static Property CollectNum = new Property(9, int.class, "collectNum", false, "COLLECT_NUM");
        public final static Property LikeFlag = new Property(10, int.class, "likeFlag", false, "LIKE_FLAG");
        public final static Property CollectFlag = new Property(11, int.class, "collectFlag", false, "COLLECT_FLAG");
        public final static Property ArticleCollectImgId = new Property(12, String.class, "articleCollectImgId", false, "ARTICLE_COLLECT_IMG_ID");
        public final static Property ArticleLikeImgId = new Property(13, String.class, "articleLikeImgId", false, "ARTICLE_LIKE_IMG_ID");
    }

    ;

    private DaoSession daoSession;

    private Query<Article> user_ArticleListQuery;

    public ArticleDao(DaoConfig config) {
        super(config);
    }

    public ArticleDao(DaoConfig config, DaoSession daoSession) {
        super(config, daoSession);
        this.daoSession = daoSession;
    }

    /**
     * Creates the underlying database table.
     */
    public static void createTable(Database db, boolean ifNotExists) {
        String constraint = ifNotExists ? "IF NOT EXISTS ": "";
        db.execSQL("CREATE TABLE " + constraint + "\"ARTICLE\" (" + //
                "\"_id\" INTEGER PRIMARY KEY AUTOINCREMENT ," + // 0: articleId
                "\"USER_ID\" INTEGER," + // 1: userId
                "\"ARTICLE_TITLE\" TEXT," + // 2: articleTitle
                "\"ARTICLE_TYPE\" TEXT," + // 3: articleType
                "\"ARTICLE_CONTENT\" TEXT," + // 4: articleContent
                "\"ARTICLE_IMG_ID\" TEXT," + // 5: articleImgId
                "\"CREATE_TIME\" INTEGER NOT NULL ," + // 6: createTime
                "\"REPLY_NUM\" INTEGER NOT NULL ," + // 7: replyNum
                "\"LIKE_NUM\" INTEGER NOT NULL ," + // 8: likeNum
                "\"COLLECT_NUM\" INTEGER NOT NULL ," + // 9: collectNum
                "\"LIKE_FLAG\" INTEGER NOT NULL ," + // 10: likeFlag
                "\"COLLECT_FLAG\" INTEGER NOT NULL ," + // 11: collectFlag
                "\"ARTICLE_COLLECT_IMG_ID\" TEXT," + // 12: articleCollectImgId
                "\"ARTICLE_LIKE_IMG_ID\" TEXT);"); // 13: articleLikeImgId
    }

    /** Drops the underlying database table. */
    public static void dropTable(Database db, boolean ifExists) {
        String sql = "DROP TABLE " + (ifExists ? "IF EXISTS " : "") + "\"ARTICLE\"";
        db.execSQL(sql);
    }

    @Override
    protected final void bindValues(DatabaseStatement stmt, Article entity) {
        stmt.clearBindings();
 
        Long articleId = entity.getArticleId();
        if (articleId != null) {
            stmt.bindLong(1, articleId);
        }
 
        Long userId = entity.getUserId();
        if (userId != null) {
            stmt.bindLong(2, userId);
        }
 
        String articleTitle = entity.getArticleTitle();
        if (articleTitle != null) {
            stmt.bindString(3, articleTitle);
        }
 
        String articleType = entity.getArticleType();
        if (articleType != null) {
            stmt.bindString(4, articleType);
        }
 
        String articleContent = entity.getArticleContent();
        if (articleContent != null) {
            stmt.bindString(5, articleContent);
        }
 
        String articleImgId = entity.getArticleImgId();
        if (articleImgId != null) {
            stmt.bindString(6, articleImgId);
        }
        stmt.bindLong(7, entity.getCreateTime());
        stmt.bindLong(8, entity.getReplyNum());
        stmt.bindLong(9, entity.getLikeNum());
        stmt.bindLong(10, entity.getCollectNum());
        stmt.bindLong(11, entity.getLikeFlag());
        stmt.bindLong(12, entity.getCollectFlag());
 
        String articleCollectImgId = entity.getArticleCollectImgId();
        if (articleCollectImgId != null) {
            stmt.bindString(13, articleCollectImgId);
        }
 
        String articleLikeImgId = entity.getArticleLikeImgId();
        if (articleLikeImgId != null) {
            stmt.bindString(14, articleLikeImgId);
        }
    }

    @Override
    protected final void bindValues(SQLiteStatement stmt, Article entity) {
        stmt.clearBindings();
 
        Long articleId = entity.getArticleId();
        if (articleId != null) {
            stmt.bindLong(1, articleId);
        }
 
        Long userId = entity.getUserId();
        if (userId != null) {
            stmt.bindLong(2, userId);
        }
 
        String articleTitle = entity.getArticleTitle();
        if (articleTitle != null) {
            stmt.bindString(3, articleTitle);
        }
 
        String articleType = entity.getArticleType();
        if (articleType != null) {
            stmt.bindString(4, articleType);
        }
 
        String articleContent = entity.getArticleContent();
        if (articleContent != null) {
            stmt.bindString(5, articleContent);
        }
 
        String articleImgId = entity.getArticleImgId();
        if (articleImgId != null) {
            stmt.bindString(6, articleImgId);
        }
        stmt.bindLong(7, entity.getCreateTime());
        stmt.bindLong(8, entity.getReplyNum());
        stmt.bindLong(9, entity.getLikeNum());
        stmt.bindLong(10, entity.getCollectNum());
        stmt.bindLong(11, entity.getLikeFlag());
        stmt.bindLong(12, entity.getCollectFlag());
 
        String articleCollectImgId = entity.getArticleCollectImgId();
        if (articleCollectImgId != null) {
            stmt.bindString(13, articleCollectImgId);
        }
 
        String articleLikeImgId = entity.getArticleLikeImgId();
        if (articleLikeImgId != null) {
            stmt.bindString(14, articleLikeImgId);
        }
    }

    @Override
    protected final void attachEntity(Article entity) {
        super.attachEntity(entity);
        entity.__setDaoSession(daoSession);
    }

    @Override
    public Long readKey(Cursor cursor, int offset) {
        return cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0);
    }    

    @Override
    public Article readEntity(Cursor cursor, int offset) {
        Article entity = new Article( //
                cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0), // articleId
                cursor.isNull(offset + 1) ? null : cursor.getLong(offset + 1), // userId
                cursor.isNull(offset + 2) ? null : cursor.getString(offset + 2), // articleTitle
                cursor.isNull(offset + 3) ? null : cursor.getString(offset + 3), // articleType
                cursor.isNull(offset + 4) ? null : cursor.getString(offset + 4), // articleContent
                cursor.isNull(offset + 5) ? null : cursor.getString(offset + 5), // articleImgId
                cursor.getLong(offset + 6), // createTime
                cursor.getInt(offset + 7), // replyNum
                cursor.getInt(offset + 8), // likeNum
                cursor.getInt(offset + 9), // collectNum
                cursor.getInt(offset + 10), // likeFlag
                cursor.getInt(offset + 11), // collectFlag
                cursor.isNull(offset + 12) ? null : cursor.getString(offset + 12), // articleCollectImgId
                cursor.isNull(offset + 13) ? null : cursor.getString(offset + 13) // articleLikeImgId
        );
        return entity;
    }
     
    @Override
    public void readEntity(Cursor cursor, Article entity, int offset) {
        entity.setArticleId(cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0));
        entity.setUserId(cursor.isNull(offset + 1) ? null : cursor.getLong(offset + 1));
        entity.setArticleTitle(cursor.isNull(offset + 2) ? null : cursor.getString(offset + 2));
        entity.setArticleType(cursor.isNull(offset + 3) ? null : cursor.getString(offset + 3));
        entity.setArticleContent(cursor.isNull(offset + 4) ? null : cursor.getString(offset + 4));
        entity.setArticleImgId(cursor.isNull(offset + 5) ? null : cursor.getString(offset + 5));
        entity.setCreateTime(cursor.getLong(offset + 6));
        entity.setReplyNum(cursor.getInt(offset + 7));
        entity.setLikeNum(cursor.getInt(offset + 8));
        entity.setCollectNum(cursor.getInt(offset + 9));
        entity.setLikeFlag(cursor.getInt(offset + 10));
        entity.setCollectFlag(cursor.getInt(offset + 11));
        entity.setArticleCollectImgId(cursor.isNull(offset + 12) ? null : cursor.getString(offset + 12));
        entity.setArticleLikeImgId(cursor.isNull(offset + 13) ? null : cursor.getString(offset + 13));
     }
    
    @Override
    protected final Long updateKeyAfterInsert(Article entity, long rowId) {
        entity.setArticleId(rowId);
        return rowId;
    }
    
    @Override
    public Long getKey(Article entity) {
        if (entity != null) {
            return entity.getArticleId();
        } else {
            return null;
        }
    }

    @Override
    protected final boolean isEntityUpdateable() {
        return true;
    }
    
    /** Internal query to resolve the "articleList" to-many relationship of User. */
    public List<Article> _queryUser_ArticleList(Long userId) {
        synchronized (this) {
            if (user_ArticleListQuery == null) {
                QueryBuilder<Article> queryBuilder = queryBuilder();
                queryBuilder.where(Properties.UserId.eq(null));
                user_ArticleListQuery = queryBuilder.build();
            }
        }
        Query<Article> query = user_ArticleListQuery.forCurrentThread();
        query.setParameter(0, userId);
        return query.list();
    }

    private String selectDeep;

    protected String getSelectDeep() {
        if (selectDeep == null) {
            StringBuilder builder = new StringBuilder("SELECT ");
            SqlUtils.appendColumns(builder, "T", getAllColumns());
            builder.append(',');
            SqlUtils.appendColumns(builder, "T0", daoSession.getUserDao().getAllColumns());
            builder.append(" FROM ARTICLE T");
            builder.append(" LEFT JOIN USER T0 ON T.\"USER_ID\"=T0.\"_id\"");
            builder.append(' ');
            selectDeep = builder.toString();
        }
        return selectDeep;
    }
    
    protected Article loadCurrentDeep(Cursor cursor, boolean lock) {
        Article entity = loadCurrent(cursor, 0, lock);
        int offset = getAllColumns().length;

        User user = loadCurrentOther(daoSession.getUserDao(), cursor, offset);
        entity.setUser(user);

        return entity;    
    }

    public Article loadDeep(Long key) {
        assertSinglePk();
        if (key == null) {
            return null;
        }

        StringBuilder builder = new StringBuilder(getSelectDeep());
        builder.append("WHERE ");
        SqlUtils.appendColumnsEqValue(builder, "T", getPkColumns());
        String sql = builder.toString();

        String[] keyArray = new String[]{key.toString() };
        Cursor cursor = db.rawQuery(sql, keyArray);
        
        try {
            boolean available = cursor.moveToFirst();
            if (!available) {
                return null;
            } else if (!cursor.isLast()) {
                throw new IllegalStateException("Expected unique result, but count was " + cursor.getCount());
            }
            return loadCurrentDeep(cursor, true);
        } finally {
            cursor.close();
        }
    }
    
    /** Reads all available rows from the given cursor and returns a list of new ImageTO objects. */
    public List<Article> loadAllDeepFromCursor(Cursor cursor) {
        int count = cursor.getCount();
        List<Article> list = new ArrayList<Article>(count);
        
        if (cursor.moveToFirst()) {
            if (identityScope != null) {
                identityScope.lock();
                identityScope.reserveRoom(count);
            }
            try {
                do {
                    list.add(loadCurrentDeep(cursor, false));
                } while (cursor.moveToNext());
            } finally {
                if (identityScope != null) {
                    identityScope.unlock();
                }
            }
        }
        return list;
    }
    
    protected List<Article> loadDeepAllAndCloseCursor(Cursor cursor) {
        try {
            return loadAllDeepFromCursor(cursor);
        } finally {
            cursor.close();
        }
    }
    

    /** A raw-style query where you can pass any WHERE clause and arguments. */
    public List<Article> queryDeep(String where, String... selectionArg) {
        Cursor cursor = db.rawQuery(getSelectDeep() + where, selectionArg);
        return loadDeepAllAndCloseCursor(cursor);
    }
 
}
