package com.hzh.greendao.gen;

import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;

import org.greenrobot.greendao.AbstractDao;
import org.greenrobot.greendao.Property;
import org.greenrobot.greendao.internal.DaoConfig;
import org.greenrobot.greendao.database.Database;
import org.greenrobot.greendao.database.DatabaseStatement;

import com.hzh.dell.tree.entity.Plan;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.

/**
 * DAO for table "PLAN".
 */
public class PlanDao extends AbstractDao<Plan, Long> {

    public static final String TABLENAME = "PLAN";

    /**
     * Properties of entity Plan.<br/>
     * Can be used for QueryBuilder and for referencing column names.
     */
    public static class Properties {
        public final static Property PlanId = new Property(0, Long.class, "planId", true, "_id");
        public final static Property UserId = new Property(1, Long.class, "userId", false, "USER_ID");
        public final static Property CreateTime = new Property(2, String.class, "createTime", false, "CREATE_TIME");
        public final static Property TimeDuration = new Property(3, String.class, "timeDuration", false, "TIME_DURATION");
        public final static Property PlanContent = new Property(4, String.class, "planContent", false, "PLAN_CONTENT");
        public final static Property CompleteState = new Property(5, int.class, "completeState", false, "COMPLETE_STATE");
    }

    ;


    public PlanDao(DaoConfig config) {
        super(config);
    }

    public PlanDao(DaoConfig config, DaoSession daoSession) {
        super(config, daoSession);
    }

    /**
     * Creates the underlying database table.
     */
    public static void createTable(Database db, boolean ifNotExists) {
        String constraint = ifNotExists ? "IF NOT EXISTS ": "";
        db.execSQL("CREATE TABLE " + constraint + "\"PLAN\" (" + //
                "\"_id\" INTEGER PRIMARY KEY AUTOINCREMENT ," + // 0: planId
                "\"USER_ID\" INTEGER," + // 1: userId
                "\"CREATE_TIME\" TEXT," + // 2: createTime
                "\"TIME_DURATION\" TEXT," + // 3: timeDuration
                "\"PLAN_CONTENT\" TEXT," + // 4: planContent
                "\"COMPLETE_STATE\" INTEGER NOT NULL );"); // 5: completeState
    }

    /** Drops the underlying database table. */
    public static void dropTable(Database db, boolean ifExists) {
        String sql = "DROP TABLE " + (ifExists ? "IF EXISTS " : "") + "\"PLAN\"";
        db.execSQL(sql);
    }

    @Override
    protected final void bindValues(DatabaseStatement stmt, Plan entity) {
        stmt.clearBindings();
 
        Long planId = entity.getPlanId();
        if (planId != null) {
            stmt.bindLong(1, planId);
        }
 
        Long userId = entity.getUserId();
        if (userId != null) {
            stmt.bindLong(2, userId);
        }
 
        String createTime = entity.getCreateTime();
        if (createTime != null) {
            stmt.bindString(3, createTime);
        }
 
        String timeDuration = entity.getTimeDuration();
        if (timeDuration != null) {
            stmt.bindString(4, timeDuration);
        }
 
        String planContent = entity.getPlanContent();
        if (planContent != null) {
            stmt.bindString(5, planContent);
        }
        stmt.bindLong(6, entity.getCompleteState());
    }

    @Override
    protected final void bindValues(SQLiteStatement stmt, Plan entity) {
        stmt.clearBindings();
 
        Long planId = entity.getPlanId();
        if (planId != null) {
            stmt.bindLong(1, planId);
        }
 
        Long userId = entity.getUserId();
        if (userId != null) {
            stmt.bindLong(2, userId);
        }
 
        String createTime = entity.getCreateTime();
        if (createTime != null) {
            stmt.bindString(3, createTime);
        }
 
        String timeDuration = entity.getTimeDuration();
        if (timeDuration != null) {
            stmt.bindString(4, timeDuration);
        }
 
        String planContent = entity.getPlanContent();
        if (planContent != null) {
            stmt.bindString(5, planContent);
        }
        stmt.bindLong(6, entity.getCompleteState());
    }

    @Override
    public Long readKey(Cursor cursor, int offset) {
        return cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0);
    }    

    @Override
    public Plan readEntity(Cursor cursor, int offset) {
        Plan entity = new Plan( //
                cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0), // planId
                cursor.isNull(offset + 1) ? null : cursor.getLong(offset + 1), // userId
                cursor.isNull(offset + 2) ? null : cursor.getString(offset + 2), // createTime
                cursor.isNull(offset + 3) ? null : cursor.getString(offset + 3), // timeDuration
                cursor.isNull(offset + 4) ? null : cursor.getString(offset + 4), // planContent
                cursor.getInt(offset + 5) // completeState
        );
        return entity;
    }
     
    @Override
    public void readEntity(Cursor cursor, Plan entity, int offset) {
        entity.setPlanId(cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0));
        entity.setUserId(cursor.isNull(offset + 1) ? null : cursor.getLong(offset + 1));
        entity.setCreateTime(cursor.isNull(offset + 2) ? null : cursor.getString(offset + 2));
        entity.setTimeDuration(cursor.isNull(offset + 3) ? null : cursor.getString(offset + 3));
        entity.setPlanContent(cursor.isNull(offset + 4) ? null : cursor.getString(offset + 4));
        entity.setCompleteState(cursor.getInt(offset + 5));
     }
    
    @Override
    protected final Long updateKeyAfterInsert(Plan entity, long rowId) {
        entity.setPlanId(rowId);
        return rowId;
    }
    
    @Override
    public Long getKey(Plan entity) {
        if (entity != null) {
            return entity.getPlanId();
        } else {
            return null;
        }
    }

    @Override
    protected final boolean isEntityUpdateable() {
        return true;
    }
    
}
