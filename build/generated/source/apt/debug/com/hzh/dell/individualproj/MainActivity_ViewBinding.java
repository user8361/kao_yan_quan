// Generated code from Butter Knife. Do not modify!
package com.hzh.dell.individualproj;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.ButtonBarLayout;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.hzh.dell.individualproj.view.JudgeNestedScrollView;
import com.hzh.dell.tree.R;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import de.hdodenhof.circleimageview.CircleImageView;
import java.lang.IllegalStateException;
import java.lang.Override;
import net.lucode.hackware.magicindicator.MagicIndicator;

public class MainActivity_ViewBinding implements Unbinder {
  private MainActivity target;

  private View view2131296491;

  private View view2131296515;

  private View view2131296864;

  private View view2131296802;

  @UiThread
  public MainActivity_ViewBinding(MainActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public MainActivity_ViewBinding(final MainActivity target, View source) {
    this.target = target;

    View view;
    view = Utils.findRequiredView(source, R.id.iv_back, "field 'ivBack' and method 'onViewClicked'");
    target.ivBack = Utils.castView(view, R.id.iv_back, "field 'ivBack'", ImageView.class);
    view2131296491 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.ivMenu = Utils.findRequiredViewAsType(source, R.id.iv_menu, "field 'ivMenu'", ImageView.class);
    target.ivHeader = Utils.findRequiredViewAsType(source, R.id.iv_header, "field 'ivHeader'", ImageView.class);
    view = Utils.findRequiredView(source, R.id.iv_userHeadimg, "field 'ivHeadImg' and method 'onViewClicked'");
    target.ivHeadImg = Utils.castView(view, R.id.iv_userHeadimg, "field 'ivHeadImg'", ImageView.class);
    view2131296515 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.tv_username, "field 'tvUserName' and method 'onViewClicked'");
    target.tvUserName = Utils.castView(view, R.id.tv_username, "field 'tvUserName'", TextView.class);
    view2131296864 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.tv_currentSchool, "field 'tvCurrentSchool' and method 'onViewClicked'");
    target.tvCurrentSchool = Utils.castView(view, R.id.tv_currentSchool, "field 'tvCurrentSchool'", TextView.class);
    view2131296802 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.tvConcernedNum = Utils.findRequiredViewAsType(source, R.id.tv_concern_num, "field 'tvConcernedNum'", TextView.class);
    target.tvFansNum = Utils.findRequiredViewAsType(source, R.id.tv_fans_num, "field 'tvFansNum'", TextView.class);
    target.refreshLayout = Utils.findRequiredViewAsType(source, R.id.refreshLayout, "field 'refreshLayout'", SmartRefreshLayout.class);
    target.toolbar = Utils.findRequiredViewAsType(source, R.id.toolbar, "field 'toolbar'", Toolbar.class);
    target.viewPager = Utils.findRequiredViewAsType(source, R.id.view_pager, "field 'viewPager'", ViewPager.class);
    target.toolbarHeadImg = Utils.findRequiredViewAsType(source, R.id.toolbar_userHeadimg, "field 'toolbarHeadImg'", CircleImageView.class);
    target.toolbarUserName = Utils.findRequiredViewAsType(source, R.id.toolbar_username, "field 'toolbarUserName'", TextView.class);
    target.scrollView = Utils.findRequiredViewAsType(source, R.id.scrollView, "field 'scrollView'", JudgeNestedScrollView.class);
    target.buttonBarLayout = Utils.findRequiredViewAsType(source, R.id.buttonBarLayout, "field 'buttonBarLayout'", ButtonBarLayout.class);
    target.magicIndicator = Utils.findRequiredViewAsType(source, R.id.magic_indicator, "field 'magicIndicator'", MagicIndicator.class);
    target.magicIndicatorTitle = Utils.findRequiredViewAsType(source, R.id.magic_indicator_title, "field 'magicIndicatorTitle'", MagicIndicator.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    MainActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.ivBack = null;
    target.ivMenu = null;
    target.ivHeader = null;
    target.ivHeadImg = null;
    target.tvUserName = null;
    target.tvCurrentSchool = null;
    target.tvConcernedNum = null;
    target.tvFansNum = null;
    target.refreshLayout = null;
    target.toolbar = null;
    target.viewPager = null;
    target.toolbarHeadImg = null;
    target.toolbarUserName = null;
    target.scrollView = null;
    target.buttonBarLayout = null;
    target.magicIndicator = null;
    target.magicIndicatorTitle = null;

    view2131296491.setOnClickListener(null);
    view2131296491 = null;
    view2131296515.setOnClickListener(null);
    view2131296515 = null;
    view2131296864.setOnClickListener(null);
    view2131296864 = null;
    view2131296802.setOnClickListener(null);
    view2131296802 = null;
  }
}
